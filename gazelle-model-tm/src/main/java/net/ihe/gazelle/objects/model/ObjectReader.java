/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetTestDefinition;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tm.gazelletest.model.reversed.AIPO;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>ObjectReader<br>
 * <br>
 * This class describes the ObjectReader object, used by the Gazelle application.
 * <p/>
 * This class belongs to the TM module.
 * <p/>
 * ObjectReader possesses the following attributes :
 * <ul>
 * <li><b>id</b> : ObjectReader id</li>
 * <li><b>object</b> : Object corresponding to ObjectReader</li>
 * <li><b>keyword</b> : Keyword corresponding to ObjectReader</li>
 * </ul>
 * </br> <b>Example of ObjectReader</b> : - <br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @author Abderrazek Boufahja / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, April 15th
 * @class ObjectReader.java
 * @package net.ihe.gazelle.objects.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "description",
        "AIPO"
})
@XmlRootElement(name = "objectReader")
@Entity
@Audited
@Table(name = "tm_object_reader", uniqueConstraints = {@UniqueConstraint(columnNames = {"AIPO_id", "object_id"})})
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_object_reader_sequence", sequenceName = "tm_object_reader_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class ObjectReader extends AuditedObject implements Serializable {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    /**
     * Serial version Id of this object!
     */
    private static final long serialVersionUID = 1881422500717741951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ObjectReader id
     */
    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_object_reader_sequence")
    private Integer id;

	/* Add attributes for sync (else we can not see them as DeclaredFields)  */

    /**
     * AIPO
     */
    @ManyToOne
    // JRC : Remove nullable constraint in : @Column(name="AIPO_id", unique = false, nullable = false) to be able to sync ObjectCreatorReader object with GMM
    @JoinColumn(name = "AIPO_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    @AuditJoinTable
    private ActorIntegrationProfileOption AIPO;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotAudited
    @JoinColumn(name = "AIPO_id", nullable = false, insertable = false, updatable = false)
    @Fetch(value = FetchMode.SELECT)
    @XmlTransient
    private AIPO reversedAipo;

    /**
     * Object
     */
    @ManyToOne
    // JRC : Remove nullable constraint in : @JoinColumn(name = "object_id", unique = false, nullable = false) to be able to sync ObjectCreatorReader object with GMM
    @JoinColumn(name = "object_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    @AuditJoinTable
    @XmlTransient
    private ObjectType object;

    /**
     * description
     */
    @Column(name = "description", unique = false, nullable = true)
    private String description;

    // constructors //////////////////////////////////////////////////////////////////////////////////////////////////////
    public ObjectReader() {
    }

    public ObjectReader(String description, ActorIntegrationProfileOption aIPO, ObjectType object) {
        super();
        AIPO = aIPO;
        this.object = object;
        this.description = description;
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    // ~ Methods ////////////////////////////////////////////////////////////////////
    public static List<ObjectReader> getObjectReaderFiltered(
            ActorIntegrationProfileOption inActorIntegrationProfileOption, ObjectType inObjectType) {
        ObjectReaderQuery query = new ObjectReaderQuery();
        query.AIPO().eqIfValueNotNull(inActorIntegrationProfileOption);
        query.object().eqIfValueNotNull(inObjectType);
        return query.getList();
    }

    public static void deleteObjectReader(ObjectType objectType) {
        if (objectType != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            List<ObjectReader> list_objectReader = ObjectReader.getObjectReaderFiltered(null, objectType);
            for (ObjectReader objectReader : list_objectReader) {
                ObjectReader objectReaderToRemove = entityManager.find(ObjectReader.class, objectReader.getId());
                entityManager.remove(objectReaderToRemove);
            }
        }
    }

    public static void deleteObjectReader(ActorIntegrationProfileOption inActorIntegrationProfileOption) {
        if (inActorIntegrationProfileOption != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            List<ObjectReader> list_objectReader = ObjectReader.getObjectReaderFiltered(
                    inActorIntegrationProfileOption, null);
            for (ObjectReader objectReader : list_objectReader) {
                ObjectReader objectReaderToRemove = entityManager.find(ObjectReader.class, objectReader.getId());
                entityManager.remove(objectReaderToRemove);
            }
        }
    }

    public static void deleteObjectReader(ObjectReader objectReader) {
        if (objectReader != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            ObjectReader objectReaderToRemove = entityManager.find(ObjectReader.class, objectReader.getId());
            entityManager.remove(objectReaderToRemove);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ActorIntegrationProfileOption getAIPO() {
        return AIPO;
    }

    public void setAIPO(ActorIntegrationProfileOption aIPO) {
        AIPO = aIPO;
    }

    public ObjectType getObject() {
        return object;
    }

    public void setObject(ObjectType object) {
        this.object = object;
    }

    // ~ hashCodes and equals ////////////////////////////////////////////////////////////////////////////////////////////////////

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AIPO getReversedAipo() {
        return HibernateHelper.getLazyValue(this, "reversedAipo", this.reversedAipo);
    }

    public void setReversedAipo(AIPO reversedAipo) {
        this.reversedAipo = reversedAipo;
    }

}
