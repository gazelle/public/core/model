/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 */
@Entity
@Audited
@Table(name = "tm_object_instance_validation")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_object_instance_validation_sequence", sequenceName = "tm_object_instance_validation_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class ObjectInstanceValidation extends AuditedObject implements Serializable {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    public static final String SAMPLE_VALID = "valid instance";
    public static final String SAMPLE_INVALID = "invalid instance";
    public static final String SAMPLE_NOT_YET_EXAMINATED = "not yet examinated";
    private static final long serialVersionUID = 1881422230766771951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * ObjectinstanceValidation id
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_object_instance_validation_sequence")
    private Integer id;

    /**
     * value
     */
    @Column(name = "value", unique = false, nullable = false)
    private String value;

    /**
     * description
     */
    @Column(name = "description", unique = true, nullable = true)
    private String description;

    @OneToMany(mappedBy = "validation", fetch = FetchType.LAZY)
    @NotAudited
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<ObjectInstance> objectInstances;

    // constructors ////////////////////////////////////////////////////////////////////////////////////////////////////

    public ObjectInstanceValidation() {
    }

    public ObjectInstanceValidation(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public ObjectInstanceValidation(ObjectInstanceValidation inObjectInstanceValidation) {
        if (inObjectInstanceValidation.value != null) {
            this.value = inObjectInstanceValidation.value;
        }
        if (inObjectInstanceValidation.description != null) {
            this.description = inObjectInstanceValidation.description;
        }
    }

    // getter ans setter //////////////////////////////////////////////////////////////////////////////////////////////

    public static List<ObjectInstanceValidation> getListObjectInstanceValidation() {
        ObjectInstanceValidationQuery query = new ObjectInstanceValidationQuery();
        return query.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @FilterLabel
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Set<ObjectInstance> getObjectInstances() {
        return HibernateHelper.getLazyValue(this, "objectInstances", this.objectInstances);
    }

    public void setObjectInstances(Set<ObjectInstance> objectInstances) {
        this.objectInstances = objectInstances;
    }

    public String getDescription() {
        return description;
    }

    // methods //////////////////////////////////////////////////////////////////////////

    public void setDescription(String description) {
        this.description = description;
    }

}
