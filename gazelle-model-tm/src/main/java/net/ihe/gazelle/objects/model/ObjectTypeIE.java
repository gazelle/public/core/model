package net.ihe.gazelle.objects.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "objectTypes")
@XmlAccessorType(XmlAccessType.FIELD)
public class ObjectTypeIE {

    @XmlElementRefs({@XmlElementRef(name = "objectType")})
    private List<ObjectType> objectTypes;

    @XmlTransient
    private List<ObjectType> ignoredObjectTypes;

    @XmlTransient
    private List<ObjectType> duplicatedObjectTypes;

    @XmlTransient
    private List<String> unknownAipo;

    @XmlTransient
    private List<String> unknownObjectFileTypes;

    @XmlTransient
    private List<String> unknownStatus;

    @XmlTransient
    private boolean imported;

    public ObjectTypeIE() {
        this.objectTypes = new ArrayList<>();
    }

    public ObjectTypeIE(List<ObjectType> pl_objectTypes) {
        this.objectTypes = new ArrayList<>(pl_objectTypes);
    }

    public List<ObjectType> getObjectTypes() {
        return objectTypes;
    }

    public void setObjectTypes(List<ObjectType> objectTypes) {
        this.objectTypes = new ArrayList<>(objectTypes);
    }

    public List<ObjectType> getIgnoredObjectTypes() {
        return ignoredObjectTypes;
    }

    public void setIgnoredObjectTypes(List<ObjectType> ignoredObjectTypes) {
        this.ignoredObjectTypes = new ArrayList<>(ignoredObjectTypes);
    }

    public void addIgnoredObjectType(ObjectType objectType) {
        if (this.ignoredObjectTypes != null) {
            this.ignoredObjectTypes.add(objectType);
        } else {
            this.ignoredObjectTypes = new ArrayList<>();
            this.ignoredObjectTypes.add(objectType);
        }
    }

    public List<ObjectType> getDuplicatedObjectTypes() {
        return duplicatedObjectTypes;
    }

    public void setDuplicatedObjectTypes(List<ObjectType> duplicatedObjectTypes) {
        this.duplicatedObjectTypes = new ArrayList<>(duplicatedObjectTypes);
    }

    public void addDuplicatedObjectType(ObjectType objectType) {
        if (this.duplicatedObjectTypes != null) {
            this.duplicatedObjectTypes.add(objectType);
        } else {
            this.duplicatedObjectTypes = new ArrayList<>();
            this.duplicatedObjectTypes.add(objectType);
        }
    }

    public List<String> getUnknownAipo() {
        return unknownAipo;
    }

    public void setUnknownAipo(List<String> unknownAipo) {
        this.unknownAipo = new ArrayList<>(unknownAipo);
    }

    public void addUnknownAipo(String aipo) {
        if (this.unknownAipo != null) {
            this.unknownAipo.add(aipo);
        } else {
            this.unknownAipo = new ArrayList<>();
            this.unknownAipo.add(aipo);
        }
    }

    public List<String> getUnknownObjectFileTypes() {
        return unknownObjectFileTypes;
    }

    public void setUnknownObjectFileTypes(List<String> unknownObjectFileTypes) {
        this.unknownObjectFileTypes = new ArrayList<>(unknownObjectFileTypes);
    }

    public void addUnknownObjectFileType(String objectFileType) {
        if (this.unknownObjectFileTypes != null) {
            this.unknownObjectFileTypes.add(objectFileType);
        } else {
            this.unknownObjectFileTypes = new ArrayList<>();
            this.unknownObjectFileTypes.add(objectFileType);
        }
    }

    public List<String> getUnknownStatus() {
        return unknownStatus;
    }

    public void setUnknownStatus(List<String> unknownStatus) {
        this.unknownStatus = new ArrayList<>(unknownStatus);
    }

    public void addUnknownStatus(String status) {
        if (this.unknownStatus != null) {
            this.unknownStatus.add(status);
        } else {
            this.unknownStatus = new ArrayList<>();
            this.unknownStatus.add(status);
        }
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        this.imported = imported;
    }
}
