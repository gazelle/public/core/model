/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author abderrazek boufahja / INRIA Rennes IHE development Project
 */
@Entity
@Table(name = "tm_object_instance_annotation")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_object_instance_annotation_sequence", sequenceName = "tm_object_instance_annotation_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class ObjectInstanceAnnotation extends AuditedObject implements Serializable {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    private static final long serialVersionUID = 1845342545451441951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ObjectInstance id
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_object_instance_annotation_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "object_instance_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private ObjectInstance objectInstance;

    @ManyToOne
    @JoinColumn(name = "annotation_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private Annotation annotation;

    // ~ Constructors ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public ObjectInstanceAnnotation() {
    }

    public ObjectInstanceAnnotation(ObjectInstance objectInstance, Annotation annotation) {
        this.objectInstance = objectInstance;
        this.annotation = annotation;
    }

    public ObjectInstanceAnnotation(ObjectInstanceAnnotation inOIA) {
        if (inOIA.objectInstance != null) {
            this.objectInstance = inOIA.objectInstance;
        }
        if (inOIA.annotation != null) {
            this.annotation = inOIA.annotation;
        }
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static List<ObjectInstanceAnnotation> getAllObjectInstanceAnnotation() {
        ObjectInstanceAnnotationQuery query = new ObjectInstanceAnnotationQuery();
        return query.getList();
    }

    public static List<ObjectInstanceAnnotation> getObjectInstanceAnnotationFiltered(ObjectInstance inObjectInstance,
                                                                                     Annotation inAnnotation) {
        ObjectInstanceAnnotationQuery query = new ObjectInstanceAnnotationQuery();
        query.objectInstance().eqIfValueNotNull(inObjectInstance);
        query.annotation().eqIfValueNotNull(inAnnotation);
        return query.getList();
    }

    public static void deleteObjectInstanceAnnotation(ObjectInstance objectInstance) {
        if (objectInstance != null) {
            List<ObjectInstanceAnnotation> list_OIA = ObjectInstanceAnnotation.getObjectInstanceAnnotationFiltered(
                    objectInstance, null);
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            for (ObjectInstanceAnnotation inObjectInstanceAnnotation : list_OIA) {
                ObjectInstanceAnnotation objectInstanceAnnToRemove = entityManager.find(ObjectInstanceAnnotation.class,
                        inObjectInstanceAnnotation.getId());
                entityManager.remove(objectInstanceAnnToRemove);
            }
            entityManager.flush();
        }
    }

    public static void deleteObjectInstanceAnnotation(Annotation annotation) {
        if (annotation != null) {
            List<ObjectInstanceAnnotation> list_OIA = ObjectInstanceAnnotation.getObjectInstanceAnnotationFiltered(
                    null, annotation);
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            for (ObjectInstanceAnnotation inObjectInstanceAnnotation : list_OIA) {
                ObjectInstanceAnnotation objectInstanceAnnToRemove = entityManager.find(ObjectInstanceAnnotation.class,
                        inObjectInstanceAnnotation.getId());
                entityManager.remove(objectInstanceAnnToRemove);
            }
            entityManager.flush();
        }
    }

    public static void deleteObjectInstanceAnnotation(ObjectInstanceAnnotation objectInstanceAnnotation) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        ObjectInstanceAnnotation objectInstanceAnnotationToRemove = entityManager.find(ObjectInstanceAnnotation.class,
                objectInstanceAnnotation.getId());
        entityManager.remove(objectInstanceAnnotationToRemove);
        entityManager.flush();
    }

    public ObjectInstance getObjectInstance() {
        return objectInstance;
    }

    // ~ methods ////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setObjectInstance(ObjectInstance objectInstance) {
        this.objectInstance = objectInstance;
    }

    public Annotation getAnnotation() {
        return annotation;
    }

    public void setAnnotation(Annotation annotation) {
        this.annotation = annotation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
