/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

/**
 * @author abderrazek boufahja / INRIA Rennes IHE development Project
 */
@Entity
@Table(name = "tm_annotation")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_annotation_sequence", sequenceName = "tm_annotation_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Annotation extends AuditedObject implements Serializable {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    private static final long serialVersionUID = 1881422545451441951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ObjectInstance id
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_annotation_sequence")
    private Integer id;

    /**
     * Location
     */
    @Column(name = "value", unique = false, nullable = true)
    @Size(max = 500)
    private String value;

    @OneToMany(mappedBy = "annotation", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @NotAudited
    private Set<ObjectInstanceAnnotation> objectInstanceAnnotations;

    // constructors ///////////////////////////////////////////////////////////////////////////////////////////////////

    public Annotation() {
    }

    public Annotation(String value) {
        this.value = value;
    }

    public Annotation(Annotation ann) {
        if (ann.value != null) {
            this.value = ann.value;
        }
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void deleteAnnotation(Annotation annotation) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Annotation ann = entityManager.find(Annotation.class, annotation.getId());
        entityManager.remove(ann);
        entityManager.flush();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<ObjectInstanceAnnotation> getObjectInstanceAnnotations() {
        return HibernateHelper.getLazyValue(this, "objectInstanceAnnotations", this.objectInstanceAnnotations);
    }

    public void setObjectInstanceAnnotations(Set<ObjectInstanceAnnotation> objectInstanceAnnotations) {
        this.objectInstanceAnnotations = objectInstanceAnnotations;
    }

    public String getValue() {
        return value;
    }

    // methods ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setValue(String value) {
        this.value = value;
    }

}
