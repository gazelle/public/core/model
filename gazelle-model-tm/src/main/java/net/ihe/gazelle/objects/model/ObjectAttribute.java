/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>ObjectAttribute<br>
 * <br>
 * This class describes the ObjectAttribute object, used by the Gazelle application.
 * <p/>
 * This class belongs to the TM module.
 * <p/>
 * ObjectAttribute possesses the following attributes :
 * <ul>
 * <li><b>id</b> : ObjectAttribute id</li>
 * <li><b>object</b> : Object corresponding to ObjectAttribute</li>
 * <li><b>keyword</b> : Keyword corresponding to ObjectAttribute</li>
 * </ul>
 * </br> <b>Example of ObjectAttribute</b> : - <br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, April 15th
 * @class ObjectAttribute.java
 * @package net.ihe.gazelle.objects.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "keyword",
        "description"
})
@XmlRootElement(name = "objectAttribute")
@Entity
@Audited
@Table(name = "tm_object_attribute")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_object_attribute_sequence", sequenceName = "tm_object_attribute_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class ObjectAttribute extends AuditedObject implements Serializable, Comparable<ObjectAttribute> {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    /**
     * Serial version Id of this object!
     */
    private static final long serialVersionUID = 1881422500711441951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ObjectAttribute id
     */
    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_object_attribute_sequence")
    private Integer id;

    /**
     * Object
     */
    @XmlTransient
    @ManyToOne
    // JRC : Remove nullable constraint in : @JoinColumn(name = "object_id", unique = false, nullable = false) to be able to sync ObjectAttribute object with GMM
    @JoinColumn(name = "object_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    @AuditJoinTable
    private ObjectType object;

    /**
     * Keyword
     */
    @Column(name = "keyword", unique = false, nullable = false)
    private String keyword;

    /**
     * Description
     */
    @Column(name = "description", unique = false, nullable = true)
    private String description;

//    @OneToMany(mappedBy = "attribute", fetch = FetchType.LAZY)
//    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
//    private Set<ObjectAttributeOption> objectAttributeOptions;

    @OneToMany(mappedBy = "attribute", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @NotAudited
    @XmlTransient
    private Set<ObjectInstanceAttribute> objectInstanceAttributes;

    // constructors ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public ObjectAttribute() {
    }

    public ObjectAttribute(ObjectType object, String keyword, String description) {
        this.object = object;
        this.keyword = keyword;
        this.description = description;
    }

    public ObjectAttribute(ObjectAttribute OAtt) {
        if (OAtt.object != null) {
            this.object = OAtt.object;
        }
        if (OAtt.keyword != null) {
            this.keyword = OAtt.keyword;
        }
        if (OAtt.description != null) {
            this.description = OAtt.description;
        }
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static List<ObjectAttribute> getObjectAttributeFiltered(ObjectType inObjectType) {
        ObjectAttributeQuery objectAttributeQuery = new ObjectAttributeQuery();
        if (inObjectType != null) {
            objectAttributeQuery.object().eq(inObjectType);
        }
        return objectAttributeQuery.getList();
    }

    public static void deleteObjectAttribute(ObjectType objectType) {
        if (objectType != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            ObjectAttributeQuery q = new ObjectAttributeQuery();
            q.object().id().eq(objectType.getId());
            List<ObjectAttribute> list_objectAttribute = q.getList();
            Collections.sort(list_objectAttribute);
            for (ObjectAttribute objectAttr : list_objectAttribute) {
                ObjectInstanceAttribute.deleteObjectInstanceAttribute(objectAttr);
//                ObjectAttributeOption.deleteObjectAttributeOption(objectAttr);
                ObjectAttribute objectAttrToRemove = entityManager.find(ObjectAttribute.class, objectAttr.getId());
                entityManager.remove(objectAttrToRemove);
            }
        }
    }

    public static void deleteObjectAttribute(ObjectAttribute objectAttribute) {
        if (objectAttribute != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            ObjectInstanceAttribute.deleteObjectInstanceAttribute(objectAttribute);
//            ObjectAttributeOption.deleteObjectAttributeOption(objectAttribute);
            ObjectAttribute objectAttrToRemove = entityManager.find(ObjectAttribute.class, objectAttribute.getId());
            entityManager.remove(objectAttrToRemove);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ObjectType getObject() {
        return object;
    }

    public void setObject(ObjectType object) {
        this.object = object;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

//    public Set<ObjectAttributeOption> getObjectAttributeOptions() {
//        return HibernateHelper.getLazyValue(this, "objectAttributeOptions", this.objectAttributeOptions);
//    }
//
//    public void setObjectAttributeOptions(Set<ObjectAttributeOption> objectAttributeOptions) {
//        this.objectAttributeOptions = objectAttributeOptions;
//    }

    public Set<ObjectInstanceAttribute> getObjectInstanceAttributes() {
        return HibernateHelper.getLazyValue(this, "objectInstanceAttributes", this.objectInstanceAttributes);
    }

    // ~ Methods ////////////////////////////////////////////////////////////////////

    public void setObjectInstanceAttributes(Set<ObjectInstanceAttribute> objectInstanceAttributes) {
        this.objectInstanceAttributes = objectInstanceAttributes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(ObjectAttribute objectAttribute) {
        return this.getId().compareTo(objectAttribute.getId());
    }
}
