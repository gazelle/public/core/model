/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.evsclient.connector.model.EVSClientValidatedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.util.ThumbnailCreator;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.apache.commons.io.FileUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tm_object_instance_file")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_object_instance_file_sequence", sequenceName = "tm_object_instance_file_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class ObjectInstanceFile extends AuditedObject implements Serializable, Comparable<ObjectInstanceFile>,
        EVSClientValidatedObject {


    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(ObjectInstanceFile.class);
    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_object_instance_file_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "instance_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private ObjectInstance instance;

    @ManyToOne
    @JoinColumn(name = "file_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private ObjectFile file;

    @ManyToOne
    @JoinColumn(name = "system_in_session_id", unique = false, nullable = true)
    @Fetch(value = FetchMode.SELECT)
    private SystemInSession system;

    @Column(name = "url")
    private String url;

    @Column(name = "result_oid")
    private ArrayList<String> resultOid;

    private transient String absolutePath;

    // constructors ////////////////////////////////////////////////////////////////////////////////////////////////////////

    public ObjectInstanceFile() {
    }

    public ObjectInstanceFile(ObjectInstance instance, ObjectFile file, SystemInSession system, String url) {
        this.instance = instance;
        this.file = file;
        this.url = url;
        this.system = system;
    }

    public ObjectInstanceFile(ObjectInstanceFile OIF) {
        if (OIF.instance != null) {
            this.instance = OIF.instance;
        }
        if (OIF.file != null) {
            this.file = OIF.file;
        }
        if (OIF.url != null) {
            this.url = OIF.url;
        }
        if (OIF.system != null) {
            this.system = OIF.system;
        }
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    protected static ObjectInstanceFileQuery getQuery(ObjectInstance inObjectInstance, ObjectFile inObjectFile,
                                                      SystemInSession inSystemInSession, String uploader, String fileTypeKeyword) {
        ObjectInstanceFileQuery query = new ObjectInstanceFileQuery();
        query.instance().eqIfValueNotNull(inObjectInstance);
        query.file().eqIfValueNotNull(inObjectFile);
        query.system().eqIfValueNotNull(inSystemInSession);
        query.file().uploader().eqIfValueNotNull(uploader);
        query.file().type().keyword().eqIfValueNotNull(fileTypeKeyword);
        return query;
    }

    public static List<ObjectInstanceFile> getObjectInstanceFileFiltered(ObjectInstance inObjectInstance,
                                                                         ObjectFile inObjectFile, SystemInSession inSystemInSession, String
                                                                                 uploader, String fileTypeKeyword) {

        if ((inObjectInstance == null) && (inObjectFile == null) && (inSystemInSession == null) && (uploader == null)
                && (fileTypeKeyword == null)) {
            return null;
        }

        ObjectInstanceFileQuery query = getQuery(inObjectInstance, inObjectFile, inSystemInSession, uploader,
                fileTypeKeyword);
        return query.getList();
    }

    public static int getCountObjectInstanceFileFiltered(ObjectInstance inObjectInstance, ObjectFile inObjectFile,
                                                         SystemInSession inSystemInSession, String uploader, String fileTypeKeyword) {
        if ((inObjectInstance == null) && (inObjectFile == null) && (inSystemInSession == null) && (uploader == null)
                && (fileTypeKeyword == null)) {
            return 0;
        }
        ObjectInstanceFileQuery query = getQuery(inObjectInstance, inObjectFile, inSystemInSession, uploader,
                fileTypeKeyword);
        return query.getCount();
    }

    public static void deleteObjectInstanceFile(ObjectFile objectFile) {
        if (objectFile != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            List<ObjectInstanceFile> list_objectInstanceFile = getObjectInstanceFileFiltered(null, objectFile, null,
                    null, null);
            for (ObjectInstanceFile objectIFile : list_objectInstanceFile) {
                ObjectInstanceFile objectIFileToRemove = entityManager.find(ObjectInstanceFile.class,
                        objectIFile.getId());
                entityManager.remove(objectIFileToRemove);
            }
        }
    }

    public static void deleteObjectInstanceFile(ObjectInstance objectInstance) {
        if (objectInstance != null) {
            List<ObjectInstanceFile> list_objectInstanceFile = getObjectInstanceFileFiltered(objectInstance, null,
                    null, null, null);
            for (ObjectInstanceFile objectIFile : list_objectInstanceFile) {
                ObjectInstanceFile.deleteObjectInstanceFile(objectIFile);
            }
        }
    }

    public static void deleteObjectInstanceFile(ObjectInstanceFile objectInstanceFile) {
        if (objectInstanceFile != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            objectInstanceFile = entityManager.find(ObjectInstanceFile.class, objectInstanceFile.getId());
            ObjectInstanceFile.deleteObjectInstanceFileFromDisc(objectInstanceFile);
            ObjectInstanceFile objectIFileToRemove = entityManager.find(ObjectInstanceFile.class,
                    objectInstanceFile.getId());
            entityManager.remove(objectIFileToRemove);

        }
    }

    public static void deleteObjectInstanceFileFromDisc(ObjectInstanceFile objectInstanceFile) {
        if (objectInstanceFile != null) {
            String OIFpath = objectInstanceFile.getFileAbsolutePath();
            java.io.File fileToDelete = new java.io.File(OIFpath);
            if (fileToDelete.exists()) {
                fileToDelete.delete();
            }
            if (objectInstanceFile.getFile().getType().getKeyword().equals("SNAPSHOT")) {
                String thumbnailpath = ThumbnailCreator.createNewFileName(OIFpath);
                java.io.File thumbnailToDelete = new java.io.File(thumbnailpath);
                if (thumbnailToDelete.exists()) {
                    thumbnailToDelete.delete();
                }
            }
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ObjectInstance getInstance() {
        return instance;
    }

    // ~methods ///////////////////////////////////////////////////////////////////////

    public void setInstance(ObjectInstance instance) {
        this.instance = instance;
    }

    public ObjectFile getFile() {
        return file;
    }

    public void setFile(ObjectFile file) {
        this.file = file;
    }

    public SystemInSession getSystem() {
        return system;
    }

    public void setSystem(SystemInSession system) {
        this.system = system;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileAbsolutePath() {
        if (absolutePath == null) {
            StringBuilder path = new StringBuilder(ObjectInstance.getObjectsPath());
            path.append(File.separatorChar);
            path.append(instance.getId());
            path.append(File.separatorChar);
            path.append(file.getId());
            path.append(File.separatorChar);
            path.append(id);
            path.append('_');
            path.append(url);
            absolutePath = path.toString();
        }
        return absolutePath;
    }

    public String getThumbnailFileAbsolutePath() {
        if (absolutePath == null) {
            StringBuilder path = new StringBuilder(ObjectInstance.getObjectsPath());
            path.append(File.separatorChar);
            path.append(instance.getId());
            path.append(File.separatorChar);
            path.append(file.getId());
            path.append(File.separatorChar);
            path.append("thumbnail_");
            path.append(id);
            path.append('_');
            path.append(url);
            absolutePath = path.toString();
        }
        return absolutePath;
    }

    @Override
    public int compareTo(ObjectInstanceFile arg0) {
        if (this.getId() > arg0.getId()) {
            return 1;
        }
        if (this.getId() < arg0.getId()) {
            return (-1);
        }
        return 0;
    }

    public boolean isWritable() {
        return this.file.getType().getWritable();
    }

    @Override
    public String getUniqueKey() {
        return "sample_" + id;
    }

    @Override
    public PartSource getPartSource() {
        String filepath = getFileAbsolutePath();
        try {
            byte[] contentBytes = Files.readAllBytes(Paths.get(filepath));
            String content = DatatypeConverter.printBase64Binary(contentBytes);
            return new ByteArrayPartSource(getType(), content.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            log.error("" + e.getMessage());
        }
        return null;
    }

    @Override
    public String getType() {
        ObjectFileType objectFileType = file.getType();
        return objectFileType.getKeyword();
    }

    public ArrayList<String> getResultOid() {
        if (resultOid == null) {
            resultOid = new ArrayList<String>();
        }
        return resultOid;
    }

    public void setResultOid(ArrayList<String> resultOid) {
        this.resultOid = resultOid;
    }

    public void addResultOid(String oid) {
        if (!getResultOid().contains(oid)) {
            getResultOid().add(oid);
        }
    }

    public void removeResultOid(String oid) {
        getResultOid().remove(oid);
    }

}
