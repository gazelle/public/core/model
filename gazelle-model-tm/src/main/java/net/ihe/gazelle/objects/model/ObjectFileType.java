/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>ObjectFileType<br>
 * <br>
 * This class describes the ObjectFileType object, used by the Gazelle application.
 * <p/>
 * This class belongs to the TM module.
 * <p/>
 * ObjectFileType possesses the following attributes :
 * <ul>
 * <li><b>id</b> : ObjectFileType id</li>
 * <li><b>object</b> : Object corresponding to ObjectFileType</li>
 * <li><b>keyword</b> : Keyword corresponding to ObjectFileType</li>
 * </ul>
 * </br> <b>Example of ObjectFileType</b> : - <br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, April 15th
 * @class ObjectFileType.java
 * @package net.ihe.gazelle.objects.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "keyword",
        "description",
        "extensions",
        "writable",
        "validate"
})
@XmlRootElement(name = "objectAttribute")
@Entity
@Audited
@Table(name = "tm_object_file_type")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_object_file_type_sequence", sequenceName = "tm_object_file_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class ObjectFileType extends AuditedObject implements Serializable {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    /**
     * Serial version Id of this object!
     */
    private static final long serialVersionUID = 1884422500711441951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ObjectFileType id
     */
    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_object_file_type_sequence")
    private Integer id;

    /**
     * Keyword
     */
    @Column(name = "keyword", unique = true, nullable = false)
    private String keyword;

    /**
     * Description
     */
    @Column(name = "description", unique = false)
    private String description;

    /**
     * Extensions
     */
    @Column(name = "extensions", unique = false)
    private String extensions;

    /**
     * Extensions
     */
    @Column(name = "writable", unique = false)
    private Boolean writable;

    /**
     * indicates if this kind of file can be validated in EVSClient
     */
    @Column(name = "validate")
    private boolean validate;

    @XmlTransient
    @OneToMany(mappedBy = "type", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<ObjectFile> objectFiles;

    // constructors //////////////////////////////////////////////////////////////////////////////////////////////////////

    public ObjectFileType() {
        this.validate = false;
    }

    public ObjectFileType(String keyword, String description, String extensions, Boolean writable, boolean validate) {
        this.keyword = keyword;
        this.description = description;
        this.extensions = extensions;
        this.writable = writable;
        this.validate = validate;
    }

    public ObjectFileType(ObjectFileType objectFileType) {
        if (objectFileType.getDescription() != null) {
            this.description = objectFileType.getDescription();
        }
        if (objectFileType.getKeyword() != null) {
            this.keyword = objectFileType.getKeyword();
        }
        if (objectFileType.getExtensions() != null) {
            this.extensions = objectFileType.getExtensions();
        }
        if (objectFileType.getWritable() != null) {
            this.writable = objectFileType.getWritable();
        }
        this.validate = objectFileType.isValidate();
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static List<ObjectFileType> getListOfAllObjectFileType() {
        ObjectFileTypeQuery query = new ObjectFileTypeQuery();
        return query.getList();
    }

    public static List<ObjectFileType> getObjectFileTypeFiltered(ObjectType inObjectType, String inKeyword) {
        ObjectFileTypeQuery query = new ObjectFileTypeQuery();
        query.objectFiles().object().eqIfValueNotNull(inObjectType);
        query.keyword().eqIfValueNotNull(inKeyword);
        return query.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @FilterLabel
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExtensions() {
        return extensions;
    }

    public void setExtensions(String extensions) {
        this.extensions = extensions;
    }

    public Set<ObjectFile> getObjectFiles() {
        return HibernateHelper.getLazyValue(this, "objectFiles", this.objectFiles);
    }

    public void setObjectFiles(Set<ObjectFile> objectFiles) {
        this.objectFiles = objectFiles;
    }

    // ~methods //////////////////////////////////////////////////////////////////

    public Boolean getWritable() {
        if (writable == null) {
            return false;
        }
        return writable;
    }

    public void setWritable(Boolean writable) {
        this.writable = writable;
    }

    public boolean hasExtension() {
        if (this.extensions != null) {
            if (!this.extensions.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public boolean userCanWriteContent() {
        boolean res = false;
        if (this.writable != null) {
            if (this.writable.booleanValue() == true) {
                res = true;
            }
        }
        return res;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }

}
