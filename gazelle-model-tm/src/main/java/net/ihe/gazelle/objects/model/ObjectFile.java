/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>ObjectFile<br>
 * <br>
 * This class describes the ObjectFile object, used by the Gazelle application.
 * <p/>
 * This class belongs to the TM module.
 * <p/>
 * ObjectFile possesses the following attributes :
 * <ul>
 * <li><b>id</b> : ObjectFile id</li>
 * <li><b>object</b> : Object type corresponding to ObjectFile</li>
 * <li><b>description</b></li>
 * <li><b>max</b></li>
 * <li><b>min</b></li>
 * <li><b>type</b></li>
 * <li><b>uploader</b></li>
 * </ul>
 * </br>
 *
 * @author Abderrazek Boufahja - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, April 15th
 * @class ObjectFile.java
 * @package net.ihe.gazelle.objects.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "description",
        "min",
        "max",
        "type",
        "uploader"
})
@XmlRootElement(name = "objectAttribute")
@Entity
@Audited
@Table(name = "tm_object_file", uniqueConstraints = @UniqueConstraint(columnNames = {"description", "object_id",
        "type_id", "uploader"}))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_object_file_sequence", sequenceName = "tm_object_file_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class ObjectFile extends AuditedObject implements Serializable, Comparable<ObjectFile> {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    /**
     * Serial version Id of this object!
     */
    private static final long serialVersionUID = 1881422500711441951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ObjectFile id
     */
    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_object_file_sequence")
    private Integer id;

    /**
     * Object
     */
    @ManyToOne
    // JRC : Remove nullable constraint in : @JoinColumn(name = "object_id", unique = false, nullable = false) to be able to sync ObjectFile object with GMM
    @JoinColumn(name = "object_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    @AuditJoinTable
    @XmlTransient
    private ObjectType object;

    /**
     * Description
     */
    @Column(name = "description", unique = false, nullable = true)
    private String description;

    /**
     * Minimum
     */
    @Column(name = "min", unique = false, nullable = true)
    private int min;

    /**
     * Maximum
     */
    @Column(name = "max", unique = false, nullable = true)
    private int max;

    /**
     * Type
     */
    @ManyToOne
    // JRC : Remove nullable constraint in : @JoinColumn(name = "type_id", unique = false, nullable = false) to be able to sync ObjectFile object with GMM
    @JoinColumn(name = "type_id", unique = false, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    @AuditJoinTable
    private ObjectFileType type;

    /**
     * Uploader
     */
    @Column(name = "uploader", unique = false, nullable = true)
    private String uploader;

    @OneToMany(mappedBy = "file", fetch = FetchType.LAZY)
    @NotAudited
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private Set<ObjectInstanceFile> objectInstanceFiles;

    // constructors ///////////////////////////////////////////////////////////////////////////////
    public ObjectFile() {
    }

    public ObjectFile(ObjectType object, String description, ObjectFileType type, int min, int max, String uploader) {
        this.object = object;
        this.description = description;
        this.type = type;
        this.min = min;
        this.max = max;
        this.uploader = uploader;
    }

    public ObjectFile(ObjectFile objectFile) {
        if (objectFile.object != null) {
            this.object = objectFile.object;
        }
        if (objectFile.description != null) {
            this.description = objectFile.description;
        }
        if (objectFile.type != null) {
            this.type = objectFile.type;
        }
        if (objectFile.uploader != null) {
            this.uploader = objectFile.uploader;
        }
        this.min = objectFile.min;
        this.max = objectFile.max;
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void deleteObjectFile(ObjectType objectType) {
        if (objectType != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            ObjectFileQuery query = new ObjectFileQuery();
            query.object().eq(objectType);
            List<ObjectFile> list_objectFile = query.getList();
            for (ObjectFile objectFile : list_objectFile) {
                ObjectInstanceFile.deleteObjectInstanceFile(objectFile);
                ObjectFile objectFileToRemove = entityManager.find(ObjectFile.class, objectFile.getId());
                entityManager.remove(objectFileToRemove);
            }

        }
    }

    public static void deleteObjectFile(ObjectFile objectFile) {
        if (objectFile != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            ObjectInstanceFile.deleteObjectInstanceFile(objectFile);
            ObjectFile objectFileToRemove = entityManager.find(ObjectFile.class, objectFile.getId());
            entityManager.remove(objectFileToRemove);
        }
    }

    public static ObjectFile saveObjectFile(ObjectFile of, EntityManager em) {
        if (of != null) {
            of = em.merge(of);
            em.flush();
            return of;
        }
        return null;
    }

    public static List<ObjectFile> getObjectFileFiltered(ObjectType object, String uploader) {
        ObjectFileQuery query = new ObjectFileQuery();
        query.object().id().eqIfValueNotNull(object.getId());
        query.uploader().eqIfValueNotNull(uploader);
        return query.getListNullIfEmpty();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ObjectType getObject() {
        return object;
    }

    public void setObject(ObjectType object) {
        this.object = object;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ObjectFileType getType() {
        return type;
    }

    public void setType(ObjectFileType type) {
        this.type = type;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        if (max == 0) {
            max = 1;
        }
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public Set<ObjectInstanceFile> getObjectInstanceFiles() {
        return HibernateHelper.getLazyValue(this, "objectInstanceFiles", this.objectInstanceFiles);
    }

    public void setObjectInstanceFiles(Set<ObjectInstanceFile> objectInstanceFiles) {
        this.objectInstanceFiles = objectInstanceFiles;
    }

    public boolean isDicom() {
        if (this.getType() != null) {
            if (this.getType().getKeyword() != null) {
                if (this.getType().getKeyword().equals("DICOM")) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isCDA() {
        if (this.getType() != null) {
            if (this.getType().getKeyword() != null) {
                if (this.getType().getKeyword().equals("CDA")) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int compareTo(ObjectFile arg0) {
        if (this.getId() > arg0.getId()) {
            return 1;
        }
        if (this.getId() < arg0.getId()) {
            return (-1);
        }
        return 0;
    }

}
