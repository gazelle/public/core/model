/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.objects.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;


/**
 * <b>Class Description : </b>ObjectType<br>
 * <br>
 * This class describes the ObjectType object, used by the Gazelle application.
 * <p/>
 * This class belongs to the TM module.
 * <p/>
 * ObjectType possesses the following attributes :
 * <ul>
 * <li><b>id</b> : ObjectType id</li>
 * <li><b>object</b> : Object corresponding to ObjectType</li>
 * <li><b>keyword</b> : Keyword corresponding to ObjectType</li>
 * </ul>
 * </br> <b>Example of ObjectType</b> : - <br>
 *
 * @author Abderrazek Boufahja - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, April 15th
 * @class ObjectType.java
 * @package net.ihe.gazelle.objects.model
 * @see > aboufahj@irisa.fr - Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "keyword",
        "description",
        "default_desc",
        "instructions",
        "objectTypeStatus",
        "objectAttributes",
        "objectCreators",
        "objectFiles",
        "objectReaders"
})
@XmlRootElement(name = "objectType")
@Entity
@Audited
@Table(name = "tm_object_type")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_object_type_sequence", sequenceName = "tm_object_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class ObjectType extends AuditedObject implements Serializable, Comparable<ObjectType> {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    /**
     * Serial version Id of this object!
     */
    private static final long serialVersionUID = 1884422500711441951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ObjectType id
     */
    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_object_type_sequence")
    private Integer id;

    /**
     * Keyword
     */
    @Column(name = "keyword", unique = true, nullable = false)
    @Size(max = 250)
    private String keyword;

    /**
     * Description
     */
    @Column(name = "description", unique = false, nullable = true)
    @Size(max = 250)
    private String description;

    /**
     * Default description
     */
    @Column(name = "default_desc", unique = false, nullable = true)
    @Size(max = 500)
    private String default_desc;

    /**
     * Instruction
     */
    @Column(name = "instructions", unique = false, nullable = true)
    @Lob
    @Type(type = "text")
    private String instructions;

    /**
     * Type
     */
    @ManyToOne
    @JoinColumn(name = "object_type_status_id", unique = false, nullable = true)
    @Fetch(value = FetchMode.SELECT)
    @AuditJoinTable
    private ObjectTypeStatus objectTypeStatus;

    @OneToMany(mappedBy = "object", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<ObjectAttribute> objectAttributes;

    @OneToMany(mappedBy = "object", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<ObjectCreator> objectCreators;

    @OneToMany(mappedBy = "object", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<ObjectFile> objectFiles;

    @OneToMany(mappedBy = "object", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @NotAudited
    @XmlTransient
    private Set<ObjectInstance> objectInstances;

    @OneToMany(mappedBy = "object", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<ObjectReader> objectReaders;

    // constructor

    public ObjectType() {
    }

    public ObjectType(String keyword, String description, String default_desc, String instructions,
                      ObjectTypeStatus inObjectTypeStatus) {
        this.keyword = keyword;
        this.description = description;
        this.default_desc = default_desc;
        this.instructions = instructions;
        this.objectTypeStatus = inObjectTypeStatus;
    }

    public ObjectType(ObjectType OT) {
        if (OT.keyword != null) {
            this.keyword = OT.keyword;
        }
        if (OT.description != null) {
            this.description = OT.description;
        }
        if (OT.default_desc != null) {
            this.default_desc = OT.default_desc;
        }
        if (OT.instructions != null) {
            this.instructions = OT.instructions;
        }
        if (OT.objectTypeStatus != null) {
            this.objectTypeStatus = OT.objectTypeStatus;
        }
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    // ~ Methods ////////////////////////////////////////////////////////////////////
    public static List<ObjectType> getAllObjectType() {
        ObjectTypeQuery q = new ObjectTypeQuery();
        return q.getList();
    }

    public static List<ObjectType> getObjectTypeFiltered(String inKeyword, ObjectTypeStatus inObjectTypeStatus,
                                                         net.ihe.gazelle.tm.systems.model.System inSystemCreator,
                                                         net.ihe.gazelle.tm.systems.model.System inSystemReader) {
        return getObjectTypeFiltered(inKeyword, inObjectTypeStatus, inSystemCreator, inSystemReader, null);
    }

    public static List<ObjectType> getObjectTypeFiltered(String inKeyword, ObjectTypeStatus inObjectTypeStatus,
                                                         net.ihe.gazelle.tm.systems.model.System inSystemCreator,
                                                         net.ihe.gazelle.tm.systems.model.System inSystemReader, ObjectFileType inObjectFileType) {
        ObjectTypeQuery query = new ObjectTypeQuery();
        query.objectTypeStatus().keyword().eqIfValueNotNull(inKeyword);
        query.objectTypeStatus().eqIfValueNotNull(inObjectTypeStatus);
        query.objectCreators().reversedAipo().systemActorProfiles().system().eqIfValueNotNull(inSystemCreator);
        query.objectReaders().reversedAipo().systemActorProfiles().system().eqIfValueNotNull(inSystemReader);
        query.objectFiles().type().eqIfValueNotNull(inObjectFileType);
        return query.getList();
    }

    public static void deleteObjectType(ObjectType objectType) {
        if (objectType != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            ObjectFile.deleteObjectFile(objectType);
            ObjectAttribute.deleteObjectAttribute(objectType);
            ObjectInstance.deleteObjectInstance(objectType);
            ObjectReader.deleteObjectReader(objectType);
            ObjectCreator.deleteObjectCreator(objectType);

            ObjectType objectTypeToRemove = entityManager.find(ObjectType.class, objectType.getId());
            entityManager.remove(objectTypeToRemove);
            entityManager.flush();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @FilterLabel
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefault_desc() {
        return default_desc;
    }

    public void setDefault_desc(String default_desc) {
        this.default_desc = default_desc;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public ObjectTypeStatus getObjectTypeStatus() {
        return objectTypeStatus;
    }

    public void setObjectTypeStatus(ObjectTypeStatus objectTypeStatus) {
        this.objectTypeStatus = objectTypeStatus;
    }

    public Set<ObjectFile> getObjectFiles() {
        return HibernateHelper.getLazyValue(this, "objectFiles", this.objectFiles);
    }

    public void setObjectFiles(Set<ObjectFile> objectFiles) {
        this.objectFiles = objectFiles;
    }

    public Set<ObjectInstance> getObjectInstances() {
        return HibernateHelper.getLazyValue(this, "objectInstances", this.objectInstances);
    }

    public void setObjectInstances(Set<ObjectInstance> objectInstances) {
        this.objectInstances = objectInstances;
    }

    public Set<ObjectReader> getObjectReaders() {
        return HibernateHelper.getLazyValue(this, "objectReaders", this.objectReaders);
    }

    public void setObjectReaders(Set<ObjectReader> objectReaders) {
        this.objectReaders = objectReaders;
    }

    public Set<ObjectCreator> getObjectCreators() {
        return HibernateHelper.getLazyValue(this, "objectCreators", this.objectCreators);
    }

    public void setObjectCreators(Set<ObjectCreator> objectCreators) {
        this.objectCreators = objectCreators;
    }

    public Set<ObjectAttribute> getObjectAttributes() {
        return HibernateHelper.getLazyValue(this, "objectAttributes", this.objectAttributes);
    }

    public void setObjectAttributes(Set<ObjectAttribute> objectAttributes) {
        this.objectAttributes = objectAttributes;
    }

    @Override
    public int compareTo(ObjectType o) {
        if (this.getKeyword().compareToIgnoreCase(o.getKeyword()) > 0) {
            return 1;
        }
        if (this.getKeyword().compareToIgnoreCase(o.getKeyword()) < 0) {
            return (-1);
        }
        return 0;
    }

}
