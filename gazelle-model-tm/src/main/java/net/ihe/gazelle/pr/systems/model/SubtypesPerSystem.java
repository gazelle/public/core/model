/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.pr.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.SystemSubtypesPerSystemType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * <b>Class Description : </b>SubtypesPerSystem<br>
 * <br>
 * This class describes the relation between a System, a System Type and a System SubType. This relation corresponds to an object, it is used by the Gazelle application.
 * <p/>
 * SubtypesPerSystem possesses the following attributes :
 * <ul>
 * <li><b>systemId</b> : System Id corresponding to the System</li>
 * <li><b>systemTypeId</b> : System type Id associated to the System</li>
 * <li><b>systemSubTypeId</b> : System subtype Id associated to the System</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, March 23
 * @class SubtypesPerSystem.java
 * @package net.ihe.gazelle.pr.systems.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "tm_subtypes_per_system", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "system_id", "system_subtypes_per_system_type_id"}))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_subtypes_per_system_sequence", sequenceName = "tm_subtypes_per_system_id_seq", allocationSize = 1)
@Deprecated
public class SubtypesPerSystem extends AuditedObject implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450908725531281710L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_subtypes_per_system_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "system_id", nullable = false)
    @NotNull
    private System system;

    @ManyToOne
    @JoinColumn(name = "system_subtypes_per_system_type_id", nullable = false)
    @NotNull
    private SystemSubtypesPerSystemType systemSubtypesPerSystemType;

    // Constructors

    public SubtypesPerSystem() {
    }

    public SubtypesPerSystem(Integer id) {
        super();
        this.id = id;
    }

    public SubtypesPerSystem(System system) {

        this.system = system;

    }

    public SubtypesPerSystem(Integer id, System system, SystemSubtypesPerSystemType systemSubtypesPerSystemType) {
        super();
        this.id = id;
        this.system = system;
        this.systemSubtypesPerSystemType = systemSubtypesPerSystemType;

    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public SystemSubtypesPerSystemType getSystemSubtypesPerSystemType() {
        return systemSubtypesPerSystemType;
    }

    public void setSystemSubtypesPerSystemType(SystemSubtypesPerSystemType systemSubtypesPerSystemType) {
        this.systemSubtypesPerSystemType = systemSubtypesPerSystemType;
    }

}
