/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.pr.search.model;

import net.ihe.gazelle.tm.systems.model.System;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

/**
 * <b>Class Description : </b>System<br>
 * <br>
 * This class describes a System with some additional attributes (for the needs of this application). It corresponds to a medical Information System, from a company (ex: a PACS or an ADT, or an
 * OF...). This class contains informations about the actors/integration profiles/options implemented. This class belongs to the ProductRegistry module.
 * <p/>
 * Integration Statement possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the System</li>
 * <li><b>isURL</b> : URL of the Integration Statement, corresponding to the System</li>
 * </ul>
 * </br> <b>Example of System</b> : PACS_IRISA is a system, ADT_AGFA1 is a system<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, March 28
 * @class System.java
 * @package net.ihe.gazelle.pr.systems.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "pr_integration_statement_download", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "pr_integration_statement_download_sequence", sequenceName = "pr_integration_statement_download_id_seq", allocationSize = 1)
public class ISDownload extends PREvent implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450905433531283760L;

    // Attributes (existing in database as a column)
    private static Logger log = LoggerFactory.getLogger(ISDownload.class);
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pr_integration_statement_download_sequence")
    private Integer id;
    // System corresponding to this download
    @ManyToOne
    @JoinColumn(name = "system_id", nullable = false)
    private System system;
    @ManyToOne
    @JoinColumn(name = "search_log_report_id", nullable = true)
    private SearchLogReport searchLogReport;

    // Constructor

    public ISDownload() {
    }

    public ISDownload(Integer id) {
        this.id = id;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public SearchLogReport getSearchLogReport() {
        return searchLogReport;
    }

    public void setSearchLogReport(SearchLogReport searchLogReport) {
        this.searchLogReport = searchLogReport;
    }

}
