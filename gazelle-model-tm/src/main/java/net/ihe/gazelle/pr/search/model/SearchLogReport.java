/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.pr.search.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.tm.systems.model.System;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description : </b>SearchLogReport<br>
 * <br>
 * <p/>
 * This class describes a report used to store all searches history. This is a search logger. A search report is mapped with some criteria, the list of criteria used for a search.
 * <p/>
 * <p/>
 * This class belongs to the ProductRegistry module. This class is an entity.
 * <p/>
 * SearchLogReport possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id of the SearchLogReport object</li>
 * <li><b>searchDate</b> : Date - when the search has been performed ?</li>
 * <li><b>searchPerformer</b> : Login - who has performed this search ? (we store the string login and not id login)</li>
 * <li><b>searchType</b> : Type of search - possible values : DEFAULT_SEARCH, ADVANCED_SEARCH, TF_SEARCH</li>
 * <li><b>intersectionSearch</b> : IntersectionSearch ? if true then the search is using 'AND' relation if false then the search is using 'OR' relation</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, April 15th
 * @class SearchLogReport.java
 * @package net.ihe.gazelle.pr.search.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "pr_search_log_report", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "pr_search_log_report_sequence", sequenceName = "pr_search_log_report_id_seq", allocationSize = 1)
public class SearchLogReport extends PREvent implements java.io.Serializable, Comparable<SearchLogReport> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -2104617352954299300L;

    // Attributes

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pr_search_log_report_sequence")
    private Integer id;

    @OneToMany(mappedBy = "searchLogReport")
    private List<ISDownload> isDownloads;

    // Variables used for Foreign Keys : pr_search_criteria_per_report table

    /**
     * Number of Integration statements found during this search
     */
    @Column(name = "number_of_criteria")
    private Integer numberOfCriteria;

    /**
     * List of criteria for this search log report
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "pr_search_criteria_per_report", joinColumns = @JoinColumn(name = "search_log_report_id"), inverseJoinColumns = @JoinColumn(name = "search_log_criterion_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "search_log_criterion_id", "search_log_report_id"}))
    private List<SearchLogCriterion> searchLogCriterion;

    /**
     * Number of Integration statements found during this search
     */
    @Column(name = "number_of_found_integration_statements")
    private Integer numberOfFoundIntegrationStatements;

    /**
     * Cross Table with systems found per search
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "pr_search_log_systems_found", joinColumns = @JoinColumn(name = "search_report_id"), inverseJoinColumns = @JoinColumn(name = "system_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "system_id", "search_report_id"}))
    private List<System> foundSystemsForSearchLog;

    // Constructor

    public SearchLogReport() {

    }

    // *********************************************************
    // Getters and Setters : setup the object properties
    // *********************************************************

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<SearchLogCriterion> getSearchLogCriterion() {
        return searchLogCriterion;
    }

    public void setSearchLogCriterion(List<SearchLogCriterion> searchLogCriterion) {
        this.searchLogCriterion = searchLogCriterion;
    }

    public Integer getNumberOfFoundIntegrationStatements() {
        return numberOfFoundIntegrationStatements;
    }

    public void setNumberOfFoundIntegrationStatements(Integer numberOfFoundIntegrationStatements) {
        this.numberOfFoundIntegrationStatements = numberOfFoundIntegrationStatements;
    }

    public List<System> getFoundSystemsForSearchLog() {
        return HibernateHelper.getLazyValue(this, "foundSystemsForSearchLog", this.foundSystemsForSearchLog);
    }

    public void setFoundSystemsForSearchLog(List<System> foundSystemsForSearchLog) {
        this.foundSystemsForSearchLog = foundSystemsForSearchLog;
    }

    public Integer getNumberOfCriteria() {
        return numberOfCriteria;
    }

    public void setNumberOfCriteria(Integer numberOfCriteria) {
        this.numberOfCriteria = numberOfCriteria;
    }

    public List<ISDownload> getIsDownloads() {
        return isDownloads;
    }

    @PrePersist
    @PreUpdate
    public void setNumbers() {
        if ((foundSystemsForSearchLog != null) && (foundSystemsForSearchLog.size() > 0)) {
            setNumberOfFoundIntegrationStatements(foundSystemsForSearchLog.size());
        } else {
            setNumberOfFoundIntegrationStatements(0);
        }
        if ((searchLogCriterion != null) && (searchLogCriterion.size() > 0)) {
            setNumberOfCriteria(searchLogCriterion.size());
        } else {
            setNumberOfCriteria(0);
        }
    }

    @Override
    public int compareTo(SearchLogReport searchLogReport) {
        return searchLogReport.getDate().compareTo(this.getDate());
    }
}
