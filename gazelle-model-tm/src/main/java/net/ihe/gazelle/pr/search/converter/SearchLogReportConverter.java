/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.pr.search.converter;


import net.ihe.gazelle.pr.search.model.ISDownload;
import net.ihe.gazelle.pr.search.model.SearchLogReport;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;
import java.io.Serializable;


/**
 * @author Guillaume Thomazon
 */
@BypassInterceptors
@Name("searchLogReportConverter")
@Converter(forClass = SearchLogReport.class)
public class SearchLogReportConverter implements javax.faces.convert.Converter, Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = 12393988987567L;


    private static Logger log = LoggerFactory.getLogger(SearchLogReportConverter.class);


    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value instanceof SearchLogReport) {
            SearchLogReport cc = (SearchLogReport) value;
            return cc.toString();
        } else if (value instanceof ISDownload) {
            ISDownload cc = (ISDownload) value;
            return cc.toString();
        } else if (value instanceof Integer) {
            return String.valueOf(value);
        } else {
            return null;
        }

    }


    @Transactional
    public Object getAsObject(FacesContext Context,
                              UIComponent Component, String value) throws ConverterException {
        if (value != null) {
            try {

                EntityManager entityManager = (EntityManager) org.jboss.seam.Component.getInstance("entityManager", true);

                SearchLogReport slr = entityManager.find(SearchLogReport.class, value);

                return slr;

            } catch (NumberFormatException e) {
                log.error("" + e.getMessage());
                e.printStackTrace();
            }
        }
        log.error("not a key !");
        return null;
    }


}
