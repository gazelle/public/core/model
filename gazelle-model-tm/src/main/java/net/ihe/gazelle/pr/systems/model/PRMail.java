package net.ihe.gazelle.pr.systems.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.tm.systems.model.System;

import javax.persistence.*;

@Entity
@Table(name = "pr_mail", schema = "public")
@SequenceGenerator(name = "pr_mail_sequence", sequenceName = "pr_mail_id_seq")
public class PRMail implements java.io.Serializable {

    private static final long serialVersionUID = -8855802554439199136L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pr_mail_sequence")
    private Integer id;

    @ManyToOne
    private System system;

    @ManyToOne
    private CrawlerReporting crawlerReporting;

    @Column(name = "page_name")
    private String pageName;

    @Column(name = "sent")
    private Boolean sent;

    public PRMail() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public CrawlerReporting getCrawlerReporting() {
        return crawlerReporting;
    }

    public void setCrawlerReporting(CrawlerReporting crawlerReporting) {
        this.crawlerReporting = crawlerReporting;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public Boolean getSent() {
        return sent;
    }

    public void setSent(Boolean sent) {
        this.sent = sent;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
