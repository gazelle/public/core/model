/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.pr.systems.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.tm.systems.model.System;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Entity
@Table(name = "pr_crawler_reporting", schema = "public")
@SequenceGenerator(name = "pr_crawler_reporting_sequence", sequenceName = "pr_crawler_reporting_id_seq", allocationSize = 1)
public class CrawlerReporting implements java.io.Serializable {

    private static final long serialVersionUID = 833587470009998950L;

    // Attributes (existing in database as a column)

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pr_crawler_reporting_sequence")
    private Integer id;

    /**
     * Date indicating when the crawler has been executed
     */
    @Column(name = "execution_starting_date")
    private Date executionStartingDate;

    /**
     * Elapsed time during execution (time in milliseconds)
     */
    @Column(name = "execution_elapsed_time")
    private Integer executionElapsedTime;

    /**
     * Total Number of Integration statements checked by the crawler
     */
    @Column(name = "number_of_integration_statements_checked")
    private Integer numberOfCheckedIntegrationStatements;

    /**
     * Number of Integration statements checked successfully by the crawler
     */
    @Column(name = "number_of_successfully_checked")
    private Integer numberOfSuccessfullyChecked;

    /**
     * Number of Integration statements checked by the crawler with a difference between remote IS and database informations
     */
    @Column(name = "number_of_unmatching_hashcode_for_integration_statements")
    private Integer numberOfUnmatchingHashcodeForIntegrationStatements;

    /**
     * Number of Integration statements checked by the crawler with an unreachable URL
     */
    @Column(name = "number_of_unreachable_integration_statements")
    private Integer numberOfUnreachableIntegrationStatements;

    @Column(name = "number_of_changed_integration_statements")
    private Integer numberOfChangedIntegrationStatements;

    /**
     * Cross Table with Unreachable Integration Statements for a crawler report
     */
    @ManyToMany
    @JoinTable(name = "pr_crawler_unreachable_integration_statements", joinColumns = @JoinColumn(name = "crawler_report_id"), inverseJoinColumns = @JoinColumn(name = "system_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "system_id", "crawler_report_id"}))
    private List<System> unreachableIntegrationStatements;

    /**
     * Cross Table with Unmatching Hashcode for Integration Statements by crawler report
     */
    @ManyToMany
    @JoinTable(name = "pr_crawler_unmatching_hascode_for_integration_statements", joinColumns = @JoinColumn(name = "crawler_report_id"), inverseJoinColumns = @JoinColumn(name = "system_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "system_id", "crawler_report_id"}))
    private List<System> unmatchingHashcodeForIntegrationStatements;

    @ManyToMany
    @JoinTable(name = "pr_crawler_changed_integration_statements", joinColumns = @JoinColumn(name = "crawler_report_id"), inverseJoinColumns = @JoinColumn(name = "system_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "system_id", "crawler_report_id"}))
    private List<System> changedIntegrationStatements;

    private transient List<Integer> systemIdsToCrawl;
    private transient Integer crawlingCounter;

    // Constructors

    public CrawlerReporting() {
    }

    public CrawlerReporting(Integer id) {
        this.id = id;
    }

    public CrawlerReporting(Date executionStartingDate) {
        this.executionStartingDate = executionStartingDate;
        this.numberOfCheckedIntegrationStatements = 0;
        this.numberOfSuccessfullyChecked = 0;
        this.numberOfChangedIntegrationStatements = 0;
        this.unreachableIntegrationStatements = new ArrayList<System>();
        this.unmatchingHashcodeForIntegrationStatements = new ArrayList<System>();
        this.changedIntegrationStatements = new ArrayList<System>();
    }

    // Getters and Setters

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getExecutionStartingDate() {
        return executionStartingDate;
    }

    public void setExecutionStartingDate(Date executionStartingDate) {
        this.executionStartingDate = executionStartingDate;
    }

    public Integer getExecutionElapsedTime() {
        return executionElapsedTime;
    }

    public void setExecutionElapsedTime(Integer executionElapsedTime) {
        this.executionElapsedTime = executionElapsedTime;
    }

    public Integer getNumberOfCheckedIntegrationStatements() {
        return numberOfCheckedIntegrationStatements;
    }

    public void setNumberOfCheckedIntegrationStatements(Integer numberOfCheckedIntegrationStatements) {
        this.numberOfCheckedIntegrationStatements = numberOfCheckedIntegrationStatements;
    }

    public Integer getNumberOfSuccessfullyChecked() {
        return numberOfSuccessfullyChecked;
    }

    public void setNumberOfSuccessfullyChecked(Integer numberOfSuccessfullyChecked) {
        this.numberOfSuccessfullyChecked = numberOfSuccessfullyChecked;
    }

    public List<System> getUnreachableIntegrationStatements() {
        return unreachableIntegrationStatements;
    }

    public void setUnreachableIntegrationStatements(List<System> unreachableIntegrationStatements) {
        this.unreachableIntegrationStatements = unreachableIntegrationStatements;
    }

    public List<System> getUnmatchingHashcodeForIntegrationStatements() {
        return unmatchingHashcodeForIntegrationStatements;
    }

    public void setUnmatchingHashcodeForIntegrationStatements(List<System> unmatchingHashcodeForIntegrationStatements) {
        this.unmatchingHashcodeForIntegrationStatements = unmatchingHashcodeForIntegrationStatements;
    }

    public Integer getNumberOfUnmatchingHashcodeForIntegrationStatements() {
        return numberOfUnmatchingHashcodeForIntegrationStatements;
    }

    public void setNumberOfUnmatchingHashcodeForIntegrationStatements(
            Integer numberOfUnmatchingHashcodeForIntegrationStatements) {
        this.numberOfUnmatchingHashcodeForIntegrationStatements = numberOfUnmatchingHashcodeForIntegrationStatements;
    }

    public Integer getNumberOfUnreachableIntegrationStatements() {
        return numberOfUnreachableIntegrationStatements;
    }

    public void setNumberOfUnreachableIntegrationStatements(Integer numberOfUnreachableIntegrationStatements) {
        this.numberOfUnreachableIntegrationStatements = numberOfUnreachableIntegrationStatements;
    }

    public List<Integer> getSystemIdsToCrawl() {
        return systemIdsToCrawl;
    }

    public void setSystemIdsToCrawl(List<Integer> systemIdsToCrawl) {
        this.systemIdsToCrawl = systemIdsToCrawl;
    }

    public Integer getCrawlingCounter() {
        return crawlingCounter;
    }

    public void setCrawlingCounter(Integer crawlingCounter) {
        this.crawlingCounter = crawlingCounter;
    }

    public Integer getNumberOfChangedIntegrationStatements() {
        return numberOfChangedIntegrationStatements;
    }

    public void setNumberOfChangedIntegrationStatements(Integer numberOfChangedIntegrationStatements) {
        this.numberOfChangedIntegrationStatements = numberOfChangedIntegrationStatements;
    }

    public List<System> getChangedIntegrationStatements() {
        return changedIntegrationStatements;
    }

    public void setChangedIntegrationStatements(List<System> changedIntegrationStatements) {
        this.changedIntegrationStatements = changedIntegrationStatements;
    }

    @PrePersist
    @PreUpdate
    public void setNumberOfSystems() {
        setNumberOfUnmatchingHashcodeForIntegrationStatements(unmatchingHashcodeForIntegrationStatements.size());
        setNumberOfUnreachableIntegrationStatements(unreachableIntegrationStatements.size());
        setNumberOfChangedIntegrationStatements(changedIntegrationStatements.size());
    }

    public String getExecutionElapsedTimeToString() {
        Date date = new Date(executionElapsedTime);
        SimpleDateFormat sdf = new SimpleDateFormat("HH'h' mm'm' ss.SSS's'", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        return sdf.format(date);
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

    public String getInformation() {
        return "Crawling is running, " + getCrawlingCounter() + " threads running. " + getSystemIdsToCrawl().size()
                + " jobs in queue.";
    }

}
