package net.ihe.gazelle.pr.search.model;

import net.ihe.gazelle.hql.HibernateHelper;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@MappedSuperclass
public class PREvent {

    /**
     * TimeStamp : last Changed
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date")
    private Date date;

    /**
     * Last modifier (string and not an Id cause a user may be deleted, in that case, we keep the log)
     */
    @Column(name = "performer_username")
    private String performerUsername;

    /**
     * IP address of the remote user
     */
    @Column(name = "performer_ip_address")
    private String performerIpAddress;

    /**
     * IP address of the remote user
     */
    @Column(name = "performer_hostname")
    private String performerHostname;

    /**
     * Web browser information of the remote user
     */
    @Column(name = "browser_information")
    private String browserInformation;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPerformerUsername() {
        return performerUsername;
    }

    public void setPerformerUsername(String performerUsername) {
        this.performerUsername = performerUsername;
    }

    public String getPerformerIpAddress() {
        return performerIpAddress;
    }

    public void setPerformerIpAddress(String performerIpAddress) {
        this.performerIpAddress = performerIpAddress;
    }

    public String getPerformerHostname() {
        return performerHostname;
    }

    public void setPerformerHostname(String performerHostname) {
        this.performerHostname = performerHostname;
    }

    public String getBrowserInformation() {
        return browserInformation;
    }

    public void setBrowserInformation(String browserInformation) {
        this.browserInformation = browserInformation;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
