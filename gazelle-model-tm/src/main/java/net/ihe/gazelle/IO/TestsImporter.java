package net.ihe.gazelle.IO;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class TestsImporter {
    private static Logger log = LoggerFactory.getLogger(TestsImporter.class);
    private JAXBContext jaxbContext;

    public int[] importTests(File file) throws JAXBException {
        int[] testsIds = null;
        this.jaxbContext = JAXBContext.newInstance(TestXmlPackager.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        try {
            TestXmlPackager testPackage = (TestXmlPackager) jaxbUnmarshaller.unmarshal(file);
            testsIds = testPackage.saveOrMerge(EntityManagerService.provideEntityManager());
        } catch (JAXBException e) {
            log.error("Failed to import tests");
            log.error("JAXBContext cannot unmarshal " + file.getAbsolutePath());
            log.error("JAXBContext cannot unmarshal " + e.getCause());
        }
        return testsIds;
    }
}