package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecution;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepExecutionStatus;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepReportFile;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Audited
@Table(name = "tm_automated_test_step_execution", schema = "public")
@SequenceGenerator(name = "tm_automated_test_step_execution_sequence", sequenceName = "tm_automated_test_step_execution_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class AutomatedTestStepExecutionEntity implements Serializable {

    private static final long serialVersionUID = 6035491206714355522L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_automated_test_step_execution_sequence")
    private int id;

    @Column(name = "test_steps_instance_id", unique = true, nullable = false)
    private Integer testStepsInstanceId;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private AutomatedTestStepExecutionStatus status;

    @Column(name = "date")
    private Date date;

    @Column(name = "report")
    @Lob
    @Type(type = "text")
    private String report;

    @OneToMany(mappedBy = "execution", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<AutomatedTestStepReportFileEntity> files;

    public AutomatedTestStepExecutionEntity() {
        // empty for serialization
    }

    public AutomatedTestStepExecutionEntity(Integer testStepsInstanceId, String userId, AutomatedTestStepExecutionStatus status, Date date, String report) {
        this.testStepsInstanceId = testStepsInstanceId;
        this.userId = userId;
        this.status = status;
        this.date = date;
        this.report = report;
    }

    public AutomatedTestStepExecutionEntity(AutomatedTestStepExecution execution) {
        this.testStepsInstanceId = execution.getTestStepsInstanceId();
        this.userId = execution.getUserId();
        this.status = execution.getStatus();
        this.date = execution.getDate();
        this.report = execution.getJsonReport();
        for (AutomatedTestStepReportFile reportFile : execution.getPdfFiles()) {
            this.files.add(new AutomatedTestStepReportFileEntity(reportFile.getReportName(), reportFile.getReport()));
        }
    }

    public AutomatedTestStepExecution asDomain() {
        AutomatedTestStepExecution domain = new AutomatedTestStepExecution();
        domain.setTestStepsInstanceId(this.testStepsInstanceId);
        domain.setUserId(this.userId);
        domain.setStatus(this.status);
        domain.setDate(this.date);
        domain.setJsonReport(this.report);
        if (files != null) {
            for (AutomatedTestStepReportFileEntity file : this.files) {
                domain.addPdfFile(file.asDomain());
            }
        }
        return domain;
    }

    public int getId() {
        return id;
    }

    public Integer getTestStepsInstanceId() {
        return testStepsInstanceId;
    }

    public void setTestStepsInstanceId(Integer testStepsInstanceId) {
        this.testStepsInstanceId = testStepsInstanceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public AutomatedTestStepExecutionStatus getStatus() {
        return status;
    }

    public void setStatus(AutomatedTestStepExecutionStatus status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String value) {
        this.report = value;
    }

    public List<AutomatedTestStepReportFileEntity> getFiles() {
        return HibernateHelper.getLazyValue(this, "files", this.files);
    }

    public void setFiles(List<AutomatedTestStepReportFileEntity> files) {
        this.files = files;
    }
}
