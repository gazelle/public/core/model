/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Entity
@Table(name = "tm_proxy_configuration_for_session", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "configurationType_id", "testing_session_id"}))
@SequenceGenerator(name = "tm_proxy_configuration_for_session_sequence", sequenceName = "tm_proxy_configuration_for_session_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class ProxyConfigurationForSession extends AuditedObject implements Serializable {


    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(ProxyConfigurationForSession.class);

    /**
     * Id attribute!
     */
    @Id
    @Column(name = "id", unique = true, nullable = true)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_proxy_configuration_for_session_sequence")
    private Integer id;

    /**
     * Name used for this type of configuration
     */

    @Column(name = "ip_for_class")
    @Pattern(regexp = NetworkConfigurationForTestingSession._stringIPRegex_, message = "{gazelle.validator.shouldBeAValidIPAddress}")
    @NotNull
    private String ipForClass;

    @Column(name = "port_range_low")
    @Min(value = 1024)
    @Max(value = 65634)
    private Integer portRangeLow;

    @Column(name = "port_range_high")
    @Min(value = 1024)
    @Max(value = 65634)
    private Integer portRangeHigh;

    @ManyToOne
    @JoinColumn(name = "testing_session_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private TestingSession testingSession;

    @ManyToOne
    @JoinColumn(name = "configurationType_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    @Deprecated
    private ConfigurationType configurationType;

    public ProxyConfigurationForSession(String ipForClass, Integer portRangeLow, Integer portRangeHigh,
                                        TestingSession testingSession) {
        super();
        this.ipForClass = ipForClass;
        this.portRangeLow = portRangeLow;
        this.portRangeHigh = portRangeHigh;
        this.testingSession = testingSession;
    }

    public ProxyConfigurationForSession() {
    }

    public static ProxyConfigurationForSession getProxyConfigurationForSession(TestingSession inTestingSession) {
        ProxyConfigurationForSessionQuery query = new ProxyConfigurationForSessionQuery();
        query.testingSession().eq(inTestingSession);
        return query.getUniqueResult();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIpForClass() {
        return ipForClass;
    }

    public void setIpForClass(String ipForClass) {
        this.ipForClass = ipForClass;
    }

    public Integer getPortRangeLow() {
        return portRangeLow;
    }

    public void setPortRangeLow(Integer portRangeLow) {
        this.portRangeLow = portRangeLow;
    }

    public Integer getPortRangeHigh() {
        return portRangeHigh;
    }

    public void setPortRangeHigh(Integer portRangeHigh) {
        this.portRangeHigh = portRangeHigh;
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    public boolean equalsWithoutConfType(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ProxyConfigurationForSession other = (ProxyConfigurationForSession) obj;

        if (!ipForClass.equals(other.ipForClass)) {
            return false;
        }
        if (portRangeHigh == null) {
            if (other.portRangeHigh != null) {
                return false;
            }
        } else if (!portRangeHigh.equals(other.portRangeHigh)) {
            return false;
        }
        if (portRangeLow == null) {
            if (other.portRangeLow != null) {
                return false;
            }
        } else if (!portRangeLow.equals(other.portRangeLow)) {
            return false;
        }
        if (testingSession == null) {
            if (other.testingSession != null) {
                return false;
            }
        } else if (!testingSession.equals(other.testingSession)) {
            return false;
        }
        return true;
    }

}
