package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.jboss.seam.Component;

import java.io.Serializable;
import java.util.Date;

public class SystemNotes implements Serializable, Comparable<SystemNotes> {


    private static final long serialVersionUID = 9095642716264107224L;

    private Date dateOfEvent;

    private String note;

    private String username;

    public SystemNotes(Date dateOfEvent, String note) {
        super();
        this.dateOfEvent = dateOfEvent;
        this.note = note;
        GazelleIdentity identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
        this.username = identity.getUsername();
    }

    public SystemNotes() {
        super();
    }

    public Date getDateOfEvent() {
        return dateOfEvent;
    }

    public void setDateOfEvent(Date dateOfEvent) {
        this.dateOfEvent = dateOfEvent;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String toStringForComments() {
        String result = "";
        result = "Note added : ";
        result = result + note;
        return result;
    }

    @Override
    public int compareTo(SystemNotes o) {
        return -getDateOfEvent().compareTo(o.getDateOfEvent());
    }

}
