package net.ihe.gazelle.tm.tee.model;

import java.util.ArrayList;
import java.util.List;

public enum TestInstanceExecutionStatusEnum {

    INACTIVE
            (1, false, "inactive", "Not active yet.",
                    "net.ihe.gazelle.tm.Inactive"),
    ACTIVE(
            2,
            true,
            "active",
            "One or more test step instances are in active status (waiting, initiated, responded).",
            "net.ihe.gazelle.tm.Active"),
    INTERRUPTED(
            3,
            false,
            "interrupted",
            "The test instance was stopped by the system after it had become active at one point.",
            "net.ihe.gazelle.tm.Interrupted"),
    SKIPPED(4, true,
            "skipped",
            "The test instance has been skipped by the participant.",
            "net.ihe.gazelle.tm.Skipped"),
    PAUSED(
            5,
            true,
            "paused",
            "The test instance has been paused by the participant after it had become active.",
            "net.ihe.gazelle.tm.Paused"),
    ABORTED(
            6,
            true,
            "aborted",
            "The test instance has been aborted by the participant after it had become active.",
            "net.ihe.gazelle.tm.Aborted"),
    COMPLETED(7, true,
            "completed", "The test instance is complete.",
            "net.ihe.gazelle.tm.Completed");

    private Integer id;
    private boolean displayable;
    private String keyword;
    private String description;
    private String displayLabelKey;

    TestInstanceExecutionStatusEnum(Integer pId, boolean pVisible, String pKey,
                                    String pDescription, String pDisplayLabelKey) {
        this.id = pId;
        this.setDisplayable(pVisible);
        this.keyword = pKey;
        this.description = pDescription;
        this.displayLabelKey = pDisplayLabelKey;
    }

    public static TestInstanceExecutionStatusEnum getExecutionStatusByKey(
            String key) {

        for (TestInstanceExecutionStatusEnum status : values()) {
            if (status.keyword.equalsIgnoreCase(key)) {
                return status;
            }
        }
        return null;
    }

    public static TestInstanceExecutionStatusEnum getExecutionStatusFromLabelKey(
            String pLabelKey) {
        for (TestInstanceExecutionStatusEnum status : values()) {
            if (status.displayLabelKey.equalsIgnoreCase(pLabelKey)) {
                return status;
            }
        }
        return null;
    }

    public static List<TestInstanceExecutionStatusEnum> getDisplayableStatuses() {
        List<TestInstanceExecutionStatusEnum> statuses = new ArrayList<TestInstanceExecutionStatusEnum>();

        for (TestInstanceExecutionStatusEnum status : values()) {
            if (status.isDisplayable()) {
                statuses.add(status);
            }
        }
        return statuses;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isDisplayable() {
        return displayable;
    }

    public void setDisplayable(boolean visible) {
        this.displayable = visible;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String key) {
        this.keyword = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayLabelKey() {
        return displayLabelKey;
    }

    public void setDisplayLabelKey(String displayLabelKey) {
        this.displayLabelKey = displayLabelKey;
    }

    @Override
    public String toString() {
        return "TestInstanceExecutionStatusEnum [id=" + id + ", keyword=" + keyword + ", description=" + description
                + ", displayLabelKey=" + displayLabelKey + "]";
    }
}
