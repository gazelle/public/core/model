/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.translate.TranslateService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

/**
 * <b>Class Description : </b>TestInstanceEvents<br>
 * <br>
 * This class describes a test instance event, used by the Gazelle application. This is a cross table storing a status, an instance of test and a date. TestInstanceEvent owns the following attributes
 * :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestInstanceEvent</li>
 * <li><b>testInstance</b> : the instance of the test referenced</li>
 * <li><b>statusId</b> : status of the current test</li>
 * <li><b>date</b> : the time at which the event happened</li>
 * </ul>
 * </br>
 *
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         jmeyer@irisa.fr
 *         </pre>
 * @version 1.0 , feb. 19th, 2009
 * @class TestIntanceEvent.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@Entity
@Table(name = "tm_test_instance_event", schema = "public")
@SequenceGenerator(name = "tm_test_instance_event_sequence", sequenceName = "tm_test_instance_event_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class TestInstanceEvent extends AuditedObject implements Serializable, Comparator<TestInstanceEvent>,
        Comparable<TestInstanceEvent> {


    public static final int STATUS_CHANGED = 0;
    public static final int COMMENT = 1;
    public static final int MONITOR_UNCLAIMED = 2;
    public static final int MONITOR_CLAIMED = 3;
    public static final int TEST_STEP_DATA_ADDED = 4;
    public static final int TEST_STEP_DATA_REMOVED = 5;
    public static final int DESCRIPTION = 6;
    public static final int FILE = 7;
    private static final long serialVersionUID = 2179001887959238503L;
    private static Logger log = LoggerFactory.getLogger(TestInstanceEvent.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_instance_event_sequence")
    private Integer id;

    @Column(name = "status_id")
    private Status status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_of_event")
    private Date dateOfEvent;

    private Integer type;

    @Lob
    @Type(type = "text")
    private String comment;

    @Lob
    @Type(type = "text")
    private String description;

    @Lob
    @Type(type = "text")
    private String filename;

    @Size(max = 250)
    @Column(name = "user_id")
    private String userId;

    public TestInstanceEvent() {
    }

    public TestInstanceEvent(TestInstance testInstance, Status newStatus) {
        GazelleIdentity identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
        dateOfEvent = new Date();
        status = newStatus;
        userId = testInstance.getLastModifierId();
        if (!"".equals(identity.getUsername())) {
            userId = identity.getUsername();
        }
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static void deleteTestInstanceEventWithFind(TestInstanceEvent testInstanceEvent) throws Exception {
        EntityManager em = EntityManagerService.provideEntityManager();
        try {
            TestInstanceEvent selectedTestInstanceEvent = em.find(TestInstanceEvent.class, testInstanceEvent.getId());
            em.remove(selectedTestInstanceEvent);
        } catch (Exception e) {
            log.error("TestInstanceEvent", e);
            throw new Exception("A test cannot be deleted -  id = " + testInstanceEvent.getId(), e);
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateOfEvent() {
        return dateOfEvent;
    }

    public void setDateOfEvent(Date dateOfEvent) {
        this.dateOfEvent = dateOfEvent;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public int compare(TestInstanceEvent o1, TestInstanceEvent o2) {
        return -o1.getDateOfEvent().compareTo(o2.getDateOfEvent());
    }

    @Override
    public int compareTo(TestInstanceEvent o) {
        return -getDateOfEvent().compareTo(o.getDateOfEvent());
    }

    public String toStringForComments() {
        String result = "";
        if (type != null) {
            result = TranslateService.getTranslation("net.ihe.gazelle.tm.event." + type.toString()) + " ";
            switch (type) {
                case STATUS_CHANGED:
                    result = result + TranslateService.getTranslation(status.getLabelToDisplay());
                    break;
                case COMMENT:
                    result = result + comment;
                    break;
                case DESCRIPTION:
                    result = result + description;
                    break;
                case FILE:
                    result = result + filename;
                    break;
                case TEST_STEP_DATA_ADDED:
                    result = result + comment;
                    break;
                case TEST_STEP_DATA_REMOVED:
                    result = result + comment;
                    break;
                default:
                    break;
            }
        }
        return result;
    }

}
