package net.ihe.gazelle.tm.configurations.model.interfaces;

import net.ihe.gazelle.tf.model.WSTransactionUsage;

public interface HTTPConfiguration extends ServerConfiguration {

    String getUrl();

    void setUrl(String url);

    WSTransactionUsage getWsTransactionUsage();

    void setWsTransactionUsage(WSTransactionUsage url);

    String getAssigningAuthority();

    void setAssigningAuthority(String assigningAuthority);

}
