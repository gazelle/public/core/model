/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model.HL7;

/**
 * @class HL7V3Configuration.java
 * @package net.ihe.gazelle.tm.systems.model
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, sep
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */

import net.ihe.gazelle.tf.model.WSTransactionUsage;
import net.ihe.gazelle.tm.configurations.model.Configuration;
import net.ihe.gazelle.tm.configurations.model.interfaces.HTTPConfiguration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tm_hl7_v3_responder_configuration", schema = "public")
@SequenceGenerator(name = "tm_hl7_v3_responder_configuration_sequence", sequenceName = "tm_hl7_v3_responder_configuration_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class HL7V3ResponderConfiguration extends AbstractHL7Configuration implements Serializable, HTTPConfiguration {

    private static final long serialVersionUID = -5114832757724859531L;

    @Column(name = "url", nullable = false)
    private String url;

    @Range(min = 0, max = 65635)
    @Column(name = "port")
    private Integer port;

    @Range(min = 0, max = 65635)
    @Column(name = "port_secured")
    private Integer portSecured;

    @Range(min = 1024, max = 65635)
    @Column(name = "port_proxy")
    private Integer portProxy;

    @Deprecated
    @Column(name = "usage")
    private String usage;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ws_transaction_usage", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    private WSTransactionUsage wsTransactionUsage;

    public HL7V3ResponderConfiguration() {
        super();
    }

    public HL7V3ResponderConfiguration(Configuration inConfiguration, String inSendingReceivingApplication,
                                       String inSendingReceivingFacility, String inAssigningAuthority, String inComments) {
        super(inConfiguration, inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority,
                inComments);
    }

    public HL7V3ResponderConfiguration(Configuration inConfiguration, String inComments) {
        super(inConfiguration, inComments);
    }

    public HL7V3ResponderConfiguration(Configuration inConfiguration) {
        super(inConfiguration);
    }

    public HL7V3ResponderConfiguration(HL7V3ResponderConfiguration hh) {
        super(hh);
        if (hh != null) {
            this.port = hh.port;
            this.portSecured = hh.portSecured;
            this.wsTransactionUsage = hh.wsTransactionUsage;
            this.url = hh.url;
            getProxyPortIfNeeded(null);
        }
    }

    public HL7V3ResponderConfiguration(Configuration inConfiguration, String url, Integer port, Integer portSecured,
                                       Integer inPortProxy, String usage, String inSendingReceivingApplication, String inSendingReceivingFacility,
                                       String inAssigningAuthority, String inComments, WSTransactionUsage wstran) {
        super(inConfiguration, inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority,
                inComments);

        this.url = url;
        this.port = port;
        this.portSecured = portSecured;
        this.portProxy = inPortProxy;
        this.wsTransactionUsage = wstran;
        this.usage = usage;
    }

    @Override
    @PrePersist
    @PreUpdate
    public void init() {
        this.setSendingReceivingApplication("-");
        this.setSendingReceivingFacility("-");
    }

    @Override
    public WSTransactionUsage getWsTransactionUsage() {
        return wsTransactionUsage;
    }

    @Override
    public void setWsTransactionUsage(WSTransactionUsage wsTransactionUsage) {
        this.wsTransactionUsage = wsTransactionUsage;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public Integer getPortSecured() {
        return portSecured;
    }

    @Override
    public void setPortSecured(Integer portSecured) {
        this.portSecured = portSecured;
    }

    public Integer getPortIfConfNotSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == false)) {
            return port;
        }
        return null;
    }

    public Integer getPortSecuredIfConfSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == true)) {
            return portSecured;
        }
        return null;
    }

    @Override
    public Integer getPortProxy() {
        return portProxy;
    }

    @Override
    public void setPortProxy(Integer portProxy) {
        this.portProxy = portProxy;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getWSTranactionUsageAsText() {
        if (this.wsTransactionUsage != null) {
            return this.wsTransactionUsage.getTransaction().getKeyword() + ":"
                    + this.getWsTransactionUsage().getUsage();
        }
        return null;
    }

    @Override
    public List<String> getCSVHeaders() {
        List<String> headers = super.getCSVHeaders();
        addCSVHTTPHeaders(this, headers);
        return headers;
    }

    @Override
    public List<String> getCSVValues() {
        List<String> values = super.getCSVValues();
        addCSVHTTPValues(this, values);
        return values;
    }

    @Override
    public String toString() {
        return "HL7V3ResponderConfiguration [url=" + url + ", port=" + port + ", portSecured=" + portSecured
                + ", portProxy=" + portProxy + ", usage=" + usage + ", wsTransactionUsage=" + wsTransactionUsage + "]";
    }

}