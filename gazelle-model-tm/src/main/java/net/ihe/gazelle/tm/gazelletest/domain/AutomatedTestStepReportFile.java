package net.ihe.gazelle.tm.gazelletest.domain;

import java.util.Arrays;
import java.util.Objects;

public class AutomatedTestStepReportFile {

    private int id;
    private String reportName;
    private byte[] report;

    public AutomatedTestStepReportFile() {
        // empty for serialization
    }

    public AutomatedTestStepReportFile(String reportName, byte[] report) {
        this.reportName = reportName;
        this.report = report;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public byte[] getReport() {
        return report;
    }

    public void setReport(byte[] report) {
        this.report = report;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AutomatedTestStepReportFile)) return false;
        AutomatedTestStepReportFile that = (AutomatedTestStepReportFile) o;
        return id == that.id
                && Objects.equals(reportName, that.reportName)
                && Objects.deepEquals(report, that.report);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, reportName, Arrays.hashCode(report));
    }
}
