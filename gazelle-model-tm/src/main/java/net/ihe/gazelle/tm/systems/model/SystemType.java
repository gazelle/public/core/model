/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.FilterLabelProvider;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <b>Class Description : </b>SystemType<br>
 * <br>
 * This class describes the SystemType object, used by the Gazelle application. It corresponds to a type of system (eg. PACS).
 * <p/>
 * There is a difference between SystemType and SystemSubType : Here is an example to explain it : - SystemType is for instance : PACS - SystemSubType is for instance : PACS Cardiology or PACS
 * Radiology
 * <p/>
 * This class belongs to the Systems module.
 * <p/>
 * SystemType possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the SystemType</li>
 * <li><b>systemTypeKeyword</b> : Keyword of the System Type (example : MOD)</li>
 * <li><b>systemTypeDescription</b> : Description of the System Type (example : Modalities)</li>
 * </ul>
 * </br> <b>Example of SystemType</b> : A6<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, December 21
 * @class SystemType.java
 * @package net.ihe.gazelle.systems.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "sys_system_type", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "sys_system_type_sequence", sequenceName = "sys_system_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class SystemType extends AuditedObject implements java.io.Serializable, FilterLabelProvider {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -5991534457658398088L;
    private static Logger log = LoggerFactory.getLogger(SystemType.class);
    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sys_system_type_sequence")
    private Integer id;
    @Column(name = "keyword")
    @NotNull
    private String systemTypeKeyword;
    @Column(name = "description")
    private String systemTypeDescription;
    @Column(name = "is_visible")
    private Boolean visible;
    @Transient
    private Boolean isSelectedInTree;

    // Constructors
    public SystemType() {
    }

    public SystemType(Integer id, String systemTypeKeyword, String systemTypeDescription) {
        this.id = id;
        this.systemTypeKeyword = systemTypeKeyword;
        this.systemTypeDescription = systemTypeDescription;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<SystemType> getPossibleSystemTypes() {
        SystemTypeQuery q = new SystemTypeQuery();
        q.systemTypeKeyword().order(true);
        List<SystemType> possibleSystemTypes = q.getList();

        return possibleSystemTypes;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSystemTypeKeyword() {
        return systemTypeKeyword;
    }

    public void setSystemTypeKeyword(String systemTypeKeyword) {
        this.systemTypeKeyword = systemTypeKeyword;
    }

    public String getSystemTypeDescription() {
        return systemTypeDescription;
    }

    public void setSystemTypeDescription(String systemTypeDescription) {
        this.systemTypeDescription = systemTypeDescription;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean getIsSelectedInTree() {
        return isSelectedInTree;
    }

    // *********************************************************
    // Methods
    // *********************************************************

    // *********************************************************
    // Hashcode and equals
    // *********************************************************

    public void setIsSelectedInTree(Boolean isSelectedInTree) {
        this.isSelectedInTree = isSelectedInTree;
    }

    @Override
    public String toString() {

        if (this == null) {
            return null;
        }

        return "SystemType [id=" + id + ", systemTypeKeyword=" + systemTypeKeyword + "]";
    }

    @Override
    @FilterLabel
    public String getSelectableLabel() {
        return getSystemTypeKeyword() + " - " + getSystemTypeDescription();
    }

}
