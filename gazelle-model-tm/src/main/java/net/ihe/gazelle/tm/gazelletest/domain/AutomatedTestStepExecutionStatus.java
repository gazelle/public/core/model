package net.ihe.gazelle.tm.gazelletest.domain;

public enum AutomatedTestStepExecutionStatus {

    DONE_UNDEFINED("UNDEFINED"),
    TIMED_OUT("TIMED_OUT"),
    RUNNING("RUNNING"),
    DONE_FAILED("FAILED"),
    DONE_PASSED("PASSED");

    private final String label;

    AutomatedTestStepExecutionStatus(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static AutomatedTestStepExecutionStatus findByLabel(String label) {
        for (AutomatedTestStepExecutionStatus status : AutomatedTestStepExecutionStatus.values()) {
            if (status.getLabel().equals(label)) {
                return status;
            }
        }
        return DONE_UNDEFINED;
    }
}
