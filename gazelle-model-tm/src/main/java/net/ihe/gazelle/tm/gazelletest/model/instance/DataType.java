/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetGazelle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author aboufahj
 */
@Entity
@Table(name = "tm_data_type", schema = "public")
@SequenceGenerator(name = "tm_data_type_sequence", sequenceName = "tm_data_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class DataType extends AuditedObject implements Serializable {


    private static final long serialVersionUID = 6953227034420364183L;

    public static String OBJECT_DT = "sample";

    public static String PROXY_DT = "proxy";

    public static String DATAHOUSE_DT = "datahouse";

    public static String EVS_DT = "EVSClient";

    public static String GSS_DT = "gss";

    public static String XDS_DT = "xds";

    public static String FILE_DT = "file";

    public static String URL = "url";

    public static String COMMENT = "comment";

    public static String AUTOMATION_INPUT_DT = "automationInput";

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_data_type_sequence")
    private Integer id;

    @Column(name = "keyword", unique = true, nullable = false)
    private String keyword;

    @Column(name = "description")
    private String description;

    public DataType() {

    }

    public DataType(Integer id, String keyword, String description) {
        super();
        this.id = id;
        this.keyword = keyword;
        this.description = description;
    }

    public static List<DataType> getAllDataType() {
        DataTypeQuery q = new DataTypeQuery();
        q.setCachable(true);
        return q.getList();
    }

    public static DataType getDataTypeByKeyword(String keyword) {
        DataTypeQuery dataTypeQuery = new DataTypeQuery();
        dataTypeQuery.keyword().eq(keyword);
        DataType result = dataTypeQuery.getUniqueResult();
        if (result == null) {

            DataTypeQuery query = new DataTypeQuery();
            query.id().order(false);
            int id = query.getUniqueResult().getId() + 1;

            result = new DataType(id, keyword, keyword);
            EntityManagerService.provideEntityManager().persist(result);
        }
        return result;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFile() {
        if (this.keyword != null) {
            if (this.keyword.equals(FILE_DT)) {
                return true;
            }
        }
        return false;
    }

    public boolean isEvsCompliant() {
        return keyword.equals(PROXY_DT) || keyword.equals(FILE_DT) || keyword.equals(EVS_DT) || keyword.equals(GSS_DT);
    }

    public boolean isSample() {
        return keyword.equals(OBJECT_DT);
    }

    public boolean isTLS() {
        return keyword.equals(GSS_DT);
    }

}
