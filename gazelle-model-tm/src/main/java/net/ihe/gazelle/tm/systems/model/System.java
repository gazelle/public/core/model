/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.common.model.PathLinkingADocument;
import net.ihe.gazelle.dates.TimestampNano;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.messaging.MessagePropertyChanged;
import net.ihe.gazelle.messaging.MessagingService;
import net.ihe.gazelle.pr.search.model.ISDownload;
import net.ihe.gazelle.pr.search.model.SearchLogReport;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.sync.SetRegistration;
import net.ihe.gazelle.tf.model.ProfileLink;
import net.ihe.gazelle.tf.model.Transaction;
import net.ihe.gazelle.tf.model.TransactionOptionType;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.util.Pair;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.envers.AuditJoinTable;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <b>Class Description : </b>System<br>
 * <br>
 * This class describes a System with some additional attributes (for the needs of this application). It corresponds to a medical Information System, from a company (ex: a PACS or an ADT, or an
 * OF...). This class contains informations about the actors/integration profiles/options implemented. This class belongs to the ProductRegistry module.
 * <p/>
 * Integration Statement possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the System</li>
 * <li><b>isURL</b> : URL of the Integration Statement, corresponding to the System</li>
 * </ul>
 * </br> <b>Example of System</b> : PACS_IRISA is a system, ADT_AGFA1 is a system<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, March 28
 * @class System.java
 * @package net.ihe.gazelle.pr.systems.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "System")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "tm_system", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"name",
        "version"}))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_system_sequence", sequenceName = "tm_system_id_seq", allocationSize = 1)
// @Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("SYS")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class System extends AuditedObject implements java.io.Serializable {

    // Max size for wrapper displaying eventsHistory text
    public static final int MAX_LENGHT_WORDWRAP = 100;
    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450912131541283760L;
    private static final Logger LOG = LoggerFactory.getLogger(System.class);
    // Attributes (existing in database as a column)
    public static SimpleDateFormat JAVAFORMAT = new SimpleDateFormat("EEE MMM dd HH:mm:ss zz yyyy", Locale.getDefault());
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_system_sequence")
    private Integer id;
    @Column(name = "name")
    @Size(min = 2, max = 128)
    @NotNull
    @Pattern(regexp = "[^\\\\/]*", message = "{gazelle.validator.nameWithoutSlash}")
    private String name;
    @Column(name = "version")
    @NotNull
    @Pattern(regexp = "[^\\\\/]*", message = "{gazelle.validator.nameWithoutSlash}")
    private String version;
    // Boolean, indicates if the system supports HL7
    @Column(name = "is_hl7_system")
    private Boolean isHL7System;
    // Boolean, indicates if the system supports DICOM
    @Column(name = "is_dicom_system")
    private Boolean isDicomSystem;
    // This is the URL of the HL7 conformance Statement
    @Column(name = "hl7_conformance_statement_url", nullable = true, updatable = true)
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$|^ftp://.+$)", message = "{gazelle.validator.http}")
    @Size(max = 255)
    private String HL7ConformanceStatementUrl;
    // This is the list of all documents used as HL7 conformance statements
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @JoinTable(name = "sys_hl7_documents_for_systems", joinColumns = @JoinColumn(name = "system_id"), inverseJoinColumns = @JoinColumn(name = "path_linking_a_document_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "system_id", "path_linking_a_document_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<PathLinkingADocument> pathsToHL7Documents;
    // This is the URL of the DICOM conformance Statement
    @Column(name = "dicom_conformance_statement_url", nullable = true, updatable = true)
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$|^ftp://.+$)", message = "{gazelle.validator.http}")
    @Size(max = 255)
    private String dicomConformanceStatementUrl;
    // This is the list of all documents used as DICOM conformance statements
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @JoinTable(name = "sys_dicom_documents_for_systems", joinColumns = @JoinColumn(name = "system_id"), inverseJoinColumns = @JoinColumn(name = "path_linking_a_document_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "system_id", "path_linking_a_document_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<PathLinkingADocument> pathsToDicomDocuments;
    // This is the URL of the Integration Statement describing this system
    @Column(name = "integration_statement_url", nullable = true, updatable = true)
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$|^ftp://.+$)", message = "{gazelle.validator.http}")
    @Size(max = 255)
    private String integrationStatementUrl;
    // This is the date of the Integration Statement
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "integration_statement_date")
    private Date integrationStatementDate;
    // Owner (user) of this system
    @Column(name = "owner_user_id", nullable = false)
    private String ownerUserId;
    // Type of this system (eg. ADT or OF...)
    @ManyToOne
    @JoinColumn(name = "system_type_id")
    @Fetch(value = FetchMode.SELECT)
    private SystemType systemType;
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @AuditJoinTable
    @JoinTable(name = "tm_subtypes_per_system", joinColumns = @JoinColumn(name = "system_id"), inverseJoinColumns = @JoinColumn(name = "system_subtypes_per_system_type_id"))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<SystemSubtypesPerSystemType> systemSubtypesPerSystemType;
    // The suffix can be null ! when there is no suffix
    // The min length of the suffix is one character
    @Column(name = "keyword_suffix")
    // @Pattern(regexp = "[a-zA-Z_0-9-]*", message =
    // "Please enter letters or digits without space" )
    @Size(max = 60)
    private String keywordSuffix;
    @XmlElement(name = "keyword")
    @Column(name = "keyword", unique = true)
    private String keyword;
    @Column(name = "is_tool")
    private boolean isTool;
    @Formula("case version when '' then keyword else keyword || ' (' || version || ')' end")
    private String keywordVersion;
    @Column(name = "comment")
    private String comment;
    /**
     * This is a history string containing all important events for this system
     */
    @Column(name = "events_history", length = 92000)
    @Deprecated
    private String eventsHistory;

    // PR Stuff
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable(name = "tm_system_events")
    @Fetch(value = FetchMode.SELECT)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<SystemEvent> systemEvents;
    @Column(name = "integration_statement_status")
    private IntegrationStatementStatus prStatus;
    /**
     * This flag indicates if the user has asked for a publication of his Integration Statement
     */
    @Column(name = "publication_request_date")
    private Date prDateAskForPublicationDate;
    /**
     * This is the date when this Integration Statement has been validated for the last time by the Crawler
     */
    @Column(name = "integration_statement_validated_last_time")
    private Date prDateValidatedByCrawler;
    /**
     * This is the checksum (MD5) of the generated PDF Integration Statement (when PDF is generated by PR). This is a way to sign the generated IS
     */
    @Column(name = "integration_statement_generated_checksum")
    private String prChecksumGenerated;
    /**
     * This is the date of generation of the PDF Integration Statement (when PDF is generated by PR). This is a way to compute the storeds IS filename
     */
    @Column(name = "integration_statement_generated_date")
    private Date prDateGenerated;
    /**
     * This is the checksum (MD5) of PDF Integration Statement validated by admin (when PDF is NOT generated by PR). This is a way to sign a vendor IS
     */
    @Column(name = "integration_statement_validated_by_admin_checksum")
    private String prChecksumValidated;
    /**
     * This is the date when this Integration Statement has been validated by the admin
     */
    @Column(name = "integration_statement_validated_by_admin_date")
    private Date prDateValidatedByAdmin;
    /**
     * This is a comment entered by the Admin, to postpone a comment during the PDF Integration Statement validation (validation done by hand)
     */
    @Column(name = "integration_statement_validated_by_admin_comment", length = 4096)
    private String prCommentValidatedByAdmin;
    /**
     * This is a counter used to know how many jobs have been done with an event : Unreachable Url
     */
    @Column(name = "counter_crawler_jobs_with_unreachable_url")
    private Integer prCounterCrawlerUnreachableUrl;
    /**
     * This is a counter used to know how many jobs have been done with an event : Unmatching hashcode
     */
    @Column(name = "counter_crawler_jobs_with_unmatching_hashcode")
    private Integer prCounterCrawlerUnmatchingHashcode;
    /**
     * This is a flag indicating if an email has been sent to system owner because of event : unreachable Url
     */
    @Column(name = "is_email_sent_because_unreachable_url")
    private Boolean prMailSentBecauseUnreachableUrl;
    /**
     * This is a flag indicating if an email has been sent to system owner because of event : Unmatching hashcode
     */
    @Column(name = "is_email_sent_because_unmatching_hashcode")
    private Boolean prMailSentBecauseUnmatchingHashcode;
    @OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    private List<ISDownload> downloads;

    // END PR Stuff
    @ManyToMany(mappedBy = "foundSystemsForSearchLog", fetch = FetchType.LAZY)
    private List<SearchLogReport> searchLogReports;
    @OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<InstitutionSystem> institutionSystems;

    @OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<SystemInSession> systemsInSession;

    @OneToMany(mappedBy = "system", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<SystemActorProfiles> systemActorProfiles;

    private transient Institution institutionForCreation;

    private transient boolean eventImporting;

    private transient double ratio;

    // Constructor

    public System() {
    }

    public System(Integer id, String name) {
        this.id = id;
        this.setName(name);
    }

    public System(Integer id, String name, String keyword, String version, Boolean isHL7System, Boolean isDicomSystem) {
        this.id = id;
        this.setName(name);
        this.setKeyword(keyword);
        this.setVersion(version);
        this.setIsHL7System(isHL7System);
        this.setIsDicomSystem(isDicomSystem);
    }

    /**
     * Copy constructor. This constructor copy all fields except the field id. In the case we use this constructor to copy a system and to persist this new object, it is necessary it doesn't have the
     * same identifier.
     *
     * @param inSystem
     */
    public System(System inSystem) {

        super();
        if (inSystem.name != null) {
            this.name = String.valueOf(inSystem.name);
        }
        if (inSystem.version != null) {
            this.version = String.valueOf(inSystem.version);
        }
        if (inSystem.HL7ConformanceStatementUrl != null) {
            this.isHL7System = Boolean.valueOf(inSystem.HL7ConformanceStatementUrl);
        }
        if (inSystem.isDicomSystem != null) {
            this.isDicomSystem = Boolean.valueOf(inSystem.isDicomSystem);
        }
        if (inSystem.HL7ConformanceStatementUrl != null) {
            this.HL7ConformanceStatementUrl = String.valueOf(inSystem.HL7ConformanceStatementUrl);
        }
        if (inSystem.dicomConformanceStatementUrl != null) {
            this.dicomConformanceStatementUrl = String.valueOf(inSystem.dicomConformanceStatementUrl);
        }
        if (inSystem.integrationStatementUrl != null) {
            this.integrationStatementUrl = String.valueOf(inSystem.integrationStatementUrl);
        }
        if (inSystem.integrationStatementDate != null) {
            this.integrationStatementDate = new Date(inSystem.getIntegrationStatementDate().getTime());
        }
        if (inSystem.ownerUserId != null) {
            this.ownerUserId = inSystem.ownerUserId;
        }
        if (inSystem.systemType != null) {
            this.systemType = inSystem.systemType;
        }

        if (inSystem.getKeyword() != null) {
            this.setKeyword(String.valueOf(inSystem.getKeyword()));
        }
        if (inSystem.getKeywordSuffix() != null) {
            this.setKeywordSuffix(String.valueOf(inSystem.getKeywordSuffix()));
        }
        if (inSystem.getIsDicomSystem() != null) {
            this.comment = String.valueOf(inSystem.getComment());
        }
        if (inSystem.isTool) {
            this.isTool = true;
        }
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    /**
     * Delete the selected system, this operation delete in order : - InstitutionSystem objects - SystemActorsProfiles objects - NetworkConfigurations - Configurations - Certificates - System objects
     * This method is used by several beans, and is not directly called by presentation layer Not defined in the security.drl)
     *
     * @param currentSystem : System to be deleted
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static void deleteSelectedSystem(System currentSystem) {

        // We retrieve the System object to delete from the entityManager
        try {

            if (isSystemOwnedBySingleCompany(currentSystem)) {

                EntityManager entityManager = EntityManagerService.provideEntityManager();
                List<SystemActorProfiles> listSystemActorProfiles;
                List<InstitutionSystem> listInstitutionSystems;

                System systemToBeDeleted = entityManager.merge(currentSystem);

                // We retrieve the InstitutionSystem objects to delete from the
                // entityManager before deleting System object
                listInstitutionSystems = getInstitutionSystems(systemToBeDeleted);

                // We delete the InstitutionSystem associated to this system
                for (int i = 0; i < listInstitutionSystems.size(); i++) {
                    entityManager.remove(listInstitutionSystems.get(i));
                }
                entityManager.flush();

                // We retrieve the SystemActorProfiles objects to delete from
                // the
                // entityManager before deleting System object
                listSystemActorProfiles = getSystemActorProfiles(systemToBeDeleted);

                // We delete the SystemActorProfiles associated to this system
                for (int i = 0; i < listSystemActorProfiles.size(); i++) {
                    entityManager.remove(listSystemActorProfiles.get(i));
                }
                entityManager.flush();

                List<SystemInSession> lsIs = SystemInSession.findSystemInSessionsWithSystem(systemToBeDeleted);

                for (int i = 0; i < lsIs.size(); i++) {
                    SystemInSession.deleteSelectedSystemInSessionWithDependencies(lsIs.get(i));
                }

                // We delete the system
                entityManager.remove(systemToBeDeleted);
                entityManager.flush();
            } else {
                LOG.error("", new Exception(
                        "This system is owned by more than one company, contact an administrator if you need to delete it"));
                throw new Exception(
                        "This system is owned by more than one company, contact an administrator if you need to delete it");
            }
        } catch (Exception e) {
            LOG.error("Cannot delete system... Gazelle internal error", e);
        }
    }

    private static List<SystemActorProfiles> getSystemActorProfiles(System systemToBeDeleted) {
        List<SystemActorProfiles> listSystemActorProfiles;
        SystemActorProfilesQuery q = new SystemActorProfilesQuery();
        q.system().id().eq(systemToBeDeleted.getId());
        listSystemActorProfiles = q.getList();
        return listSystemActorProfiles;
    }

    private static List<InstitutionSystem> getInstitutionSystems(System systemToBeDeleted) {
        List<InstitutionSystem> listInstitutionSystems;
        InstitutionSystemQuery q = new InstitutionSystemQuery();
        q.system().id().eq(systemToBeDeleted.getId());
        listInstitutionSystems = q.getList();
        return listInstitutionSystems;
    }

    /**
     * Retrieve a list of System for an Institution using the InstitutionSystem Class
     *
     * @param inst
     * @return List of system
     */
    public static List<System> getSystemsForAnInstitution(Institution inst) {

        if (inst != null) {
            try {
                SystemQuery q = new SystemQuery();
                q.institutionSystems().institution().eq(inst);
                q.keyword().order(true);
                return q.getList();
            } catch (Exception e) {
                LOG.error("getSystemsForAnInstitution", e);
                return null;
            }
        } else {

            LOG.error("getSystemsForAnInstitution - institution given as parameter is null !!!");

            return null;
        }
    }

    /**
     * Retrieve a number of systems for an Institution using the InstitutionSystem Class
     *
     * @param inst
     * @return Integer
     */
    public static Integer getNumberOfSystemsForAnInstitution(Institution inst) {

        try {
            InstitutionSystemQuery q = new InstitutionSystemQuery();
            q.institution().id().eq(inst.getId());
            return q.system().getCountOnPath();
        } catch (Exception e) {
            LOG.error("getNumberOfSystemsForAnInstitution", e);
            return null;
        }
    }

    /**
     * Retrieve a list of Institution for a system using the InstitutionSystem Class
     *
     * @param inSystem
     * @return List of Institution
     */
    public static List<Institution> getInstitutionsForASystem(System inSystem) {

        try {
            InstitutionSystemQuery q = new InstitutionSystemQuery();
            if (inSystem == null) {
                LOG.warn("getInstitutionsForASystem - system given as parameter is null");
                return null;
            }
            q.system().eq(inSystem);
            List<Institution> li = q.institution().getListDistinct();
            Collections.sort(li);
            return li;
        } catch (Exception e) {
            LOG.error("getInstitutionsForASystem", e);
            return null;
        }
    }

    public static List<String> getInstitutionsKeywordsForASystem(System inSystem) {

        try {

            if (inSystem == null) {
                LOG.warn("getInstitutionsForASystem - system given as parameter is null");
                return null;
            }

            InstitutionSystemQuery query = new InstitutionSystemQuery();
            query.system().id().eq(inSystem.getId());
            return query.institution().keyword().getListDistinct();
        } catch (Exception e) {
            LOG.error("getInstitutionsKeywordsForASystem", e);
            return null;
        }
    }

    public static List<System> getSystemsForCompanyForSession(Institution inCompany, TestingSession testingSession) {

        try {
            SystemInSessionQuery q = new SystemInSessionQuery();
            InstitutionSystemQuery q1 = new InstitutionSystemQuery();
            q1.institution().id().eq(inCompany.getId());
            q.testingSession().id().eq(testingSession.getId());
            q.system().id().in(q1.system().id().getListDistinct());

            return q.system().getListDistinct();
        } catch (Exception e) {
            LOG.error("getSystemsForCompanyForSession", e);
            return null;
        }
    }

    /**
     * In theory, a system may belong to several companies/institutions. But we need to know that fact for instance, before deleting a system
     *
     * @param inSystem System to check
     * @return True if this system belongs to only ONE company
     */
    public static boolean isSystemOwnedBySingleCompany(System inSystem) {

        try {
            InstitutionSystemQuery q = new InstitutionSystemQuery();
            q.system().id().eq(inSystem.getId());
            int size = q.institution().getCountOnPath();
            return (size <= 1);
        } catch (Exception e) {
            LOG.error("isSystemOwnedBySingleCompany", e);
            return false;
        }
    }

    public static System getSystemByAllKeyword(String keyword) {
        SystemQuery query = new SystemQuery();
        query.keyword().eq(keyword);
        return query.getUniqueResult();
    }

    /**
     * Retrieve the list of transactions to support for a system
     *
     * @param syst : System
     * @return List of Transaction objects
     */
    //TODO
    @SuppressWarnings("unchecked")
    public static List<ProfileLink> getProfileLinksToSupport(System syst) {

        try {
            EntityManager em = EntityManagerService.provideEntityManager();
            Query query = em.createQuery("SELECT distinct pl " + "FROM ProfileLink pl, SystemActorProfiles sap "
                    + "WHERE sap.system.id =:id and sap.actorIntegrationProfileOption.actorIntegrationProfile = pl.actorIntegrationProfile ");
            query.setParameter("id", syst.getId());

            HashSet<ProfileLink> myHashSetPL = new HashSet<ProfileLink>(query.getResultList());
            return new ArrayList<ProfileLink>(myHashSetPL);
        } catch (Exception e) {
            LOG.error("getProfileLinksToSupport", e);
            return null;
        }
    }

    //TODO
    @SuppressWarnings("unchecked")
    public static List<Pair<Transaction, TransactionOptionType>> getTransactionsToSupport(System syst) {

        try {
            EntityManager em = EntityManagerService.provideEntityManager();
            Query query = em.createQuery("SELECT distinct pl " + "FROM ProfileLink pl, SystemActorProfiles sap "
                    + "WHERE sap.system.id =:id and sap.actorIntegrationProfileOption.actorIntegrationProfile = pl.actorIntegrationProfile ");
            // "order by pl.transaction.keyword asc ");
            query.setParameter("id", syst.getId());
            List<ProfileLink> profileLinks = query.getResultList();

            HashMap<Transaction, HashSet<TransactionOptionType>> hashmap = new HashMap<Transaction, HashSet<TransactionOptionType>>();

            for (ProfileLink profileLink : profileLinks) {
                if (!hashmap.containsKey(profileLink)) {

                    hashmap.put(profileLink.getTransaction(), new HashSet<TransactionOptionType>());
                }

                HashSet<TransactionOptionType> set = hashmap.get(profileLink.getTransaction());
                set.add(profileLink.getTransactionOptionType());
            }

            List<Pair<Transaction, TransactionOptionType>> listPair = new ArrayList<Pair<Transaction, TransactionOptionType>>();

            for (Iterator<Transaction> iter = hashmap.keySet().iterator(); iter.hasNext(); )

            {
                Transaction t = iter.next();

                List<TransactionOptionType> list = new ArrayList<TransactionOptionType>(hashmap.get(t));

                for (TransactionOptionType optionType : list) {
                    listPair.add(new Pair<Transaction, TransactionOptionType>(t, optionType));
                }
            }

            return listPair;
        } catch (Exception e) {
            LOG.error("getTransactionsToSupport", e);
            return null;
        }
    }

    /**
     * Delete all systems for a given institution/company
     *
     * @param inst : Institution where systems will be deleted from database
     * @throws Exception
     */
    public static void deleteSystemsForCompany(Institution inst) throws Exception {
        if (inst == null) {
            return;
        }
        List<System> systemList = getSystemsForAnInstitution(inst);

        for (int i = 0; i < systemList.size(); i++) {
            System.deleteSelectedSystem(systemList.get(i));
        }
    }

    /**
     * Delete all systems for a given institution/company
     *
     * @throws Exception
     */
    //TODO
    @SuppressWarnings("unchecked")
    public static List<Institution> getPossibleInstitutionsInActivatedSessionForSystemCreationByAdmin() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        List<Institution> possibleInstitutionsForRegistration = entityManager
                .createQuery("from Institution row order by row.keyword asc").getResultList();

        return possibleInstitutionsForRegistration;
    }

    public static List<System> getSystemsForCompanyByCompanyNameForSession(Institution inCompany,
                                                                           TestingSession testingSession) {
        try {
            SystemInSessionQuery q = new SystemInSessionQuery();
            InstitutionSystemQuery q1 = new InstitutionSystemQuery();
            q1.institution().name().eq(inCompany.getName());
            q.testingSession().id().eq(testingSession.getId());
            q.system().id().in(q1.system().id().getListDistinct());

            return q.system().getListDistinct();
        } catch (Exception e) {
            LOG.error("getSystemsForCompanyByCompanyNameForSession", e);
            return null;
        }
    }

    @Deprecated
    public static System getSystemByKeyword(String keyword) {

        try {
            SystemQuery q = new SystemQuery();
            q.keywordSuffix().eq(keyword);
            return q.getUniqueResult();
        } catch (Exception e) {
            LOG.error("getSystemByKeyword", e);
            return null;
        }
    }

    public static System getSystemForGivenKeywordSuffix(String inSuffix) {
        if ((inSuffix == null) || (inSuffix.length() == 0)) {
            return null;
        }
        try {
            SystemQuery q = new SystemQuery();
            q.keywordSuffix().eq(inSuffix);
            return q.getUniqueResult();
        } catch (Exception e) {
            LOG.error("getSystemForGivenKeywordSuffix", e);
            return null;
        }
    }

    public static System getSystemById(Integer systemId){
        SystemQuery query = new SystemQuery();
        query.id().eq(systemId);
        return query.getUniqueResult();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getIsTool() {
        return isTool;
    }

    public void setIsTool(boolean isTool) {
        this.isTool = isTool;
    }

    public String getKeywordWithoutSuffix(GazelleIdentity identity) {

        if (this.getId() == null || this.getSystemType() == null) {
            return "";
        }

        StringBuffer institutionsKeyword = new StringBuffer();
        List<String> foundInstitutions = System.getInstitutionsKeywordsForASystem(this);
        if (foundInstitutions.size() == 0) {
            if (identity.hasRole(Role.ADMIN) || identity.hasRole(Role.PROJECT_MANAGER)) {
                institutionsKeyword.append("_" + getInstitutionForCreation().getKeyword());
            } else {
                institutionsKeyword.append("_" + Institution.getLoggedInInstitution().getKeyword());
            }
        } else {
            for (String keywd : foundInstitutions) {
                institutionsKeyword.append("_" + keywd);
            }
        }

        String computedKeyword = this.getSystemType().getSystemTypeKeyword() + institutionsKeyword.toString();

        return computedKeyword;
    }

    @FilterLabel
    public String getKeywordVersion() {
        return keywordVersion;
    }

    public String computeKeyword(GazelleIdentity identity) {
        if ((getKeywordSuffix() != null) && (getKeywordSuffix().length() != 0)) {
            return this.getKeywordWithoutSuffix(identity) + "_" + this.getKeywordSuffix();
        } else {
            return this.getKeywordWithoutSuffix(identity);
        }
    }

    public String getKeyword() {
        if (!PreferenceService.getBoolean("is_product_registry")) {
            if ((keyword == null) || (keyword.equals(""))) {
                EntityManager em = EntityManagerService.provideEntityManager();
                GazelleIdentity identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
                keyword = computeKeyword(identity);
                em.merge(this);
                em.flush();
            }
            return keyword;
        } else {
            return name;
        }
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getKeywordSuffix() {
        return keywordSuffix;
    }

    public void setKeywordSuffix(String keywordSuffix) {
        this.keywordSuffix = keywordSuffix;
    }

    public Institution getUniqueInstitution() {
        if ((getInstitutionSystems() != null) && (getInstitutionSystems().size() > 0)) {
            return getInstitutionSystems().iterator().next().getInstitution();
        } else {
            return null;
        }
    }

    public Set<InstitutionSystem> getInstitutionSystems() {
        return HibernateHelper.getLazyValue(this, "institutionSystems", this.institutionSystems);
    }

    public void setInstitutionSystems(Set<InstitutionSystem> institutionSystems) {
        this.institutionSystems = institutionSystems;
    }

    public Set<SystemInSession> getSystemsInSession() {
        return HibernateHelper.getLazyValue(this, "systemsInSession", this.systemsInSession);
    }

    public void setSystemsInSession(Set<SystemInSession> systemsInSession) {
        this.systemsInSession = systemsInSession;
    }

    public Set<SystemActorProfiles> getSystemActorProfiles() {
        return HibernateHelper.getLazyValue(this, "systemActorProfiles", this.systemActorProfiles);
    }

    public void setSystemActorProfiles(Set<SystemActorProfiles> systemActorProfiles) {
        this.systemActorProfiles = systemActorProfiles;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Institution getInstitutionForCreation() {
        if (institutionForCreation == null) {
            institutionForCreation = Institution.getLoggedInInstitution();
        }
        return institutionForCreation;
    }

    public void setInstitutionForCreation(Institution institutionForCreation) {
        this.institutionForCreation = institutionForCreation;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if (PreferenceService.getBoolean("is_product_registry")) {
            this.keyword = name;
        }
        MessagingService.publishMessage(new MessagePropertyChanged<System, String>(this, "name", this.name, name));
        this.name = name;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        MessagingService
                .publishMessage(new MessagePropertyChanged<System, String>(this, "version", this.version, version));
        this.version = version;
    }

    public Boolean getIsHL7System() {
        return this.isHL7System;
    }

    public void setIsHL7System(Boolean isHL7System) {
        this.isHL7System = isHL7System;
    }

    public Boolean getIsDicomSystem() {
        return this.isDicomSystem;
    }

    public void setIsDicomSystem(Boolean isDicomSystem) {
        this.isDicomSystem = isDicomSystem;
    }

    public String getHL7ConformanceStatementUrl() {
        return HL7ConformanceStatementUrl;
    }

    public void setHL7ConformanceStatementUrl(String conformanceStatementUrl) {
        MessagingService.publishMessage(new MessagePropertyChanged<System, String>(this, "HL7ConformanceStatementUrl",
                this.HL7ConformanceStatementUrl, conformanceStatementUrl));
        HL7ConformanceStatementUrl = conformanceStatementUrl;
    }

    public List<PathLinkingADocument> getPathsToHL7Documents() {
        return pathsToHL7Documents;
    }

    public void setPathsToHL7Documents(List<PathLinkingADocument> pathsToHL7Documents) {
        this.pathsToHL7Documents = pathsToHL7Documents;
    }

    public String getDicomConformanceStatementUrl() {
        return dicomConformanceStatementUrl;
    }

    public void setDicomConformanceStatementUrl(String dicomConformanceStatementUrl) {
        MessagingService.publishMessage(new MessagePropertyChanged<System, String>(this, "dicomConformanceStatementUrl",
                this.dicomConformanceStatementUrl, dicomConformanceStatementUrl));
        this.dicomConformanceStatementUrl = dicomConformanceStatementUrl;
    }

    public List<PathLinkingADocument> getPathsToDicomDocuments() {
        return pathsToDicomDocuments;
    }

    public void setPathsToDicomDocuments(List<PathLinkingADocument> pathsToDicomDocuments) {
        this.pathsToDicomDocuments = pathsToDicomDocuments;
    }

    public String getIntegrationStatementUrl() {
        return integrationStatementUrl;
    }

    public void setIntegrationStatementUrl(String integrationStatementUrl) {
        MessagingService.publishMessage(new MessagePropertyChanged<System, String>(this, "integrationStatementUrl",
                this.integrationStatementUrl, integrationStatementUrl));
        this.integrationStatementUrl = integrationStatementUrl;
    }

    public Date getIntegrationStatementDate() {
        return integrationStatementDate;
    }

    public void setIntegrationStatementDate(Date integrationStatementDate) {
        MessagingService.publishMessage(new MessagePropertyChanged<System, Date>(this, "integrationStatementDate",
                this.integrationStatementDate, integrationStatementDate));
        this.integrationStatementDate = integrationStatementDate;
    }

    public String getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(String ownerUser) {
        this.ownerUserId = ownerUser;
    }

    public SystemType getSystemType() {
        return systemType;
    }

    public void setSystemType(SystemType systemType) {
        this.systemType = systemType;
    }

    public List<SystemEvent> getSystemEvents() {
        if (systemEvents == null) {
            systemEvents = new ArrayList<SystemEvent>();
        }

        if (!eventImporting && (eventsHistory != null) && (eventsHistory.length() > 0)) {
            try {
                eventImporting = true;
                importEvents(eventsHistory);
                eventsHistory = "";
            } catch (Exception e) {
                LOG.error("Failed to import old events for system " + getId() + " - " + getName());
            }
            eventImporting = false;
        }

        Collections.sort(systemEvents);

        return systemEvents;
    }

    public void setSystemEvents(List<SystemEvent> systemEvents) {
        this.systemEvents = systemEvents;
    }

    public IntegrationStatementStatus getPrStatus() {
        return prStatus;
    }

    public void setPrStatus(IntegrationStatementStatus newStatus) {
        MessagingService.publishMessage(
                new MessagePropertyChanged<System, IntegrationStatementStatus>(this, "prStatus", this.prStatus,
                        newStatus));
        this.prStatus = newStatus;
    }

    public Date getPrDateValidatedByCrawler() {
        return prDateValidatedByCrawler;
    }

    public void setPrDateValidatedByCrawler(Date prDateValidatedByCrawler) {
        this.prDateValidatedByCrawler = prDateValidatedByCrawler;
    }

    public String getPrChecksumGenerated() {
        return prChecksumGenerated;
    }

    public void setPrChecksumGenerated(String prChecksumGenerated) {
        this.prChecksumGenerated = prChecksumGenerated;
    }

    public Date getPrDateGenerated() {
        return prDateGenerated;
    }

    public void setPrDateGenerated(Date prDateGenerated) {
        this.prDateGenerated = prDateGenerated;
    }

    public Date getPrDateAskForPublicationDate() {
        return prDateAskForPublicationDate;
    }

    public void setPrDateAskForPublicationDate(Date prDateAskForPublicationDate) {
        this.prDateAskForPublicationDate = prDateAskForPublicationDate;
    }

    public String getPrChecksumValidated() {
        return prChecksumValidated;
    }

    public void setPrChecksumValidated(String prChecksumValidated) {
        this.prChecksumValidated = prChecksumValidated;
    }

    public Date getPrDateValidatedByAdmin() {
        return prDateValidatedByAdmin;
    }

    public void setPrDateValidatedByAdmin(Date prDateValidatedByAdmin) {
        this.prDateValidatedByAdmin = prDateValidatedByAdmin;
    }

    public String getPrCommentValidatedByAdmin() {
        return prCommentValidatedByAdmin;
    }

    public void setPrCommentValidatedByAdmin(String prCommentValidatedByAdmin) {
        this.prCommentValidatedByAdmin = prCommentValidatedByAdmin;
    }

    public Integer getPrCounterCrawlerUnreachableUrl() {
        return prCounterCrawlerUnreachableUrl;
    }

    public void setPrCounterCrawlerUnreachableUrl(Integer prCounterCrawlerUnreachableUrl) {
        this.prCounterCrawlerUnreachableUrl = prCounterCrawlerUnreachableUrl;
    }

    public Integer getPrCounterCrawlerUnmatchingHashcode() {
        return prCounterCrawlerUnmatchingHashcode;
    }

    public void setPrCounterCrawlerUnmatchingHashcode(Integer prCounterCrawlerUnmatchingHashcode) {
        this.prCounterCrawlerUnmatchingHashcode = prCounterCrawlerUnmatchingHashcode;
    }

    public Boolean getPrMailSentBecauseUnreachableUrl() {
        return prMailSentBecauseUnreachableUrl;
    }

    public void setPrMailSentBecauseUnreachableUrl(Boolean prMailSentBecauseUnreachableUrl) {
        this.prMailSentBecauseUnreachableUrl = prMailSentBecauseUnreachableUrl;
    }

    public Boolean getPrMailSentBecauseUnmatchingHashcode() {
        return prMailSentBecauseUnmatchingHashcode;
    }

    public void setPrMailSentBecauseUnmatchingHashcode(Boolean prMailSentBecauseUnmatchingHashcode) {
        this.prMailSentBecauseUnmatchingHashcode = prMailSentBecauseUnmatchingHashcode;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public List<SystemSubtypesPerSystemType> getSystemSubtypesPerSystemType() {
        return systemSubtypesPerSystemType;
    }

    public void setSystemSubtypesPerSystemType(List<SystemSubtypesPerSystemType> systemSubtypesPerSystemType) {
        this.systemSubtypesPerSystemType = systemSubtypesPerSystemType;
    }

    public SystemEvent addEvent(SystemEventType type, String comment, String username) {
        return addEvent(type, new TimestampNano(), comment, username);
    }

    private SystemEvent addEvent(SystemEventType type, Date date, String comment, String username) {
        SystemEvent systemEvent = new SystemEvent();
        systemEvent.setEventDate(date);
        systemEvent.setUserName(username);
        systemEvent.setType(type);
        systemEvent.setComment(comment);
        getSystemEvents().add(systemEvent);
        return systemEvent;
    }

    private void importEvent(String dateStr, String username, String comment) {
        Date date;
        try {
            date = JAVAFORMAT.parse(dateStr);
        } catch (ParseException e) {
            date = new Date(0);
        }
        addEvent(SystemEventType.IMPORTED, date, comment, username);
    }

    private void importEvents(String oldEventsHistory) {
        /*
        // In old code, event history was built using these lines :
		// 10006
		String history = this.getEventsHistory() + (new Date()).toString() + " : " + loginUser.getUsername() +  " - " + text + "  <br/>" ;
		// 10081
		String history = this.getEventsHistory() + "<b><font color=\"#808080\"><i>" + (new Date()).toString() + "</i> : </font><font color=\"#408080\"> " +
			loginUser.getUsername() +  "</font></b><br/> " + " - " +  WordUtils.wrap ( text, MAX_LENGHT_WORDWRAP, "<br/>", true ) + " <hr>" ;
		// 10084
		String history = this.getEventsHistory() + "<b><font color=\"#808080\"><i>" + (new Date()).toString() + "</i> : </font><font color=\"#408080\"> " +
			loginUser.getUsername() +  "</font></b><br/> " + " - " +  WordUtils.wrap ( text, MAX_LENGHT_WORDWRAP, "<br/>", true ) + " <hr/>" ;
		// 10115
		String history = this.getEventsHistory() + "<b><font color=\"#808080\"><i>" + (new Date()).toString() + "</i> : </font><font color=\"#408080\"> " +
			loginUser.getUsername() +  "</font></b><br/> " + " - " +
			org.w3c.tidy.servlet.util.HTMLEncode.encode( WordUtils.wrap ( text, MAX_LENGHT_WORDWRAP, "\n", true )) + " <hr/>" ;
		*/

        String[] split1 = StringUtils.splitByWholeSeparator(oldEventsHistory, "<b><font color=\"#808080\"><i>");
        for (String string : split1) {
            if (string.isEmpty()) {

            } else {
                if (!string.contains("</i> : </font><font color=\"#408080\">")) {

                    String[] split2 = StringUtils.splitByWholeSeparator(string, "  <br/>");
                    for (String string2 : split2) {
                        if (string2.isEmpty()) {

                        } else {
                            int sep1 = string2.indexOf(" : ");
                            int sep2 = string2.indexOf(" - ");
                            String dateStr = string2.substring(0, sep1);
                            String username = string2.substring(sep1 + 3, sep2);
                            String comment = string2.substring(sep2 + 3);
                            importEvent(dateStr, username, comment);
                        }
                    }
                } else {
                    String[] split2 = StringUtils
                            .splitByWholeSeparator(string, "</i> : </font><font color=\"#408080\"> ");
                    String dateStr = split2[0];
                    String[] split3 = StringUtils.splitByWholeSeparator(split2[1], "</font></b><br/>  - ");
                    String username = split3[0];
                    String comment = split3[1];
                    comment = StringUtils.trim(comment.substring(0, comment.length() - 5));
                    comment = StringEscapeUtils.unescapeHtml(comment);
                    importEvent(dateStr, username, comment);
                }
            }
        }
    }

    public String getInstitutionsForASystemAsString(System inSystem) {
        StringBuilder sb = new StringBuilder();
        List<Institution> institutionsList = getInstitutionsForASystem(inSystem);
        int listSize = institutionsList.size();
        int i = 1;
        for (Institution inst : institutionsList) {
            sb.append(inst.getName());
            if (listSize > 1 && listSize != i) {
                sb.append("\n");
            }
            i++;
        }
        return sb.toString();
    }

    /**
     * Retrieve the list of transactions to support for a system
     *
     * @return List of Transaction objects
     */
    public List<ProfileLink> getProfileLinksToSupport() {
        return getProfileLinksToSupport(this);
    }

    public List<Pair<Transaction, TransactionOptionType>> getTransactionsToSupport() {
        return getTransactionsToSupport(this);
    }

    @Override
    public String toString() {
        return "System [keyword=" + keyword + ", keywordVersion=" + keywordVersion + "]";
    }

    public String getPrintableInstitutions() {
        List<String> institutionLabels = new ArrayList<String>();
        if (institutionSystems != null) {
            for (InstitutionSystem institutionSystem : institutionSystems) {
                institutionLabels.add(institutionSystem.getInstitution().getName());
            }
        }
        return StringUtils.join(institutionLabels, ", ");
    }

    public String getPrintableInstitutionsKeyword() {
        List<String> institutionLabels = new ArrayList<String>();
        if (institutionSystems != null) {
            for (InstitutionSystem institutionSystem : institutionSystems) {
                institutionLabels.add(institutionSystem.getInstitution().getKeyword());
            }
        }
        return StringUtils.join(institutionLabels, " - ");
    }

    public String getIntegrationStatementDownloadLabel(String defaultValue) {
        String result = defaultValue;
        try {
            if (StringUtils.trimToNull(getIntegrationStatementUrl()) != null) {
                String integrationStatementUrl = URLDecoder.decode(getIntegrationStatementUrl(), StandardCharsets.UTF_8.name());
                URL url = new URL(integrationStatementUrl);
                if ((url.getPath() != null) && url.getPath().toLowerCase().endsWith("pdf")) {
                    result = url.getPath().substring(url.getPath().lastIndexOf('/') + 1);
                    result = result.replace('_', ' ');
                    result = StringUtils.abbreviateMiddle(result, "...", 30);
                }
            }
        } catch (Throwable e) {
            result = defaultValue;
        }
        return result;
    }

    public List<ISDownload> getDownloads() {
        return HibernateHelper.getLazyValue(this, "downloads", this.downloads);
    }

    public List<SearchLogReport> getSearchLogReports() {
        return HibernateHelper.getLazyValue(this, "searchLogReports", this.searchLogReports);
    }

    public boolean isOwnedBy(Institution institution) {
        for (InstitutionSystem institutionSystem : getInstitutionSystems()) {
            if (institutionSystem.getInstitution().equals(institution)) {
                return true;
            }
        }
        return false;
    }
}
