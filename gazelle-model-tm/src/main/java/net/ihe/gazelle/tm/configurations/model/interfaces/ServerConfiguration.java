package net.ihe.gazelle.tm.configurations.model.interfaces;

public interface ServerConfiguration {

    Integer getPort();

    void setPort(Integer portProxy);

    Integer getPortSecured();

    void setPortSecured(Integer portProxy);

    Integer getPortProxy();

    void setPortProxy(Integer portProxy);

}
