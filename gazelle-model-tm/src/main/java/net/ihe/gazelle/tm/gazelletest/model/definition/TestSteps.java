/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.ExchangeUnique;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.sync.SetTestDefinition;
import net.ihe.gazelle.tf.model.Transaction;
import net.ihe.gazelle.tf.model.WSTransactionUsage;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepType;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import net.ihe.gazelle.tm.tee.model.MessageDirection;
import net.ihe.gazelle.tm.tee.model.MessageValidationService;
import net.ihe.gazelle.tm.tee.model.TmTestStepMessageProfile;
import org.apache.commons.lang.ObjectUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <b>Class Description : </b>TestSteps<br>
 * <br>
 * This class describes the TestSteps object, used by the Gazelle application. This class belongs to the Test Management module.
 * <p/>
 * Test possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestSteps</li>
 * <li><b>testRolesInitiator</b> : testRolesInitiator of the TestSteps</li>
 * <li><b>testRolesResponder</b> : testRolesResponder of the TestSteps</li>
 * <li><b>description</b> : description of the TestSteps</li>
 * <li><b>transaction</b> : transaction of the TestSteps</li>
 * <li><b>stepIndex</b> : stepIndex that describes the TestSteps</li>
 * <li><b>testStepsOption</b> : testStepsOption that describes the TestSteps</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *                 http://ihe.irisa.fr
 *                 </pre>
 *         <p/>
 *         <pre>
 *                 amiladi@irisa.fr
 *                 </pre>
 * @version 1.0 , 4 juin 2009
 * @class TestSteps.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */
@XmlRootElement(name = "testStep")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tm_test_steps", schema = "public")
@SequenceGenerator(name = "tm_test_steps_sequence", sequenceName = "tm_test_steps_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
@Inheritance(strategy = InheritanceType.JOINED)
public class TestSteps extends AuditedObject implements Serializable, Comparable<TestSteps> {


    public static final int MAX_DESCRIPTION_LENGTH = 1024;
    private static final long serialVersionUID = -1077614967800301821L;
    private static Logger log = LoggerFactory.getLogger(TestSteps.class);
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_steps_sequence")
    @XmlTransient
    private Integer id;

    @ManyToOne
    // JRC : Remove nullable constraint in : @JoinColumn(name = "test_roles_initiator_id",nullable=false) to be able to sync TestSteps object with GMM
    @JoinColumn(name = "test_roles_initiator_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private TestRoles testRolesInitiator;

    @ManyToOne
    @JoinColumn(name = "test_roles_responder_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    private TestRoles testRolesResponder;

    @ManyToOne
    @JoinColumn(name = "transaction_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    @XmlElement(name = "transaction")
    private Transaction transaction;

    // added for wsUsage
    @ManyToOne
    @JoinColumn(name = "ws_transaction_usage_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    @XmlElement(name = "wsTransactionUsage")
    private WSTransactionUsage wstransactionUsage;

    @XmlElement(name = "StepIndex")
    @Column(name = "step_index", nullable = false)
    @ExchangeUnique
    private Integer stepIndex;

    @ManyToOne
    @JoinColumn(name = "test_steps_option_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    private TestStepsOption testStepsOption;

    @Column(name = "description", nullable = false)
    @Size(max = TestSteps.MAX_DESCRIPTION_LENGTH)
    private String description;

    @Column(name = "message_type", nullable = true)
    private String messageType;

    @Column(name = "responder_message_type", nullable = true)
    @NotAudited
    private String responderMessageType;

    @Column(name = "hl7_version", nullable = true)
    @NotAudited
    private String hl7Version;

    @Column(name = "secured", nullable = true)
    private Boolean secured;

    @Column(name = "auto_complete", nullable = true)
    @NotAudited
    private Boolean autoComplete;

    @Column(name = "auto_triggered", nullable = true)
    @NotAudited
    private Boolean autoTriggered;

    @Column(name = "expected_message_count", nullable = true)
    @NotAudited
    private Integer expectedMessageCount;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @AuditJoinTable
    @JoinTable(name = "tm_test_steps_input_ci", joinColumns = @JoinColumn(name = "test_steps_id"), inverseJoinColumns = @JoinColumn(name = "contextual_information_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_steps_id", "contextual_information_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<ContextualInformation> inputContextualInformationList;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @AuditJoinTable
    @JoinTable(name = "tm_test_steps_output_ci", joinColumns = @JoinColumn(name = "test_steps_id"), inverseJoinColumns = @JoinColumn(name = "contextual_information_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_steps_id", "contextual_information_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<ContextualInformation> outputContextualInformationList;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotAudited
    @JoinTable(name = "tm_test_test_steps", joinColumns = @JoinColumn(name = "test_steps_id"), inverseJoinColumns = @JoinColumn(name = "test_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_id", "test_steps_id"}))
    @Fetch(value = FetchMode.SELECT)
    @ExchangeUnique
    @XmlTransient
    private Test testParent;

    @OneToMany(mappedBy = "testSteps", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @NotAudited
    @XmlTransient
    private List<TestStepsInstance> testStepsInstances;

    @OneToMany(mappedBy = "tmTestStep", fetch = FetchType.EAGER)
    @NotAudited
    private List<TmTestStepMessageProfile> tmTestStepMessageProfiles;

    public TestSteps() {

    }

    public TestSteps(TestRoles testRolesInitiator, TestRoles testRolesResponder) {
        this.testRolesInitiator = testRolesInitiator;
        this.testRolesResponder = testRolesResponder;
    }

    public TestSteps(Test test, TestRoles testRolesInitiator, TestRoles testRolesResponder, String description,
                     TestStepsOption testStepsOption) {
        this.testRolesInitiator = testRolesInitiator;
        this.testRolesResponder = testRolesResponder;
        this.description = description;
        this.testStepsOption = testStepsOption;
    }

    public TestSteps(Test test, TestRoles testRolesInitiator, TestRoles testRolesResponder, String description,
                     TestStepsOption testStepsOption, int stepIndex, String messageType, boolean secured) {
        this.testRolesInitiator = testRolesInitiator;
        this.testRolesResponder = testRolesResponder;
        this.description = description;
        this.testStepsOption = testStepsOption;
        this.stepIndex = stepIndex;
        this.messageType = messageType;
        this.secured = secured;
    }

    public TestSteps(TestSteps inTestSteps) {
        if (inTestSteps.getTestRolesInitiator() != null) {
            this.setTestRolesInitiator(inTestSteps.getTestRolesInitiator());
        }
        if (inTestSteps.getTestRolesResponder() != null) {
            this.setTestRolesResponder(inTestSteps.getTestRolesResponder());
        }
        if (inTestSteps.getDescription() != null) {
            this.setDescription(inTestSteps.getDescription());
        }
        if (inTestSteps.getTestStepsOption() != null) {
            this.setTestStepsOption(inTestSteps.getTestStepsOption());
        }
        if (inTestSteps.getStepIndex() != null) {
            this.setStepIndex(inTestSteps.getStepIndex());
        }
        if (inTestSteps.getTransaction() != null) {
            this.setTransaction(inTestSteps.getTransaction());
        }
        if (inTestSteps.getMessageType() != null) {
            this.setMessageType(inTestSteps.getMessageType());
        }
        if (inTestSteps.getHl7Version() != null) {
            this.setHl7Version(inTestSteps.getHl7Version());
        }

        if (inTestSteps.getResponderMessageType() != null) {
            this.setResponderMessageType(inTestSteps.getResponderMessageType());
        }

        this.setTmTestStepMessageProfiles(this.copyTmTestStepMessageProfiles(inTestSteps.getTmTestStepMessageProfiles()));
        this.setAutoComplete(inTestSteps.autoComplete);
        this.setAutoTriggered(inTestSteps.autoTriggered);
        this.setSecured(inTestSteps.secured);
    }

    public TestSteps(TestStepDomain domain) {
        this.testRolesInitiator = domain.getTestRolesInitiator();
        this.testRolesResponder = domain.getTestRolesResponder();
        this.transaction = domain.getTransaction();
        this.wstransactionUsage = domain.getWstransactionUsage();
        this.stepIndex = domain.getStepIndex();
        this.testStepsOption = domain.getTestStepsOption();
        this.description = domain.getDescription();
        this.messageType = domain.getMessageType();
        this.responderMessageType = domain.getResponderMessageType();
        this.hl7Version = domain.getHl7Version();
        this.secured = domain.getSecured();
        this.autoComplete = domain.getAutoComplete();
        this.autoTriggered = domain.getAutoTriggered();
        this.expectedMessageCount = domain.getExpectedMessageCount();
        this.inputContextualInformationList = domain.getInputContextualInformationList();
        this.outputContextualInformationList = domain.getOutputContextualInformationList();
        this.testParent = domain.getTestParent();
        this.tmTestStepMessageProfiles = domain.getTmTestStepMessageProfiles();
        this.lastChanged = domain.getLastChanged();
        this.lastModifierId = domain.getLastModifierId();
    }

    public TestStepDomain asDomain() {
        TestStepDomain domain = new TestStepDomain();
        domain.setId(this.id);
        domain.setTestRolesInitiator(this.testRolesInitiator);
        domain.setTestRolesResponder(this.testRolesResponder);
        domain.setTransaction(this.transaction);
        domain.setWstransactionUsage(this.wstransactionUsage);
        domain.setStepIndex(this.stepIndex);
        domain.setTestStepsOption(this.testStepsOption);
        domain.setDescription(this.description);
        domain.setMessageType(this.messageType);
        domain.setResponderMessageType(this.responderMessageType);
        domain.setHl7Version(this.hl7Version);
        domain.setSecured(this.secured);
        domain.setAutoComplete(this.autoComplete);
        domain.setAutoTriggered(this.autoTriggered);
        domain.setExpectedMessageCount(this.expectedMessageCount);
        domain.setInputContextualInformationList(this.inputContextualInformationList);
        domain.setOutputContextualInformationList(this.outputContextualInformationList);
        domain.setTestParent(this.testParent);
        domain.setTmTestStepMessageProfiles(this.tmTestStepMessageProfiles);
        domain.setLastChanged(this.lastChanged);
        domain.setLastModifierId(this.lastModifierId);
        domain.setType(TestStepType.BASIC);
        return domain;
    }

    public static int getMAX_DESCRIPTION_LENGTH() {
        return MAX_DESCRIPTION_LENGTH;
    }

    // TestSteps list was added to Test entity, we can delete TestSteps directly from Test.
    @Deprecated
    public static List<TestSteps> getTestStepsListForATestRoles(TestRoles testRoles) {
        TestStepsQuery q = new TestStepsQuery();
        q.testRolesInitiator().id().eq(testRoles.getId());
        List<TestSteps> list = q.getList();

        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    public static void deleteTestStepsWithFind(TestSteps testSteps) throws Exception {
        EntityManager em = EntityManagerService.provideEntityManager();

        try {
            TestSteps selectedTestSteps = em.find(TestSteps.class, testSteps.getId());

            if (selectedTestSteps != null && selectedTestSteps.getTmTestStepMessageProfiles() != null) {
                for (TmTestStepMessageProfile profile : selectedTestSteps.tmTestStepMessageProfiles) {
                    TmTestStepMessageProfile.deleteTmTestStepMessageProfile(em, profile);
                }
            }

            em.remove(selectedTestSteps);
            em.flush();

            for (ContextualInformation ci : selectedTestSteps.inputContextualInformationList) {
                ContextualInformation.deleteContextualInformation(em, ci);
            }

            for (ContextualInformation ci : selectedTestSteps.outputContextualInformationList) {
                ContextualInformation.deleteContextualInformation(em, ci);
            }

        } catch (Exception e) {
            log.error("deleteTestStepsWithFind", e);
            throw new Exception("A testSteps cannot be deleted -  id = " + testSteps.getId(), e);
        }
    }

    public static TestSteps mergeTestSteps(TestSteps testSteps, EntityManager em) {
        if (testSteps.getInputContextualInformationList() != null) {
            List<ContextualInformation> lcc2persist = new ArrayList<ContextualInformation>();
            for (ContextualInformation ci : testSteps.getInputContextualInformationList()) {
                lcc2persist.add(ci);
            }
            testSteps.setInputContextualInformationList(new ArrayList<ContextualInformation>());
            for (ContextualInformation ci : lcc2persist) {
                ci = em.merge(ci);
                em.flush();
                testSteps.getInputContextualInformationList().add(ci);
            }
        }

        if (testSteps.getOutputContextualInformationList() != null) {
            List<ContextualInformation> lcco2persist = new ArrayList<ContextualInformation>();
            for (ContextualInformation ci : testSteps.getOutputContextualInformationList()) {
                lcco2persist.add(ci);
            }
            testSteps.setOutputContextualInformationList(new ArrayList<ContextualInformation>());
            for (ContextualInformation ci : lcco2persist) {
                ci = em.merge(ci);
                em.flush();
                testSteps.getOutputContextualInformationList().add(ci);
            }
        }

        // added for ITB, we also need to merge in the TmTestStepMessageProfile objects

        if (testSteps.getTmTestStepMessageProfiles() != null) {
            List<TmTestStepMessageProfile> messageTypesToPersist = testSteps.getTmTestStepMessageProfiles();
            testSteps.setTmTestStepMessageProfiles(new ArrayList<TmTestStepMessageProfile>());

            // merge to save the testSteps
            testSteps = em.merge(testSteps);
            em.flush();

            for (TmTestStepMessageProfile msgType : messageTypesToPersist) {

                Set<MessageValidationService> msgValSrvsToPersist = msgType.getMessageValidationServices();
                msgType.setMessageValidationServices(new HashSet<MessageValidationService>());
                msgType.setTmTestStep(testSteps);
                msgType = em.merge(msgType);
                em.flush();
                if (msgValSrvsToPersist != null && msgValSrvsToPersist.size() > 0) {
                    for (MessageValidationService srv : msgValSrvsToPersist) {
                        srv.setTmTestStepMessageProfile(msgType);
                        srv = em.merge(srv);
                        em.flush();
                        msgType.getMessageValidationServices().add(srv);
                    }
                }

                msgType = em.merge(msgType);
                em.flush();
                testSteps.getTmTestStepMessageProfiles().add(msgType);
            }
        }

        testSteps = em.merge(testSteps);
        em.flush();
        return testSteps;

    }

    public static List<TestSteps> getTestStepsFiltered(ContextualInformation inContextualInformation) {
        TestStepsQuery query = new TestStepsQuery();
        HQLRestriction in = query.inputContextualInformationList().id().eqRestriction(inContextualInformation.getId());
        HQLRestriction out = query.outputContextualInformationList().id()
                .eqRestriction(inContextualInformation.getId());
        query.addRestriction(HQLRestrictions.or(in, out));
        return query.getList();
    }

    public List<TestStepsInstance> getTestStepsInstances() {
        return HibernateHelper.getLazyValue(this, "testStepsInstances", this.testStepsInstances);
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public void setTestStepsInstances(List<TestStepsInstance> testStepsInstances) {
        this.testStepsInstances = testStepsInstances;
    }

    public Test getTestParent() {
        return HibernateHelper.getLazyValue(this, "testParent", this.testParent);
    }

    public void setTestParent(Test testParent) {
        this.testParent = testParent;
    }

    public WSTransactionUsage getWstransactionUsage() {
        return wstransactionUsage;
    }

    public void setWstransactionUsage(WSTransactionUsage wstransactionUsage) {
        this.wstransactionUsage = wstransactionUsage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TestRoles getTestRolesInitiator() {
        return testRolesInitiator;
    }

    public void setTestRolesInitiator(TestRoles testRolesInitiator) {
        this.testRolesInitiator = testRolesInitiator;
    }

    public TestRoles getTestRolesResponder() {
        return testRolesResponder;
    }

    public void setTestRolesResponder(TestRoles testRolesResponder) {
        this.testRolesResponder = testRolesResponder;
    }

    public TestStepsOption getTestStepsOption() {
        return testStepsOption;
    }

    public void setTestStepsOption(TestStepsOption testStepsOption) {
        this.testStepsOption = testStepsOption;
    }

    public Integer getStepIndex() {
        return stepIndex;
    }

    public void setStepIndex(Integer stepIndex) {
        this.stepIndex = stepIndex;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Boolean getSecured() {
        return secured;
    }

    public void setSecured(Boolean secured) {
        this.secured = secured;
    }

    public Boolean getAutoComplete() {
        return autoComplete;
    }

    public void setAutoComplete(Boolean autoComplete) {
        this.autoComplete = autoComplete;
    }

    public Boolean getAutoTriggered() {
        return autoTriggered;
    }

    public void setAutoTriggered(Boolean autoTriggered) {
        this.autoTriggered = autoTriggered;
    }

    public Integer getExpectedMessageCount() {
        return expectedMessageCount;
    }

    public void setExpectedMessageCount(Integer expectedMessageCount) {
        this.expectedMessageCount = expectedMessageCount;
    }

    public List<TmTestStepMessageProfile> getTmTestStepMessageProfiles() {
        if (tmTestStepMessageProfiles != null) {
            return new ArrayList<>(tmTestStepMessageProfiles);
        } else {
            return null;
        }
    }

    public void setTmTestStepMessageProfiles(List<TmTestStepMessageProfile> tmTestStepMessageProfiles) {
        this.tmTestStepMessageProfiles = new ArrayList<>(tmTestStepMessageProfiles);
    }

    public List<ContextualInformation> getInputContextualInformationList() {
        if (inputContextualInformationList != null) {
            return new ArrayList<>(inputContextualInformationList);
        } else {
            return null;
        }
    }

    public void setInputContextualInformationList(List<ContextualInformation> inputContextualInformationList) {
        this.inputContextualInformationList = new ArrayList<>(inputContextualInformationList);
    }

    public List<ContextualInformation> getOutputContextualInformationList() {
        if (outputContextualInformationList != null) {
            return new ArrayList<>(outputContextualInformationList);
        } else {
            return null;
        }
    }

    public void setOutputContextualInformationList(List<ContextualInformation> outputContextualInformationList) {
        this.outputContextualInformationList = new ArrayList<>(outputContextualInformationList);
    }

    public String getResponderMessageType() {
        return responderMessageType;
    }

    public void setResponderMessageType(String responderMessageType) {
        this.responderMessageType = responderMessageType;
    }

    public String getHl7Version() {
        return hl7Version;
    }

    public void setHl7Version(String hl7Version) {
        this.hl7Version = hl7Version;
    }

    public void addInputContextualInformation(ContextualInformation selectedContextualInformation) {
        if (inputContextualInformationList == null) {
            inputContextualInformationList = new ArrayList<ContextualInformation>();
        }
        inputContextualInformationList.add(selectedContextualInformation);
    }

    public void addOutputContextualInformation(ContextualInformation selectedContextualInformation) {
        if (outputContextualInformationList == null) {
            outputContextualInformationList = new ArrayList<ContextualInformation>();
        }
        outputContextualInformationList.add(selectedContextualInformation);
    }

    public void addTmTestStepMessageProfile(TmTestStepMessageProfile profileToAdd) {
        if (tmTestStepMessageProfiles == null) {
            tmTestStepMessageProfiles = new ArrayList<TmTestStepMessageProfile>();
        }
        tmTestStepMessageProfiles.add(profileToAdd);
        profileToAdd.setTmTestStep(this);
    }

    public void removeTmTestStepMessageProfile(TmTestStepMessageProfile profileToRemove) {
        if (tmTestStepMessageProfiles == null) {
            throw new IllegalStateException(
                    "TestSteps.tmTestStepMessageTypes is NULL or Empty. Can't remove TmTestStepMessageProfile.");
        }

        tmTestStepMessageProfiles.remove(profileToRemove);
        profileToRemove.setTmTestStep(null);

    }

    @Override
    public int compareTo(TestSteps o) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;
        if (this == o) {
            return EQUAL;
        }
        if (getStepIndex() == null) {
            return BEFORE;
        }
        if ((o == null) || (o.getStepIndex() == null)) {
            return AFTER;
        }
        if (this.getStepIndex() < o.getStepIndex()) {
            return BEFORE;
        }
        if (this.getStepIndex() > o.getStepIndex()) {
            return AFTER;
        }
        return EQUAL;
    }

    public int sizeOfInputCI() {
        if (this.inputContextualInformationList == null) {
            return 0;
        }
        return this.inputContextualInformationList.size();
    }

    public int sizeOfOutputCI() {
        if (this.outputContextualInformationList == null) {
            return 0;
        }
        return this.outputContextualInformationList.size();
    }

    public TmTestStepMessageProfile getMessageProfile(MessageDirection direction) {
        switch (direction) {
            case REQUEST:
                return this.getRequestMessageProfile();
            case RESPONSE:
                return this.getResponseMessageProfile();
            default:
                return null;
        }
    }

    public TmTestStepMessageProfile getRequestMessageProfile() {
        TmTestStepMessageProfile messageType = null;

        if (this != null) {
            if (null != this.getTmTestStepMessageProfiles()) {
                if (this.getTmTestStepMessageProfiles().size() > 0) {
                    for (TmTestStepMessageProfile profile : this.getTmTestStepMessageProfiles()) {
                        if (profile.getDirection().equals(MessageDirection.REQUEST)) {
                            return profile;
                        }
                    }
                }
            }
        }
        return messageType;
    }

    public TmTestStepMessageProfile getResponseMessageProfile() {
        TmTestStepMessageProfile messageType = null;

        if (this != null) {
            if (null != this.getTmTestStepMessageProfiles()) {
                if (this.getTmTestStepMessageProfiles().size() > 0) {
                    for (TmTestStepMessageProfile profile : this.getTmTestStepMessageProfiles()) {
                        if (profile.getDirection().equals(MessageDirection.RESPONSE)) {
                            return profile;
                        }
                    }
                }
            }
        }
        return messageType;
    }

    public List<TmTestStepMessageProfile> getRequestMessageProfiles() {
        List<TmTestStepMessageProfile> msgTypes = new ArrayList<TmTestStepMessageProfile>();

        if (this != null) {
            if (null != this.getTmTestStepMessageProfiles()) {
                if (this.getTmTestStepMessageProfiles().size() > 0) {
                    for (TmTestStepMessageProfile profile : this.getTmTestStepMessageProfiles()) {
                        if (profile.getDirection().equals(MessageDirection.REQUEST)) {
                            msgTypes.add(profile);
                        }
                    }
                }
            }
        }

        return msgTypes;
    }

    public List<TmTestStepMessageProfile> getResponseMessageProfiles() {
        List<TmTestStepMessageProfile> msgTypes = new ArrayList<TmTestStepMessageProfile>();

        if (this != null) {
            if (null != this.getTmTestStepMessageProfiles()) {
                if (this.getTmTestStepMessageProfiles().size() > 0) {
                    for (TmTestStepMessageProfile profile : this.getTmTestStepMessageProfiles()) {
                        if (profile.getDirection().equals(MessageDirection.RESPONSE)) {
                            msgTypes.add(profile);
                        }
                    }
                }
            }
        }
        return msgTypes;
    }

    public List<TmTestStepMessageProfile> copyTmTestStepMessageProfiles(List<TmTestStepMessageProfile> inList) {
        List<TmTestStepMessageProfile> profiles = new ArrayList<TmTestStepMessageProfile>();
        for (TmTestStepMessageProfile profile : inList) {
            TmTestStepMessageProfile temp = new TmTestStepMessageProfile(profile);
            profiles.add(temp);
        }

        return profiles;
    }

    @Override
    public String toString() {
        return "TestSteps [id=" + id + ", stepIndex=" + stepIndex + ", description=" + description + ", messageType="
                + messageType + ", secured=" + secured + ", autoTriggered=" + autoTriggered + ", autoComplete="
                + autoComplete + ", responderMessageType=" + responderMessageType + "]";
    }

    public void saveOrMerge(EntityManager entityManager, Test test) {
        this.id = null;

        if (testStepsOption != null) {
            testStepsOption.setLastModifierId(this.lastModifierId);
            testStepsOption.saveOrMerge(entityManager);
        }
        testRolesInitiator.setLastModifierId(this.lastModifierId);
        testRolesInitiator.saveOrMerge(entityManager, test);
        testRolesResponder.setLastModifierId(this.lastModifierId);
        testRolesResponder.saveOrMerge(entityManager, test);
        if (inputContextualInformationList != null) {
            for (ContextualInformation contextualInformation : inputContextualInformationList) {
                contextualInformation.setLastModifierId(this.lastModifierId);
                contextualInformation.saveOrMerge(entityManager);
            }
        }
        if (outputContextualInformationList != null) {
            for (ContextualInformation contextualInformation : outputContextualInformationList) {
                contextualInformation.setLastModifierId(this.lastModifierId);
                contextualInformation.saveOrMerge(entityManager);
            }
        }
        if (transaction != null) {
            transaction.setLastModifierId(this.lastModifierId);
            transaction = transaction.saveOrMerge(entityManager);
        }
        if (wstransactionUsage != null) {
            wstransactionUsage.setLastModifierId(this.lastModifierId);
            wstransactionUsage.saveOrMerge(entityManager);
        }

        List<TmTestStepMessageProfile> tmTestStepMessageProfilesBackup = (List<TmTestStepMessageProfile>) ObjectUtils.clone(tmTestStepMessageProfiles);
        tmTestStepMessageProfiles = null;

        TestStepsQuery query = new TestStepsQuery();
        query.description().eq(this.description);
        query.messageType().eq(this.messageType);
        query.secured().eq(this.secured);
        query.stepIndex().eq(this.stepIndex);
        query.testRolesInitiator().eq(this.testRolesInitiator);
        query.testRolesResponder().eq(this.testRolesResponder);
        query.testStepsOption().eq(this.testStepsOption);
        query.transaction().eq(this.transaction);
        List<TestSteps> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }

        if (tmTestStepMessageProfilesBackup != null) {
            for (TmTestStepMessageProfile tmTestStepMessageProfile : tmTestStepMessageProfilesBackup) {
                tmTestStepMessageProfile.saveOrMerge(entityManager, this);
            }
        }

        tmTestStepMessageProfiles = tmTestStepMessageProfilesBackup;

        entityManager.merge(this);
        entityManager.flush();

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

    public List<AssertionStatus> getAssertionsFromDescription() {
        List<String> assertions = new ArrayList<String>();
        Pattern pattern = Pattern.compile("\\[(\\w+-\\d+)\\]");
        Matcher matcher = pattern.matcher(description);
        while (matcher.find()) {
            assertions.add(matcher.group(1));
        }
        List<AssertionStatus> assertionStatus = new ArrayList<AssertionStatus>();
        for (String assertionId : assertions) {

            AssertionStatus currentAssertion = new AssertionStatus(assertionId, id);
            assertionStatus.add(currentAssertion);
        }

        return assertionStatus;
    }
}
