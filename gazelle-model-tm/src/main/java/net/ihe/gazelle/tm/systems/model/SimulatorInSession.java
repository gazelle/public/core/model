package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipants;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import java.util.List;

@Entity
@DiscriminatorValue("SIM")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class SimulatorInSession extends SystemInSession {

    private static final long serialVersionUID = 1L;
    private static Logger log = LoggerFactory.getLogger(SimulatorInSession.class);
    @Column(name = "comment")
    String comment;

    public SimulatorInSession(Simulator simulator) {
        super(simulator, null);
    }

    public SimulatorInSession(Simulator simulator, TestingSession ts) {
        super(simulator, ts);
    }

    public SimulatorInSession() {
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void removeSimulatorInSessionWithFind(SimulatorInSession inSimulatorInSession) throws Exception {
        List<TestInstanceParticipants> testInstanceParticipantsList = TestInstanceParticipants
                .getTestInstancesParticipantsFiltered(null, null, inSimulatorInSession.getSystem(), null, null, null,
                        null, null, null);
        if ((testInstanceParticipantsList != null) && (testInstanceParticipantsList.size() > 0)) {
            throw new Exception("The simulator " + inSimulatorInSession.getSystem().getKeyword()
                    + " is used in many test instances.\n it can not be deleted.");
        }
        EntityManager em = EntityManagerService.provideEntityManager();
        try {
            SimulatorInSession simulatorInSession = em.find(SimulatorInSession.class, inSimulatorInSession.getId());
            em.remove(simulatorInSession);
        } catch (Exception e) {
            throw new Exception("A Simulator cannot be deleted -  keyword = "
                    + inSimulatorInSession.getSystem().getKeyword(), e);
        }
    }

}
