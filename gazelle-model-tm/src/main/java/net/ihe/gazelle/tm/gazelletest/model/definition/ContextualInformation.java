package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import net.ihe.gazelle.tm.gazelletest.model.instance.ContextualInformationInstance;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "contextualInformation")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tm_contextual_information", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "label", "value"}))
@SequenceGenerator(name = "tm_contextual_information_sequence", sequenceName = "tm_contextual_information_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class ContextualInformation extends AuditedObject implements Serializable, Comparable<ContextualInformation> {

    private static final long serialVersionUID = 1L;
    private static Logger log = LoggerFactory.getLogger(ContextualInformation.class);
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_contextual_information_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "label", nullable = false)
    private String label;

    @ManyToOne
    @JoinColumn(name = "path_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private Path path;

    @Column(name = "value", nullable = true)
    private String value;

    @ManyToMany(fetch = FetchType.LAZY)
    @NotAudited
    @JoinTable(name = "tm_test_steps_input_ci", joinColumns = @JoinColumn(name = "contextual_information_id"), inverseJoinColumns = @JoinColumn(name = "test_steps_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_steps_id", "contextual_information_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private List<TestSteps> testStepsAsInput;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotAudited
    @JoinTable(name = "tm_test_steps_output_ci", joinColumns = @JoinColumn(name = "contextual_information_id"), inverseJoinColumns = @JoinColumn(name = "test_steps_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_steps_id", "contextual_information_id"}))
    @Fetch(value = FetchMode.SELECT)
    @XmlTransient
    private TestSteps testStepsAsOutput;

    @OneToMany(mappedBy = "contextualInformation", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @NotAudited
    private List<ContextualInformationInstance> contextualInformationInstances;

    // constructors /////////

    public ContextualInformation() {

    }

    public ContextualInformation(String label, Path path) {
        this.label = label;
        this.path = path;
    }

    public ContextualInformation(ContextualInformation inContextualInformation) {
        if (inContextualInformation.getLabel() != null) {
            this.label = inContextualInformation.getLabel();
        }
        if (inContextualInformation.getPath() != null) {
            this.path = inContextualInformation.getPath();
        }
        if (inContextualInformation.getValue() != null) {
            this.value = inContextualInformation.getValue();
        }
    }

    // getter and setters ///////

    public static void deleteContextualInformation(EntityManager em, ContextualInformation ci) {
        List<ContextualInformationInstance> lcii = ContextualInformationInstance
                .getContextualInformationInstanceListForContextualInformation(ci);
        if (lcii != null) {
            if (lcii.size() > 0) {
                return;
            }
        }
        List<TestSteps> lts = TestSteps.getTestStepsFiltered(ci);
        if (lts != null) {
            if (lts.size() > 0) {
                return;
            }
        }
        ci = em.find(ContextualInformation.class, ci.getId());
        em.remove(ci);
        em.flush();

    }

    public static List<ContextualInformation> getContextualInformationFiltered(String inValue, Path inPath,
                                                                               String inLabel) {
        ContextualInformationQuery query = new ContextualInformationQuery();
        query.value().eqIfValueNotNull(inValue);
        query.path().eqIfValueNotNull(inPath);
        query.label().eqIfValueNotNull(inLabel);
        return query.getList();

    }

    public Integer getId() {
        return id;
    }

    public List<TestSteps> getTestStepsAsInput() {
        return HibernateHelper.getLazyValue(this, "testStepsAsInput", this.testStepsAsInput);
    }

    public void setTestStepsAsInput(List<TestSteps> testStepsAsInput) {
        this.testStepsAsInput = testStepsAsInput;
    }

    public TestSteps getTestStepsAsOutput() {
        return HibernateHelper.getLazyValue(this, "testStepsAsOutput", this.testStepsAsOutput);
    }

    public void setTestStepsAsOutput(TestSteps testStepsAsOutput) {
        this.testStepsAsOutput = testStepsAsOutput;
    }

    public List<ContextualInformationInstance> getContextualInformationInstances() {
        return HibernateHelper
                .getLazyValue(this, "contextualInformationInstances", this.contextualInformationInstances);
    }

    public void setContextualInformationInstances(List<ContextualInformationInstance> contextualInformationInstances) {
        this.contextualInformationInstances = contextualInformationInstances;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    @Override
    public int compareTo(ContextualInformation o) {
        if (o == null) {
            return -1;
        }
        if (o.getLabel() == null) {
            return -1;
        }
        if (getLabel() == null) {
            return 1;
        }

        return getLabel().compareTo(o.getLabel());
    }

    // methods //////////////

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRelatedTestKeyword() {
        List<TestSteps> lts = TestSteps.getTestStepsFiltered(this);
        if (lts != null) {
            if (lts.size() > 0) {
                TestSteps tss = lts.get(0);
                Test testParent = tss.getTestParent();
                if (testParent != null) {
                    return testParent.getKeyword();
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "ContextualInformation [id=" + id + ", label=" + label + ", path=" + path + ", value=" + value + "]";
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;
        ContextualInformationQuery query = new ContextualInformationQuery();

        query.label().eq(this.label);
        query.value().eq(this.value);

        List<ContextualInformation> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            this.testStepsAsInput = listDistinct.get(0).testStepsAsInput;
            this.testStepsAsOutput = listDistinct.get(0).testStepsAsOutput;
            entityManager.merge(this);
        } else {
            if (id != null) {
                entityManager.merge(this);
            } else {
                entityManager.persist(this);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

}
