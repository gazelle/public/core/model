package net.ihe.gazelle.tm.tee.model;

import net.ihe.gazelle.tm.configurations.model.Configuration;
import net.ihe.gazelle.tm.configurations.model.Host;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestStepsInstance;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.users.model.Institution;

import javax.persistence.*;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the tm_step_instance_message database table.
 *
 * @author tnabeel
 */
@Entity
@Table(name = "tm_step_instance_message", uniqueConstraints = @UniqueConstraint(columnNames = {"proxy_msg_id", "proxy_type_id", "proxy_host_name"}))
@SequenceGenerator(name = "tm_step_instance_message_id_generator", sequenceName = "tm_step_instance_message_sequence")
// Don't cache.  Noticed stale instances in query results once in a blue moon.
//@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class TmStepInstanceMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_step_instance_message_id_generator")
    private int id;

    @Enumerated(EnumType.ORDINAL)
    private MessageDirection direction;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_changed")
    private Date lastChanged;

    @Column(name = "proxy_msg_id")
    private int proxyMsgId;

    /**
     * An id that binds request and response messages
     */
    @Column(name = "connection_Id")
    private int connectionId;

    @Column(name = "proxy_host_name")
    private String proxyHostName;

    @ManyToOne
    @JoinColumn(name = "proxy_type_id")
    private ProxyType proxyType;

    @Column(name = "proxy_port")
    private Integer proxyPort;

    @Column(name = "sender_port")
    private Integer senderPort;

    @Column(name = "receiver_port")
    private Integer receiverPort;

    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "message_type_recieved")
    private String receivedMessageType;

    @Column(name = "message_contents")
    @Basic(fetch = FetchType.LAZY)
    private byte[] messageContents;

    @ManyToOne
    @JoinColumn(name = "sender_tm_configuration_id")
    private Configuration senderTmConfiguration;

    @ManyToOne
    @JoinColumn(name = "receiver_tm_configuration_id")
    private Configuration receiverTmConfiguration;

    @ManyToOne
    @JoinColumn(name = "tm_test_step_instance_id")
    private TestStepsInstance tmTestStepInstance;

    @ManyToOne
    @JoinColumn(name = "tm_test_step_message_profile_id")
    private TmTestStepMessageProfile tmTestStepMessageProfile;

    @OneToMany(mappedBy = "tmStepInstanceMessage", cascade = CascadeType.REMOVE)
    private Set<TmStepInstanceMsgValidation> tmStepInstanceMsgValidations;

    @ManyToOne
    @JoinColumn(name = "relates_to")
    private TmStepInstanceMessage relatesToStepInstanceMessage;

    @ManyToOne
    @JoinColumn(name = "process_status_id")
    private TestStepInstMessageProcessStatus processStatus;

    @ManyToOne
    @JoinColumn(name = "sender_institution_id")
    private Institution senderInstitution;

    @ManyToOne
    @JoinColumn(name = "sender_system_id")
    private System senderSystem;

    @ManyToOne
    @JoinColumn(name = "sender_host_id")
    private Host senderHost;

    @ManyToOne
    @JoinColumn(name = "receiver_institution_id")
    private Institution receiverInstitution;

    @ManyToOne
    @JoinColumn(name = "receiver_system_id")
    private System receiverSystem;

    @ManyToOne
    @JoinColumn(name = "receiver_host_id")
    private Host receiverHost;

    public TmStepInstanceMessage() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MessageDirection getDirection() {
        return this.direction;
    }

    public void setDirection(MessageDirection direction) {
        this.direction = direction;
    }

    public Date getLastChanged() {
        return this.lastChanged;
    }

    public void setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
    }

    public int getProxyMsgId() {
        return this.proxyMsgId;
    }

    public void setProxyMsgId(int proxyMsgId) {
        this.proxyMsgId = proxyMsgId;
    }

    public ProxyType getProxyType() {
        return this.proxyType;
    }

    public void setProxyType(ProxyType proxyType) {
        this.proxyType = proxyType;
    }

    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    public String getProxyHostName() {
        return proxyHostName;
    }

    public void setProxyHostName(String proxyHostName) {
        this.proxyHostName = proxyHostName;
    }

    public byte[] getMessageContents() {
        return messageContents;
    }

    public void setMessageContents(byte[] messageContents) {
        this.messageContents = messageContents.clone();
    }

    public Configuration getSenderTmConfiguration() {
        return senderTmConfiguration;
    }

    public void setSenderTmConfiguration(Configuration senderTmConfiguration) {
        this.senderTmConfiguration = senderTmConfiguration;
    }

    public Configuration getReceiverTmConfiguration() {
        return receiverTmConfiguration;
    }

    public void setReceiverTmConfiguration(Configuration receiverTmConfiguration) {
        this.receiverTmConfiguration = receiverTmConfiguration;
    }

    public TestStepsInstance getTmTestStepInstance() {
        return tmTestStepInstance;
    }

    public void setTmTestStepInstance(TestStepsInstance tmTestStepInstance) {
        this.tmTestStepInstance = tmTestStepInstance;
    }

    public TmTestStepMessageProfile getTmTestStepMessageProfile() {
        return this.tmTestStepMessageProfile;
    }

    public void setTmTestStepMessageProfile(TmTestStepMessageProfile tmTestStepMessageProfile) {
        this.tmTestStepMessageProfile = tmTestStepMessageProfile;
    }

    public Set<TmStepInstanceMsgValidation> getTmStepInstanceMsgValidations() {
        return this.tmStepInstanceMsgValidations;
    }

    public void setTmStepInstanceMsgValidations(Set<TmStepInstanceMsgValidation> tmStepInstanceMsgValidations) {
        this.tmStepInstanceMsgValidations = tmStepInstanceMsgValidations;
    }

    public TmStepInstanceMessage getRelatesToStepInstanceMessage() {
        return relatesToStepInstanceMessage;
    }

    public void setRelatesToStepInstanceMessage(TmStepInstanceMessage relatesToStepInstanceMessage) {
        this.relatesToStepInstanceMessage = relatesToStepInstanceMessage;
    }

    public TmStepInstanceMsgValidation addTmStepInstanceMsgValidation(TmStepInstanceMsgValidation tmStepInstanceMsgValidation) {
        getTmStepInstanceMsgValidations().add(tmStepInstanceMsgValidation);
        tmStepInstanceMsgValidation.setTmStepInstanceMessage(this);

        return tmStepInstanceMsgValidation;
    }

    public TmStepInstanceMsgValidation removeTmStepInstanceMsgValidation(TmStepInstanceMsgValidation tmStepInstanceMsgValidation) {
        getTmStepInstanceMsgValidations().remove(tmStepInstanceMsgValidation);
        tmStepInstanceMsgValidation.setTmStepInstanceMessage(null);

        return tmStepInstanceMsgValidation;
    }

    public int getMessageLength() {
        if (messageContents != null) {
            return messageContents.length;
        } else {
            return 0;
        }
    }

    public String getMessageContentsAsString() {
        return new String(messageContents, Charset.forName("UTF-8"));
    }

    public Integer getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(Integer proxyPort) {
        this.proxyPort = proxyPort;
    }

    public Integer getSenderPort() {
        return senderPort;
    }

    public void setSenderPort(Integer senderPort) {
        this.senderPort = senderPort;
    }

    public Integer getReceiverPort() {
        return receiverPort;
    }

    public void setReceiverPort(Integer receiverPort) {
        this.receiverPort = receiverPort;
    }

    public TestStepInstMessageProcessStatus getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(TestStepInstMessageProcessStatus processStatus) {
        this.processStatus = processStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Institution getSenderInstitution() {
        return senderInstitution;
    }

    public void setSenderInstitution(Institution senderInstitution) {
        this.senderInstitution = senderInstitution;
    }

    public System getSenderSystem() {
        return senderSystem;
    }

    public void setSenderSystem(System senderSystem) {
        this.senderSystem = senderSystem;
    }

    public Host getSenderHost() {
        return senderHost;
    }

    public void setSenderHost(Host senderHost) {
        this.senderHost = senderHost;
    }

    public Institution getReceiverInstitution() {
        return receiverInstitution;
    }

    public void setReceiverInstitution(Institution receiverInstitution) {
        this.receiverInstitution = receiverInstitution;
    }

    public System getReceiverSystem() {
        return receiverSystem;
    }

    public void setReceiverSystem(System receiverSystem) {
        this.receiverSystem = receiverSystem;
    }

    public Host getReceiverHost() {
        return receiverHost;
    }

    public void setReceiverHost(Host receiverHost) {
        this.receiverHost = receiverHost;
    }

    public String getReceivedMessageType() {
        return receivedMessageType;
    }

    public void setReceivedMessageType(String receivedMessageType) {
        this.receivedMessageType = receivedMessageType;
    }

    @Override
    public String toString() {
        return "TestStepInstanceMessage [id=" + id + ", MessageDirection=" + direction.name() + ", TestStepsInstance Id=" + tmTestStepInstance.getId()
                + ", proxyPort=" + proxyPort + "]";
    }
}