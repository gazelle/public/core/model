package net.ihe.gazelle.tm.application.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Entity
@Table(name = "tm_section", schema = "public")
@SequenceGenerator(name = "tm_section_sequence", sequenceName = "tm_section_id_seq", allocationSize = 1)
public class Section implements Serializable, Comparable<Section> {

    private static final long serialVersionUID = 5149059276625532787L;

    @Id
    @GeneratedValue(generator = "tm_section_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "content")
    @Lob
    @Type(type = "text")
    private String content;

    @Column(name = "rendered")
    private Boolean rendered;

    @Column(name = "style_classes")
    private String styleClasses;

    @Column(name = "section_order")
    private Integer sectionOrder;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "section_type_id")
    private SectionType sectionType;

    public Section(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getRendered() {
        return rendered;
    }

    public void setRendered(Boolean rendered) {
        this.rendered = rendered;
    }

    public String getStyleClasses() {
        return styleClasses;
    }

    public void setStyleClasses(String styleClasses) {
        this.styleClasses = styleClasses;
    }

    public Integer getSectionOrder() {
        return sectionOrder;
    }

    public void setSectionOrder(Integer sectionOrder) {
        this.sectionOrder = sectionOrder;
    }

    public SectionType getSectionType() {
        return sectionType;
    }

    public void setSectionType(SectionType sectionType) {
        this.sectionType = sectionType;
    }

    @Override
    public int compareTo(Section section) {
        return this.sectionOrder - section.sectionOrder;
    }
}
