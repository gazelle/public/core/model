/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model.DICOM;

import net.ihe.gazelle.tm.configurations.model.Configuration;
import net.ihe.gazelle.tm.configurations.model.SopClass;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Class doing nothing more than the abstract DicomConfiguration but allowing to differentiate it from the DicomSCPConfigurationClass
 *
 * @author jbmeyer
 */
@Entity
@Table(name = "tm_dicom_scu_configuration", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class DicomSCUConfiguration extends AbstractDicomConfiguration {

    private static final long serialVersionUID = 8561497684360554911L;

    public DicomSCUConfiguration() {
        super();
    }

    public DicomSCUConfiguration(Configuration inConfiguration, String inComments) {
        super(inConfiguration, inComments);
    }

    public DicomSCUConfiguration(Configuration inConfiguration) {
        super(inConfiguration);
    }

    public DicomSCUConfiguration(DicomSCUConfiguration inAbstractDicomConf) {
        super(inAbstractDicomConf);
    }

    public DicomSCUConfiguration(Configuration inConfiguration, String inAETitle, Integer inPort, Integer inPortSecure,
                                 Integer inPortProxy, SopClass inSopClass, String inTransfertRole) {
        super(inConfiguration, inAETitle, inPort, inPortSecure, inPortProxy, inSopClass, inTransfertRole);
    }

}
