package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

@Entity
@Table(name = "tm_system_event", schema = "public")
@SequenceGenerator(name = "tm_system_event_sequence", sequenceName = "tm_system_event_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class SystemEvent extends AuditedObject implements Serializable, Comparator<SystemEvent>,
        Comparable<SystemEvent> {

    private static final long serialVersionUID = -1541163870397239137L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_system_event_sequence")
    private Integer id;

    private SystemEventType type;

    @Temporal(TemporalType.TIMESTAMP)
    private Date eventDate;

    private String userName;

    @Lob
    @Type(type = "text")
    private String comment;

    public SystemEvent() {
    }

    @Override
    public int compareTo(SystemEvent o) {
        return -getEventDate().compareTo(o.getEventDate());
    }

    @Override
    public int compare(SystemEvent o1, SystemEvent o2) {
        return o1.compareTo(o2);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SystemEventType getType() {
        return type;
    }

    public void setType(SystemEventType type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "SystemEvent [type=" + type + ", eventDate=" + eventDate + ", userName=" + userName + ", comment="
                + comment + "]";
    }

}
