/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.users.model;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetGazelle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <b>Class Description : </b>ConnectathonParticipant<br>
 * <br>
 * This class describes a ConnectathonParticipant with some additional attributes (for the needs of this application). It corresponds to the details of a Connectathon participant (firstname, lastname,
 * email, meals...). This class belongs to the TestManagement module.
 * <p/>
 * ConnectathonParticipant possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the participant</li>
 * <li><b>firstname</b> : firstname corresponding to the participant</li>
 * <li><b>lastname</b> : lastname corresponding to the participant</li>
 * <li><b>email</b> : email corresponding to the participant</li>
 * <li><b>institution</b> : institution corresponding to the participant</li>
 * <li><b>mondayMeal</b> : the participant will have lunch on monday</li>
 * <li><b>tuesdayMeal</b> : the participant will have lunch on tueday</li>
 * <li><b>wednesdayMeal</b> : the participant will have lunch on wednesday</li>
 * <li><b>thursdayMeal</b> : the participant will have lunch on thursday</li>
 * <li><b>fridayMeal</b> : the participant will have lunch on friday</li>
 * <li><b>vegetarianMeal</b> : the participant is vegetarian</li>
 * <li><b>connectathonParticipantStatus</b> : status corresponding to the participant (monitor, vendor, visitor...)</li>
 * <li><b>testingSession</b> : participant for the testing session</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, March 28
 * @class ConnectathonParticipant
 * @package net.ihe.gazelle.tm.users.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "tm_connectathon_participant_status", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_connectathon_participant_status_sequence", sequenceName = "tm_connectathon_participant_status_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class ConnectathonParticipantStatus implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -4509120102224283760L;

    // Attributes (existing in database as a column)
    public static Integer STATUS_VENDOR = 1;
    public static Integer STATUS_MONITOR = 2;
    public static Integer STATUS_COMMITTEE = 3;
    public static Integer STATUS_VISITOR = 4;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_connectathon_participant_status_sequence")
    private Integer id;
    /**
     * Name of this status
     */
    @Column(name = "name")
    @NotNull
    private String name;
    /**
     * Description of this status
     */
    @Column(name = "description")
    private String description;

    // Constructors

    public ConnectathonParticipantStatus() {
        super();
    }

    public ConnectathonParticipantStatus(String name, String description) {
        super();
        this.name = name;
        this.description = description;
    }

    // Getters and setters

    /**
     * Get the VENDOR status object
     */
    public static ConnectathonParticipantStatus getVendorStatus() {
        EntityManager em = EntityManagerService.provideEntityManager();
        return em.find(ConnectathonParticipantStatus.class, STATUS_VENDOR);
    }

    /**
     * Get the MONITOR status object
     */
    public static ConnectathonParticipantStatus getMonitorStatus() {
        EntityManager em = EntityManagerService.provideEntityManager();
        return em.find(ConnectathonParticipantStatus.class, STATUS_MONITOR);
    }

    /**
     * Get the COMMITTEE status object
     */
    public static ConnectathonParticipantStatus getCommitteeStatus() {
        EntityManager em = EntityManagerService.provideEntityManager();
        return em.find(ConnectathonParticipantStatus.class, STATUS_COMMITTEE);
    }

    /**
     * Get the VISITOR status object
     */
    public static ConnectathonParticipantStatus getVisitorStatus() {
        EntityManager em = EntityManagerService.provideEntityManager();
        return em.find(ConnectathonParticipantStatus.class, STATUS_VISITOR);
    }

    /**
     * Get the all status objects
     */
    public static List<ConnectathonParticipantStatus> getAllStatus() {
        ConnectathonParticipantStatusQuery q = new ConnectathonParticipantStatusQuery();
        q.setCachable(true);
        return q.getList();

    }

    public Integer getId() {
        return id;
    }

    // *********************************************************
    // Hashcodes and Equals methods
    // *********************************************************

    public void setId(Integer id) {
        this.id = id;
    }

    @FilterLabel
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
