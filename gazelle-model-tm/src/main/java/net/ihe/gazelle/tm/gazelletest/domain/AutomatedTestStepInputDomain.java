package net.ihe.gazelle.tm.gazelletest.domain;

import java.util.Objects;

public class AutomatedTestStepInputDomain {

    private int id;

    private String key;

    private String label;

    private String description;

    private String type;

    private boolean required;

    public AutomatedTestStepInputDomain() {
        // for injection
    }

    public AutomatedTestStepInputDomain(String key, String label, String description, String type, boolean required) {
        this.key = key;
        this.label = label;
        this.description = description;
        this.type = type;
        this.required = required;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AutomatedTestStepInputDomain)) return false;
        AutomatedTestStepInputDomain that = (AutomatedTestStepInputDomain) o;
        return id == that.id
                && required == that.required
                && Objects.equals(key, that.key)
                && Objects.equals(label, that.label)
                && Objects.equals(description, that.description)
                && Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, key, label, description, type, required);
    }
}
