/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@XmlRootElement(name = "transportLayer")
@XmlAccessorType(XmlAccessType.FIELD)

@Entity
@Audited
@Table(name = "tm_transport_layer_for_config", schema = "public")
@SequenceGenerator(name = "tm_transport_layer_for_config_sequence", sequenceName = "tm_transport_layer_for_config_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class TransportLayer extends AuditedObject implements Serializable, Comparable<TransportLayer> {

    private static final long serialVersionUID = 3297457478952688123L;

    private static TransportLayer TCP_Protocol;
    private static TransportLayer UDP_Protocol;

    private static String TCP_String = "TCP";
    private static String UDP_String = "UDP";

    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_transport_layer_for_config_sequence")
    private Integer id;

    @XmlAttribute(name = "keyword")
    @Column(name = "keyword", nullable = false, unique = true)
    private String keyword;

    @XmlAttribute(name = "name")
    @Column(name = "name")
    private String name;

    // ~ Constructors
    // //////////////////////////////////////////////////////////////////////////////////////////////////

    public TransportLayer() {

    }

    public TransportLayer(String keyword, String name) {
        this.keyword = keyword;
        this.name = name;
    }

    public static List<TransportLayer> listTransportLayer() {
        TransportLayerQuery q = new TransportLayerQuery();
        List<TransportLayer> res = q.getList();
        Collections.sort(res);
        return res;
    }

    public static TransportLayer GetTransportLayer(String keyword) {
        TransportLayerQuery q = new TransportLayerQuery();
        q.keyword().eq(keyword);
        TransportLayer toReturn = null;
        try {
            toReturn = (TransportLayer) q.getUniqueResult();
        } catch (Exception e) {

        }
        return toReturn;

    }

    public static TransportLayer getTCP_Protocol() {
        if (TCP_Protocol == null) {
            TCP_Protocol = GetTransportLayer(TCP_String);
        }
        return TCP_Protocol;
    }

    public static TransportLayer getUDP_Protocol() {
        if (UDP_Protocol == null) {
            UDP_Protocol = GetTransportLayer(UDP_String);
        }

        return UDP_Protocol;
    }

    public Integer getId() {
        return id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TransportLayer [id=" + id + ", keyword=" + keyword + ", name=" + name + "]";
    }

    @Override
    public int compareTo(TransportLayer transportLayer) {
        return this.keyword.compareTo(transportLayer.keyword);
    }
}
