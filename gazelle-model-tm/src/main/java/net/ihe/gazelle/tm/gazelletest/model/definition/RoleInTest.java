/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.sync.SetTestDefinition;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipants;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;
import net.ihe.gazelle.tm.systems.model.SystemInSessionStatus;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * <b>Class Description : </b>RoleInTest<br>
 * <br>
 * This class describes the Assertion object, used by the Gazelle application. This class belongs to the Test Management module.
 * <p/>
 * Assertion possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the RoleInTest</li>
 * <li><b>keyword</b> : keyword that describes the RoleInTest</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *                                                                                                         http://ihe.irisa.fr
 *                                                                                                         </pre>
 *         <p/>
 *         <pre>
 *                                                                                                         amiladi@irisa.fr
 *                                                                                                         </pre>
 * @version 1.0 , 7 mai 2009
 * @class RoleInTest.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@XmlRootElement(name = "RoleInTest")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tm_role_in_test", schema = "public")
@SequenceGenerator(name = "tm_role_in_test_sequence", sequenceName = "tm_role_in_test_id_seq", allocationSize = 1)
//@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class RoleInTest extends AuditedObject implements Serializable, Comparable<RoleInTest> {

    private static final long serialVersionUID = 8313314903061745130L;

    private static Logger log = LoggerFactory.getLogger(RoleInTest.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_role_in_test_sequence")
    @XmlTransient
    private Integer id;

    @XmlElement(name = "keyword")
    @Column(name = "keyword", unique = true)
    private String keyword;

    @Column(name = "is_role_played_by_a_tool")
    private boolean isRolePlayedByATool;

    @ManyToMany(fetch = FetchType.EAGER)
    //@Fetch(value = FetchMode.SUBSELECT)
    @AuditJoinTable
    @JoinTable(name = "tm_role_in_test_test_participants", joinColumns = @JoinColumn(name = "role_in_test_id"), inverseJoinColumns = @JoinColumn(name = "test_participants_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "role_in_test_id", "test_participants_id"}))
    // @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<TestParticipants> testParticipantsList;

    @OneToMany(mappedBy = "roleInTest", fetch = FetchType.LAZY)
    // @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @NotAudited
    @XmlTransient
    private List<TestRoles> testRoles;

    @OneToMany(mappedBy = "roleInTest", fetch = FetchType.LAZY)
    //@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @NotAudited
    @XmlTransient
    private List<TestInstanceParticipants> testInstanceParticipants;

    @Transient
    private Boolean isTestedForAllParticipants;

    public RoleInTest() {

    }

    public RoleInTest(String keyword, List<TestParticipants> testParticipantsList) {
        this.keyword = keyword;
        this.testParticipantsList = testParticipantsList;
    }

    /**
     * Copy constructor. This constructor copy all fields except the field id. In the case we use this constructor to copy a RoleInTest and to persist this new object, it is necessary it doesn't have
     * the same identifier.
     *
     * @param inRoleInTest
     */

    public RoleInTest(RoleInTest inRoleInTest) {
        if (inRoleInTest.getKeyword() != null) {
            this.keyword = new String(inRoleInTest.getKeyword());
        }
        this.isRolePlayedByATool = inRoleInTest.getIsRolePlayedByATool();

        this.testParticipantsList = inRoleInTest.testParticipantsList;
    }

    public static List<RoleInTest> getAllRoleInTest() {
        RoleInTestQuery q = new RoleInTestQuery();
        return q.getList();
    }

    public static List<RoleInTest> getRoleInTestFilteredOnKeyword(String inElementToFind) {
        if (inElementToFind == null) {
            return null;
        }
        String elementToFind = "%" + inElementToFind + "%";

        RoleInTestQuery q = new RoleInTestQuery();
        q.addRestriction(HQLRestrictions.like("keyword", elementToFind));

        HashSet<RoleInTest> set = new HashSet<RoleInTest>(q.getList());

        q = new RoleInTestQuery();
        HQLRestriction and = HQLRestrictions
                .and(HQLRestrictions.like("keyword", elementToFind), q.testParticipantsList().isEmptyRestriction());
        q.addRestriction(and);
        set.addAll(q.getList());
        List<RoleInTest> ritList = new ArrayList<RoleInTest>(set);
        Collections.sort(ritList);

        return ritList;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<RoleInTest> getRoleInTestFiltered(String inElementToFind) {
        if (inElementToFind == null) {
            return null;
        }
        RoleInTestQuery q = new RoleInTestQuery();

        String elementToFind = "%" + inElementToFind + "%";
        HQLRestriction or = HQLRestrictions.
                or(HQLRestrictions.
                        or(HQLRestrictions.
                                or(HQLRestrictions.
                                        or(HQLRestrictions.
                                                or(HQLRestrictions.
                                                        or(HQLRestrictions.like("keyword", elementToFind),
                                                                HQLRestrictions
                                                                        .like("testParticipantsList.actorIntegrationProfileOption.actorIntegrationProfile.integrationProfile.keyword",
                                                                                elementToFind)), HQLRestrictions
                                                        .like("testParticipantsList.actorIntegrationProfileOption.actorIntegrationProfile.integrationProfile.name",
                                                                elementToFind)), HQLRestrictions
                                                .like("testParticipantsList.actorIntegrationProfileOption.actorIntegrationProfile.actor.keyword",
                                                        elementToFind)), HQLRestrictions
                                        .like("testParticipantsList.actorIntegrationProfileOption.actorIntegrationProfile.actor.name",
                                                elementToFind)), HQLRestrictions
                                .like("testParticipantsList.actorIntegrationProfileOption.integrationProfileOption.keyword",
                                        elementToFind)), HQLRestrictions
                        .like("testParticipantsList.actorIntegrationProfileOption.integrationProfileOption.name",
                                elementToFind));

        q.addRestriction(or);

        HashSet<RoleInTest> set = new HashSet<RoleInTest>(q.getList());

        q = new RoleInTestQuery();
        HQLRestriction and = HQLRestrictions
                .and(HQLRestrictions.like("keyword", elementToFind), q.testParticipantsList().isEmptyRestriction());
        q.addRestriction(and);
        set.addAll(q.getList());

        List<RoleInTest> ritList = new ArrayList<RoleInTest>(set);

        Collections.sort(ritList);

        return ritList;
    }

    public static List<SystemInSession> getSystemInSessionByRoleInTestByTestingSessionBySISStatus(
            RoleInTest inRoleInTest, TestingSession inTestingSession, SystemInSessionStatus inSystemInSessionStatus,
            Boolean acceptedToSession) {
        SystemInSessionQuery query = new SystemInSessionQuery();
        if (inRoleInTest != null) {
            query.system().systemActorProfiles().aipo().testParticipants().roleInTest().id().eq(inRoleInTest.getId());
        }

        if (!inRoleInTest.getIsRolePlayedByATool()) {
            query.system().systemActorProfiles().aipo().testParticipants().tested().eq(true);
        }

        query.testingSession().eq(inTestingSession);
        query.systemInSessionStatus().eqIfValueNotNull(inSystemInSessionStatus);
        query.acceptedToSession().eqIfValueNotNull(acceptedToSession);
        return query.getListNullIfEmpty();
    }

    public static RoleInTest getRoleInTestWithAllParameters(RoleInTest inRoleInTest) {
        if ((inRoleInTest != null) && (inRoleInTest.id != null)) {
            RoleInTestQuery q = new RoleInTestQuery();
            q.id().eq(inRoleInTest.getId());
            return (RoleInTest) q.getUniqueResult();
        } else {
            return null;
        }
    }

    public static void deleteRoleInTest(RoleInTest rit, EntityManager em) {
        if ((rit != null) && (rit.getId() != null)) {
            rit = em.find(RoleInTest.class, rit.getId());
            List<TestParticipants> ltp = rit.getTestParticipantsList();
            em.remove(rit);
            em.flush();
            if (ltp != null) {
                for (TestParticipants tp : ltp) {
                    TestParticipants.deleteTestParticipants(tp, em);
                }
            }
        }
    }

    public List<TestRoles> getTestRoles() {
        return HibernateHelper.getLazyValue(this, "testRoles", this.testRoles);
    }

    public void setTestRoles(List<TestRoles> testRoles) {
        this.testRoles = testRoles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<TestInstanceParticipants> getTestInstanceParticipants() {
        return HibernateHelper.getLazyValue(this, "testInstanceParticipants", this.testInstanceParticipants);
    }

    public void setTestInstanceParticipants(List<TestInstanceParticipants> testInstanceParticipants) {
        this.testInstanceParticipants = testInstanceParticipants;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<TestParticipants> getTestParticipantsList() {
        if (testParticipantsList == null) {
            testParticipantsList = new ArrayList<TestParticipants>();
        }
        return testParticipantsList;
    }

    public void setTestParticipantsList(List<TestParticipants> testParticipantsList) {
        this.testParticipantsList = testParticipantsList;
    }

    public Boolean getIsTestedForAllParticipants() {
        if (isTestedForAllParticipants == null) {
            boolean forall = true;
            if (this.testParticipantsList != null) {
                for (TestParticipants testParticipant : this.testParticipantsList) {
                    forall = (testParticipant.getTested() != null) && forall && testParticipant.getTested();
                }
            }
            isTestedForAllParticipants = forall;
        }
        return isTestedForAllParticipants;
    }

    public void setIsTestedForAllParticipants(Boolean isTestedForAllParticipants) {
        this.isTestedForAllParticipants = isTestedForAllParticipants;
    }

    public boolean getIsRolePlayedByATool() {
        return isRolePlayedByATool;
    }

    public void setIsRolePlayedByATool(boolean isRolePlayedByATool) {
        this.isRolePlayedByATool = isRolePlayedByATool;
    }

    public Boolean isAllParticipantsTested() {
        boolean elementNotToTestFound = false;
        if ((testParticipantsList != null) && (testParticipantsList.size() > 0)) {
            for (TestParticipants tp : testParticipantsList) {
                if (!tp.getTested()) {
                    elementNotToTestFound = true;
                    break;
                }
            }

            return !elementNotToTestFound;
        } else {
            return null;
        }
    }

    public Boolean isAllParticipantsNotTested() {
        boolean elementToTestFound = false;
        if ((testParticipantsList != null) && (testParticipantsList.size() > 0)) {
            for (TestParticipants tp : testParticipantsList) {
                if (tp.getTested()) {
                    elementToTestFound = true;
                    break;
                }
            }

            return !elementToTestFound;
        } else {
            return null;
        }
    }

    @Override
    public int compareTo(RoleInTest rit) {
        if (rit == null) {
            return -1;
        }
        if (getKeyword() == null) {
            return 1;
        } else if (rit.getKeyword() == null) {
            return -1;
        } else {
            return getKeyword().compareToIgnoreCase(rit.getKeyword());
        }
    }

    @Override
    public String toString() {
        return "RoleInTest [id=" + id + ", keyword=" + keyword + "]";
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;
        List<TestParticipants> testParticipantUnique = new ArrayList<TestParticipants>();

        if (testParticipantsList != null) {
            for (TestParticipants testParticipant : testParticipantsList) {
                testParticipant.setLastModifierId(this.lastModifierId);
                testParticipant.saveOrMerge(entityManager);
            }
            for (TestParticipants testParticipant : testParticipantsList) {
                if (!testParticipantUnique.contains(testParticipant)) {
                    testParticipantUnique.add(testParticipant);
                }
            }
            testParticipantsList = testParticipantUnique;
        }

        RoleInTestQuery query = new RoleInTestQuery();
        query.keyword().eq(this.keyword);
        List<RoleInTest> listDistinct;
        listDistinct = query.getListDistinct();

        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        entityManager.flush();
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }
}
