/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetGazelle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author abderrazek boufahja / INRIA Rennes IHE development Project
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "oidRootDefinition")
@Entity
@Table(name = "tm_oid_root_definition")
@SequenceGenerator(name = "tm_oid_root_definition_sequence", sequenceName = "tm_oid_root_definition_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class OIDRootDefinition extends AuditedObject implements Serializable {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    private static final long serialVersionUID = 1881422545451441951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////
    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_oid_root_definition_sequence")
    private Integer id;

    @Column(name = "root_oid", unique = false, nullable = true)
    private String rootOID;

    @Column(name = "last_value", unique = false, nullable = true)
    private Integer lastValue;

    @OneToOne
    private OIDRootDefinitionLabel label;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "oidRootDefinition", targetEntity = OIDRequirement.class)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<OIDRequirement> oidRequirements = new HashSet<OIDRequirement>(0);

    // constructors ///////////////////////////////////////////////////////////////////////////////////////////////////

    public OIDRootDefinition() {
    }

    public OIDRootDefinition(String rootOID, Integer lastValue, OIDRootDefinitionLabel label) {
        this.rootOID = rootOID;
        this.lastValue = lastValue;
        this.label = label;
    }

    public OIDRootDefinition(String rootOID, Integer lastValue) {
        super();
        this.rootOID = rootOID;
        this.lastValue = lastValue;
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static String getOIDRootDefinitionAsString(OIDRootDefinition oidrd) {
        if (oidrd != null) {
            return oidrd.rootOID + "." + oidrd.getLastValue();
        } else {
            return "";
        }
    }

    public static List<OIDRootDefinition> getAllOIDRootDefinition() {
        OIDRootDefinitionQuery q = new OIDRootDefinitionQuery();
        return q.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRootOID() {
        return rootOID;
    }

    public void setRootOID(String rootOID) {
        this.rootOID = rootOID;
    }

    public Integer getLastValue() {
        return lastValue;
    }

    public void setLastValue(Integer lastValue) {
        this.lastValue = lastValue;
    }

    public Set<OIDRequirement> getOidRequirements() {
        return HibernateHelper.getLazyValue(this, "oidRequirements", this.oidRequirements);
    }

    public List<OIDRequirement> getListOIDRequirementsNotPersisted() {
        return new ArrayList<>(this.oidRequirements);
    }

    public void setOidRequirements(Set<OIDRequirement> oidRequirements) {
        this.oidRequirements = oidRequirements;
    }

    public OIDRootDefinitionLabel getLabel() {
        return label;
    }

    public void setLabel(OIDRootDefinitionLabel label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "OIDRootDefinition [id=" + id + ", rootOID=" + rootOID + ", lastValue=" + lastValue + ", label="
                + label.getLabel() + "]";
    }

    public String getAsStringToView() {
        String res = "";
        res = this.rootOID + "(" + this.lastValue + ")";
        if (this.label != null) {
            if (!this.label.getLabel().equals("")) {
                res = res + "(" + this.label.getLabel() + ")";
            }
        }
        return res;
    }

    public List<OIDRequirement> getListOIDRequirements() {
        return OIDRequirement.getOIDRequirementFiltered(this, null);
    }

    public String generateNewOID() {
        String res = "";
        res = this.rootOID + "." + this.lastValue;
        return res;
    }

}
