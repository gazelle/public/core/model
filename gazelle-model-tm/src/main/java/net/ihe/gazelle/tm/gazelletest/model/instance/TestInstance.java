/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.dates.DateDisplayer;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.messaging.MessagePropertyChanged;
import net.ihe.gazelle.messaging.MessagingService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.services.GenericServiceLoader;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.systems.model.SimulatorInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.tee.model.TestInstanceExecutionStatus;
import net.ihe.gazelle.users.model.Institution;
import net.ihe.gazelle.util.HTMLFilter;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.Serializable;
import java.util.*;

/**
 * <b>Class Description : </b>TestInstance<br>
 * <br>
 * This class describes the TestInstance object, used by the Gazelle application. This class belongs to the Test Management module.
 * <p/>
 * TestInstance possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestInstance</li>
 * <li><b>test</b> : test to which belongs TestInstance</li>
 * <li><b>testInstanceStatus</b> : status of the TestInstance</li>
 * <li><b>bpelServerID</b> : The ID of the TestInstance on the Bpel Server</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / JB Meyer INRIA Rennes IHE development Project
 * <p/>
 * <pre>
 *                                                                                 http://ihe.irisa.fr
 *                                                                                 </pre>
 * <p/>
 * <pre>
 *                                                                                 amiladi@irisa.fr
 *                                                                                 </pre>
 * <p/>
 * <pre>
 *                                                                                 jmeyer@irisa.fr
 *                                                                                 </pre>
 * @version 1.0 , 27 janv. 2009
 * @class TestInstance.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@Entity
@Table(name = "tm_test_instance", schema = "public")
@SequenceGenerator(name = "tm_test_instance_sequence", sequenceName = "tm_test_instance_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class TestInstance extends AuditedObject implements Serializable {

    public static final int MAX_STEPS_NUMBER_TO_DISPLAY_IN_SEQUENCE_DIAGRAM = 30;
    private static final long serialVersionUID = -8280418662212334195L;
    private static Logger log = LoggerFactory.getLogger(TestInstance.class);
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_instance_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "test_id")
    @Fetch(value = FetchMode.SELECT)
    private Test test;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "tm_test_instance_test_instance_path_to_log_file", joinColumns = @JoinColumn(name = "test_instance_id", referencedColumnName
            = "ID"), inverseJoinColumns = @JoinColumn(name = "test_instance_path_id", referencedColumnName = "ID"))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<TestInstancePathToLogFile> fileLogPathReturn;

    @OneToMany(mappedBy = "testInstance", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<TestInstanceParticipants> testInstanceParticipants;

    @Deprecated
    @Column(name = "bpel_server_id")
    private String bpelServerID;

    @Column(name = "description")
    @Lob
    @Type(type = "text")
    private String description;

    @Column(name = "last_status_id")
    private Status lastStatus;

    @ManyToOne
    @JoinColumn(name = "monitor_id")
    @Fetch(value = FetchMode.SELECT)
    private MonitorInSession monitorInSession;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @AuditJoinTable
    @JoinTable(name = "tm_test_instance_test_status", joinColumns = @JoinColumn(name = "test_instance_id"), inverseJoinColumns = @JoinColumn(name =
            "status_id"))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<TestInstanceEvent> listTestInstanceEvent;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "tm_test_instance_test_steps_instance", joinColumns = @JoinColumn(name = "test_instance_id"), inverseJoinColumns =
    @JoinColumn(name = "test_steps_instance_id"))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<TestStepsInstance> testStepsInstanceList;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @AuditJoinTable
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<TestStepsData> testStepsDataList;

    @Column(name = "comments", nullable = true)
    @Lob
    @Type(type = "text")
    private String comments;

    @Column(name = "test_version", nullable = true)
    private Integer testVersion;

    @Column(name = "proxy_used")
    private Boolean proxyUsed;

    @ManyToOne
    @JoinColumn(name = "testing_session_id")
    @Fetch(value = FetchMode.SELECT)
    @NotNull
    private TestingSession testingSession;

    @ManyToOne
    @JoinColumn(name = "execution_status_id", referencedColumnName = "id", nullable = true)
    private TestInstanceExecutionStatus executionStatus;

    private transient Test versionedTest = null;
    private transient UserService userClient;

    /**
     * this attribute is set to true when a system is acting as partner for several roles
     * or when several systems from the same company are registered as partners of this test instance
     */
    @Column(name = "contains_same_system_or_company")
    private boolean containsSameSystemsOrFromSameCompany;

    /**
     * The list of systems which are involved in the TI for several roles
     */
    @Transient
    private Set<SystemInSession> duplicateSystems;

    /**
     * The list of companies from which more than one system is involved in the TI
     */
    @Transient
    private Set<Institution> duplicateCompanies;

    public TestInstance() {
        duplicateCompanies = null;
        duplicateSystems = null;
    }

    public TestInstance(Test inTest, String inDescription, MonitorInSession inMonitorInSession) {
        this.test = inTest;
        this.description = inDescription;
        this.monitorInSession = inMonitorInSession;
    }

    /**
     * Copy constructor. This constructor copy all fields except the field id. In the case we use this constructor to copy a testInstance and to
     * persist this new object, it is necessary it doesn't
     * have the same identifier.
     *
     * @param inTestInstance
     */

    public TestInstance(TestInstance inTestInstance) {
        if (inTestInstance.getTest() != null) {
            this.test = inTestInstance.getTest();
        }
        if (inTestInstance.getLastStatus() != null) {
            this.lastStatus = inTestInstance.getLastStatus();
        }
        if (inTestInstance.getListTestInstanceEvent() != null) {
            this.listTestInstanceEvent = inTestInstance.getListTestInstanceEvent();
        }
        if (inTestInstance.getDescription() != null) {
            this.description = new String(inTestInstance.getDescription());
        }
        if (inTestInstance.getMonitorInSession() != null) {
            this.monitorInSession = inTestInstance.getMonitorInSession();
        }
        if (inTestInstance.getTestVersion() != null) {
            this.testVersion = inTestInstance.getTestVersion();
        }
        if (inTestInstance.getProxyUsed() != null) {
            this.proxyUsed = inTestInstance.getProxyUsed();
        }
        if (inTestInstance.getTestingSession() != null) {
            this.testingSession = inTestInstance.getTestingSession();
        }
        this.fileLogPathReturn = inTestInstance.getFileLogPathReturn();
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static int getMaxStepsNumberToDisplayInSequenceDiagram() {
        return MAX_STEPS_NUMBER_TO_DISPLAY_IN_SEQUENCE_DIAGRAM;
    }

    public static List<TestInstance> getTestInstancesForATest(Test t) {
        return getTestInstanceForATestByStatus(t, null);
    }

    public static List<TestInstance> getTestInstanceForATestByStatus(Test inTest, Status inStatus) {
        if (inTest != null) {
            TestInstanceQuery query = new TestInstanceQuery();
            query.test().eq(inTest);
            query.lastStatus().eqIfValueNotNull(inStatus);
            return query.getList();
        }
        return null;
    }

    public static List<TestInstance> getTestInstanceListToBeVerifiedByTest(Test inTest) {
        TestInstanceQuery query = new TestInstanceQuery();
        query.test().eq(inTest);
        HQLRestriction completed = query.lastStatus().eqRestriction(Status.COMPLETED);
        HQLRestriction critical = query.lastStatus().eqRestriction(Status.CRITICAL);
        HQLRestriction partially_verified = query.lastStatus().eqRestriction(Status.PARTIALLY_VERIFIED);
        query.addRestriction(HQLRestrictions.or(completed, critical, partially_verified));
        return query.getList();
    }

    public static void deleteAllConnectathonTestInstanceForATestingSession(TestingSession inTestingSession)
            throws Exception {
        TestInstanceQuery query = new TestInstanceQuery();
        query.test().testType().in(inTestingSession.getTestTypes());
        query.testingSession().eq(inTestingSession);
        List<TestInstance> testInstanceList = query.getList();
        if (testInstanceList != null) {
            for (TestInstance testInstance : testInstanceList) {
                TestInstance.deleteConnectathonTestInstanceWithFind(testInstance);
            }
        }
    }



    public static void deleteConnectathonTestInstanceWithFind(TestInstance inTestInstance) throws Exception {
        EntityManager em = EntityManagerService.provideEntityManager();
        Hibernate.initialize(inTestInstance.testStepsInstanceList);
        List<TestStepsInstance> testStepsInstanceList = inTestInstance.getTestStepsInstanceList();
        Hibernate.initialize(inTestInstance.testStepsInstanceList);
        inTestInstance.setTestStepsInstanceList(null);
        em.merge(inTestInstance);
        em.flush();

        if (testStepsInstanceList != null) {
            for (TestStepsInstance testStepsInstance : testStepsInstanceList) {
                TestStepsInstance.deleteTestStepsInstanceWithFind(testStepsInstance);
            }
        }

        List<TestInstanceParticipants> testInstanceParticipantsList = TestInstanceParticipants
                .getTestInstanceParticipantsListForATest(inTestInstance);
        if (testInstanceParticipantsList != null) {
            for (TestInstanceParticipants testInstanceParticipants : testInstanceParticipantsList) {
                TestInstanceParticipants.deleteTestParticipantsWithFind(testInstanceParticipants);
            }
        }

        try {
            TestInstance selectedTestInstance = em.find(TestInstance.class, inTestInstance.getId());
            em.remove(selectedTestInstance);
        } catch (Exception e) {
            log.error("deleteConnectathonTestInstanceWithFind", e);
            throw new Exception("A test cannot be deleted -  id = " + inTestInstance.getId(), e);
        }
    }

    public static TestInstance getTestInstanceByTestStepsInstance(TestStepsInstance inTestStepsInstance) {
        TestInstanceQuery query = new TestInstanceQuery();
        query.testStepsInstanceList().id().eq(inTestStepsInstance.getId());
        return query.getUniqueResult();
    }

    public static TestInstance getTestInstanceById(Integer testInstanceId) {
        if (testInstanceId != null) {
            TestInstanceQuery query = new TestInstanceQuery();
            query.id().eq(testInstanceId);
           return query.getUniqueResult();
        }
        return null;
    }

    public static boolean isInstitutionParticipatingToATestInstance(TestInstance inTestInstance,
                                                                    Institution inInstitution) {
        if (inTestInstance != null) {
            TestInstanceParticipantsQuery query = new TestInstanceParticipantsQuery();
            query.testInstance().eq(inTestInstance);
            query.systemInSessionUser().systemInSession().system().institutionSystems().institution().eq(inInstitution);
            return query.getCount() > 0;
        }
        return false;
    }

    public static List<TestInstance> getTestInstancesFiltered(MonitorInSession inMonitorInSession,
                                                              Status inTestInstanceStatus, Institution inInstitution, SystemInSession
                                                                      inSystemInSession, Domain inDomain,
                                                              IntegrationProfile inIntegrationProfile, Actor inActor,
                                                              IntegrationProfileOption inIntegrationProfileOption, List<TestType> inTestTypes,
                                                              TestingSession inTestingSession, Test inTest) {
        TestInstanceQuery query = new TestInstanceQuery();
        query.monitorInSession().eqIfValueNotNull(inMonitorInSession);
        query.lastStatus().eqIfValueNotNull(inTestInstanceStatus);
        if ((inTestTypes != null) && !inTestTypes.isEmpty()) {
            query.test().testType().in(inTestTypes);
        }
        query.test().eqIfValueNotNull(inTest);
        query.testInstanceParticipants().systemInSessionUser().systemInSession().system().institutionSystems()
                .institution().eqIfValueNotNull(inInstitution);
        query.testInstanceParticipants().systemInSessionUser().systemInSession().eqIfValueNotNull(inSystemInSession);
        query.testingSession().eqIfValueNotNull(inTestingSession);

        ActorIntegrationProfileOptionEntity<ActorIntegrationProfileOption> aipoPath = query.test().testRoles()
                .roleInTest().testParticipantsList().actorIntegrationProfileOption();

        if (inDomain != null) {
            aipoPath.actorIntegrationProfile().integrationProfile().domainsForDP().id().eq(inDomain.getId());
        }
        aipoPath.actorIntegrationProfile().integrationProfile().eqIfValueNotNull(inIntegrationProfile);
        aipoPath.actorIntegrationProfile().actor().eqIfValueNotNull(inActor);
        aipoPath.integrationProfileOption().eqIfValueNotNull(inIntegrationProfileOption);

        return query.getList();

    }

    public static List<TestInstance> getAllTestInstance() {
        return new TestInstanceQuery().getList();
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTestVersion() {
        return testVersion;
    }

    public void setTestVersion(Integer testVersion) {
        this.testVersion = testVersion;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public String getBpelServerID() {
        return bpelServerID;
    }

    public void setBpelServerID(String bpelServerID) {
        this.bpelServerID = bpelServerID;
    }

    public Status getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(Status lastStatus) {
        // sends a message to related users
        MessagingService.publishMessage(new MessagePropertyChanged<TestInstance, Status>(this, "lastStatus",
                this.lastStatus, lastStatus));

        TestInstanceEvent tie = new TestInstanceEvent(this, lastStatus);
        tie.setType(TestInstanceEvent.STATUS_CHANGED);
        tie.setComment("");
        addTestInstanceEvent(tie);
        this.lastStatus = lastStatus;
    }

    public Status getPreCatLastStatus() {
        return lastStatus;
    }

    public void setPreCatLastStatus(Status lastStatus) {
        // sends a message to related users
        if (this.lastStatus != lastStatus) {
            if (testInstanceParticipants.size() == 1) {
                for (TestInstanceParticipants tip : testInstanceParticipants) {
                    MessagingService.publishMessage(new MessagePropertyChanged<TestInstanceParticipants, Status>(tip, "preCatLastStatus",
                            this.lastStatus, lastStatus));
                }
            }
            TestInstanceEvent tie = new TestInstanceEvent(this, lastStatus);
            tie.setType(TestInstanceEvent.STATUS_CHANGED);
            tie.setComment("");
            addTestInstanceEvent(tie);
            this.lastStatus = lastStatus;
        }
    }

    public void setPreCatLastStatus(Status lastStatus, TestInstanceParticipants tip) {
        // sends a message to related users
        MessagingService.publishMessage(new MessagePropertyChanged<TestInstanceParticipants, Status>(tip, "preCatLastStatus",
                this.lastStatus, lastStatus));

        TestInstanceEvent tie = new TestInstanceEvent(this, lastStatus);
        tie.setType(TestInstanceEvent.STATUS_CHANGED);
        tie.setComment("");
        addTestInstanceEvent(tie);
        this.lastStatus = lastStatus;
    }

    public MonitorInSession getMonitorInSession() {
        return monitorInSession;
    }

    public void setMonitorInSession(MonitorInSession monitorInSession) {
        MonitorInSession realMonitorInSession = monitorInSession;
        if ((realMonitorInSession != null) && (realMonitorInSession.getTestingSession() != null)
                && (testingSession != null)) {
            if (!realMonitorInSession.getTestingSession().getId().equals(testingSession.getId())) {
                MonitorInSessionQuery monitorInSessionQuery = new MonitorInSessionQuery();
                monitorInSessionQuery.testingSession().id().eq(testingSession.getId());
                MonitorInSession okMonitorInSession = monitorInSessionQuery.getUniqueResult();
                if (okMonitorInSession == null) {
                    log.error(monitorInSession.getUserId() + " claimed test instance " + getId()
                            + " but is not a monitor for " + getTestingSession().getDescription());
                } else {
                    realMonitorInSession = okMonitorInSession;
                }
            }
        }
        // sends a message to related users
        MessagingService.publishMessage(new MessagePropertyChanged<TestInstance, MonitorInSession>(this,
                "monitorInSession", this.monitorInSession, realMonitorInSession));
        monitorInSessionChanged(realMonitorInSession);
        this.monitorInSession = realMonitorInSession;
    }

    private void monitorInSessionChanged(MonitorInSession newMonitorInSession) {
        String lastMonitor = "None";
        if ((this.monitorInSession != null) && (this.monitorInSession.getUserId() != null)) {
            lastMonitor = this.monitorInSession.getUserId();
        }
        String newMonitor = "None";
        if ((newMonitorInSession != null) && (newMonitorInSession.getUserId() != null)) {
            newMonitor = newMonitorInSession.getUserId();
        }
        if (!lastMonitor.equals(newMonitor)) {
            // Add test instance event
            TestInstanceEvent tie = new TestInstanceEvent(this, getLastStatus());
            if (newMonitor.equals("None")) {
                tie.setType(TestInstanceEvent.MONITOR_UNCLAIMED);
            } else {
                tie.setUserId(newMonitor);
                tie.setType(TestInstanceEvent.MONITOR_CLAIMED);
            }
            tie.setComment("");
            addTestInstanceEvent(tie);
        }
    }

    public List<TestInstancePathToLogFile> getFileLogPathReturn() {
        return HibernateHelper.getLazyValue(this, "fileLogPathReturn", this.fileLogPathReturn);
    }

    public void setFileLogPathReturn(List<TestInstancePathToLogFile> fileLogPathReturn) {
        this.fileLogPathReturn = fileLogPathReturn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String inDescription) {
        this.description = inDescription;
    }

    public String getDescriptionXMLValid() {
        userClient = (UserService) Component.getInstance("gumUserService");
        StringBuilder commentsSB = new StringBuilder();

        if ((description != null) && !description.isEmpty()) {
            commentsSB.append("<p>");
            commentsSB.append(new HTMLFilter().filter(description)).toString();
            commentsSB.append(" </p>\r\n");
        }
        List<TestInstanceEvent> listTie = getListTestInstanceEvent();
        Collections.sort(listTie);

        int listSize = listTie.size();
        for (int i = 0; i < (listSize - 1); i++) {
            if ((listTie.get(i).getType() != null) && (listTie.get(i).getType() == 0)
                    && listTie.get(i).getStatus().getKeyword().equals(listTie.get(i + 1).getStatus().getKeyword())) {
                listTie.remove(i);
                listSize = listSize - 1;
            }
        }
        for (TestInstanceEvent testInstanceEvent : listTie) {
            if ((testInstanceEvent.getType() != null)
                    && ((testInstanceEvent.getType() == 6) || (testInstanceEvent.getType() == 0) || (testInstanceEvent
                    .getType() == 7))) {
                commentsSB.append("<p><b>");
                commentsSB.append(GenericServiceLoader.getService(DateDisplayer.class).displayDateTime(
                        testInstanceEvent.getDateOfEvent()));
                commentsSB.append(" : ");
                String userId = testInstanceEvent.getUserId() != null
                        ? testInstanceEvent.getUserId()
                        : testInstanceEvent.getLastModifierId();
                if (userId != null) {
                    try {
                        User user = userClient.getUserById(userId);
                        commentsSB.append(user.getFirstNameAndLastName());
                        commentsSB.append("</b> ");
                        commentsSB.append("<i>(").append(user.getOrganizationId()).append(")</i> ");
                    } catch (NoSuchElementException e) {
                        commentsSB.append(userId);
                        commentsSB.append("</b> ");
                    }
                }
                commentsSB.append(testInstanceEvent.toStringForComments());
                commentsSB.append(" </p>\r\n");
            } else if (testInstanceEvent.getType() != null) {
                commentsSB.append("<p>");
                commentsSB.append(testInstanceEvent.toStringForComments());
                commentsSB.append(" </p>\r\n");
            }
        }
        return commentsSB.toString();
    }

    // methods
    // ///////////////////////////////////////////////////////////////////

    public void setDescriptionXMLValid(String newDescription) {
        this.description = newDescription;
    }

    public String getDescriptionXMLValidWithoutP(String descriptionXMLValid) {
        descriptionXMLValid = descriptionXMLValid.replaceAll("<p>", "");
        descriptionXMLValid = descriptionXMLValid.replaceAll("</p>", "");
        return descriptionXMLValid;
    }

    public void addDescription(String descriptionToAdd) {
        TestInstanceEvent tie = new TestInstanceEvent(this, getLastStatus());
        tie.setType(TestInstanceEvent.DESCRIPTION);
        tie.setDescription(descriptionToAdd);
        addTestInstanceEvent(tie);

        MessagingService.publishMessage(new MessagePropertyChanged<TestInstance, TestInstanceEvent>(this,
                "description", null, tie));
    }

    public void addFile(String filename) {
        TestInstanceEvent tie = new TestInstanceEvent(this, getLastStatus());
        tie.setType(TestInstanceEvent.FILE);
        tie.setFilename(new HTMLFilter().filter(filename));
        addTestInstanceEvent(tie);

        MessagingService.publishMessage(new MessagePropertyChanged<TestInstance, TestInstanceEvent>(this, "file", null,
                tie));
    }

    public void removeFile(String filename) {
        TestInstanceEvent tie = new TestInstanceEvent(this, getLastStatus());
        tie.setType(TestInstanceEvent.FILE);
        tie.setFilename(new HTMLFilter().filter(filename));
        addTestInstanceEvent(tie);

        MessagingService.publishMessage(new MessagePropertyChanged<TestInstance, TestInstanceEvent>(this, "file", null,
                tie));
    }

    public Boolean getProxyUsed() {
        return proxyUsed;
    }

    public void setProxyUsed(Boolean proxyUsed) {
        this.proxyUsed = proxyUsed;
    }

    public void addTestStepsData(TestStepsData testStepsData) {
        if (getTestStepsDataList() == null) {
            setTestStepsDataList(new ArrayList<TestStepsData>());
        }
        getTestStepsDataList().add(testStepsData);
        // testStepsDataAdded(null, testStepsData);
    }

    public boolean removeTestStepsDataList(TestStepsData testStepsData) {
        if (getTestStepsDataList() != null) {
            // testStepsDataRemoved(null, testStepsData);
            return getTestStepsDataList().remove(testStepsData);
        }
        return false;
    }

    public List<TestStepsData> getTestStepsDataList() {
        return testStepsDataList;
    }

    public void setTestStepsDataList(List<TestStepsData> testStepsDataList) {
        this.testStepsDataList = testStepsDataList;
    }

    public boolean isOrchestrated() {
        boolean result = false;
        if (this.getTest() == null) {
            return false;
        }
        if (this.getTest().getOrchestrable() == null) {
            return false;
        }
        if (this.getTest().getOrchestrable() == false) {
            return false;
        }
        if (this.id == null) {
            return false;
        }
        List<TestInstanceParticipants> ltip = TestInstanceParticipants.getTestInstancesParticipantsFiltered(true, null,
                null, null, null, null, null, null, null, null, this);
        for (TestInstanceParticipants tip : ltip) {
            if (tip.getSystemInSessionUser().getSystemInSession() instanceof SimulatorInSession) {
                result = true;
            }
        }
        return result;
    }

    public List<TestInstanceEvent> getListTestInstanceEvent() {
        List<TestInstanceEvent> value = HibernateHelper.getLazyValue(this, "listTestInstanceEvent",
                this.listTestInstanceEvent);
        if (value == null) {
            setListTestInstanceEvent(new ArrayList<TestInstanceEvent>());
            return this.listTestInstanceEvent;
        } else {
            return value;
        }
    }

    private void setListTestInstanceEvent(List<TestInstanceEvent> listTestInstanceEvent) {
        this.listTestInstanceEvent = listTestInstanceEvent;
    }

    private void addTestInstanceEvent(TestInstanceEvent tie) {
        List<TestInstanceEvent> listTie = getListTestInstanceEvent();
        listTie.add(tie);
        setListTestInstanceEvent(listTie);
    }

    public void addComment(String comment) {
        TestInstanceEvent tie = new TestInstanceEvent(this, getLastStatus());
        tie.setType(TestInstanceEvent.COMMENT);
        tie.setComment(comment);
        addTestInstanceEvent(tie);

        MessagingService.publishMessage(
                new MessagePropertyChanged<TestInstance, TestInstanceEvent>(this, "comment", null, tie));
    }

    public boolean isOldComments() {
        if ((comments == null) || comments.isEmpty()) {
            return false;
        } else {
            return true;
        }

    }

    public String getComments() {
        userClient = (UserService) Component.getInstance("gumUserService");
        if ((comments == null) || comments.isEmpty()) {
            List<TestInstanceEvent> listTie = getListTestInstanceEvent();
            Collections.sort(listTie);
            StringBuilder commentsSB = new StringBuilder();

            for (TestInstanceEvent testInstanceEvent : listTie) {
                commentsSB.append("<p class=\"comment-item\"><b>");
                String userId = testInstanceEvent.getUserId() != null
                        ? testInstanceEvent.getUserId()
                        : testInstanceEvent.getLastModifierId();
                if (userId != null) {
                    try {
                        User user = userClient.getUserById(userId);
                        commentsSB.append(user.getFirstNameAndLastName());
                        commentsSB.append("</b> ");
                        commentsSB.append("<i>(").append(user.getOrganizationId()).append(")</i> ");
                    } catch (NoSuchElementException e) {
                        commentsSB.append("Unknown user");
                        commentsSB.append("</b> ");
                    }
                }
                commentsSB.append(testInstanceEvent.toStringForComments());

                commentsSB.append("<i class=\"comment-meta\">");
                commentsSB.append(GenericServiceLoader.getService(DateDisplayer.class).displayDateTime(
                        testInstanceEvent.getDateOfEvent()));
                commentsSB.append("</i>");
                commentsSB.append("</p>\r\n");
            }
            return commentsSB.toString();
        } else {
            return comments;
        }
    }

    public List<TestStepsInstance> getTestStepsInstanceList() {
        return HibernateHelper.getLazyValue(this, "testStepsInstanceList", this.testStepsInstanceList);
    }

    public void setTestStepsInstanceList(List<TestStepsInstance> testStepsInstanceList) {
        this.testStepsInstanceList = testStepsInstanceList;
    }

    public Test getVersionedTest() {
        if (versionedTest == null) {
            if (testVersion != null) {
                EntityManager entityManager = EntityManagerService.provideEntityManager();
                AuditReader auditReader = AuditReaderFactory.get(entityManager);
                versionedTest = auditReader.find(Test.class, test.getId(), testVersion);
            } else {
                versionedTest = test;
            }
        }
        return versionedTest;
    }

    public String getSequenceDiagramAsUrl() {
        return PreferenceService.getString("application_url") + "testInstanceSequenceDiagram.seam?id=" + getId();
    }

    public void deleteTestInstance() {
        EntityManager em = EntityManagerService.provideEntityManager();

        List<TestInstancePathToLogFile> tmpList = new ArrayList<TestInstancePathToLogFile>(fileLogPathReturn);
        for (TestInstancePathToLogFile tiptlf : tmpList) {
            fileLogPathReturn.remove(tiptlf);
            em.merge(this);
            tiptlf = em.find(TestInstancePathToLogFile.class, tiptlf.getId());
            tiptlf.deleteTestPathToLogFile();
        }

        tmpList = null;
        try {
            String directoryPathForThisInstance = PreferenceService.getString("gazelle_home_path") + File.separatorChar
                    + PreferenceService.getString("data_path") + File.separatorChar
                    + PreferenceService.getString("log_return_path") + java.io.File.separatorChar + this.id;
            java.io.File directoryForInstance = new java.io.File(directoryPathForThisInstance);
            String fileInfoPath = directoryPathForThisInstance + java.io.File.separatorChar + "instance.info";
            java.io.File fileInfo = new java.io.File(fileInfoPath);

            fileInfo.delete();
            directoryForInstance.delete();
        } catch (Exception e) {
            log.error("deleteTestInstance", e);
        }

        em.merge(this);
        em.remove(this);
        em.flush();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("TestInstance [");
        if (id != null) {
            builder.append("id=");
            builder.append(id);
            builder.append(", ");
        }
        if (test != null) {
            builder.append("test=");
            builder.append(test);
            builder.append(", ");
        }
        if (lastStatus != null) {
            builder.append("lastStatus=");
            builder.append(lastStatus);
        }
        builder.append("]");
        return builder.toString();
    }

    /**
     * @return
     */
    public String getQRCodeUrl() {
        return PreferenceService.getString("application_url") + "testInstanceQRCode.seam?id=" + getId();
    }

    public List<TestInstanceParticipants> getTestInstanceParticipants() {
        return HibernateHelper.getLazyValue(this, "testInstanceParticipants", this.testInstanceParticipants);
    }

    public void setTestInstanceParticipants(List<TestInstanceParticipants> testInstanceParticipants) {
        this.testInstanceParticipants = testInstanceParticipants;
    }

    public String getTestInstanceIdForSimulator() {
        String ss = PreferenceService.getString("gazelle_name");
        String testInstanceId = this.getId().toString();
        if (ss != null) {
            testInstanceId = ss + "-" + testInstanceId;
        }
        return testInstanceId;
    }

    public void initTestInstance(EntityManager em) {
        this.setLastStatus(Status.getSTATUS_STARTED());
        if (this.getTestStepsInstanceList() != null) {
            for (TestStepsInstance tsi : this.getTestStepsInstanceList()) {
                tsi.initTestStepsInstance(em);
            }
        }
        em.merge(this);
        em.flush();
    }

    public Date getStartDate() {
        List<TestInstanceEvent> listTestInstanceEvent2 = getListTestInstanceEvent();
        if (listTestInstanceEvent2.size() > 0) {
            Collections.sort(listTestInstanceEvent2);
            return listTestInstanceEvent2.get(listTestInstanceEvent2.size() - 1).getDateOfEvent();
        } else {
            return new Date();
        }

    }

    public TestInstanceExecutionStatus getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(
            TestInstanceExecutionStatus testInstanceExecutionStatus) {
        this.executionStatus = testInstanceExecutionStatus;
    }

    public boolean isContainsSameSystemsOrFromSameCompany() {
        return containsSameSystemsOrFromSameCompany;
    }

    public void setContainsSameSystemsOrFromSameCompany(boolean containsSameSystemsOrFromSameCompany) {
        this.containsSameSystemsOrFromSameCompany = containsSameSystemsOrFromSameCompany;
    }

    public Set<SystemInSession> getDuplicateSystems() {
        return duplicateSystems;
    }

    public void setDuplicateSystems(Set<SystemInSession> duplicateSystems) {
        this.duplicateSystems = duplicateSystems;
    }

    public Set<Institution> getDuplicateCompanies() {
        return duplicateCompanies;
    }

    public void setDuplicateCompanies(Set<Institution> duplicateCompanies) {
        this.duplicateCompanies = duplicateCompanies;
    }
}
