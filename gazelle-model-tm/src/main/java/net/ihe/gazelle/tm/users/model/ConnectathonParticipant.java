/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.users.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <b>Class Description : </b>ConnectathonParticipant<br>
 * <br>
 * This class describes a ConnectathonParticipant with some additional attributes (for the needs of this application). It corresponds to the details of a Connectathon participant (firstname, lastname,
 * email, meals...). This class belongs to the TestManagement module. ConnectathonParticipant possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the participant</li>
 * <li><b>firstname</b> : firstname corresponding to the participant</li>
 * <li><b>lastname</b> : lastname corresponding to the participant</li>
 * <li><b>email</b> : email corresponding to the participant</li>
 * <li><b>institution</b> : institution corresponding to the participant</li>
 * <li><b>mondayMeal</b> : the participant will have lunch on monday</li>
 * <li><b>tuesdayMeal</b> : the participant will have lunch on tueday</li>
 * <li><b>wednesdayMeal</b> : the participant will have lunch on wednesday</li>
 * <li><b>thursdayMeal</b> : the participant will have lunch on thursday</li>
 * <li><b>fridayMeal</b> : the participant will have lunch on friday</li>
 * <li><b>vegetarianMeal</b> : the participant is vegetarian</li>
 * <li><b>connectathonParticipantStatus</b> : status corresponding to the participant (monitor, vendor, visitor...)</li>
 * <li><b>testingSession</b> : participant for the testing session</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, March 28
 * @class ConnectathonParticipant
 * @package net.ihe.gazelle.tm.users.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "tm_connectathon_participant", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "email", "testing_session_id"}))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_connectathon_participant_sequence", sequenceName = "tm_connectathon_participant_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class ConnectathonParticipant extends AuditedObject implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -4509120202224283760L;

    // Attributes (existing in database as a column)
    private static Logger log = LoggerFactory.getLogger(ConnectathonParticipant.class);
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_connectathon_participant_sequence")
    private Integer id;
    /**
     * Firstname of this Connectathon participant
     */
    @Size(min = 1, max = 128)
    @Column(name = "firstname")
    @NotNull
    private String firstName;
    /**
     * Lastname of this Connectathon participant
     */
    @Size(min = 1, max = 128)
    @Column(name = "lastname")
    @NotNull
    private String lastName;
    /**
     * Email of this Connectathon participant
     */
    @Column(name = "email")
    @Email
    @Pattern(regexp = "^.+@[^\\.].*\\.[a-z]{2,}$", message = "{gazelle.validator.email}")
    @Size(max = 255)
    @NotNull
    private String email;
    /**
     * Institution (from DB) of this Connectathon participant
     */
    @Column(name = "institution_id")
    @Fetch(FetchMode.SELECT)
    private Institution institutionOld;
    /**
     * Institution (from DB) of this Connectathon participant
     */
    @ManyToOne
    @JoinColumn(name = "institution_ok_id")
    @Fetch(FetchMode.SELECT)
    private Institution institution;
    /**
     * INstitution (not exists in DB) of this Connectathon participant
     */
    @Size(max = 255)
    @Column(name = "institution_name")
    private String institutionName;
    @Column(name = "monday_meal")
    private Boolean mondayMeal;
    @Column(name = "tuesday_meal")
    private Boolean tuesdayMeal;
    @Column(name = "wednesday_meal")
    private Boolean wednesdayMeal;
    @Column(name = "thursday_meal")
    private Boolean thursdayMeal;
    @Column(name = "friday_meal")
    private Boolean fridayMeal;
    @Column(name = "vegetarian_meal")
    private Boolean vegetarianMeal;
    @Column(name = "social_event")
    private Boolean socialEvent;
    /**
     * Testing Session corresponding to this participation
     */
    @ManyToOne
    @JoinColumn(name = "testing_session_id")
    @NotNull
    @Fetch(FetchMode.SELECT)
    private TestingSession testingSession;
    /**
     * Status of this participant (ie. Vendor, Monitor, Committee, Visitor...)
     */
    @ManyToOne
    @JoinColumn(name = "status_id")
    @NotNull
    @Fetch(FetchMode.SELECT)
    private ConnectathonParticipantStatus connectathonParticipantStatus;

    // Constructor

    public ConnectathonParticipant() {
    }

    public ConnectathonParticipant(String firstName, String lastName, String email, Institution institution,
                                   Institution institutionOld, Boolean mondayMeal, Boolean tuesdayMeal, Boolean wednesdayMeal,
                                   Boolean thursdayMeal, Boolean fridayMeal, Boolean vegetarianMeal, TestingSession testingSession,
                                   ConnectathonParticipantStatus connectathonParticipantStatus) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.institutionOld = institutionOld;
        this.institution = institution;
        this.mondayMeal = mondayMeal;
        this.tuesdayMeal = tuesdayMeal;
        this.wednesdayMeal = wednesdayMeal;
        this.thursdayMeal = thursdayMeal;
        this.fridayMeal = fridayMeal;
        this.vegetarianMeal = vegetarianMeal;
        this.testingSession = testingSession;
        this.connectathonParticipantStatus = connectathonParticipantStatus;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<ConnectathonParticipant> filterConnectathonParticipant(EntityManager entityManager,
                                                                              Institution institution, TestingSession ts) {
        ConnectathonParticipantQuery q = new ConnectathonParticipantQuery();
        if (institution != null) {
            q.institution().eq(institution);
        }
        if (ts != null) {
            q.testingSession().eq(ts);
        }
        return q.getList();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Institution getInstitutionOld() {
        return institutionOld;
    }

    public void setInstitutionOld(Institution institution) {
        this.institutionOld = institution;
    }

    public Boolean getMondayMeal() {
        return mondayMeal;
    }

    public void setMondayMeal(Boolean mondayMeal) {
        this.mondayMeal = mondayMeal;
    }

    public Boolean getTuesdayMeal() {
        return tuesdayMeal;
    }

    public void setTuesdayMeal(Boolean tuesdayMeal) {
        this.tuesdayMeal = tuesdayMeal;
    }

    public Boolean getWednesdayMeal() {
        return wednesdayMeal;
    }

    public void setWednesdayMeal(Boolean wednesdayMeal) {
        this.wednesdayMeal = wednesdayMeal;
    }

    public Boolean getThursdayMeal() {
        return thursdayMeal;
    }

    public void setThursdayMeal(Boolean thursdayMeal) {
        this.thursdayMeal = thursdayMeal;
    }

    public Boolean getFridayMeal() {
        return fridayMeal;
    }

    public void setFridayMeal(Boolean fridayMeal) {
        this.fridayMeal = fridayMeal;
    }

    public Boolean getVegetarianMeal() {
        return vegetarianMeal;
    }

    public void setVegetarianMeal(Boolean vegetarianMeal) {
        this.vegetarianMeal = vegetarianMeal;
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    public ConnectathonParticipantStatus getConnectathonParticipantStatus() {
        return connectathonParticipantStatus;
    }

    public void setConnectathonParticipantStatus(ConnectathonParticipantStatus connectathonParticipantStatus) {
        this.connectathonParticipantStatus = connectathonParticipantStatus;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public Boolean getSocialEvent() {
        return socialEvent;
    }

    public void setSocialEvent(Boolean socialEvent) {
        this.socialEvent = socialEvent;
    }

    public Institution getInstitution() {
        return institution;
    }

    // *********************************************************
    // Hashcodes and Equals methods
    // *********************************************************

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    /**
     * If the 'institution' attribute is filled in, return the attached mailing address
     * added for [GZL-4828]
     * @return company mailing address.
     */
    public String getCompanyMailingAddress(){
        if (this.getInstitution() != null && this.getInstitution().getMailingAddress() != null){
            return this.getInstitution().getMailingAddress().toString();
        } else {
            // need to return empty string instead of null otherwise the generation of the excel file fails
            return "";
        }
    }
    // /// methods ////

    @Override
    public String toString() {
        return "ConnectathonParticipant [id=" + id + ", firstname=" + firstName + ", lastname=" + lastName + ", email="
                + email + ", institutionOld=" + institutionOld + ", institution=" + institution + ", institutionName="
                + institutionName + ", mondayMeal=" + mondayMeal + ", tuesdayMeal=" + tuesdayMeal + ", wednesdayMeal="
                + wednesdayMeal + ", thursdayMeal=" + thursdayMeal + ", fridayMeal=" + fridayMeal + ", vegetarianMeal="
                + vegetarianMeal + ", socialEvent=" + socialEvent + ", testingSession=" + testingSession
                + ", connectathonParticipantStatus=" + connectathonParticipantStatus + "]";
    }

}