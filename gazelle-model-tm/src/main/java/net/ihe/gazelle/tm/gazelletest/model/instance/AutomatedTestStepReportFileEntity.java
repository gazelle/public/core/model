package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepReportFile;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Audited
@Table(name = "tm_automated_test_step_report_file", schema = "public")
@SequenceGenerator(name = "tm_automated_test_step_report_file_sequence", sequenceName = "tm_automated_test_step_report_file_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class AutomatedTestStepReportFileEntity implements Serializable {

    private static final long serialVersionUID = 6945722308882769698L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_automated_test_step_report_file_sequence")
    private int id;

    @Column(name = "report_name", nullable = false)
    private String reportName;

    @Column(name = "pdf_report", nullable = false)
    private byte[] report;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "execution_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private AutomatedTestStepExecutionEntity execution;

    public AutomatedTestStepReportFileEntity() {
        // empty for serialization
    }

    public AutomatedTestStepReportFileEntity(String reportName, byte[] report) {
        this.reportName = reportName;
        this.report = report;
    }

    public AutomatedTestStepReportFileEntity(AutomatedTestStepReportFile reportFile) {
        this.id = reportFile.getId();
        this.reportName = reportFile.getReportName();
        this.report = reportFile.getReport();
    }

    public AutomatedTestStepReportFile asDomain() {
        AutomatedTestStepReportFile reportFile = new AutomatedTestStepReportFile(reportName, report);
        reportFile.setId(id);
        return reportFile;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public byte[] getReport() {
        return report;
    }

    public void setReport(byte[] report) {
        this.report = report;
    }

    public AutomatedTestStepExecutionEntity getExecution() {
        return execution;
    }

    public void setExecution(AutomatedTestStepExecutionEntity execution) {
        this.execution = execution;
    }
}
