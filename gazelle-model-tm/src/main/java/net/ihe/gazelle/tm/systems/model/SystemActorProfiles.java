/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetRegistration;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.gazelletest.model.instance.SystemAIPOResultForATestingSession;
import net.ihe.gazelle.tm.gazelletest.model.reversed.AIPO;
import net.ihe.gazelle.users.model.Institution;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description : </b>SystemActorProfiles<br>
 * <br>
 * This class describes the relation between an Actor, a System, an Integration Profile, and an Integration Profile Option. This relation corresponds to an object, it is used by the Gazelle
 * application.
 * <p/>
 * SystemActorProfiles possesses the following attributes :
 * <ul>
 * <li><b>systemId</b> : System Id corresponding to the System</li>
 * <li><b>actorId</b> : Actor Id associated to the System</li>
 * <li><b>integrationProfileId</b> : Integration Profile Id associated to the System</li>
 * <li><b>integrationProfileOptionId</b> : Integration Profile Option Id associated to the System</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, December 21
 * @class SystemActorProfiles.java
 * @package net.ihe.gazelle.pr.systems.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "tm_system_actor_profiles", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "system_id", "actor_integration_profile_option_id"}))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tm_system_actor_profiles_sequence", sequenceName = "tm_system_actor_profiles_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class SystemActorProfiles extends AuditedObject implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450811131541283760L;
    private static Logger log = LoggerFactory.getLogger(SystemActorProfiles.class);
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_system_actor_profiles_sequence")
    private Integer id;

    @ManyToOne(targetEntity = net.ihe.gazelle.tf.model.ActorIntegrationProfileOption.class)
    @JoinColumn(name = "actor_integration_profile_option_id")
    private ActorIntegrationProfileOption actorIntegrationProfileOption;

    @ManyToOne
    @JoinColumn(name = "system_id")
    @Fetch(value = FetchMode.SELECT)
    private System system;

    @ManyToOne
    @JoinColumn(name = "testing_type_id")
    @Fetch(value = FetchMode.SELECT)
    private TestingDepth testingDepth;

    @ManyToOne
    @JoinColumn(name = "wanted_testing_type_id")
    @Fetch(value = FetchMode.SELECT)
    private TestingDepth wantedTestingDepth;

    @Column(name = "testing_type_reviewed")
    private Boolean testingDepthReviewed;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actor_integration_profile_option_id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.SELECT)
    private AIPO aipo;

    @OneToMany(mappedBy = "systemActorProfile", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<SystemAIPOResultForATestingSession> results;

    // Constructors

    public SystemActorProfiles() {
    }

    public SystemActorProfiles(Integer id) {
        super();
        this.id = id;
    }

    public SystemActorProfiles(net.ihe.gazelle.tm.systems.model.System system, Actor actor,
                               IntegrationProfile integrationProfile, IntegrationProfileOption option) {

        this.system = system;

        ActorIntegrationProfileOption actorIntegrationProfilesResult = ActorIntegrationProfileOption
                .getActorIntegrationProfileOption(actor, integrationProfile, option);

        this.setActorIntegrationProfileOption(actorIntegrationProfilesResult);

    }

    public SystemActorProfiles(net.ihe.gazelle.tm.systems.model.System system, Actor actor,
                               IntegrationProfile integrationProfile, IntegrationProfileOption integrationProfileOption,
                               TestingDepth testingDepth) {

        this.system = system;

        this.setTestingDepth(testingDepth);

        ActorIntegrationProfileOption actorIntegrationProfilesResult = ActorIntegrationProfileOption
                .getActorIntegrationProfileOption(actor, integrationProfile, integrationProfileOption);

        this.setActorIntegrationProfileOption(actorIntegrationProfilesResult);

    }

    /**
     * Copy constructor. This constructor copy all fields except the field id. In the case we use this constructor to copy a systemActorProfile and to persist this new object, it is necessary it
     * doesn't have the same identifier.
     *
     * @param inSystem
     */
    public SystemActorProfiles(SystemActorProfiles inSystemActorProfile) {

        super();
        if (inSystemActorProfile != null) {
            if (inSystemActorProfile.actorIntegrationProfileOption != null) {
                actorIntegrationProfileOption = new ActorIntegrationProfileOption(
                        inSystemActorProfile.actorIntegrationProfileOption);
            }
        }

        if (inSystemActorProfile != null) {
            if (inSystemActorProfile.system != null) {
                this.system = new System(inSystemActorProfile.system);
            }
            if (inSystemActorProfile.testingDepth != null) {
                this.testingDepth = new TestingDepth(inSystemActorProfile.testingDepth);
            }
        }

    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    /**
     * The purpose of the following method is to get all the Profile/Actors/Options (ActorIntegrationProfileOption) that the system implements.
     *
     * @param system : System to check
     * @return Vector<ActorIntegrationProfileOption> : list of ActorIntegrationProfileOption combination
     */
    public static List<ActorIntegrationProfileOption> getListOfActorIntegrationProfileOptions(System system,
                                                                                              EntityManager em) {
        if (system == null) {
            log.error("getListOfActorIntegrationProfileOptions - system is null");
            return null;
        }

        SystemActorProfilesQuery query = new SystemActorProfilesQuery();
        query.system().eq(system);
        return query.actorIntegrationProfileOption().getListDistinct();
    }

    /**
     * The purpose of the following method is to get all the Profile/Actors/Options (ActorIntegrationProfileOption) with the associated testing type option implemented by the given system.
     *
     * @param system : System to check
     * @return Vector<SystemActorProfiles> : list of SystemActorProfiles combination
     */
    public static List<SystemActorProfiles> getListOfActorIntegrationProfileOptionsWithTestingDepth(System system) {
        return getListOfActorIntegrationProfileOptionsWithTestingDepth(system, null);
    }

    /**
     * Return all the Profile/Actors/Options (ActorIntegrationProfileOption) with the associated testing type option implemented by the given system and authorized in the given testing session.
     *
     * @param system         : System to check
     * @param testingSession : Testing session
     * @return Vector<SystemActorProfiles> : list of SystemActorProfiles combination
     */
    public static List<SystemActorProfiles> getListOfActorIntegrationProfileOptionsWithTestingDepth(System system,
                                                                                                    TestingSession testingSession) {
        if (system == null) {
            log.error("getListOfActorIntegrationProfileOptionsWithTestingDepth - system is null");
            return null;
        }
        SystemActorProfilesQuery query = new SystemActorProfilesQuery();
        query.system().eq(system);
        if (testingSession != null) {
            query.actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile()
                    .in(testingSession.getIntegrationProfilesUnsorted());
        }
        return query.getList();
    }

    public static List<Actor> getListOfActorsImplementedByASystem(System inSystem) {
        return getListOfActorsImplementedByASystemForAnIntegrationProfile(inSystem, null);
    }

    public static List<Actor> getListOfActorsImplementedByASystemForAnIntegrationProfile(System inSystem,
                                                                                         IntegrationProfile ip) {
        SystemActorProfilesQuery query = getSystemActorProfilesQuery(null, inSystem, null, null, null, ip, null, null);
        query.actorIntegrationProfileOption().actorIntegrationProfile().actor().keyword().order(true);
        return query.actorIntegrationProfileOption().actorIntegrationProfile().actor().getListDistinct();
    }

    public static List<IntegrationProfile> getListOfIntegrationProfilesImplementedByASystem(System inSystem) {
        SystemActorProfilesQuery query = getSystemActorProfilesQuery(null, inSystem, null, null, null, null, null, null);
        query.actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile().keyword().order(true);
        return query.actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile().getListDistinct();
    }

    public static List<SystemActorProfiles> getSystemActorProfilesFiltered(EntityManager em, Institution inInstitution,
                                                                           System inSystem, TestingDepth inTestingDepth, ActorIntegrationProfileOption inActorIntegrationProfileOption,
                                                                           ActorIntegrationProfile inActorIntegrationProfile, IntegrationProfile inIntegrationProfile,
                                                                           IntegrationProfileOption inIntegrationProfileOption, Actor inActor) {
        SystemActorProfilesQuery query = getSystemActorProfilesQuery(inInstitution, inSystem, inTestingDepth,
                inActorIntegrationProfileOption, inActorIntegrationProfile, inIntegrationProfile,
                inIntegrationProfileOption, inActor);
        return query.getList();
    }

    private static SystemActorProfilesQuery getSystemActorProfilesQuery(Institution inInstitution, System inSystem,
                                                                        TestingDepth inTestingDepth, ActorIntegrationProfileOption inActorIntegrationProfileOption,
                                                                        ActorIntegrationProfile inActorIntegrationProfile, IntegrationProfile inIntegrationProfile,
                                                                        IntegrationProfileOption inIntegrationProfileOption, Actor inActor) {
        SystemActorProfilesQuery query = new SystemActorProfilesQuery();

        query.system().institutionSystems().institution().eqIfValueNotNull(inInstitution);
        query.system().eqIfValueNotNull(inSystem);
        query.testingDepth().eqIfValueNotNull(inTestingDepth);
        query.actorIntegrationProfileOption().eqIfValueNotNull(inActorIntegrationProfileOption);
        query.actorIntegrationProfileOption().actorIntegrationProfile().eqIfValueNotNull(inActorIntegrationProfile);
        query.actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile()
                .eqIfValueNotNull(inIntegrationProfile);
        query.actorIntegrationProfileOption().integrationProfileOption().eqIfValueNotNull(inIntegrationProfileOption);
        query.actorIntegrationProfileOption().actorIntegrationProfile().actor().eqIfValueNotNull(inActor);

        return query;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ActorIntegrationProfileOption getActorIntegrationProfileOption() {
        return actorIntegrationProfileOption;
    }

    public void setActorIntegrationProfileOption(ActorIntegrationProfileOption actorIntegrationProfileOption) {
        this.actorIntegrationProfileOption = actorIntegrationProfileOption;
    }

    public System getSystem() {
        return this.system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public TestingDepth getTestingDepth() {
        return this.testingDepth;
    }

    public void setTestingDepth(TestingDepth testingDepth) {
        this.testingDepth = testingDepth;
    }

    public TestingDepth getWantedTestingDepth() {
        return wantedTestingDepth;
    }

    public void setWantedTestingDepth(TestingDepth wantedTestingDepth) {
        this.wantedTestingDepth = wantedTestingDepth;
    }

    public Boolean getTestingDepthReviewed() {
        if (testingDepthReviewed == null) {
            return false;
        }
        return testingDepthReviewed;
    }

    public void setTestingDepthReviewed(Boolean testingDepthReviewed) {
        this.testingDepthReviewed = testingDepthReviewed;
    }

    public AIPO getAipo() {
        return HibernateHelper.getLazyValue(this, "aipo", this.aipo);
    }

    public List<SystemAIPOResultForATestingSession> getResults() {
        return HibernateHelper.getLazyValue(this, "results", this.results);
    }

    @Override
    public String toString() {
        return getActorIntegrationProfileOption().getActorIntegrationProfile().getActor().getName()
                + " with "
                + getActorIntegrationProfileOption().getActorIntegrationProfile().getIntegrationProfile().getKeyword()
                + (getActorIntegrationProfileOption().getIntegrationProfileOption() != null ? " and option "
                + getActorIntegrationProfileOption().getIntegrationProfileOption().getKeyword() : "");
    }

    public int compareTo(SystemActorProfiles o) {
        if (o == null) {
            return -1;
        }
        if (actorIntegrationProfileOption == null) {
            return 1;
        }
        if (o.actorIntegrationProfileOption == null) {
            return -1;
        }

        if ((actorIntegrationProfileOption.getActorIntegrationProfile() != null)
                && (o.actorIntegrationProfileOption.getActorIntegrationProfile() != null)
                && (actorIntegrationProfileOption.getActorIntegrationProfile().compareTo(
                o.actorIntegrationProfileOption.getActorIntegrationProfile()) == 0)) {
            if (actorIntegrationProfileOption.getActorIntegrationProfile() == null) {
                return 1;
            }
            if (o.actorIntegrationProfileOption.getActorIntegrationProfile() == null) {
                return -1;
            }

            return actorIntegrationProfileOption.getIntegrationProfileOption().compareTo(
                    o.actorIntegrationProfileOption.getIntegrationProfileOption());

        } else if (actorIntegrationProfileOption.getActorIntegrationProfile() == null) {
            return 1;
        } else if (o.actorIntegrationProfileOption.getActorIntegrationProfile() == null) {
            return -1;
        } else {
            return actorIntegrationProfileOption.getActorIntegrationProfile().compareTo(
                    o.actorIntegrationProfileOption.getActorIntegrationProfile());
        }

    }
}
