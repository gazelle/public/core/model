/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.common.utils.Mergeable;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>TestOption<br>
 * <br>
 * This class describes the TestOption object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * TestOption possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestOption</li>
 * <li><b>value</b> : value of the TestOption</li>
 * <li><b>description</b> : description of the TestOption</li>
 * <li><b>labelToDisplay</b> : labelToDisplay of the TestOption</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / JB Meyer INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         amiladi@irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         jmeyer@irisa.fr
 *         </pre>
 * @version 1.0 , 27 janv. 2009
 * @class TestOption.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@Entity
@Table(name = "tm_test_option", schema = "public")
@SequenceGenerator(name = "tm_test_option_sequence", sequenceName = "tm_test_option_id_seq", allocationSize = 1)
@Audited
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class TestOption extends AuditedObject implements Serializable {


    private static final long serialVersionUID = -5889074570447672172L;

    private static String TEST_OPTION_REQUIRED_KEYWORD = "R";

    private static TestOption TEST_OPTION_REQUIRED;

    private static Logger log = LoggerFactory.getLogger(TestOption.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_option_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "keyword", unique = true)
    private String keyword;

    @Column(name = "label_to_display")
    private String labelToDisplay;

    @Column(name = "description")
    private String description;

    @Column(name = "available")
    private Boolean available;

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public TestOption() {
        super();
    }

    public TestOption(TestOption inTestOption) {
        if (inTestOption.getDescription() != null) {
            this.description = inTestOption.getDescription();
        }
        if (inTestOption.getKeyword() != null) {
            this.keyword = inTestOption.getKeyword();
        }
        if (inTestOption.getLabelToDisplay() != null) {
            this.labelToDisplay = inTestOption.getLabelToDisplay();
        }
        this.available = inTestOption.isAvailable();
    }

    public TestOption(String keyword, String labelToDisplay, String description) {
        this.description = description;
        this.labelToDisplay = labelToDisplay;
        this.keyword = keyword;
    }

    public TestOption(Integer id, String keyword, String labelToDisplay, String description, boolean available) {
        super();
        this.id = id;
        this.keyword = keyword;
        this.labelToDisplay = labelToDisplay;
        this.description = description;
        this.available = available;
    }

    public static List<TestOption> listTestOptions() {
        TestOptionQuery query = new TestOptionQuery();
        query.available().eq(true);
        query.setCachable(true);
        query.keyword().order(true);
        return query.getList();
    }

    public static TestOption getTestOptionByKeyword(String keyword) {
        if (keyword != null) {
            TestOptionQuery query = new TestOptionQuery();
            query.keyword().eq(keyword);
            query.setCachable(true);
            return query.getUniqueResult();
        }
        return null;
    }

    public static TestOption getTEST_OPTION_REQUIRED() {
        if (TEST_OPTION_REQUIRED == null) {
            TEST_OPTION_REQUIRED = getTestOptionByKeyword(TEST_OPTION_REQUIRED_KEYWORD);
        }
        return TEST_OPTION_REQUIRED;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public void setLabelToDisplay(String labelToDisplay) {
        this.labelToDisplay = labelToDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public TestOption saveOrMerge(EntityManager entityManager) {
        this.id = null;
        TestOption testOption;

        TestOptionQuery query = new TestOptionQuery();
        query.keyword().eq(this.keyword);
        TestOption foundTestOption = query.getUniqueResult();

        testOption = Mergeable.useExistingVersionIfItExists(foundTestOption, this, entityManager);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }

        return testOption;
    }
}
