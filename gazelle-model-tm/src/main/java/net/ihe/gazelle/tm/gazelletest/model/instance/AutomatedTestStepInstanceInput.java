package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.model.definition.AutomatedTestStepInput;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Audited
@Table(name = "tm_automated_test_step_instance_input", schema = "public")
@SequenceGenerator(name = "tm_automated_test_step_instance_input_sequence", sequenceName = "tm_automated_test_step_instance_input_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class AutomatedTestStepInstanceInput extends AuditedObject implements Serializable {

    private static final long serialVersionUID = 8720445190690317164L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_automated_test_step_instance_input_sequence")
    private int id;

    @Column(name = "test_steps_instance_id", nullable = false)
    private Integer testStepsInstanceId;

    @Column(name = "test_step_data_id")
    private Integer testStepDataId;

    @Column(name = "key")
    private String key;

    @Column(name = "label")
    private String label;

    @Column(name = "description")
    private String description;

    @Column(name = "type")
    private String type;

    @Column(name = "required")
    private boolean required;

    public AutomatedTestStepInstanceInput() {
        // empty for database
    }

    public AutomatedTestStepInstanceInput(Integer testStepsInstanceId, AutomatedTestStepInputDomain input) {
        this.testStepsInstanceId = testStepsInstanceId;
        this.key = input.getKey();
        this.label = input.getLabel();
        this.description = input.getDescription();
        this.type = input.getType();
        this.required = input.getRequired();
    }

    public AutomatedTestStepInstanceInput(Integer testStepsInstanceId, Integer testStepDataId, AutomatedTestStepInput input) {
        this.testStepsInstanceId = testStepsInstanceId;
        this.testStepDataId = testStepDataId;
        this.key = input.getKey();
        this.label = input.getLabel();
        this.description = input.getDescription();
        this.type = input.getType();
        this.required = input.getRequired();
    }

    public int getId() {
        return id;
    }

    public Integer getTestStepsInstanceId() {
        return testStepsInstanceId;
    }

    public void setTestStepsInstanceId(Integer testStepsInstanceId) {
        this.testStepsInstanceId = testStepsInstanceId;
    }

    public Integer getTestStepDataId() {
        return testStepDataId;
    }

    public void setTestStepDataId(Integer testStepDataId) {
        this.testStepDataId = testStepDataId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
