/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Set;

/**
 * <b>Class Description : </b>SystemInSessionManager<br>
 * <br>
 * This class describes the SystemInSessionManager object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * SystemInSessionManager possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the SystemInSessionManager</li>
 * <li><b>systemInSession</b> : systemInSession corresponding to the SystemInSessionManager</li>
 * <li><b>user</b> : user corresponding to the SystemInSessionManager</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         amiladi@irisa.fr
 *         </pre>
 * @version 1.0 , 27 janv. 2009
 * @class SystemInSessionManager.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@XmlRootElement(name = "SystemInSessionUser")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "tm_system_in_session_user", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "user_id", "system_in_session_id"}))
@SequenceGenerator(name = "tm_system_in_session_user_sequence", sequenceName = "tm_system_in_session_user_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class SystemInSessionUser implements Serializable {

    private static final long serialVersionUID = 7315170692514935719L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_system_in_session_user_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "system_in_session_id")
    @Fetch(FetchMode.SELECT)
    @XmlElement(name = "systemInSession")
    private SystemInSession systemInSession;

    @Column(name = "user_id")
    @Fetch(FetchMode.SELECT)
    private String userID;

    @OneToMany(mappedBy = "systemInSessionUser", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<TestInstanceParticipants> testInstanceParticipants;

    public SystemInSessionUser() {

    }

    public SystemInSessionUser(SystemInSession inSystemInSession, String inUser) {
        this.systemInSession = inSystemInSession;
        this.userID = inUser;
    }

    public SystemInSessionUser(SystemInSessionUser inSystemInSessionUser) {
        if (inSystemInSessionUser.getSystemInSession() != null) {
            this.systemInSession = inSystemInSessionUser.getSystemInSession();
        }
        if (inSystemInSessionUser.getUser() != null) {
            this.userID = inSystemInSessionUser.getUser();
        }
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    protected static SystemInSessionUserQuery getQuery(SystemInSession inSystemInSession, String username) {
        SystemInSessionUserQuery query = new SystemInSessionUserQuery();
        query.systemInSession().eqIfValueNotNull(inSystemInSession);
        return query;
    }

    public static SystemInSessionUser getSystemInSessionUserBySystemInSessionByUser(SystemInSession inSystemInSession,
                                                                                    String username) {
        SystemInSessionUserQuery query = getQuery(inSystemInSession, username);
        return query.getUniqueResult();
    }

    public static int getCountSystemInSessionUserFiltered(SystemInSession inSystemInSession, String username) {
        SystemInSessionUserQuery query = getQuery(inSystemInSession, username);
        return query.getCount();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SystemInSession getSystemInSession() {
        return systemInSession;
    }

    public void setSystemInSession(SystemInSession systemInSession) {
        this.systemInSession = systemInSession;
    }

    public String getUser() {
        return userID;
    }

    public void setUser(String userID) {
        this.userID = userID;
    }

    public Set<TestInstanceParticipants> getTestInstanceParticipants() {
        return HibernateHelper.getLazyValue(this, "testInstanceParticipants", this.testInstanceParticipants);
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
