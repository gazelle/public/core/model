package net.ihe.gazelle.tm.messages.model;

import java.io.Serializable;
import java.util.Objects;

public class MessageUserKey implements Serializable {
    private static final long serialVersionUID = -1364857564353919870L;

    private String userId;
    private Integer messageId;

    public MessageUserKey() {
    }

    public MessageUserKey(String userId, Integer messageId) {
        this.userId = userId;
        this.messageId = messageId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MessageUserKey that = (MessageUserKey) o;
        return this.getUserId().equals(that.getUserId())
                && this.getMessageId().equals(that.getMessageId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, messageId);
    }
}
