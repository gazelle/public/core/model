/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.tools;

import java.io.Serializable;

/**
 * <b>Class Description :  </b>Host<br><br>
 * This class describes the host configuration.
 * <p/>
 * </ul></br>
 */

/**
 *
 * @class Host.java
 * @package net.ihe.gazelle.tm.systems.model
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 * @version 1.0 - 2008, sep
 *
 */
public class StringIP implements Serializable, Comparable<StringIP> {

    private static final long serialVersionUID = 10970734834L;

    // ~ Statics variables and Class initializer
    // ///////////////////////////////////////////////////////////////////////

    private String ipAddressString;

    public StringIP() {

    }

    public StringIP(String IP) {
        ipAddressString = IP;
    }

    public static Long toDec(String octalIp) {
        try {
            if (octalIp == null) {
                return 0L;
            }
            String[] parts = octalIp.split("\\.");
            String decimalForm = "";
            for (int i = 0; i < parts.length; i++) {

                decimalForm += pad(Integer.toBinaryString(Integer.valueOf(parts[i])));

            }
            return Long.parseLong(decimalForm, 2);
        } catch (RuntimeException e) {
            return 0L;
        }
    }

    /**
     * Pads a String with length of less than 8 with "0"s
     *
     * @param toPad
     *            - the String to pad
     * @return the padded String
     */
    public static String pad(String toPad) {
        String padded = toPad;
        for (int i = 8 - toPad.length(); i > 0; i--) {
            padded = "0" + padded;
        }
        return padded;
    }

    public String getIpAddressString() {
        return ipAddressString;
    }

    public void setIpAddressString(String ipAddressString) {
        this.ipAddressString = ipAddressString;
    }

    @Override
    public int compareTo(StringIP o) {
        return toDec(ipAddressString).compareTo(toDec(o.ipAddressString));
    }

    @Override
    public String toString() {
        if (ipAddressString == null) {
            return "";
        }
        return ipAddressString;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((ipAddressString == null) ? 0 : ipAddressString.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        StringIP other = (StringIP) obj;
        if (ipAddressString == null) {
            if (other.ipAddressString != null) {
                return false;
            }
        } else if (!ipAddressString.equals(other.ipAddressString)) {
            return false;
        }
        return true;
    }

}
