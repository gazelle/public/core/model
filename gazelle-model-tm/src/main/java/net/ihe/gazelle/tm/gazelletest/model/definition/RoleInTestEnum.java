package net.ihe.gazelle.tm.gazelletest.model.definition;

public enum RoleInTestEnum {

    INITIATOR,
    RESPONDER;

    public static RoleInTestEnum getRoleInTest(String roleName) throws Exception {
        RoleInTestEnum match = null;
        for (RoleInTestEnum val : values()) {
            if (val.name().equalsIgnoreCase(roleName)) {
                match = val;
            }
        }

        if (match == null) {
            throw new Exception("Invalid Role type.");
        } else {
            return match;
        }
    }

}
