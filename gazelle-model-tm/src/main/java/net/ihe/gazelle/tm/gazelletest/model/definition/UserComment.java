package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tm_user_comment", schema = "public")
@SequenceGenerator(name = "tm_user_comment_sequence", sequenceName = "tm_user_comment_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class UserComment extends AuditedObject implements Serializable {


    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(UserComment.class);

    /**
     * Id of this object
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_user_comment_sequence")
    private Integer id;

    @Column(name = "user_id", nullable = false)
    private String userID;

    @Column(name = "comment_content", nullable = false)
    @Lob
    @Type(type = "text")
    private String commentContent;

    /**
     * Date of creation of the comment
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date")
    private Date creationDate;

    public UserComment() {

    }

    public UserComment(String inCommentContent) {
        this.commentContent = inCommentContent;
    }

    public UserComment(String inUser, String inCommentContent) {
        this.commentContent = inCommentContent;
        this.userID = inUser;
    }

    public UserComment(UserComment inUserComment) {
        if (inUserComment.getUserID() != null) {
            this.setUserID(inUserComment.getUserID());
        }
        if (inUserComment.getCreationDate() != null) {
            this.setCreationDate(inUserComment.getCreationDate());
        }
        if (inUserComment.getCommentContent() != null) {
            this.setCommentContent(inUserComment.getCommentContent());
        }

    }

    public static void deleteUserCommentWithFind(UserComment inUserComment) throws Exception {
        if (inUserComment != null) {
            try {
                EntityManager em = EntityManagerService.provideEntityManager();
                UserComment userComment = em.find(UserComment.class, inUserComment.getId());
                em.remove(userComment);
            } catch (Exception e) {
                log.error("deleteUserCommentWithFind", e);
                throw new Exception("The  UserComment cannot be deleted -  id = " + inUserComment.getId(), e);
            }
        }
    }

    public Integer getId() {
        return id;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String user) {
        this.userID = user;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

}
