/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * <b>Class Description :  </b>Host<br><br>
 * This class describes the network configuration for a testing session.
 * <p/>
 * </ul></br>
 */

/**
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, February
 * @class NetworkConfigurationForTestingSession.java
 * @package net.ihe.gazelle.tm.systems.model
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */
@Entity
@Table(name = "tm_network_config_for_session", schema = "public")
@SequenceGenerator(name = "tm_network_config_for_session_sequence", sequenceName = "tm_network_config_for_session_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class NetworkConfigurationForTestingSession extends AuditedObject implements Serializable {
    // ~ Statics variables and Class initializer
    // ///////////////////////////////////////////////////////////////////////


    public final static String _255 = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    public final static String _stringIPRegex_ = "^(?:" + NetworkConfigurationForTestingSession._255 + "\\.){3}"
            + NetworkConfigurationForTestingSession._255 + "$";
    private static final long serialVersionUID = 122352637263972816L;

    // ~ Attributes
    // ///////////////////////////////////////////////////////////////////////////
    // /////////////////////////

    /**
     * Id attribute!
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_network_config_for_session_sequence")
    private Integer id;

    @Column(name = "ip_range_low")
    @Pattern(regexp = _stringIPRegex_, message = "{gazelle.validator.shouldBeAValidIPAddress}")
    @NotNull
    private String ipRangeLow;

    @Column(name = "ip_range_high")
    @Pattern(regexp = _stringIPRegex_, message = "{gazelle.validator.shouldBeAValidIPAddress}")
    @NotNull
    private String ipRangeHigh;

    @Column(name = "subnet_mask")
    @Pattern(regexp = _stringIPRegex_, message = "{gazelle.validator.shouldBeAValidIPAddress}")
    @NotNull
    private String subnetMask;

    @Column(name = "dns_address")
    @Pattern(regexp = _stringIPRegex_, message = "{gazelle.validator.shouldBeAValidIPAddress}")
    @NotNull
    private String ipDns;

    @Column(name = "dns_header", length = 20000)
    private String dnsHeader;

    @Column(name = "reverse_header", length = 20000)
    private String reverseHeader;

    @Column(name = "host_header", length = 20000)
    private String hostHeader;

    @Column(name = "domain_name")
    private String domainName;

    @ManyToOne
    @JoinColumn(name = "testing_session_id", nullable = false, unique = true)
    @Fetch(FetchMode.SELECT)
    private TestingSession testingSession;

    // ~ Constructors
    // ///////////////////////////////////////////////////////////////////////////
    // ///////////////////////

    public NetworkConfigurationForTestingSession() {

    }

    public NetworkConfigurationForTestingSession(TestingSession inTestingSession) {
        testingSession = inTestingSession;
    }

    public static List<String> getNumberOfAddress(String beginning, String end, String dns) {
        List<String> listReturn = new ArrayList<String>();
        String addressBegining = beginning;
        String addressEnding = end;

        int[] addressBegSplited = NetworkConfigurationForTestingSession.splitAddress(addressBegining);
        int[] addressEndSplited = NetworkConfigurationForTestingSession.splitAddress(addressEnding);

        int nbOfAddress = 0;

        for (int a = addressBegSplited[0]; a <= addressEndSplited[0]; a++) {
            int addressABeginning = addressBegSplited[1];
            int addressAEnd = addressEndSplited[1];

            if (a > addressBegSplited[0]) {
                addressABeginning = 1;
            }
            if (a < addressEndSplited[0]) {
                addressAEnd = 254;
            }

            for (int b = addressABeginning; b <= addressAEnd; b++) {
                int addressBBeginning = addressBegSplited[2];
                int addressBEnd = addressEndSplited[2];

                if (b > addressBegSplited[1]) {
                    addressBBeginning = 1;
                }
                if (b < addressEndSplited[1]) {
                    addressBEnd = 254;
                }

                for (int c = addressBBeginning; c <= addressBEnd; c++) {
                    int addressCBeginning = addressBegSplited[3];
                    int addressCEnd = addressEndSplited[3];

                    if (c > addressBegSplited[2]) {
                        addressCBeginning = 2;
                    }
                    if (c < addressEndSplited[2]) {
                        addressCEnd = 254;
                    }
                    for (int d = addressCBeginning; d <= addressCEnd; d++) {
                        nbOfAddress++;
                        listReturn.add(a + "." + b + "." + c + "." + d);

                        if (nbOfAddress > 10000) {
                            break;
                        }
                    }
                    if (nbOfAddress > 10000) {
                        break;
                    }

                }
                if (nbOfAddress > 10000) {
                    break;
                }
            }
            if (nbOfAddress > 10000) {
                break;
            }
        }

        List<String> tmpList = new ArrayList<String>(listReturn);

        for (String address : listReturn) {
            if (address.trim().toLowerCase().equals(dns.trim().toLowerCase())) {
                tmpList.remove(address);
            }
        }
        return tmpList;
    }

    public static LinkedList<String> getAllIPs(TestingSession ts) {
        NetworkConfigurationForTestingSession networkConfigurationForTestingSession = NetworkConfigurationForTestingSession
                .getConfigurationParametersForSession(ts);
        if (networkConfigurationForTestingSession == null) {
            return new LinkedList<String>();
        }
        return new LinkedList<String>(getNumberOfAddress(networkConfigurationForTestingSession.getIpRangeLow(),
                networkConfigurationForTestingSession.getIpRangeHigh(),
                networkConfigurationForTestingSession.getIpDns()));
    }

    public static LinkedList<String> getAvailableIPs(TestingSession testingSession) {
        LinkedList<String> ips = getAllIPs(testingSession);
        HostQuery hostQuery = new HostQuery();
        hostQuery.testingSession().eq(testingSession);
        hostQuery.ip().neq("");
        hostQuery.ip().isNotNull();
        List<Host> hosts = hostQuery.getList();
        for (Host host : hosts) {
            ips.remove(host.getIp());
        }
        return ips;
    }

    public static int[] splitAddress(String address) {
        StringBuffer sb = new StringBuffer(address);
        sb.append(".");

        int[] array = new int[4];

        for (int i = 0; i < 4; i++) {
            int index = sb.indexOf(".", 0);
            array[i] = Integer.valueOf(sb.substring(0, index));
            sb.delete(0, index + 1);

        }

        return array;
    }

    public static NetworkConfigurationForTestingSession getConfigurationParametersForSession(
            TestingSession inTestingSession) {
        NetworkConfigurationForTestingSessionQuery q = new NetworkConfigurationForTestingSessionQuery();
        q.testingSession().eq(inTestingSession);

        List<NetworkConfigurationForTestingSession> result = q.getList();

        if (result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public String getIpRangeLow() {
        return ipRangeLow;
    }

    public void setIpRangeLow(String ipRangeLow) {
        this.ipRangeLow = ipRangeLow;
    }

    public String getIpRangeHigh() {
        return ipRangeHigh;
    }

    public void setIpRangeHigh(String ipRangeHigh) {
        this.ipRangeHigh = ipRangeHigh;
    }

    public String getSubnetMask() {
        return subnetMask;
    }

    public void setSubnetMask(String subnetMask) {
        this.subnetMask = subnetMask;
    }

    public String getIpDns() {
        return ipDns;
    }

    public void setIpDns(String ipDns) {
        this.ipDns = ipDns;
    }

    public Integer getId() {
        return id;
    }

    public String getDnsHeader() {
        return dnsHeader;
    }

    public void setDnsHeader(String dnsHeader) {
        this.dnsHeader = dnsHeader;
    }

    public String getReverseHeader() {
        return reverseHeader;
    }

    public void setReverseHeader(String reverseHeader) {
        this.reverseHeader = reverseHeader;
    }

    public String getHostHeader() {
        return hostHeader;
    }

    public void setHostHeader(String hostHeader) {
        this.hostHeader = hostHeader;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

}
