/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * WebServiceType
 *
 * @author jbmeyer
 */
@XmlRootElement(name = "webServiceType")
@XmlAccessorType(XmlAccessType.PROPERTY)

@Entity
@Table(name = "tm_web_service_type", schema = "public")
@Audited
@SequenceGenerator(name = "tm_web_service_type_sequence", sequenceName = "tm_web_service_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class WebServiceType extends AuditedObject implements Serializable {

    private static final long serialVersionUID = 9212414100912871L;

    private static Logger log = LoggerFactory.getLogger(WebServiceType.class);

    /**
     * Id attribute!
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_web_service_type_sequence")
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "profile_id", unique = true)
    @Fetch(value = FetchMode.SELECT)
    private IntegrationProfile profile;

    @Column(name = "description", nullable = true)
    private String description;

    @Deprecated
    @Column(name = "needs_OID")
    private Boolean needsOID;

    public WebServiceType() {

    }

    public WebServiceType(IntegrationProfile inProfile, String inDescription, Boolean inNeedsOid) {
        profile = inProfile;
        description = inDescription;
        needsOID = inNeedsOid;
    }

    public static List<WebServiceType> listAllWebServices() {
        return listWebServicesTypeFiltered(null, null);
    }

    public static List<WebServiceType> listWebServicesTypeFiltered(IntegrationProfile inIntegrationProfile,
            Boolean inNeedsOID) {
        ArrayList<WebServiceType> listToReturn = null;
        WebServiceTypeQuery q = new WebServiceTypeQuery();

        if ((inIntegrationProfile == null) && (inNeedsOID == null)) {
            q.profile().keyword().order(true);
            q.getList();
        }
        if (inIntegrationProfile != null) {
            q.profile().eq(inIntegrationProfile);
        }
        if (inNeedsOID != null) {
            q.needsOID().eq(inNeedsOID);
        }

        if (q != null) {
            listToReturn = (ArrayList<WebServiceType>) q.getList();
        }

        return listToReturn;
    }

    public static WebServiceType findWebServiceType(IntegrationProfile inIntegrationProfile) {

        WebServiceType result = null;
        WebServiceTypeQuery q = new WebServiceTypeQuery();

        if (inIntegrationProfile == null) {
            return null;
        } else {
            q.profile().eq(inIntegrationProfile);
        }

        try {
            result = (WebServiceType) q.getUniqueResult();
        } catch (NoResultException e) {
            log.error("findWebServiceType", e);
        }
        return result;
    }

    @XmlTransient
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlElementRef(name = "integrationProfile")
    public IntegrationProfile getProfile() {
        return profile;
    }

    public void setProfile(IntegrationProfile profile) {
        this.profile = profile;
    }

    @XmlElement(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement(name = "needsOID")
    public Boolean getNeedsOID() {
        return needsOID;
    }

    public void setNeedsOID(Boolean needsOID) {
        this.needsOID = needsOID;
    }

}
