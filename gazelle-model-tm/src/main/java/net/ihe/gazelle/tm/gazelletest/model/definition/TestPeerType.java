/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.common.model.LabelKeywordDescriptionClass;
import net.ihe.gazelle.common.model.LabelKeywordDescriptionClassQuery;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import net.ihe.gazelle.translate.TranslateService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>TestPeerType<br>
 * <br>
 * This class describes the TestPeerType object, used by the Gazelle application. This class belongs to the Test Management module.
 * <p/>
 * TestPeerType possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestPeerType</li>
 * <li><b>keyword</b> : keyword of the TestPeerType.</li>
 * <li><b>description</b> : description of the TestPeerType</li>
 * <li><b>labelToDisplay</b> : labelToDisplay of the TestPeerType</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan CHATEL / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         jchatel@irisa.fr
 *         </pre>
 * @version 1.0 , 28 mai 2009
 * @class TestPeerType.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@XmlRootElement(name = "testPeerType")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tm_test_peer_type", schema = "public")
@SequenceGenerator(name = "tm_test_peer_type_sequence", sequenceName = "tm_test_peer_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class TestPeerType extends AuditedObject implements Serializable {


    private static final long serialVersionUID = -7731807795158884913L;

    private final static String TEST_NO_PEER_STRING = "NO_PEER_TEST";
    private final static String TEST_P2P_PEER_STRING = "P2P_TEST";
    private final static String TEST_WORKFLOW_STRING = "WORKFLOW_TEST";

    private static Logger log = LoggerFactory.getLogger(TestPeerType.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_peer_type_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "keyword", unique = true)
    private String keyword;

    @Column(name = "label_to_display")
    private String labelToDisplay;

    @Column(name = "description")
    private String description;

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public TestPeerType() {
    }

    public TestPeerType(String keyword, String labelToDisplay, String description) {
        this.description = description;
        this.labelToDisplay = labelToDisplay;
        this.keyword = keyword;
    }

    public TestPeerType(TestPeerType inTestPeerType) {
        if (inTestPeerType.getDescription() != null) {
            this.description = inTestPeerType.getDescription();
        }
        if (inTestPeerType.getKeyword() != null) {
            this.keyword = inTestPeerType.getKeyword();
        }
        if (inTestPeerType.getLabelToDisplay() != null) {
            this.labelToDisplay = inTestPeerType.getLabelToDisplay();
        }
    }

    public static TestPeerType getTestPeerTypeByKeyword(String keyword) {
        if (keyword != null) {
            TestPeerTypeQuery q = new TestPeerTypeQuery();
            q.keyword().eq(keyword);
            q.setCachable(true);
            return q.getUniqueResult();
        }
        return null;
    }

    public static List<LabelKeywordDescriptionClass> getListStatus() {
        LabelKeywordDescriptionClassQuery query = new LabelKeywordDescriptionClassQuery();
        query.keyword().order(true);
        return query.getList();
    }

    public static TestPeerType getNO_PEER_TEST() {

        return TestPeerType.getTestPeerTypeByKeyword(TEST_NO_PEER_STRING);
    }

    public static TestPeerType getP2P_TEST() {
        return TestPeerType.getTestPeerTypeByKeyword(TEST_P2P_PEER_STRING);
    }

    public static TestPeerType getWORKFLOW_TEST() {
        return TestPeerType.getTestPeerTypeByKeyword(TEST_WORKFLOW_STRING);
    }

    /**
     * Get the list of possibles Test Peer Types
     *
     * @return List of possible TestPeerType
     */
    public static List<TestPeerType> getPeerTypeList() {
        return new TestPeerTypeQuery().getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public void setLabelToDisplay(String labelToDisplay) {
        this.labelToDisplay = labelToDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @FilterLabel
    public String getSelectableLabel() {
        return TranslateService.getTranslation(getLabelToDisplay());
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        TestPeerTypeQuery query = new TestPeerTypeQuery();
        query.description().eq(this.description);
        query.labelToDisplay().eq(this.labelToDisplay);
        query.keyword().eq(this.keyword);
        List<TestPeerType> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

}
