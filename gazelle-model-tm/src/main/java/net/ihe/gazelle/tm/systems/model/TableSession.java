/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * <b>Class Description : </b>TableSession<br>
 * <br>
 * This class describes the TableSession object, used by the Gazelle application. It corresponds to a table number during an event/session. This class belongs to the Systems module.
 * <p/>
 * TableSession possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the System</li>
 * <li><b>tableKeyword</b> : Table during the Session (example : Table A6)</li>
 * </ul>
 * </br> <b>Example of TableSession</b> : A6<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, December 21
 * @class TableSession.java
 * @package net.ihe.gazelle.systems.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "TableSession")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "sys_table_session", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "table_keyword"))
@SequenceGenerator(name = "sys_table_session_sequence", sequenceName = "sys_table_session_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class TableSession extends AuditedObject implements java.io.Serializable, Comparable<TableSession> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -5990034457658398088L;

    private static final String DEFAULT_TABLE_SESSION_STRING = "None";

    // Attributes (existing in database as a column)
    private Integer id;
    private String tableKeyword;

    // Constructors
    public TableSession() {
    }

    public TableSession(Integer id, String tableKeyword) {
        this.id = id;
        this.tableKeyword = tableKeyword;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static String getDEFAULT_TABLE_SESSION_STRING() {
        return DEFAULT_TABLE_SESSION_STRING;
    }

    public static List<TableSession> listTableSession() {
        return new TableSessionQuery().getList();
    }

    public static TableSession getTableSessionByKeyword(String tableKeyword) {
        TableSessionQuery tableSessionQuery = new TableSessionQuery();
        tableSessionQuery.tableKeyword().eq(tableKeyword);
        return tableSessionQuery.getUniqueResult();
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sys_table_session_sequence")
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "table_keyword")
    @NotNull
    @XmlElement(name = "keyword")
    public String getTableKeyword() {
        return this.tableKeyword;
    }

    public void setTableKeyword(String tableKeyword) {
        this.tableKeyword = tableKeyword;
    }

    @Override
    public int compareTo(TableSession arg0) {
        if (this.getTableKeyword().compareTo(arg0.getTableKeyword()) > 0) {
            return 1;
        }
        if (this.getTableKeyword().compareTo(arg0.getTableKeyword()) < 0) {
            return (-1);
        }
        return 0;
    }

}
