/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

/**
 * Class doing nothing more than the abstract DicomConfiguration but allowing to differentiate it from the DicomSCUConfigurationClass
 *
 * @author jbmeyer
 */
@XmlRootElement(name = "sopClass")
@XmlAccessorType(XmlAccessType.FIELD)

@Entity
@Audited
@Table(name = "tm_sop_class", schema = "public")
@SequenceGenerator(name = "tm_sop_class_sequence", sequenceName = "tm_sop_class_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class SopClass extends AuditedObject implements Serializable {


    private static final long serialVersionUID = 114417097017L;

    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_sop_class_sequence")
    private Integer id;

    @XmlAttribute(name = "keyword")
    @Column(name = "keyword", nullable = false, unique = true)
    private String keyword;

    @XmlAttribute(name = "name")
    @Column(name = "name")
    private String name;

    public SopClass() {

    }

    public SopClass(String keyword, String name) {
        this.keyword = keyword;
        this.name = name;
    }

    public static List<SopClass> listSopClasses() {
        SopClassQuery query = new SopClassQuery();
        query.keyword().order(true);
        return query.getList();

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
