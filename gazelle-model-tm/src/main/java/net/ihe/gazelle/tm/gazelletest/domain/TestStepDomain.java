package net.ihe.gazelle.tm.gazelletest.domain;

import net.ihe.gazelle.tf.model.Transaction;
import net.ihe.gazelle.tf.model.WSTransactionUsage;
import net.ihe.gazelle.tm.gazelletest.model.definition.ContextualInformation;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRoles;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestStepsOption;
import net.ihe.gazelle.tm.tee.model.TmTestStepMessageProfile;

import java.util.*;

public class TestStepDomain {

    private Integer id;
    private TestRoles testRolesInitiator;
    private TestRoles testRolesResponder;
    private Transaction transaction;
    private WSTransactionUsage wstransactionUsage;
    private Integer stepIndex;
    private TestStepsOption testStepsOption;
    private String description;
    private String messageType;
    private String responderMessageType;
    private String hl7Version;
    private Boolean secured;
    private Boolean autoComplete;
    private Boolean autoTriggered;
    private Integer expectedMessageCount;
    private List<ContextualInformation> inputContextualInformationList;
    private List<ContextualInformation> outputContextualInformationList;
    private Test testParent;
    private List<TmTestStepMessageProfile> tmTestStepMessageProfiles;
    private Date lastChanged;
    private String lastModifierId;
    private TestStepType type;
    private String testScriptID;
    private List<AutomatedTestStepInputDomain> inputs;

    public TestStepDomain() {
    }

    public TestStepDomain(TestStepDomain testStep) {
        this.setId(testStep.getId());
        this.setTestRolesInitiator(testStep.getTestRolesInitiator());
        this.setTestRolesResponder(testStep.getTestRolesResponder());
        this.setTransaction(testStep.getTransaction());
        this.setWstransactionUsage(testStep.getWstransactionUsage());
        this.setStepIndex(testStep.getStepIndex());
        this.setTestStepsOption(testStep.getTestStepsOption());
        this.setDescription(testStep.getDescription());
        this.setMessageType(testStep.getMessageType());
        this.setResponderMessageType(testStep.getResponderMessageType());
        this.setHl7Version(testStep.getHl7Version());
        this.setSecured(testStep.getSecured());
        this.setAutoComplete(testStep.getAutoComplete());
        this.setAutoTriggered(testStep.getAutoTriggered());
        this.setExpectedMessageCount(testStep.getExpectedMessageCount());
        this.setInputContextualInformationList(testStep.getInputContextualInformationList());
        this.setOutputContextualInformationList(testStep.getOutputContextualInformationList());
        this.setTestParent(testStep.getTestParent());
        this.setTmTestStepMessageProfiles(testStep.getTmTestStepMessageProfiles());
        this.setLastChanged(testStep.getLastChanged());
        this.setLastModifierId(testStep.getLastModifierId());
        this.setType(testStep.getType());
        if (testStep.isAutomated()) {
            this.setTestScriptID(testStep.getTestScriptID());
            this.setInputs(testStep.getInputs());
        }
    }

    public boolean isAutomated() {
        return TestStepType.AUTOMATED.equals(getType());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TestRoles getTestRolesInitiator() {
        return testRolesInitiator;
    }

    public void setTestRolesInitiator(TestRoles testRolesInitiator) {
        this.testRolesInitiator = testRolesInitiator;
    }

    public TestRoles getTestRolesResponder() {
        return testRolesResponder;
    }

    public void setTestRolesResponder(TestRoles testRolesResponder) {
        this.testRolesResponder = testRolesResponder;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public WSTransactionUsage getWstransactionUsage() {
        return wstransactionUsage;
    }

    public void setWstransactionUsage(WSTransactionUsage wstransactionUsage) {
        this.wstransactionUsage = wstransactionUsage;
    }

    public Integer getStepIndex() {
        return stepIndex;
    }

    public void setStepIndex(Integer stepIndex) {
        this.stepIndex = stepIndex;
    }

    public TestStepsOption getTestStepsOption() {
        return testStepsOption;
    }

    public void setTestStepsOption(TestStepsOption testStepsOption) {
        this.testStepsOption = testStepsOption;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getHl7Version() {
        return hl7Version;
    }

    public void setHl7Version(String hl7Version) {
        this.hl7Version = hl7Version;
    }

    public Boolean getSecured() {
        return secured;
    }

    public void setSecured(Boolean secured) {
        this.secured = secured;
    }

    public String getResponderMessageType() {
        return responderMessageType;
    }

    public void setResponderMessageType(String responderMessageType) {
        this.responderMessageType = responderMessageType;
    }

    public Boolean getAutoComplete() {
        return autoComplete;
    }

    public void setAutoComplete(Boolean autoComplete) {
        this.autoComplete = autoComplete;
    }

    public Boolean getAutoTriggered() {
        return autoTriggered;
    }

    public void setAutoTriggered(Boolean autoTriggered) {
        this.autoTriggered = autoTriggered;
    }

    public Integer getExpectedMessageCount() {
        return expectedMessageCount;
    }

    public void setExpectedMessageCount(Integer expectedMessageCount) {
        this.expectedMessageCount = expectedMessageCount;
    }

    public List<ContextualInformation> getInputContextualInformationList() {
        if (inputContextualInformationList != null) {
            return new ArrayList<>(inputContextualInformationList);
        } else {
            return Collections.emptyList();
        }
    }

    public void setInputContextualInformationList(List<ContextualInformation> inputContextualInformationList) {
        if (inputContextualInformationList != null) {
            this.inputContextualInformationList = new ArrayList<>(inputContextualInformationList);
        }
    }

    public List<ContextualInformation> getOutputContextualInformationList() {
        if (outputContextualInformationList != null) {
            return new ArrayList<>(outputContextualInformationList);
        } else {
            return Collections.emptyList();
        }
    }

    public void setOutputContextualInformationList(List<ContextualInformation> outputContextualInformationList) {
        if (outputContextualInformationList != null) {
            this.outputContextualInformationList = new ArrayList<>(outputContextualInformationList);
        }
    }

    public Test getTestParent() {
        return testParent;
    }

    public void setTestParent(Test testParent) {
        this.testParent = testParent;
    }

    public List<TmTestStepMessageProfile> getTmTestStepMessageProfiles() {
        if (tmTestStepMessageProfiles != null) {
            return new ArrayList<>(tmTestStepMessageProfiles);
        } else {
            return Collections.emptyList();
        }
    }

    public void setTmTestStepMessageProfiles(List<TmTestStepMessageProfile> tmTestStepMessageProfiles) {
        if (tmTestStepMessageProfiles != null) {
            this.tmTestStepMessageProfiles = new ArrayList<>(tmTestStepMessageProfiles);
        }
    }

    public Date getLastChanged() {
        return lastChanged;
    }

    public void setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
    }

    public String getLastModifierId() {
        return lastModifierId;
    }

    public void setLastModifierId(String lastModifierId) {
        this.lastModifierId = lastModifierId;
    }

    public TestStepType getType() {
        return type;
    }

    public void setType(TestStepType type) {
        this.type = type;
    }

    public String getTestScriptID() {
        return testScriptID;
    }

    public void setTestScriptID(String testScriptID) {
        this.testScriptID = testScriptID;
    }

    public List<AutomatedTestStepInputDomain> getInputs() {
        if (inputs != null) {
            Collections.sort(inputs, new Comparator<AutomatedTestStepInputDomain>() {
                @Override
                public int compare(AutomatedTestStepInputDomain o1, AutomatedTestStepInputDomain o2) {
                    return Integer.compare(o1.getId(), o2.getId());
                }
            });
            return new ArrayList<>(inputs);
        } else {
            return Collections.emptyList();
        }
    }

    public void setInputs(List<AutomatedTestStepInputDomain> inputs) {
        if (inputs != null) {
            this.inputs = new ArrayList<>(inputs);
        }
    }

    public void addInput(AutomatedTestStepInputDomain input) {
        if (inputs == null) {
            inputs = new ArrayList<>();
        }
        this.inputs.add(input);
    }

    public int sizeOfInputCI() {
        if (this.inputContextualInformationList == null) {
            return 0;
        }
        return this.inputContextualInformationList.size();
    }

    public int sizeOfOutputCI() {
        if (this.outputContextualInformationList == null) {
            return 0;
        }
        return this.outputContextualInformationList.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestStepDomain)) return false;
        TestStepDomain that = (TestStepDomain) o;
        return Objects.equals(id, that.id)
                && Objects.equals(testRolesInitiator, that.testRolesInitiator)
                && Objects.equals(testRolesResponder, that.testRolesResponder)
                && Objects.equals(transaction, that.transaction)
                && Objects.equals(wstransactionUsage, that.wstransactionUsage)
                && Objects.equals(stepIndex, that.stepIndex)
                && Objects.equals(testStepsOption, that.testStepsOption)
                && Objects.equals(description, that.description)
                && Objects.equals(messageType, that.messageType)
                && Objects.equals(responderMessageType, that.responderMessageType)
                && Objects.equals(hl7Version, that.hl7Version)
                && Objects.equals(secured, that.secured)
                && Objects.equals(autoComplete, that.autoComplete)
                && Objects.equals(autoTriggered, that.autoTriggered)
                && Objects.equals(expectedMessageCount, that.expectedMessageCount)
                && Objects.equals(inputContextualInformationList, that.inputContextualInformationList)
                && Objects.equals(outputContextualInformationList, that.outputContextualInformationList)
                && Objects.equals(testParent, that.testParent)
                && Objects.equals(tmTestStepMessageProfiles, that.tmTestStepMessageProfiles)
                && Objects.equals(lastChanged, that.lastChanged)
                && Objects.equals(lastModifierId, that.lastModifierId)
                && type == that.type && Objects.equals(testScriptID, that.testScriptID)
                && Objects.equals(inputs, that.inputs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, testRolesInitiator, testRolesResponder, transaction, wstransactionUsage, stepIndex, testStepsOption, description, messageType, responderMessageType, hl7Version, secured, autoComplete, autoTriggered, expectedMessageCount, inputContextualInformationList, outputContextualInformationList, testParent, tmTestStepMessageProfiles, lastChanged, lastModifierId, type, testScriptID, inputs);
    }
}
