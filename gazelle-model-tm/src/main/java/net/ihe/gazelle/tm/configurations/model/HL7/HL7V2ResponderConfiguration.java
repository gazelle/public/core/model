/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model.HL7;

import net.ihe.gazelle.tm.configurations.model.Configuration;
import net.ihe.gazelle.tm.configurations.model.interfaces.ServerConfiguration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "tm_hl7_responder_configuration", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class HL7V2ResponderConfiguration extends AbstractHL7Configuration implements ServerConfiguration {

    private static final long serialVersionUID = 4682954974077739886L;

    @Range(min = 0, max = 65635)
    @Column(name = "port")
    private Integer port;

    @Range(min = 1024, max = 65635)
    @Column(name = "port_proxy")
    private Integer portProxy;

    @Range(min = 0, max = 65635)
    @Column(name = "port_out")
    private Integer portOut;

    @Column(name = "port_secure")
    @Range(min = 0, max = 65635)
    private Integer portSecured;

    public HL7V2ResponderConfiguration() {
        super();
    }

    public HL7V2ResponderConfiguration(Configuration inConfiguration, String inSendingReceivingApplication,
                                       String inSendingReceivingFacility, String inAssigningAuthority, String inComments) {
        super(inConfiguration, inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority,
                inComments);
    }

    public HL7V2ResponderConfiguration(Configuration inConfiguration, String inComments) {
        super(inConfiguration, inComments);
    }

    public HL7V2ResponderConfiguration(Configuration inConfiguration) {
        super(inConfiguration);
    }

    public HL7V2ResponderConfiguration(HL7V2ResponderConfiguration hh) {
        super(hh);
        if (hh != null) {
            this.port = hh.port;
            this.portOut = hh.portOut;
            this.portSecured = hh.portSecured;
            getProxyPortIfNeeded(null);
        }
    }

    public HL7V2ResponderConfiguration(Configuration inConfiguration, Integer inPortIn, Integer inPortSecure,
                                       Integer inPortProxy, String inSendingReceivingApplication, String inSendingReceivingFacility,
                                       String inAssigningAuthority, String inAssigningAuthorityOID, String inComments) {
        super(inConfiguration, inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority,
                inComments);
        port = inPortIn;
        portOut = 0;
        portSecured = inPortSecure;
        portProxy = inPortProxy;
    }

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public void setPort(Integer portIn) {
        this.port = portIn;
    }

    public Integer getPortOut() {
        return portOut;
    }

    public void setPortOut(Integer portOut) {
        this.portOut = portOut;
    }

    @Override
    public Integer getPortProxy() {
        return portProxy;
    }

    @Override
    public void setPortProxy(Integer portProxy) {
        this.portProxy = portProxy;
    }

    @Override
    public Integer getPortSecured() {
        return portSecured;
    }

    @Override
    public void setPortSecured(Integer portSecured) {
        this.portSecured = portSecured;
    }

    public Integer getPortIfConfNotSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == false)) {
            return port;
        }
        return null;
    }

    public Integer getPortSecuredIfConfSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == true)) {
            return portSecured;
        }
        return null;
    }

    @Override
    public List<String> getCSVHeaders() {
        List<String> headers = super.getCSVHeaders();
        headers.add("receiving application");
        headers.add("receiving facility");
        addCSVServerHeaders(this, headers);
        return headers;
    }

    @Override
    public List<String> getCSVValues() {
        List<String> values = super.getCSVValues();

        if (this.getSendingReceivingApplication() != null) {
            values.add(this.getSendingReceivingApplication());
        } else {
            values.add("");
        }
        if (this.getSendingReceivingFacility() != null) {
            values.add(this.getSendingReceivingFacility());
        } else {
            values.add("");
        }

        addCSVServerValues(this, values);
        return values;
    }

    @Override
    public String toString() {
        return "HL7V2ResponderConfiguration [port=" + port + ", portProxy=" + portProxy + ", portOut=" + portOut
                + ", portSecured=" + portSecured + ", id=" + id + ", configuration=" + configuration + ", comments="
                + comments + ", lastChanged=" + lastChanged + ", lastModifierId=" + lastModifierId + "]";
    }

}
