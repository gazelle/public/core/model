package net.ihe.gazelle.tm.application.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Class description: Home
 * <p/>
 * This class is used to fill in the home page. Each field of the home page is representing as an attribute. An instance of this object need to be created for each language.
 *
 * @author Anne-Gaëlle Bergé /INRIA Rennes - Bretagne Atlantique
 * @version 1.0 - April 27th 2011
 *          <p/>
 *          Home possesses the following attributes:
 *          <ul>
 *          <li><b>id</b> : ID in the database</li>
 *          <li><b>language</b> : defines for which selected language this object has to be used</li>
 *          <li><b>mainPanelHeader</b> : title of the main rich:panel (with id "MainPanel")</li>
 *          <li><b>mainPanelContent</b> : text contained in the panel entitled "MainPanel"</li>
 *          <li><b>secondaryPanelHeader</b> : title of the main rich:panel (with id "SecondaryPanel")</li>
 *          <li><b>secondaryPanelContent</b> : text contained in the panel entitled "SecondaryPanel". This panel is displayed only if this attribute is not empty</li>
 *          <li><b>secondaryPanelPosition</b> : indicates the relative position of the secondary panel from the main panel</li>
 *          </ul>
 * @class Home.java
 * @package net.ihe.gazelle.tm.application.model
 * @see > anne-gaelle.berge@inria.fr
 */

@Entity
@Table(name = "tm_home", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "language"))
@SequenceGenerator(name = "tm_home_sequence", sequenceName = "tm_home_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Home implements Serializable {


    private static final long serialVersionUID = 5149029516625532787L;
    private static final String ABOVE = "above";
    private static final String BELOW = "below";
    private static Logger log = LoggerFactory.getLogger(Home.class);
    @Id
    @GeneratedValue(generator = "tm_home_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id")
    private Integer id;

    @Column(name = "language")
    private String language;

    @Column(name = "main_panel_header")
    @Lob
    @Type(type = "text")
    private String mainPanelHeader;

    @Column(name = "main_panel_content")
    @Lob
    @Type(type = "text")
    private String mainPanelContent;

    @Column(name = "secondary_panel_header")
    @Lob
    @Type(type = "text")
    private String secondaryPanelHeader;

    @Column(name = "secondary_panel_content")
    @Lob
    @Type(type = "text")
    private String secondaryPanelContent;

    @Column(name = "secondary_panel_position")
    private String secondaryPanelPosition;

    public Home() {

    }

    public Home(String language) {
        this.language = language;
        this.secondaryPanelPosition = "below";
    }

    /**
     * @param inHome
     * @return
     */
    public static Home saveHome(Home inHome) {
        if (inHome != null) {
            EntityManager em = EntityManagerService.provideEntityManager();
            if ((inHome.getMainPanelContent() != null) && inHome.getMainPanelContent().isEmpty()) {
                inHome.setMainPanelContent(null);
            }
            if ((inHome.getSecondaryPanelContent() != null) && inHome.getSecondaryPanelContent().isEmpty()) {
                inHome.setSecondaryPanelContent(null);
            }
            try {
                inHome = em.merge(inHome);
                em.flush();
                return inHome;
            } catch (Exception e) {
                log.error("Cannot save this home", e);
                return null;
            }
        } else {
            return null;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMainPanelHeader() {
        return mainPanelHeader;
    }

    public void setMainPanelHeader(String mainPanelHeader) {
        this.mainPanelHeader = mainPanelHeader;
    }

    public String getMainPanelContent() {
        return mainPanelContent;
    }

    public void setMainPanelContent(String mainPanelContent) {
        this.mainPanelContent = mainPanelContent;
    }

    public String getSecondaryPanelHeader() {
        return secondaryPanelHeader;
    }

    public void setSecondaryPanelHeader(String secondaryPanelHeader) {
        this.secondaryPanelHeader = secondaryPanelHeader;
    }

    public String getSecondaryPanelContent() {
        return secondaryPanelContent;
    }

    public void setSecondaryPanelContent(String secondaryPanelContent) {
        this.secondaryPanelContent = secondaryPanelContent;
    }

    public String getSecondaryPanelPosition() {
        return secondaryPanelPosition;
    }

    public void setSecondaryPanelPosition(String secondaryPanelPosition) {
        this.secondaryPanelPosition = secondaryPanelPosition;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
