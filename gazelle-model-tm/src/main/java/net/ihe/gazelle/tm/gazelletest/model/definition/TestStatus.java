/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>TestStatus<br>
 * <br>
 * This class describes the TestStatus object, used by the Gazelle application. This class belongs to the Test Management module.
 * <p/>
 * TestStatus possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestStatus</li>
 * <li><b>keyword</b> : keyword of the TestStatus.</li>
 * <li><b>description</b> : description of the TestStatus</li>
 * <li><b>labelToDisplay</b> : labelToDisplay of the TestStatus</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         amiladi@irisa.fr
 *         </pre>
 * @version 1.0 , 28 mai 2009
 * @class TestStatus.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@XmlRootElement(name = "testStatus")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tm_test_status", schema = "public")
@SequenceGenerator(name = "tm_test_status_sequence", sequenceName = "tm_test_status_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class TestStatus extends AuditedObject implements Serializable {


    public final static String STATUS_READY_STRING = "ready";
    public final static String STATUS_DEPRECATED_STRING = "deprecated";
    public final static String STATUS_TOBECOMPLETED_STRING = "to be completed";
    public final static String STATUS_STORAGESUBSTITUTE_STRING = "storage/substitute";
    private static final long serialVersionUID = -7731807731694624913L;
    private static Logger log = LoggerFactory.getLogger(TestStatus.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_status_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "keyword", unique = true)
    private String keyword;

    @Column(name = "label_to_display")
    private String labelToDisplay;

    @Column(name = "description")
    private String description;

    public TestStatus() {
    }

    public TestStatus(TestStatus inTestStatus) {
        if (inTestStatus.getDescription() != null) {
            this.description = inTestStatus.getDescription();
        }
        if (inTestStatus.getKeyword() != null) {
            this.keyword = inTestStatus.getKeyword();
        }
        if (inTestStatus.getLabelToDisplay() != null) {
            this.labelToDisplay = inTestStatus.getLabelToDisplay();
        }
    }

    public TestStatus(String keyword, String labelToDisplay, String description) {
        this.description = description;
        this.labelToDisplay = labelToDisplay;
        this.keyword = keyword;
    }

    public static TestStatus getStatusByKeyword(String keyword) {
        EntityManager em = EntityManagerService.provideEntityManager();
        return getStatusByKeyword(em, keyword);
    }

    public static TestStatus getStatusByKeyword(EntityManager em, String keyword) {
        TestStatusQuery q = new TestStatusQuery();
        q.keyword().eq(keyword);
        q.setCachable(true);
        return q.getUniqueResult();
    }

    public static TestStatus getSTATUS_READY() {

        return TestStatus.getStatusByKeyword(STATUS_READY_STRING);
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static TestStatus getSTATUS_DEPRECATED() {
        return TestStatus.getStatusByKeyword(STATUS_DEPRECATED_STRING);
    }

    public static TestStatus getSTATUS_STORAGESUBSTITUTE() {
        return TestStatus.getStatusByKeyword(STATUS_STORAGESUBSTITUTE_STRING);
    }

    public static TestStatus getSTATUS_TOBECOMPLETED() {
        return TestStatus.getStatusByKeyword(STATUS_TOBECOMPLETED_STRING);
    }

    public static List<TestStatus> getListStatus() {
        TestStatusQuery q = new TestStatusQuery();
        q.setCachable(true);
        return q.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @FilterLabel
    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public void setLabelToDisplay(String labelToDisplay) {
        this.labelToDisplay = labelToDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        TestStatusQuery query = new TestStatusQuery();
        query.description().eq(this.description);
        query.labelToDisplay().eq(this.labelToDisplay);
        query.keyword().eq(this.keyword);
        List<TestStatus> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }
}
