/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

/**
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, sep
 * @class ConfigurationMappingWithAIPO.java
 * @package net.ihe.gazelle.tm.systems.model
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "configurationTypeMappedWithAipo")
@XmlAccessorType(XmlAccessType.FIELD)

@Entity
@Audited
@Table(name = "tm_configuration_mapped_with_aipo", schema = "public")
@SequenceGenerator(name = "tm_configuration_mapped_with_aipo_sequence", sequenceName = "tm_configuration_mapped_with_aipo_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class ConfigurationTypeMappedWithAIPO extends AuditedObject implements Serializable,
        Comparable<ConfigurationTypeMappedWithAIPO>, Comparator<ConfigurationTypeMappedWithAIPO> {

    private static final long serialVersionUID = 186149987619456701L;

    private static Logger log = LoggerFactory.getLogger(ConfigurationTypeMappedWithAIPO.class);

    /**
     * Id attribute!
     */
    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_configuration_mapped_with_aipo_sequence")
    private Integer id;

    @ManyToOne
    // JRC : Remove nullable constraint in : @JoinColumn(name = "aipo_id", nullable = false , unique=true ) to be able to sync ConfigurationTypeMappedWithAIPO object with GMM
    @JoinColumn(name = "aipo_id", unique = true, nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private ActorIntegrationProfileOption actorIntegrationProfileOption;

    @XmlElementWrapper(name = "configurationTypeWithPortsWSTypeAndSopClassList")
    @XmlElement(name="configurationTypeWithPortsWSTypeAndSopClass")
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "tm_conf_mapping_w_aipo_w_conftypes", joinColumns = @JoinColumn(name = "tm_conf_mapped_id"), inverseJoinColumns = @JoinColumn(name = "tm_conftypes_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "tm_conf_mapped_id", "tm_conftypes_id"}))
    // @OneToMany(mappedBy = "configurationTypeMappedWithAIPO", fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<ConfigurationTypeWithPortsWSTypeAndSopClass> listOfConfigurationTypes;

    public ConfigurationTypeMappedWithAIPO() {

    }

    public ConfigurationTypeMappedWithAIPO(ConfigurationTypeMappedWithAIPO inConfiguration) {
        if (inConfiguration.actorIntegrationProfileOption != null) {
            actorIntegrationProfileOption = new ActorIntegrationProfileOption(
                    inConfiguration.actorIntegrationProfileOption);
        }
    }

    public static List<ConfigurationTypeMappedWithAIPO> getConfigurationTypeMappingWithAIPOFiltered(
            ActorIntegrationProfileOption inActorIntegrationProfileOption, Actor inActor,
            ConfigurationTypeWithPortsWSTypeAndSopClass inConfigurationTypeWPorts) {
        ConfigurationTypeMappedWithAIPOQuery query = new ConfigurationTypeMappedWithAIPOQuery();
        query.actorIntegrationProfileOption().eqIfValueNotNull(inActorIntegrationProfileOption);
        query.actorIntegrationProfileOption().actorIntegrationProfile().actor().eqIfValueNotNull(inActor);
        if (inConfigurationTypeWPorts != null) {
            query.listOfConfigurationTypes().id().eq(inConfigurationTypeWPorts.getId());
        }
        return query.getList();
    }

    public static List<ConfigurationTypeMappedWithAIPO> getAllConfigurationTypeMappedWithAIPO() {
        ConfigurationTypeMappedWithAIPOQuery query = new ConfigurationTypeMappedWithAIPOQuery();
        query.actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile().keyword().order(true);
        return query.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ActorIntegrationProfileOption getActorIntegrationProfileOption() {
        return actorIntegrationProfileOption;
    }

    public void setActorIntegrationProfileOption(ActorIntegrationProfileOption actorIntegrationProfileOption) {
        this.actorIntegrationProfileOption = actorIntegrationProfileOption;
    }

    public List<ConfigurationTypeWithPortsWSTypeAndSopClass> getListOfConfigurationTypes() {
        return listOfConfigurationTypes;
    }

    public void setListOfConfigurationTypes(List<ConfigurationTypeWithPortsWSTypeAndSopClass> listOfConfigurationTypes) {
        this.listOfConfigurationTypes = listOfConfigurationTypes;
    }

    @Override
    public int compareTo(ConfigurationTypeMappedWithAIPO o) {

        if (o == null) {
            return -1;
        }
        if (o.getActorIntegrationProfileOption() == null) {
            return -1;
        }
        if (getActorIntegrationProfileOption() == null) {
            return 1;
        }

        return getActorIntegrationProfileOption().compareTo(o.getActorIntegrationProfileOption());
    }

    @Override
    public int compare(ConfigurationTypeMappedWithAIPO o1, ConfigurationTypeMappedWithAIPO o2) {
        return o1.getActorIntegrationProfileOption().compareTo(o2.getActorIntegrationProfileOption());
    }

}
