package net.ihe.gazelle.tm.systems.model;

public enum SystemInSessionRegistrationStatus {
    IN_PROGRESS("In progress", "IN_PROGRESS", 0, "net.ihe.gazelle.tm.RegistrationStatus.in_progress", "gzl-label gzl-label-blue"),
    DROPPED("Dropped", "DROPPED", 1, "net.ihe.gazelle.tm.RegistrationStatus.dropped", "gzl-label gzl-label-red"),
    COMPLETED("Completed", "COMPLETED", 2, "net.ihe.gazelle.tm.RegistrationStatus.completed", "gzl-label gzl-label-green");

    private String friendlyName;

    private String keyword;

    private String labelToDisplay;

    private int weight;

    private String cssStyle;

    SystemInSessionRegistrationStatus(String friendlyName, String keyword, int weight, String labelToDisplay,
                                      String cssStyle) {
        this.friendlyName = friendlyName;
        this.keyword = keyword;
        this.setLabelToDisplay(labelToDisplay);
        this.weight = weight;
        this.setCssStyle(cssStyle);
    }

    public static SystemInSessionRegistrationStatus valueOfByKeyword(String keyword) throws IllegalArgumentException {
        for (SystemInSessionRegistrationStatus value : values()) {
            String docKeyword = value.getKeyword();
            if (docKeyword.equals(keyword)) {
                return value;
            }
        }
        throw new IllegalArgumentException("keyword " + keyword + "is not defined in DocumentLifeCycleStatus");
    }

    public static SystemInSessionRegistrationStatus[] getPossibleRegistrationStatusBeforeDeadLine() {
        SystemInSessionRegistrationStatus[] status = {SystemInSessionRegistrationStatus.IN_PROGRESS,
                SystemInSessionRegistrationStatus.COMPLETED};
        return status;
    }

    public static SystemInSessionRegistrationStatus[] getPossibleRegistrationStatusAfterDeadLine() {
        SystemInSessionRegistrationStatus[] status = {SystemInSessionRegistrationStatus.COMPLETED,
                SystemInSessionRegistrationStatus.DROPPED};
        return status;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public void setLabelToDisplay(String labelToDisplay) {
        this.labelToDisplay = labelToDisplay;
    }

    public String getCssStyle() {
        return cssStyle;
    }

    public void setCssStyle(String cssStyle) {
        this.cssStyle = cssStyle;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}