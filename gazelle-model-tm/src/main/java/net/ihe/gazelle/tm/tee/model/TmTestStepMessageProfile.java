package net.ihe.gazelle.tm.tee.model;

import net.ihe.gazelle.tf.model.Hl7MessageProfile;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestSteps;
import org.apache.commons.lang.ObjectUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The persistent class for the tm_test_step_message_profile database table.
 *
 * @author tnabeel
 */
@XmlRootElement(name = "tmTestStepMessageProfile")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "tm_test_step_message_profile")
@SequenceGenerator(name = "tm_test_step_message_profile_id_generator", sequenceName = "tm_test_step_message_profile_sequence")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class TmTestStepMessageProfile implements Serializable {

    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(TmTestStepMessageProfile.class);

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_step_message_profile_id_generator")
    @XmlTransient
    private int id;

    @Column(name = "auto_validate")
    private byte autoValidate;

    @Enumerated(EnumType.ORDINAL)
    private MessageDirection direction;

    @OneToMany(mappedBy = "tmTestStepMessageProfile", fetch = FetchType.EAGER)
    @XmlTransient
    private Set<TmStepInstanceMessage> tmStepInstanceMessages;

    @OneToMany(mappedBy = "tmTestStepMessageProfile", fetch = FetchType.EAGER)
    private Set<MessageValidationService> messageValidationServices;

    @ManyToOne
    @JoinColumn(name = "tf_hl7_message_profile_id")
    private Hl7MessageProfile tfHl7MessageProfile;

    @ManyToOne
    @JoinColumn(name = "tm_test_steps_id")
    @XmlTransient
    private TestSteps tmTestStep;

    // /---###--- Config Files
    @Column(name = "validation_context_content", length = 2000000)
    @Basic(fetch = FetchType.LAZY)
    private String validationContextContent;

    @Column(name = "validation_context_file_name")
    private String validationContextFileName;

    @Column(name = "example_msg_content", length = 2000000)
    @Basic(fetch = FetchType.LAZY)
    private String exampleMsgContent;

    @Column(name = "example_msg_file_name")
    private String exampleMsgFileName;

    public TmTestStepMessageProfile() {
    }

    public TmTestStepMessageProfile(TmTestStepMessageProfile msgType) {
        this.autoValidate = msgType.autoValidate;
        this.direction = msgType.direction;
        this.tfHl7MessageProfile = msgType.tfHl7MessageProfile;
        this.validationContextContent = msgType.getValidationContextContent();
        this.validationContextFileName = msgType.getValidationContextFileName();
        this.exampleMsgContent = msgType.getExampleMsgContent();
        this.exampleMsgFileName = msgType.getExampleMsgFileName();
        this.messageValidationServices = this.copyMessageValidationServices(msgType.messageValidationServices);
    }

    public static void deleteTmTestStepMessageProfile(EntityManager em, TmTestStepMessageProfile msgType) {

        if (msgType == null) {

            return;
        }

        if (msgType.getTmStepInstanceMessages() != null) {
            if (msgType.getTmStepInstanceMessages().size() > 0) {

                return;
            }
        }

        // before we delete the TmTestStepMessageProfile object,
        // we need to delete all associated MessageValidationService records
        for (MessageValidationService srv : msgType.getMessageValidationServices()) {
            srv = em.find(MessageValidationService.class, srv.getId());
            em.remove(srv);
            em.flush();
        }


        msgType = em.find(TmTestStepMessageProfile.class, msgType.getId());
        em.remove(msgType);
        em.flush();
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte getAutoValidate() {
        return this.autoValidate;
    }

    public void setAutoValidate(byte autoValidate) {
        this.autoValidate = autoValidate;
    }

    public MessageDirection getDirection() {
        return this.direction;
    }

    public void setDirection(MessageDirection direction) {
        this.direction = direction;
    }

    public Set<TmStepInstanceMessage> getTmStepInstanceMessages() {
        if (this.tmStepInstanceMessages == null) {
            this.tmStepInstanceMessages = new HashSet<TmStepInstanceMessage>();
        }
        return this.tmStepInstanceMessages;
    }

    public void setTmStepInstanceMessages(Set<TmStepInstanceMessage> tmStepInstanceMessages) {
        this.tmStepInstanceMessages = tmStepInstanceMessages;
    }

    public Set<MessageValidationService> getMessageValidationServices() {
        if (this.messageValidationServices == null) {
            this.messageValidationServices = new HashSet<MessageValidationService>();
        }
        return this.messageValidationServices;
    }

    public void setMessageValidationServices(Set<MessageValidationService> messageValidationServices) {
        this.messageValidationServices = messageValidationServices;
    }

    public Hl7MessageProfile getTfHl7MessageProfile() {
        return this.tfHl7MessageProfile;
    }

    public void setTfHl7MessageProfile(Hl7MessageProfile tfHl7MessageProfile) {
        this.tfHl7MessageProfile = tfHl7MessageProfile;
    }

    public TestSteps getTmTestStep() {
        return this.tmTestStep;
    }

    public void setTmTestStep(TestSteps tmTestStep) {
        this.tmTestStep = tmTestStep;
    }

    public String getValidationContextContent() {
        return validationContextContent;
    }

    public void setValidationContextContent(String validationContextContent) {
        this.validationContextContent = validationContextContent;
    }

    public String getValidationContextFileName() {
        return validationContextFileName;
    }

    public void setValidationContextFileName(String validationContextFileName) {
        this.validationContextFileName = validationContextFileName;
    }

    public String getExampleMsgContent() {
        return exampleMsgContent;
    }

    public void setExampleMsgContent(String exampleMsgContent) {
        this.exampleMsgContent = exampleMsgContent;
    }

    public String getExampleMsgFileName() {
        return exampleMsgFileName;
    }

    // ======================
    // Helper Methods
    // ======================

    public void setExampleMsgFileName(String exampleMsgFileName) {
        this.exampleMsgFileName = exampleMsgFileName;
    }

    private Set<MessageValidationService> copyMessageValidationServices(Set<MessageValidationService> services) {
        Set<MessageValidationService> set = new HashSet<MessageValidationService>();
        for (MessageValidationService service : services) {
            MessageValidationService temp = new MessageValidationService();
            temp.setValidationService(service.getValidationService());
            set.add(temp);
        }
        return set;
    }

    public TmStepInstanceMessage addTmStepInstanceMessage(TmStepInstanceMessage tmStepInstanceMessage) {
        getTmStepInstanceMessages().add(tmStepInstanceMessage);
        tmStepInstanceMessage.setTmTestStepMessageProfile(this);

        return tmStepInstanceMessage;
    }

    public TmStepInstanceMessage removeTmStepInstanceMessage(TmStepInstanceMessage tmStepInstanceMessage) {
        getTmStepInstanceMessages().remove(tmStepInstanceMessage);
        tmStepInstanceMessage.setTmTestStepMessageProfile(null);

        return tmStepInstanceMessage;
    }

    public MessageValidationService addMessageValidationService(MessageValidationService messageValidationService) {
        getMessageValidationServices().add(messageValidationService);
        messageValidationService.setTmTestStepMessageProfile(this);
        return messageValidationService;
    }

    public MessageValidationService removeMessageValidationService(MessageValidationService messageValidationService) {
        getMessageValidationServices().remove(messageValidationService);
        messageValidationService.setTmTestStepMessageProfile(null);

        return messageValidationService;
    }

    @Override
    public String toString() {
        return "TmTestStepMessageProfile [id=" + id + ", direction=" + direction + ", \nvalidationContextContent="
                + validationContextContent + ", validationContextFileName=" + validationContextFileName + "]";
    }

    public void saveOrMerge(EntityManager entityManager, TestSteps tmTestStep) {
        this.tmTestStep = tmTestStep;
        if (tfHl7MessageProfile != null) {
            tfHl7MessageProfile = tfHl7MessageProfile.saveOrMerge(entityManager);
        }

        Set<MessageValidationService> messageValidationServicesBackup = (Set<MessageValidationService>) ObjectUtils
                .clone(messageValidationServices);
        messageValidationServices = null;

        TmTestStepMessageProfileQuery query = new TmTestStepMessageProfileQuery();
        query.tfHl7MessageProfile().eq(this.tfHl7MessageProfile);
        query.autoValidate().eq(this.autoValidate);
        query.direction().eq(this.direction);
        query.validationContextContent().eq(this.validationContextContent);
        query.validationContextFileName().eq(this.validationContextFileName);
        query.exampleMsgContent().eq(this.exampleMsgContent);
        query.exampleMsgFileName().eq(this.exampleMsgFileName);

        List<TmTestStepMessageProfile> listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }

        if (messageValidationServicesBackup != null) {
            for (MessageValidationService messageValidationService : messageValidationServicesBackup) {
                messageValidationService.saveOrMerge(entityManager, this);
            }
        }

        messageValidationServices = messageValidationServicesBackup;

        entityManager.merge(this);
    }

}