package net.ihe.gazelle.tm.tools;

import net.ihe.gazelle.tm.gazelletest.model.instance.Status;

import java.util.Comparator;

public class StatusComparator implements Comparator<Status> {

    public static final Comparator<Status> INSTANCE = new StatusComparator();

    @Override
    public int compare(Status o1, Status o2) {
        return o1.getWeight() - o2.getWeight();
    }

}
