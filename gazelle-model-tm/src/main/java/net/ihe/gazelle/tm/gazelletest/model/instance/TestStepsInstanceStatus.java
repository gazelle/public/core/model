/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetGazelle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>TestStepsInstanceStatus<br>
 * <br>
 * This class describes the TestStepsInstanceStatus object, used by the Gazelle application. This class belongs to the Test Management module.
 * <p/>
 * TestStepsInstanceStatus possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestStepsInstanceStatus</li>
 * <li><b>keyword</b> : keyword of the TestStepsInstanceStatus.</li>
 * <li><b>description</b> : description of the TestStepsInstanceStatus</li>
 * <li><b>labelToDisplay</b> : labelToDisplay of the TestStepsInstanceStatus</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         amiladi@irisa.fr
 *         </pre>
 * @version 1.0 , 12 october 2009
 * @class TestStepsInstanceStatus.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */
@Entity
@Table(name = "tm_test_steps_instance_status", schema = "public")
@SequenceGenerator(name = "tm_test_steps_instance_status_sequence", sequenceName = "tm_test_steps_instance_status_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class TestStepsInstanceStatus implements Serializable {


    public final static String STATUS_FAILED_STRING = "failed";
    public final static String STATUS_DONE_STRING = "done";
    public final static String STATUS_ACTIVATED_STRING = "activated";
    public final static String STATUS_DISACTIVATED_STRING = "disactivated";
    public final static String STATUS_ACTIVATED_SIMU_STRING = "activated-sim";
    public final static String STATUS_SKIPPED_STRING = "skipped";
    public final static String STATUS_VERIFIED_STRING = "verified";
    private static final long serialVersionUID = -6696581774593313658L;
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_steps_instance_status_sequence")
    private Integer id;

    @Column(name = "keyword", unique = true)
    private String keyword;

    @Column(name = "label_to_display")
    private String labelToDisplay;

    @Column(name = "description")
    private String description;

    public TestStepsInstanceStatus() {
    }

    public TestStepsInstanceStatus(TestStepsInstanceStatus inTestStepsInstanceStatus) {
        if (inTestStepsInstanceStatus.getDescription() != null) {
            this.description = inTestStepsInstanceStatus.getDescription();
        }
        if (inTestStepsInstanceStatus.getKeyword() != null) {
            this.keyword = inTestStepsInstanceStatus.getKeyword();
        }
        if (inTestStepsInstanceStatus.getLabelToDisplay() != null) {
            this.labelToDisplay = inTestStepsInstanceStatus.getLabelToDisplay();
        }
    }

    public TestStepsInstanceStatus(String keyword, String labelToDisplay, String description) {
        this.description = description;
        this.labelToDisplay = labelToDisplay;
        this.keyword = keyword;
    }

    public static TestStepsInstanceStatus getStatusByKeyword(String keyword) {
        TestStepsInstanceStatusQuery q = new TestStepsInstanceStatusQuery();
        q.setCachable(true);
        q.keyword().eq(keyword);
        return q.getUniqueResult();
    }

    public static List<TestStepsInstanceStatus> getListStatus() {
        TestStepsInstanceStatusQuery q = new TestStepsInstanceStatusQuery();
        q.setCachable(true);
        return q.getList();
    }

    public static TestStepsInstanceStatus getSTATUS_FAILED() {
        return TestStepsInstanceStatus.getStatusByKeyword(STATUS_FAILED_STRING);
    }

    public static TestStepsInstanceStatus getSTATUS_DONE() {
        return TestStepsInstanceStatus.getStatusByKeyword(STATUS_DONE_STRING);
    }

    public static TestStepsInstanceStatus getSTATUS_ACTIVATED() {
        return TestStepsInstanceStatus.getStatusByKeyword(STATUS_ACTIVATED_STRING);
    }

    public static TestStepsInstanceStatus getSTATUS_ACTIVATED_SIMU() {
        return TestStepsInstanceStatus.getStatusByKeyword(STATUS_ACTIVATED_SIMU_STRING);
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static TestStepsInstanceStatus getSTATUS_DISACTIVATED() {
        return TestStepsInstanceStatus.getStatusByKeyword(STATUS_DISACTIVATED_STRING);
    }

    public static TestStepsInstanceStatus getSTATUS_SKIPPED() {
        return TestStepsInstanceStatus.getStatusByKeyword(STATUS_SKIPPED_STRING);
    }

    public static TestStepsInstanceStatus getSTATUS_VERIFIED() {
        return TestStepsInstanceStatus.getStatusByKeyword(STATUS_VERIFIED_STRING);
    }

    public static List<TestStepsInstanceStatus> getStatusListForActivatedTestSteps() {
        List<TestStepsInstanceStatus> testStepsInstanceStatusList = new ArrayList<TestStepsInstanceStatus>();
        testStepsInstanceStatusList.add(TestStepsInstanceStatus.getSTATUS_DONE());
        testStepsInstanceStatusList.add(TestStepsInstanceStatus.getSTATUS_FAILED());
        return testStepsInstanceStatusList;
    }

    public Integer getId() {
        return id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public void setLabelToDisplay(String labelToDisplay) {
        this.labelToDisplay = labelToDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

    @Override
    public String toString() {
        return "TestStepsInstanceStatus [id=" + id + ", keyword=" + keyword + "]";
    }

}
