package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;

@Entity
@DiscriminatorValue("SIM")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Simulator extends System {


    private static final long serialVersionUID = 1L;

    @Column(name = "url")
    private String url;

    public Simulator() {

    }

    public Simulator(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void removeSimulatorWithFind(Simulator inSimulator) throws Exception {
        EntityManager em = EntityManagerService.provideEntityManager();

        try {
            Simulator simulator = em.find(Simulator.class, inSimulator.getId());
            em.remove(simulator);
        } catch (Exception e) {
            throw new Exception("A Simulator cannot be deleted -  keyword = " + inSimulator.getKeyword(), e);
        }
    }

}
