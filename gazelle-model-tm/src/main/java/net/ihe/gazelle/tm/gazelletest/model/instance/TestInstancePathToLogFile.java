/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.FileTypeMap;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.Serializable;

/**
 * <b>Class Description : </b>TestInstancePathToLogFile<br>
 * <br>
 * This class stores the path of a log for a test instance.
 * <p/>
 * TestInstance possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestInstancePathToLogFile</li>
 * <li><b>path</b> : String of the filename containing the log stored</li>
 * <li><b>type</b> : String representing the type of file, xml, dicom..., will be probably change to a class containing possible types</li>
 * </ul>
 * </br>
 *
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         jmeyer@irisa.fr
 *         </pre>
 * @version 1.0 , Feb. 13. 2009
 * @class TestInstancePathToLogFile.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@Entity
@Table(name = "tm_test_instance_path_to_log_file", schema = "public")
@SequenceGenerator(name = "tm_test_instance_path_to_log_file_sequence", sequenceName = "tm_test_instance_path_to_log_file_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class TestInstancePathToLogFile extends AuditedObject implements Serializable {

    private static final long serialVersionUID = 280418297624123345L;

    private static Logger log = LoggerFactory.getLogger(TestInstancePathToLogFile.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_instance_path_to_log_file_sequence")
    private Integer id;

    @Column(name = "path", unique = true)
    @NotNull
    private String path;

    @Column(name = "filename")
    private String filename;

    @Column(name = "type")
    @NotNull
    @Deprecated
    private String type;

    @Column(name = "withoutFileName")
    private Boolean withoutFileName;

    public TestInstancePathToLogFile() {

    }

    public TestInstancePathToLogFile(TestInstancePathToLogFile testToCopy) {
        if (testToCopy.getPath() != null) {
            path = new String(testToCopy.getPath());
        }
        if (testToCopy.getType() != null) {
            type = new String(testToCopy.getType());
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        if ((withoutFileName != null) && withoutFileName) {
            return FileTypeMap.getDefaultFileTypeMap().getContentType(filename);
        } else {
            return FileTypeMap.getDefaultFileTypeMap().getContentType(path);
        }
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getWithoutFileName() {
        return withoutFileName;
    }

    public void setWithoutFileName(Boolean withoutFileName) {
        this.withoutFileName = withoutFileName;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getName() {
        if ((withoutFileName != null) && withoutFileName) {
            return filename;
        } else {
            int indexOfSeparator = path.lastIndexOf(java.io.File.separatorChar);
            if (indexOfSeparator >= 0) {
                return path.substring(indexOfSeparator + 1);
            } else {
                return path;
            }
        }
    }

    public File getFile() {
        return new File(PreferenceService.getString("gazelle_home_path") + File.separatorChar
                + PreferenceService.getString("data_path") + File.separatorChar
                + PreferenceService.getString("log_return_path") + java.io.File.separatorChar + this.getPath());
    }

    public void deleteTestPathToLogFile() {
        EntityManager em = EntityManagerService.provideEntityManager();

        em.remove(this);

        String fullAbsolutePath = PreferenceService.getString("gazelle_home_path") + File.separatorChar
                + PreferenceService.getString("data_path") + File.separatorChar
                + PreferenceService.getString("log_return_path") + java.io.File.separatorChar + this.getPath();

        try {
            java.io.File f = new java.io.File(fullAbsolutePath);
            f.delete();
        } catch (Exception e) {
            log.error("deleteTestPathToLogFile", e);

        }
        em.flush();
    }

}
