/*
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.users.model.Institution;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description : </b>ConSystemStatus<br>
 * <br>
 * This class describes the states a system can take during a testing session/connectathon. States are concepts such as ready, busy.
 *
 * @author Abderrazek Boufahja
 * @version 1.0
 * @class OIDInstitution.java
 * @package net.ihe.gazelle.tm.systems.model
 * @see > abderrazek.boufahja@irisa.fr
 */

@Entity
@Table(name = "tm_oid_institution", schema = "public")
@SequenceGenerator(name = "tm_oid_institution_sequence", sequenceName = "tm_oid_institution_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class OIDInstitution implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450985131541283760L;

    private static Logger log = LoggerFactory.getLogger(OIDInstitution.class);
    // Attributes (existing in database as a column)

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_oid_institution_sequence")
    private Integer id;

    private String oid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "institution_id")
    @Fetch(value = FetchMode.SELECT)
    private Institution institution;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "testing_session_id")
    @Fetch(value = FetchMode.SELECT)
    private TestingSession testingSession;

    // Constructor

    public OIDInstitution() {
    }

    public OIDInstitution(String oid, Institution institution, TestingSession testingSession) {
        super();
        this.oid = oid;
        this.institution = institution;
        this.testingSession = testingSession;
    }

    // getter and setter /////////////////////

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger log) {
        OIDInstitution.log = log;
    }

    public static List<OIDInstitution> getOIDInstitutionFiltered(Institution inInstitution,
                                                                 TestingSession inTestingSession, String oid) {
        if ((inInstitution == null) && (inTestingSession == null) && (oid == null)) {
            return null;
        }

        OIDInstitutionQuery query = new OIDInstitutionQuery();
        query.institution().eqIfValueNotNull(inInstitution);
        query.testingSession().eqIfValueNotNull(inTestingSession);
        query.oid().eqIfValueNotNull(oid);

        return query.getList();
    }

    @SuppressWarnings("unchecked")
    public static List<OIDInstitution> getAllOIDInstitution() {
        return new OIDInstitutionQuery().getList();
    }

    @SuppressWarnings("unchecked")
    public static List<OIDInstitution> getAllOIDInstitutionForCurrentTestingSession(TestingSession selectedTestingSession) {
        return getOIDInstitutionFiltered(null, selectedTestingSession, null);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Institution getInstitution() {
        return institution;
    }

    // hashcode and equals /////////

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

    @Override
    public String toString() {
        return "OIDInstitution [id=" + id + ", oid=" + oid + ", institution=" + institution.getKeyword()
                + ", testingSession=" + testingSession.getDescription() + "]";
    }

}
