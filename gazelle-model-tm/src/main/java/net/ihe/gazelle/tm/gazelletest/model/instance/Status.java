/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.tm.tools.StatusComparator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <b>Class Description : </b>Status<br>
 * <br>
 * This class describes the Status object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * Status possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Status</li>
 * <li><b>keyword</b> : keyword of the Status.</li>
 * <li><b>description</b> : description of the Status</li>
 * <li><b>labelToDisplay</b> : labelToDisplay of the Status</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *                 http://ihe.irisa.fr
 *                 </pre>
 *
 *         <pre>
 *                 amiladi@irisa.fr
 *                 </pre>
 * @version 1.0 , 27 janv. 2009
 * @class Status.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

public enum Status {

    BROKEN(false, 11, "broken", "Test broken", "Test broken, do not run", "gzl-label-default"),

    STARTED(true, 0, "started", "gazelle.tm.testing.status.running", "When the instance is started and currently running", "gzl-label-default"),

    COMPLETED(true, 1, "completed", "gazelle.tm.testing.status.completed", "When the instance is completed", "gzl-label gzl-label-blue"),

    PAUSED(true, 2, "paused", "gazelle.tm.testing.status.paused", "Test paused", "gzl-label-default"),

    VERIFIED(true, 3, "verified", "gazelle.tm.testing.status.verified", "When the test is verified", "gzl-label gzl-label-green"),

    ABORTED(true, 4, "aborted", "gazelle.tm.testing.status.aborted", "test aborted", "gzl-label-default"),

    PARTIALLY_VERIFIED(true, 5, "partially verified", "gazelle.tm.testing.status.partiallyVerified", "partially verified", "gzl-label gzl-label-yellow"),

    FAILED(true, 6, "failed", "gazelle.tm.testing.status.failed", "Test failed", "gzl-label gzl-label-red"),

    CRITICAL(true, 7, "critical", "gazelle.tm.testing.status.critical", "Critical test", "gzl-label-default gzl-label-orange"),

    SELF_VERIFIED(true, 8, "self verified", "gazelle.tm.testing.status.selfVerified", "The user asserts the test is finished and correctly done", "gzl-label gzl-label-green"),

    SUPPORTIVE(true, 9, "supportive", "gazelle.tm.testing.status.supportive", "The user asserts supportive testing/no results required", "gzl-label-default"),

    COMPLETED_ERRORS(true, 10, "completed errors", "gazelle.tm.testing.status.completedwitherrors", "When the instance is completed but with errors", "gzl-label gzl-label-red");

    private String keyword;

    private String labelToDisplay;

    private String description;

    private int weight;

    private boolean visible;
    private String cssStyle;

    Status(boolean visible, int weight, String keyword, String labelToDisplay, String description, String cssStyle) {
        this.description = description;
        this.labelToDisplay = labelToDisplay;
        this.keyword = keyword;
        this.weight = weight;
        this.visible = visible;
        this.cssStyle = cssStyle;
    }

    public static Status getSTATUS_PARTIALLY_VERIFIED() {
        return PARTIALLY_VERIFIED;
    }

    public static Status getSTATUS_STARTED() {
        return STARTED;
    }

    public static Status getSTATUS_COMPLETED() {
        return COMPLETED;
    }

    public static Status getSTATUS_PAUSED() {
        return PAUSED;
    }

    public static Status getSTATUS_VERIFIED() {
        return VERIFIED;
    }

    public static Status getSTATUS_ABORTED() {
        return ABORTED;
    }

    public static Status getSTATUS_FAILED() {
        return FAILED;
    }

    public static Status getSTATUS_CRITICAL() {
        return CRITICAL;
    }

    public static Status getStatusByKeyword(String keyword) {
        return getStatusByKeyword(keyword, null);
    }

    public static Status getStatusByKeyword(String keyword, EntityManager em) {
        for (Status status : values()) {
            if (status.getKeyword().equals(keyword)) {
                return status;
            }
        }
        return null;
    }

    public static String getGraphColor(Status lastStatus) {
        switch (lastStatus) {
            case ABORTED:
                return "brown1";
            case COMPLETED:
                return "#CCFFCC";
            case CRITICAL:
                return "chocolate1";
            case FAILED:
                return "red";
            case PARTIALLY_VERIFIED:
                return "crimson";
            case PAUSED:
                return "darkgoldenrod1";
            case STARTED:
                return "blue";
            case VERIFIED:
                return "green";
            default:
                return "red";
        }
    }

    public static List<Status> getListStatus() {
        Status[] values = Status.values();
        List<Status> result = new ArrayList<Status>();
        for (Status status : values) {
            if (status.visible) {
                result.add(status);
            }
        }
        Collections.sort(result, StatusComparator.INSTANCE);
        return result;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public String getDescription() {
        return description;
    }

    public int getWeight() {
        return weight;
    }

    public boolean isVisible() {
        return visible;
    }

    public String getCssStyle() {
        return cssStyle;
    }
}
