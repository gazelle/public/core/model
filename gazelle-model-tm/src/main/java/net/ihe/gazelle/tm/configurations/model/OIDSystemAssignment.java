/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.csv.CSVExportable;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author abderrazek boufahja / INRIA Rennes IHE development Project
 */
@Entity
@Table(name = "tm_oid_system_assignment", uniqueConstraints = @UniqueConstraint(columnNames = {"system_in_session_id",
        "oid_requirement_id"}))
@SequenceGenerator(name = "tm_oid_system_assignment_sequence", sequenceName = "tm_oid_system_assignment_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class OIDSystemAssignment extends AuditedObject implements Serializable, CSVExportable {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    private static final long serialVersionUID = 1881422545451441951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_oid_system_assignment_sequence")
    private Integer id;

    @Column(name = "comment")
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "system_in_session_id")
    @Fetch(value = FetchMode.SELECT)
    private SystemInSession systemInSession;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "oid_requirement_id")
    @Fetch(value = FetchMode.SELECT)
    private OIDRequirement oidRequirement;

    @Column(name = "oid_value", unique = true)
    private String oid;

    // constructors ///////////////////////////////////////////////////////////////////////////////////////////////////

    public OIDSystemAssignment() {
    }

    public OIDSystemAssignment(String comment, SystemInSession systemInSession, OIDRequirement oidRequirement,
                               String oid) {
        super();
        this.comment = comment;
        this.systemInSession = systemInSession;
        this.oidRequirement = oidRequirement;
        this.oid = oid;
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static List<OIDSystemAssignment> getAllOIDSystemAssignment() {
        return new OIDSystemAssignmentQuery().getList();
    }

    //TODO I have passed FetchType.LAZY to escape SerializationException: could not deserialize EOFException
    public static List<OIDSystemAssignment> getOIDSystemAssignmentFiltered(SystemInSession sis, String label,
                                                                           TestingSession ts, ActorIntegrationProfileOption aipo) {
        OIDSystemAssignmentQuery query = new OIDSystemAssignmentQuery();
        query.systemInSession().eqIfValueNotNull(sis);
        query.oidRequirement().label().eqIfValueNotNull(label);
        query.systemInSession().testingSession().eqIfValueNotNull(ts);
        if (aipo != null) {
            query.oidRequirement().actorIntegrationProfileOptionList().id().eq(aipo.getId());
        }
        return query.getList();
    }

    public static void deleteAllOIDSystemAssignment(SystemInSession sis, String label, TestingSession ts,
                                                    ActorIntegrationProfileOption aipo, EntityManager em) {
        List<OIDSystemAssignment> losa = OIDSystemAssignment.getOIDSystemAssignmentFiltered(sis, label, ts, aipo);
        if (losa != null) {
            for (OIDSystemAssignment oidSystemAssignment : losa) {
                em.remove(oidSystemAssignment);
                em.flush();
            }
        }
        em.flush();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public SystemInSession getSystemInSession() {
        return systemInSession;
    }

    public void setSystemInSession(SystemInSession systemInSession) {
        this.systemInSession = systemInSession;
    }

    public OIDRequirement getOidRequirement() {
        return oidRequirement;
    }

    public void setOidRequirement(OIDRequirement oidRequirement) {
        this.oidRequirement = oidRequirement;
    }

    // methods ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @Override
    public String toString() {
        return "OIDSystemAssignment [id=" + id + ", comment=" + comment + ", systemInSession=" + systemInSession
                + ", oidRequirement=" + oidRequirement + ", oid=" + oid + "]";
    }

    public String getOIDValue() {
        String res = "";
        if (this.oidRequirement != null) {
            if (this.oidRequirement.getPrefix() != null) {
                res = this.oidRequirement.getPrefix();
            }
        }
        res = res + this.oid;
        return res;
    }

    public String getLastIndex() {
        if (this.oid != null) {
            String[] ll = this.oid.split("\\.");

            if (ll != null) {
                if (ll.length > 0) {
                    return ll[ll.length - 1];
                }
            }
        }
        return null;
    }

    @Override
    public List<String> getCSVHeaders() {
        List<String> result = new ArrayList<String>();
        result.add("system");
        result.add("oid requirement");
        result.add("OID");
        return result;
    }

    @Override
    public List<String> getCSVValues() {
        List<String> result = new ArrayList<String>();
        if (this.systemInSession != null) {
            result.add(this.systemInSession.getSystem().getKeyword());
        } else {
            result.add("");
        }
        if (this.oidRequirement != null) {
            result.add(this.oidRequirement.getLabel());
        } else {
            result.add("");
        }
        if (this.oid != null) {
            result.add(getOIDValue());
        } else {
            result.add("");
        }
        return result;
    }

}
