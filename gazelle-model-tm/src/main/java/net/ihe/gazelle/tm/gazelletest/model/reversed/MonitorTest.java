package net.ihe.gazelle.tm.gazelletest.model.reversed;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name = "tm_monitor_test", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class MonitorTest implements java.io.Serializable {

    private static final long serialVersionUID = 6180677033859712739L;

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "monitorId", column = @Column(name = "monitor_id", nullable = false)),
            @AttributeOverride(name = "testId", column = @Column(name = "test_id", nullable = false))})
    private MonitorTestId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_id", nullable = false, insertable = false, updatable = false)
    @Fetch(value = FetchMode.SELECT)
    private Test test;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "monitor_id", nullable = false, insertable = false, updatable = false)
    @Fetch(value = FetchMode.SELECT)
    private MonitorInSession monitorInSession;

    public MonitorTest() {
    }

    public MonitorTest(MonitorTestId id, Test test, MonitorInSession monitorInSession) {
        this.id = id;
        this.test = test;
        this.monitorInSession = monitorInSession;
    }

    public MonitorTestId getId() {
        return this.id;
    }

    public void setId(MonitorTestId id) {
        this.id = id;
    }

    public Test getTest() {
        return HibernateHelper.getLazyValue(this, "test", this.test);
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public MonitorInSession getMonitorInSession() {
        return HibernateHelper.getLazyValue(this, "monitorInSession", this.monitorInSession);
    }

    public void setMonitorInSession(MonitorInSession monitorInSession) {
        this.monitorInSession = monitorInSession;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
