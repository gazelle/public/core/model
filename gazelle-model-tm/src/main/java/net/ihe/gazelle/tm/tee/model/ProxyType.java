package net.ihe.gazelle.tm.tee.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * The persistent class for the proxy_type database table.
 *
 * @author tnabeel
 */
@Entity
@Table(name = "proxy_type")
@SequenceGenerator(name = "proxy_type_id_generator", sequenceName = "proxy_type_sequence")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class ProxyType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "proxy_type_id_generator")
    private int id;

    private String description;

    @Enumerated(EnumType.STRING)
    private ProxyTypeEnum keyword;

    @Column(name = "label_key_for_display")
    private String labelKeyForDisplay;

    @OneToMany(mappedBy = "proxyType")
    private Set<TmStepInstanceMessage> tmStepInstanceMessages;

    public ProxyType() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProxyTypeEnum getKeyword() {
        return this.keyword;
    }

    public void setKeyword(ProxyTypeEnum keyword) {
        this.keyword = keyword;
    }

    public String getLabelKeyForDisplay() {
        return this.labelKeyForDisplay;
    }

    public void setLabelKeyForDisplay(String labelKeyForDisplay) {
        this.labelKeyForDisplay = labelKeyForDisplay;
    }

    public Set<TmStepInstanceMessage> getTmStepInstanceMessages() {
        return this.tmStepInstanceMessages;
    }

    public void setTmStepInstanceMessages(Set<TmStepInstanceMessage> tmStepInstanceMessages) {
        this.tmStepInstanceMessages = tmStepInstanceMessages;
    }

    public TmStepInstanceMessage addTmStepInstanceMessage(TmStepInstanceMessage tmStepInstanceMessage) {
        getTmStepInstanceMessages().add(tmStepInstanceMessage);
        tmStepInstanceMessage.setProxyType(this);

        return tmStepInstanceMessage;
    }

    public TmStepInstanceMessage removeTmStepInstanceMessage(TmStepInstanceMessage tmStepInstanceMessage) {
        getTmStepInstanceMessages().remove(tmStepInstanceMessage);
        tmStepInstanceMessage.setProxyType(null);

        return tmStepInstanceMessage;
    }

}