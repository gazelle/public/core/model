package net.ihe.gazelle.tm.tee.model;

import java.util.ArrayList;
import java.util.List;

public enum TestStepInstanceExecutionStatusEnum {

    INACTIVE(1, false, "inactive", "Not active yet.",
            "net.ihe.gazelle.tm.Inactive"),
    WAITING(
            2,
            true,
            "waiting",
            "Active but no messages have been intercepted yet from sender and receiver.",
            "net.ihe.gazelle.tm.Waiting"),
    INTERRUPTED(
            3,
            false,
            "interrupted",
            "The test step instance was stopped by the system after it had become active at one point.",
            "net.ihe.gazelle.tm.Interrupted"),
    SKIPPED(4, true,
            "skipped", "The test step has been skipped by the participant.",
            "net.ihe.gazelle.tm.Skipped"),
    PAUSED(
            5,
            true,
            "paused",
            "The test step has been paused by the participant after it had become active.",
            "net.ihe.gazelle.tm.Paused"),
    ABORTED(
            6,
            true,
            "aborted",
            "The test step has been aborted by the participant after it had become active.",
            "net.ihe.gazelle.tm.Aborted"),
    INITIATED(7,
            false,
            "initiated",
            "A request message has been intercepted.",
            "net.ihe.gazelle.tm.Initiated"),
    RESPONDED(8, false,
            "responded", "A response message has been intercepted.",
            "net.ihe.gazelle.tm.Responded"),
    COMPLETED(9, true,
            "completed", "The test instance is complete.",
            "net.ihe.gazelle.tm.Completed");

    private Integer id;
    private boolean displayable;
    private String keyword;
    private String description;
    private String displayLabelKey;

    TestStepInstanceExecutionStatusEnum(Integer pId, boolean pVisible,
                                        String pKey, String pDescription, String pDisplayLabelKey) {
        this.id = pId;
        this.setDisplayable(pVisible);
        this.keyword = pKey;
        this.description = pDescription;
        this.displayLabelKey = pDisplayLabelKey;
    }

    public static TestStepInstanceExecutionStatusEnum getExecutionStatusByKey(
            String key) {

        for (TestStepInstanceExecutionStatusEnum status : values()) {
            if (status.keyword.equalsIgnoreCase(key)) {
                return status;
            }
        }
        return null;
    }

    public static TestStepInstanceExecutionStatusEnum getExecutionStatusFromLabelKey(
            String pLabelKey) {
        for (TestStepInstanceExecutionStatusEnum status : values()) {
            if (status.displayLabelKey.equalsIgnoreCase(pLabelKey)) {
                return status;
            }
        }
        return null;
    }

    public static List<TestStepInstanceExecutionStatusEnum> getDisplayableStatuses() {
        List<TestStepInstanceExecutionStatusEnum> statuses = new ArrayList<TestStepInstanceExecutionStatusEnum>();

        for (TestStepInstanceExecutionStatusEnum status : values()) {
            if (status.isDisplayable()) {
                statuses.add(status);
            }
        }
        return statuses;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isDisplayable() {
        return displayable;
    }

    public void setDisplayable(boolean visible) {
        this.displayable = visible;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String key) {
        this.keyword = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayLabelKey() {
        return displayLabelKey;
    }

    public void setDisplayLabelKey(String displayLabelKey) {
        this.displayLabelKey = displayLabelKey;
    }

    /**
     * Returns true if the step instance is done from participant's view point, false otherwise.
     *
     * @return
     */
    public boolean isDone() {
        switch (this) {
            case COMPLETED:
            case SKIPPED:
                return true;
            // Note that RESPONDED status implies that user wishes to mark the step instance COMPLETE manually.
            // Until that's done, we cannot assume that the step instance is "done"
            default:
                return false;
        }
    }

    @Override
    public String toString() {
        return "TestStepInstanceExecutionStatusEnum [id=" + id + ", keyword=" + keyword + ", description=" + description
                + ", displayLabelKey=" + displayLabelKey + "]";
    }

}
