package net.ihe.gazelle.tm.configurations.model;


import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "oidRootDefinitions")
@XmlAccessorType(XmlAccessType.FIELD)
public class OIDRootDefinitionIE {

    @XmlElementRefs({@XmlElementRef(name = "oidRootDefinition")})
    private List<OIDRootDefinition> oidRootDefinitions;

    @XmlTransient
    private List<OIDRootDefinition> duplicatedOIDRootDefinitions;

    @XmlTransient
    private List<OIDRootDefinition> ignoredOIDRootDefinitions;

    @XmlTransient
    private List<OIDRequirement> newRequirements;


    @XmlTransient
    private List<String> unknownLabel;

    @XmlTransient
    private List<String> unknownAipo;

    @XmlTransient
    private boolean imported;

    public OIDRootDefinitionIE() {
    }

    public OIDRootDefinitionIE(List<OIDRootDefinition> oidRootDefinitions) {
        setOidRootDefinitions(oidRootDefinitions);
    }

    public List<OIDRootDefinition> getOidRootDefinitions() {
        if (oidRootDefinitions != null) {
            return new ArrayList<>(oidRootDefinitions);
        }
        return null;
    }

    public void setOidRootDefinitions(List<OIDRootDefinition> oidRootDefinitions) {
        this.oidRootDefinitions = new ArrayList<>(oidRootDefinitions);
    }

    public List<OIDRootDefinition> getDuplicatedOIDRootDefinitions() {
        if (duplicatedOIDRootDefinitions != null) {
            return new ArrayList<>(duplicatedOIDRootDefinitions);
        }
        return null;
    }

    public void setDuplicatedOIDRootDefinitions(List<OIDRootDefinition> duplicatedOIDRootDefinitions) {
        this.duplicatedOIDRootDefinitions = new ArrayList<>(duplicatedOIDRootDefinitions);
    }

    public void addDuplicatedOIDRootDefinition(OIDRootDefinition oidRootDefinition) {
        if (this.duplicatedOIDRootDefinitions == null) {
            this.duplicatedOIDRootDefinitions = new ArrayList<>();
        }
        this.duplicatedOIDRootDefinitions.add(oidRootDefinition);
    }

    public List<OIDRootDefinition> getIgnoredOIDRootDefinitions() {
        if (ignoredOIDRootDefinitions != null) {
            return new ArrayList<>(ignoredOIDRootDefinitions);
        }
        return null;
    }

    public void setIgnoredOIDRootDefinitions(List<OIDRootDefinition> ignoredOIDRootDefinitions) {
        this.ignoredOIDRootDefinitions = new ArrayList<>(ignoredOIDRootDefinitions);
    }

    public void addIgnoredOIDRootDefinition(OIDRootDefinition oidRootDefinition) {
        if (this.ignoredOIDRootDefinitions == null) {
            this.ignoredOIDRootDefinitions = new ArrayList<>();
        }
        this.ignoredOIDRootDefinitions.add(oidRootDefinition);
    }

    public List<OIDRequirement> getNewRequirements() {
        if (newRequirements != null) {
            return new ArrayList<>(newRequirements);
        }
        return null;
    }

    public void setNewRequirements(List<OIDRequirement> newRequirements) {
        this.newRequirements = new ArrayList<>(newRequirements);
    }

    public void addNewRequirement(OIDRequirement oidRequirement) {
        if (this.newRequirements == null) {
            this.newRequirements = new ArrayList<>();
        }
        this.newRequirements.add(oidRequirement);
    }

    public List<String> getUnknownLabel() {
        if (unknownLabel != null) {
            return new ArrayList<>(unknownLabel);
        }
        return null;
    }

    public void setUnknownLabel(List<String> unknownLabel) {
        this.unknownLabel = new ArrayList<>(unknownLabel);
    }

    public void addUnknownLabel(String label) {
        if (this.unknownLabel == null) {
            this.unknownLabel = new ArrayList<>();
        }
        this.unknownLabel.add(label);
    }

    public List<String> getUnknownAipo() {
        if (unknownAipo != null) {
            return new ArrayList<>(unknownAipo);
        }
        return null;
    }

    public void setUnknownAipo(List<String> unknownAipo) {
        this.unknownAipo = new ArrayList<>(unknownAipo);
    }

    public void addUnknownAipo(String aipo) {
        if (this.unknownAipo == null) {
            this.unknownAipo = new ArrayList<>();
        }
        this.unknownAipo.add(aipo);
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        this.imported = imported;
    }
}
