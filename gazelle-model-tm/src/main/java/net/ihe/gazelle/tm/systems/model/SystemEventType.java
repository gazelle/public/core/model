package net.ihe.gazelle.tm.systems.model;

public enum SystemEventType {

    PR_INTEGRATION_STATEMENT_STATUS,
    PR_CRAWLER,
    MODIFIED,
    IMPORTED,
    IS_GENERATED,
    PUBLISHED;

}
