package net.ihe.gazelle.tm.systems.model.reversed;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DemonstrationSystemInSessionId implements java.io.Serializable {

    private static final long serialVersionUID = -3929810057168270324L;

    @Column(name = "system_in_session_id", nullable = false)
    private int systemInSessionId;

    @Column(name = "demonstration_id", nullable = false)
    private int demonstrationId;

    public DemonstrationSystemInSessionId() {
    }

    public DemonstrationSystemInSessionId(int systemInSessionId, int demonstrationId) {
        this.systemInSessionId = systemInSessionId;
        this.demonstrationId = demonstrationId;
    }

    public int getSystemInSessionId() {
        return this.systemInSessionId;
    }

    public void setSystemInSessionId(int systemInSessionId) {
        this.systemInSessionId = systemInSessionId;
    }

    public int getDemonstrationId() {
        return this.demonstrationId;
    }

    public void setDemonstrationId(int demonstrationId) {
        this.demonstrationId = demonstrationId;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof DemonstrationSystemInSessionId)) {
            return false;
        }
        DemonstrationSystemInSessionId castOther = (DemonstrationSystemInSessionId) other;

        return (this.getSystemInSessionId() == castOther.getSystemInSessionId())
                && (this.getDemonstrationId() == castOther.getDemonstrationId());
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = (37 * result) + this.getSystemInSessionId();
        result = (37 * result) + this.getDemonstrationId();
        return result;
    }

}
