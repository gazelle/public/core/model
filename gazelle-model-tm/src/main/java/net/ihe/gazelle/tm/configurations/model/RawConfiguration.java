package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.tm.configurations.model.interfaces.ServerConfiguration;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by ycadoret on 06/04/18.
 */

@Entity
@Table(name = "tm_raw_configuration", schema = "public")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RawConfiguration extends AbstractConfiguration implements ServerConfiguration {


    private static final long serialVersionUID = -4693167526388352292L;

    @Column(name = "port")
    @Range(min = 0, max = 65635)
    private Integer port;

    @Column(name = "port_proxy")
    @Range(min = 0, max = 65635)
    private Integer portProxy;

    @Column(name = "port_secured")
    @Range(min = 0, max = 65635)
    private Integer portSecured;


    @Column(name = "transaction_description")
    private String transactionDescription;


    //==== Constructor
    //===============================

    /**
     * Creates a new RawConfiguration object.
     */
    public RawConfiguration() {
        super();
    }

    public RawConfiguration(Configuration inConfiguration, String inComments) {
        super(inConfiguration, inComments);
    }

    public RawConfiguration(Configuration inConfiguration) {
        super(inConfiguration);
    }

    public RawConfiguration(RawConfiguration rc) {
        super(rc);
        if (rc != null) {
            this.port = rc.port;
            this.portSecured = rc.portSecured;
            this.transactionDescription = rc.transactionDescription;
            getProxyPortIfNeeded(null);
        }
    }

    public RawConfiguration(Configuration inConfiguration, Integer inPort, Integer inPortProxy,
                            Integer inPortSecured, String inTransactionDescription) {
        super(inConfiguration);
        port = inPort;
        portProxy = inPortProxy;
        portSecured = inPortSecured;
        transactionDescription = inTransactionDescription;
    }


    //==== Method
    //======================================

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public Integer getPortProxy() {
        return portProxy;
    }

    @Override
    public void setPortProxy(Integer portProxy) {
        this.portProxy = portProxy;
    }

    @Override
    public Integer getPortSecured() {
        return portSecured;
    }

    @Override
    public void setPortSecured(Integer portSecured) {
        this.portSecured = portSecured;
    }


    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }


    public Integer getPortIfConfNotSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == false)) {
            return port;
        }
        return null;
    }

    public Integer getPortSecuredIfConfSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == true)) {
            return portSecured;
        }
        return null;
    }


    @Override
    public List<String> getCSVHeaders() {
        List<String> result = super.getCSVHeaders();
        addCSVServerHeaders(this, result);
        result.add("transactionDescription");
        return result;
    }

    @Override
    public List<String> getCSVValues() {
        List<String> result = super.getCSVValues();
        addCSVServerValues(this, result);
        if (this.transactionDescription != null) {
            result.add(this.getTransactionDescription());
        } else {
            result.add("");
        }
        return result;
    }

}

