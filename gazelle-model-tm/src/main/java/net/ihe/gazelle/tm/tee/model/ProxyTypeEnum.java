package net.ihe.gazelle.tm.tee.model;

/**
 * Enum for different proxy types
 *
 * @author tnabeel
 */
public enum ProxyTypeEnum {
    GAZELLE, NIST;
}
