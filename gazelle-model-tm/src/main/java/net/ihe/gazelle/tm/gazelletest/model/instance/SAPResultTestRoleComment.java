package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRoles;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "tm_sap_result_tr_comment", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "sap_id", "tr_id"}))
@SequenceGenerator(name = "tm_sap_result_tr_comment_sequence", sequenceName = "tm_sap_result_tr_comment_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class SAPResultTestRoleComment extends AuditedObject implements Serializable {

    private static final long serialVersionUID = -6878592236247390456L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_sap_result_tr_comment_sequence")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "sap_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private SystemAIPOResultForATestingSession systemAIPOResultForATestingSession;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tr_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private TestRoles testRoles;

    @Column(name = "comment")
    private String comment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SystemAIPOResultForATestingSession getSystemAIPOResultForATestingSession() {
        return systemAIPOResultForATestingSession;
    }

    public void setSystemAIPOResultForATestingSession(
            SystemAIPOResultForATestingSession systemAIPOResultForATestingSession) {
        this.systemAIPOResultForATestingSession = systemAIPOResultForATestingSession;
    }

    public TestRoles getTestRoles() {
        return testRoles;
    }

    public void setTestRoles(TestRoles testRoles) {
        this.testRoles = testRoles;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
