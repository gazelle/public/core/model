package net.ihe.gazelle.tm.gazelletest.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class AutomatedTestStepExecution {

    private int id;
    private Integer testStepsInstanceId;
    private String userId;
    private AutomatedTestStepExecutionStatus status;
    private Date date;
    private String jsonReport;
    private List<AutomatedTestStepReportFile> pdfFiles;

    public AutomatedTestStepExecution() {
        this.pdfFiles = new ArrayList<>();
    }

    public AutomatedTestStepExecution(Integer testStepsInstanceId, String userId, AutomatedTestStepExecutionStatus status, Date date) {
        this();
        this.testStepsInstanceId = testStepsInstanceId;
        this.userId = userId;
        this.status = status;
        this.date = date;
    }

    public AutomatedTestStepExecution(Integer testStepsInstanceId, String userId, AutomatedTestStepExecutionStatus status, Date date, String jsonReport) {
        this(testStepsInstanceId, userId, status, date);
        this.jsonReport = jsonReport;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getTestStepsInstanceId() {
        return testStepsInstanceId;
    }

    public void setTestStepsInstanceId(Integer testStepsInstanceId) {
        this.testStepsInstanceId = testStepsInstanceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public AutomatedTestStepExecutionStatus getStatus() {
        return status;
    }

    public void setStatus(AutomatedTestStepExecutionStatus status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getJsonReport() {
        return jsonReport;
    }

    public void setJsonReport(String jsonReport) {
        this.jsonReport = jsonReport;
    }

    public List<AutomatedTestStepReportFile> getPdfFiles() {
        return new ArrayList<>(pdfFiles);
    }

    public void setPdfFiles(List<AutomatedTestStepReportFile> pdfFiles) {
        this.pdfFiles = new ArrayList<>(pdfFiles);
    }

    public void addPdfFile(AutomatedTestStepReportFile pdfFile) {
        this.pdfFiles.add(pdfFile);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AutomatedTestStepExecution)) return false;
        AutomatedTestStepExecution execution = (AutomatedTestStepExecution) o;
        return id == execution.id
                && Objects.equals(testStepsInstanceId, execution.testStepsInstanceId)
                && Objects.equals(status, execution.status)
                && Objects.equals(date, execution.date)
                && Objects.equals(jsonReport, execution.jsonReport)
                && Objects.equals(pdfFiles, execution.pdfFiles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, testStepsInstanceId, status, date, jsonReport, pdfFiles);
    }
}
