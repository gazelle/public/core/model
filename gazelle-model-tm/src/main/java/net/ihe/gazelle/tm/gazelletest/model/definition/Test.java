/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.sync.SetTestDefinition;
import net.ihe.gazelle.tm.gazelletest.jira.JiraIssuesToTest;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipantsQuery;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceQuery;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;
import org.apache.commons.lang.ObjectUtils;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.*;
import org.hibernate.envers.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>Test<br>
 * <br>
 * This class describes the Test object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * Test possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Test</li>
 * <li><b>name</b> : name of the Test</li>
 * <li><b>version</b> : version of the Test</li>
 * <li><b>description</b> : description of the Test</li>
 * <li><b>url</b> : url of the Test</li>
 * <li><b>keyword</b> : keyword that describes the Test</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *                                                                                                                 http://ihe.irisa.fr
 *                                                                                                                 </pre>
 *         <p>
 *         <pre>
 *                                                                                                                 amiladi@irisa.fr
 *                                                                                                                 </pre>
 * @version 1.0 , 27 janv. 2009
 * @class Test.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */
@XmlRootElement(name = "test")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tm_test", schema = "public")
@SequenceGenerator(name = "tm_test_sequence", sequenceName = "tm_test_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class Test extends AuditedObject implements Serializable, Comparable<Test> {

    public static final int MAX_SHORT_DESCRIPTION_LENGTH = 512;
    public static final int MAX_KEYWORD_LENGTH = 255;
    private static final long serialVersionUID = -5149029516625532749L;
    private static Logger log = LoggerFactory.getLogger(Test.class);
    /**
     * Attribute lastValidatorId
     */
    @Column(name = "last_validator_id")
    // added @NotAudited annotation. (rizwan.tanoli@aegis.net)
    // reason for this is that in the "tm_test_aud" table this column
    // is of type integer whereas in this Entity class it is of
    // type string. This is causing the StandardBasicTypes transaction to
    // fail
    @NotAudited
    protected String lastValidatorId;
    /**
     * Attribute lastValidatorId
     */
    @Column(name = "author")
    protected String author;
    @OneToMany(fetch = FetchType.LAZY)
    @AuditJoinTable
    @JoinTable(name = "tm_test_test_steps", joinColumns = @JoinColumn(name = "test_id"), inverseJoinColumns = @JoinColumn(name = "test_steps_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_id", "test_steps_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlElement(name = "testStep")
    @Cascade({CascadeType.MERGE})
    List<TestSteps> testStepsList;
    @OneToMany(fetch = FetchType.LAZY)
    @NotAudited
    @JoinTable(name = "tm_test_user_comment", joinColumns = @JoinColumn(name = "test_id"), inverseJoinColumns = @JoinColumn(name = "user_comment_id"))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    List<UserComment> userCommentList;
    @OneToMany(mappedBy = "test", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @NotAudited
    @XmlTransient
    List<TestInstance> instances;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "test")
    @NotAudited
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    List<TestRoles> testRoles;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "testList")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @NotAudited
    @XmlTransient
    List<MonitorInSession> monitorsInSession;
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_sequence")
    @XmlTransient
    private Integer id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "keyword", unique = true, nullable = false)
    @Size(max = Test.MAX_KEYWORD_LENGTH)
    private String keyword;
    @ManyToOne
    // JRC : Remove nullable constraint in : @JoinColumn(name="test_type_id",nullable=false) to be able to sync Test object with GMM
    @JoinColumn(name = "test_type_id", nullable = false)
    @Fetch(value = FetchMode.JOIN)
    @Cascade({CascadeType.MERGE})
    private TestType testType;
    @Column(name = "short_description", nullable = false)
    @Size(max = Test.MAX_SHORT_DESCRIPTION_LENGTH)
    private String shortDescription;
    @Column(name = "version")
    private String version;
    @Column(name = "orchestrable")
    private Boolean orchestrable;
    @Column(name = "validated")
    private Boolean validated = false;
    @Column(name = "auto_complete", nullable = true)
    @NotAudited
    private Boolean autoComplete;
    @ManyToOne
    @JoinColumn(name = "test_status_id", nullable = true)
    @Fetch(value = FetchMode.JOIN)
    @Cascade({CascadeType.MERGE})
    private TestStatus testStatus;
    @ManyToOne
    @JoinColumn(name = "test_peer_type_id", nullable = true)
    @Fetch(value = FetchMode.JOIN)
    @Cascade({CascadeType.MERGE})
    private TestPeerType testPeerType;
    @OneToMany(fetch = FetchType.LAZY)
    @AuditJoinTable
    @JoinTable(name = "tm_test_test_description", joinColumns = @JoinColumn(name = "test_id"), inverseJoinColumns = @JoinColumn(name = "test_description_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_id", "test_description_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @Cascade({CascadeType.MERGE})
    private List<TestDescription> testDescription;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "test")
    @XmlTransient
    private List<JiraIssuesToTest> jiraIssues;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "child_node_update", columnDefinition = "timestamp with time zone")
    private Date childNodeUpdate;

    public Test() {

    }

    public Test(String authorUsername, String testName, String version, List<TestDescription> testDescription, String keyword, TestType inTestType,
                TestStatus testStatus, String shortDescription) {
        this.author = authorUsername;
        this.name = testName;
        this.version = version;
        this.testDescription = testDescription;
        this.keyword = keyword;
        this.testStatus = testStatus;
        this.testType = inTestType;
        this.shortDescription = shortDescription;
    }

    public Test(String authorUsername, String testName, String version, List<TestDescription> testDescription, String keyword, TestType inTestType,
                TestStatus testStatus, String shortDescription, Boolean synchronizedWithGMM) {
        this.author = authorUsername;
        this.name = testName;
        this.version = version;
        this.testDescription = testDescription;
        this.keyword = keyword;
        this.testStatus = testStatus;
        this.testType = inTestType;
        this.shortDescription = shortDescription;
    }

    public Test(String authorUsername, String name, String version, List<TestDescription> testDescription, String keyword, TestType inTestType,
                TestStatus testStatus, String shortDescription, Boolean synchronizedWithGMM, TestPeerType testPeerType) {
        this.author = authorUsername;
        this.name = name;
        this.version = version;
        this.testDescription = testDescription;
        this.keyword = keyword;
        this.testStatus = testStatus;
        this.testType = inTestType;
        this.shortDescription = shortDescription;
        this.testPeerType = testPeerType;
    }

    /**
     * Copy constructor. This constructor copy all fields except the field id. In the case we use this constructor to copy a test and to persist this new object, it is necessary it doesn't have the
     * same identifier.
     *
     * @param inTest
     */

    public Test(Test inTest) {
        if (inTest.getName() != null) {
            this.name = inTest.getName();
        }
        if (inTest.getVersion() != null) {
            this.version = inTest.getVersion();
        }
        if (inTest.getTestDescription() != null) {
            this.testDescription = inTest.getTestDescription();
        }
        if (inTest.getKeyword() != null) {
            this.keyword = inTest.getKeyword();
        }
        if (inTest.getTestStatus() != null) {
            this.testStatus = inTest.getTestStatus();
        }
        if (inTest.getTestPeerType() != null) {
            this.testPeerType = inTest.getTestPeerType();
        }
        if (inTest.getTestStepsList() != null) {
            this.testStepsList = inTest.getTestStepsList();
        }
        if (inTest.getTestType() != null) {
            this.testType = inTest.getTestType();
        }
        if (inTest.getShortDescription() != null) {
            this.shortDescription = inTest.getShortDescription();
        }
        if (inTest.getUserCommentList() != null) {
            this.userCommentList = inTest.getUserCommentList();
        }
        if (inTest.getOrchestrable() != null) {
            this.orchestrable = inTest.getOrchestrable();
        }
        if (inTest.getValidated() != null) {
            this.validated = inTest.getValidated();
        }
    }

    public static int getMAX_SHORT_DESCRIPTION_LENGTH() {
        return MAX_SHORT_DESCRIPTION_LENGTH;
    }

    public static int getMAX_KEYWORD_LENGTH() {
        return MAX_KEYWORD_LENGTH;
    }

    public static Test getTestByName(String name) {
        TestQuery query = new TestQuery();
        query.name().eq(name);
        List<Test> list = query.getList();

        if (!list.isEmpty()) {
            return list.get(0);
        } else {
            return null;
        }
    }

    public static Test getTestByKeyword(String keyword) {
        TestQuery query = new TestQuery();
        query.keyword().eq(keyword);
        List<Test> list = query.getList();

        if (!list.isEmpty()) {
            return list.get(0);
        } else {
            return null;
        }
    }

    public static List<Test> getTestByTestType(TestType inTestType) {
        TestQuery query = new TestQuery();
        query.testType().eq(inTestType);
        List<Test> list = query.getList();
        Collections.sort(list);
        if (!list.isEmpty()) {
            return list;
        }
        return null;
    }

    public static List<Test> getTestByTestTypeAndTestingSession(TestType inTestType, TestingSession inTestingSession) {
        TestInstanceParticipantsQuery query = new TestInstanceParticipantsQuery();
        query.testInstance().test().testType().eq(inTestType);
        query.systemInSessionUser().systemInSession().testingSession().eq(inTestingSession);
        List<Test> list = query.testInstance().test().getListDistinct();
        if (!list.isEmpty()) {
            return list;
        }
        return null;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static void deleteTestWithFind(Test selectedTest) throws Exception {
        EntityManager em = EntityManagerService.provideEntityManager();

        List<UserComment> userCommentList = selectedTest.getUserCommentList();
        selectedTest.setUserCommentList(null);
        selectedTest = em.merge(selectedTest);
        if (userCommentList != null) {
            for (UserComment userComment : userCommentList) {
                UserComment.deleteUserCommentWithFind(userComment);
            }
        }

        List<TestInstance> testInstanceList = TestInstance.getTestInstancesForATest(selectedTest);
        if (testInstanceList != null) {
            for (TestInstance testInstance : testInstanceList) {
                TestInstance.deleteConnectathonTestInstanceWithFind(testInstance);
            }
        }

        List<TestSteps> testStepsList = selectedTest.getTestStepsList();
        selectedTest.setTestStepsList(null);
        selectedTest = em.merge(selectedTest);
        if (testStepsList != null) {
            for (TestSteps testSteps : testStepsList) {
                if (!testSteps.asDomain().isAutomated()) {
                    TestSteps.deleteTestStepsWithFind(testSteps);
                }
            }
        }

        List<TestRoles> testRolesList = TestRoles.getTestRolesListForATest(selectedTest);
        if (testRolesList != null) {
            for (TestRoles testRoles : testRolesList) {
                TestRoles.deleteTestRolesWithFind(testRoles);
            }
        }

        List<MonitorInSession> monitorInSessionList = MonitorInSession.getMonitorInSessionListForATest(selectedTest);
        if (monitorInSessionList != null) {
            for (MonitorInSession monitorInSession : monitorInSessionList) {
                List<Test> monitorInSessionTestList = monitorInSession.getTestList();
                if (monitorInSessionTestList != null) {
                    monitorInSessionTestList.remove(selectedTest);
                }
                monitorInSession.setTestList(monitorInSessionTestList);
                em.merge(monitorInSession);
            }
        }

        try {
            Test test = em.find(Test.class, selectedTest.getId());
            em.remove(test);
        } catch (Exception e) {
            log.error("deleteTestWithFind", e);
            throw new Exception("A test cannot be deleted -  id = " + selectedTest.getId(), e);
        }
    }

    public static Test getTestWithAllAttributes(Integer testId) {
        if (testId != null) {
            TestQuery query = new TestQuery();
            query.id().eq(testId);
            return query.getUniqueResult();
        }
        return null;
    }

    public static Test getTestWithAllAttributes(Test inTest) {
        if (inTest != null) {
            return getTestWithAllAttributes(inTest.getId());
        }
        return null;
    }

    //TODO no need to use entitymanger after update the query
    @Deprecated
    public static List<MonitorInSession> getAllMonitors(EntityManager entityManager, Test test,
                                                        TestingSession session) {
        return getAllMonitors(test, session);
    }

    public static List<MonitorInSession> getAllMonitors(Test test,
                                                        TestingSession session) {
        TestInstanceQuery q = new TestInstanceQuery();
        q.test().eq(test);
        if (session != null) {
            q.testingSession().eq(session);
        }
        List<MonitorInSession> result = q.monitorInSession().getListDistinct();
        return result;
    }

    //TODO no need to use entitymanger after update the query
    @Deprecated
    public static List<SystemInSession> getAllSystems(EntityManager entityManager, Test test, TestingSession session) {
        return getAllSystems(test, session);
    }

    public static List<SystemInSession> getAllSystems(Test test, TestingSession session) {
        TestInstanceParticipantsQuery q = new TestInstanceParticipantsQuery();
        q.testInstance().test().eq(test);
        if (session != null) {
            q.testInstance().testingSession().eq(session);
        }
        List<SystemInSession> result = q.systemInSessionUser().systemInSession().getListDistinct();
        return result;
    }

    //TODO no need to use entitymanger after update the query
    @Deprecated
    public static List<TestInstance> getAllTestInstances(EntityManager entityManager, Test test,
                                                         TestingSession session) {
        return getAllTestInstances(test, session);
    }

    public static List<TestInstance> getAllTestInstances(Test test,
                                                         TestingSession session) {
        TestInstanceQuery q = new TestInstanceQuery();
        q.test().eq(test);
        if (session != null) {
            q.testingSession().eq(session);
        }
        List<TestInstance> result = q.getListDistinct();
        return result;
    }

    public static List<Test> listTests() {
        TestQuery q = new TestQuery();
        List<Test> listResult = q.getList();
        Collections.sort(listResult);
        return listResult;
    }

    @XmlTransient
    public String getLastValidatorId() {
        return lastValidatorId;
    }

    public void setLastValidatorId(String lastValidatorId) {
        this.lastValidatorId = lastValidatorId;
    }

    @XmlTransient
    public String getAuthor() {
        if (author == null) {
            author = "";
        }
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Boolean getOrchestrable() {
        return orchestrable;
    }

    public void setOrchestrable(Boolean orchestrable) {
        this.orchestrable = orchestrable;
    }

    public Boolean getValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public List<TestDescription> getTestDescription() {
        return HibernateHelper.getLazyValue(this, "testDescription", this.testDescription);
    }

    public void setTestDescription(List<TestDescription> testDescription) {
        this.testDescription = testDescription;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public TestStatus getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(TestStatus testStatus) {
        this.testStatus = testStatus;
    }

    public TestPeerType getTestPeerType() {
        return testPeerType;
    }

    public void setTestPeerType(TestPeerType testPeerType) {
        this.testPeerType = testPeerType;
    }

    public TestType getTestType() {
        return testType;
    }

    public void setTestType(TestType testType) {
        this.testType = testType;
    }

    public List<TestSteps> getTestStepsList() {
        return HibernateHelper.getLazyValue(this, "testStepsList", this.testStepsList);
    }

    public void setTestStepsList(List<TestSteps> testStepsList) {
        this.testStepsList = testStepsList;
    }

    // -----------------------------------

    public String getShortDescription() {
        return shortDescription;
    }

    // */

    // -----------------------------------

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Boolean getAutoComplete() {
        return autoComplete;
    }

    public boolean isAutoComplete() {
        return autoComplete != null ? autoComplete : true;
    }

    public void setAutoComplete(Boolean autoComplete) {
        this.autoComplete = autoComplete;
    }

    public List<TestRoles> getTestRoles() {
        return HibernateHelper.getLazyValue(this, "testRoles", this.testRoles);
    }

    public void setTestRoles(List<TestRoles> testRoles) {
        this.testRoles = testRoles;
    }

    public List<UserComment> getUserCommentList() {
        return HibernateHelper.getLazyValue(this, "userCommentList", this.userCommentList);
    }

    public void setUserCommentList(List<UserComment> userCommentList) {
        this.userCommentList = userCommentList;
    }

    @FilterLabel
    public String getSelectableLabel() {
        return getKeyword() + " - " + getName();
    }

    public void deleteTestDescription(TestDescription inTestDescription) throws Exception {
        this.getTestDescription().remove(inTestDescription);
    }

    public String getSequenceDiagramAsUrl() {
        return PreferenceService.getString("application_url") + "testSequenceDiagram.seam?id=" + getId();
    }

    public List<TestSteps> getTestStepsListAntecedent(TestSteps testSteps) {
        List<TestSteps> lts = new ArrayList<TestSteps>();
        List<TestSteps> lts_temp = this.getTestStepsList();
        if (lts_temp != null) {
            for (TestSteps ts : lts_temp) {
                if (testSteps.getStepIndex() != null) {
                    if (ts.getStepIndex() < testSteps.getStepIndex()) {
                        lts.add(ts);
                    }
                } else {
                    lts.add(ts);
                }
            }
        }
        return lts;
    }

    public TestDescription getContextualDescription() {
        if (this.getTestDescription() != null) {
            if (this.getTestDescription().size() == 1) {
                return this.getTestDescription().get(0);
            } else {
                if (this.getTestDescription().size() > 0) {
                    String language = null;
                    GazelleLanguage gazelleLanguage = GazelleLanguage
                            .getGazelleLanguageDescriptionIgnoringCase(language);
                    if (gazelleLanguage == null) {
                        gazelleLanguage = GazelleLanguage.getDefaultLanguage();
                    }
                    for (TestDescription testDescription : this.getTestDescription()) {
                        try {
                            Hibernate.initialize(testDescription.getGazelleLanguageLazy());
                            GazelleLanguage gazelleLanguageLazy = testDescription.getGazelleLanguageLazy();
                            if ((gazelleLanguageLazy != null) && gazelleLanguageLazy.equals(gazelleLanguage)) {
                                return testDescription;
                            }
                        } catch (javax.persistence.EntityNotFoundException e) {
                            log.error("Cannot fetch language definition");
                        }
                    }
                    // if no description matching the default language has been found we return the first description found.
                    int lastTestDescriptionIndex = this.getTestDescription().size() - 1;
                    return this.getTestDescription().get(lastTestDescriptionIndex);
                }
            }
        }
        return null;
    }

    @Override
    public int compareTo(Test o) {
        return getKeyword().compareToIgnoreCase(o.getKeyword());
    }

    @Override
    public String toString() {
        return keyword + " - " + name;
    }

    public String getLabelToDisplayOfTPT() {
        if (this.testPeerType != null) {
            return this.testPeerType.getLabelToDisplay();
        }
        return "-";
    }

    public List<TestInstance> getInstances() {
        return HibernateHelper.getLazyValue(this, "instances", this.instances);
    }

    public void setInstances(List<TestInstance> instances) {
        this.instances = instances;
    }

    public List<MonitorInSession> getMonitorsInSession() {
        return HibernateHelper.getLazyValue(this, "monitorsInSession", this.monitorsInSession);
    }

    public void setMonitorsInSession(List<MonitorInSession> monitorsInSession) {
        this.monitorsInSession = monitorsInSession;
    }

    public int saveOrMerge(EntityManager entityManager, int i, int totalTests) {
        if (!CacheManager.getInstance().cacheExists("TestImportCache")) {
            CacheConfiguration cacheConfig = new CacheConfiguration("TestImportCache", 50000).timeToIdleSeconds(120)
                    .memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.LRU);
            net.sf.ehcache.Cache cache = new net.sf.ehcache.Cache(cacheConfig);

            CacheManager.getInstance().addCache(cache);
        }
        Ehcache cache = CacheManager.getInstance().getEhcache("TestImportCache");

        this.id = null;
        testStatus.setLastModifierId(this.lastModifierId);
        testStatus.saveOrMerge(entityManager);
        if (testPeerType != null) {
            testPeerType.setLastModifierId(this.lastModifierId);
            testPeerType.saveOrMerge(entityManager);
        }
        testType.setLastModifierId(this.lastModifierId);
        testType.saveOrMerge(entityManager);
        if (testDescription != null) {
            for (TestDescription testDescription_ : testDescription) {
                testDescription_.setLastModifierId(this.lastModifierId);
                testDescription_.saveOrMerge(entityManager);
            }
        }

        List<TestSteps> testStepsListBackup = (List<TestSteps>) ObjectUtils.clone(testStepsList);
        testStepsList = null;

        List<TestRoles> testRolesBackup = (List<TestRoles>) ObjectUtils.clone(testRoles);
        testRoles = null;

        TestQuery query = new TestQuery();
        query.keyword().eq(this.keyword);

        List<Test> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            Test test = listDistinct.get(0);
            this.id = test.getId();
            entityManager.merge(this);
        } else {
            if (id != null) {
                entityManager.merge(this);
            } else {
                entityManager.persist(this);
            }
        }
        if (log.isWarnEnabled()) {
            log.warn(this.getClass().getSimpleName().toString() + " imported " + this);
        }
        if (testStepsListBackup != null) {
            for (TestSteps testStep : testStepsListBackup) {
                testStep.setLastModifierId(this.lastModifierId);
                testStep.saveOrMerge(entityManager, this);
            }
        }
        testStepsList = testStepsListBackup;

        if (testRolesBackup != null) {
            for (TestRoles testRole : testRolesBackup) {
                testRole.setLastModifierId(this.lastModifierId);
                testRole.saveOrMerge(entityManager, this);
            }
        }
        testRoles = testRolesBackup;

        entityManager.merge(this);
        entityManager.flush();
        cache.removeAll();
        if (log.isWarnEnabled()) {
            log.warn(i + "/" + totalTests + " " + this.getClass().getSimpleName().toString() + " saved ! (" + this + " was successfully imported)");
        }
        return this.id;
    }

    public List<AssertionStatus> getAssertionsFromTestSteps() {
        List<AssertionStatus> assertions = new ArrayList<AssertionStatus>();
        if (testStepsList != null) {
            for (TestSteps testStep : testStepsList) {

                assertions.addAll(testStep.getAssertionsFromDescription());
            }
        }
        return assertions;
    }

    public List<JiraIssuesToTest> getJiraIssues() {
        return jiraIssues;
    }

    public void setJiraIssues(List<JiraIssuesToTest> jiraIssues) {
        this.jiraIssues = jiraIssues;
    }

    public boolean isTypeInteroperability() {
        if (getTestType() == null) {
            return false;
        }
        return getTestType().getKeyword().equals(TestType.TYPE_INTEROPERABILITY_STRING);
    }

    public String viewFolder() {
        if (isTypeInteroperability()) {
            return "testingITB";
        } else {
            return "testing";
        }
    }

    public List<Number> getAllVersions() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        AuditReader reader = AuditReaderFactory.get(entityManager);
        List<Number> revNumbers = reader.getRevisions(Test.class, this.getId());
        Collections.reverse(revNumbers);
        return revNumbers;
    }

    public Date getChildNodeUpdate() {
        return this.childNodeUpdate;
    }

    public void setChildNodeUpdate(Date date) {
        this.childNodeUpdate = date;
    }
}
