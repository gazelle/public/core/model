package net.ihe.gazelle.tm.configurations.model.export;

import net.ihe.gazelle.tm.configurations.model.ConfigurationTypeMappedWithAIPO;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="configurationTypeMappedWithAIPOList")
@XmlAccessorType(XmlAccessType.FIELD)
public class ConfigurationTypeMappedWithAIPOIE {

    @XmlElementRefs({@XmlElementRef(name = "standard")})
    private List<ConfigurationTypeMappedWithAIPO> configurationTypeMappedWithAIPOS;

    @XmlTransient
    private List<ConfigurationTypeMappedWithAIPO> ignoredConfigurations;

    @XmlTransient
    private List<String> unknownIntegrationProfiles;

    @XmlTransient
    private List<String> unknownTransactions;

    @XmlTransient
    private List<ConfigurationTypeMappedWithAIPO> duplicatedConfigurations;

    @XmlTransient
    private boolean imported;

    public ConfigurationTypeMappedWithAIPOIE(){
        configurationTypeMappedWithAIPOS = new ArrayList<>();
    }

    public ConfigurationTypeMappedWithAIPOIE(List<ConfigurationTypeMappedWithAIPO> configurationTypeMappedWithAIPOS){
        if (configurationTypeMappedWithAIPOS != null){
            this.configurationTypeMappedWithAIPOS = configurationTypeMappedWithAIPOS;
        } else {
            this.configurationTypeMappedWithAIPOS = new ArrayList<>();
        }
    }

    public List<ConfigurationTypeMappedWithAIPO> getConfigurationTypeMappedWithAIPOS() {
        return configurationTypeMappedWithAIPOS;
    }

    public void setConfigurationTypeMappedWithAIPOS(List<ConfigurationTypeMappedWithAIPO> configurationTypeMappedWithAIPOS) {
        this.configurationTypeMappedWithAIPOS = configurationTypeMappedWithAIPOS;
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        this.imported = imported;
    }

    public List<ConfigurationTypeMappedWithAIPO> getIgnoredConfigurations() {
        return ignoredConfigurations;
    }

    public void setIgnoredConfigurations(List<ConfigurationTypeMappedWithAIPO> ignoredConfigurations) {
        this.ignoredConfigurations = ignoredConfigurations;
    }

    public void addIgnoredConfiguration(ConfigurationTypeMappedWithAIPO configuration){
        if (this.ignoredConfigurations != null){
            this.ignoredConfigurations.add(configuration);
        } else {
            this.ignoredConfigurations = new ArrayList<>();
            this.ignoredConfigurations.add(configuration);
        }
    }

    public List<String> getUnknownIntegrationProfiles() {
        return unknownIntegrationProfiles;
    }

    public void setUnknownIntegrationProfiles(List<String> unknownIntegrationProfiles) {
        this.unknownIntegrationProfiles = unknownIntegrationProfiles;
    }

    public void addUnknownIntegrationProfile(String keyword){
        if (this.unknownIntegrationProfiles != null){
            this.unknownIntegrationProfiles.add(keyword);
        } else {
            this.unknownIntegrationProfiles = new ArrayList<>();
            this.unknownIntegrationProfiles.add(keyword);
        }
    }

    public List<String> getUnknownTransactions() {
        return unknownTransactions;
    }

    public void setUnknownTransactions(List<String> unknownTransactions) {
        this.unknownTransactions = unknownTransactions;
    }

    public void addUnknownTransaction(String keyword){
        if (this.unknownTransactions != null){
            this.unknownTransactions.add(keyword);
        } else {
            this.unknownTransactions = new ArrayList<>();
            this.unknownTransactions.add(keyword);
        }
    }

    public List<ConfigurationTypeMappedWithAIPO> getDuplicatedConfigurations() {
        return duplicatedConfigurations;
    }

    public void setDuplicatedConfigurations(List<ConfigurationTypeMappedWithAIPO> duplicatedConfigurations) {
        this.duplicatedConfigurations = duplicatedConfigurations;
    }

    public void addDuplicatedConfiguration(ConfigurationTypeMappedWithAIPO configuration){
        if (this.duplicatedConfigurations != null){
            this.duplicatedConfigurations.add(configuration);
        } else {
            this.duplicatedConfigurations = new ArrayList<>();
            this.duplicatedConfigurations.add(configuration);
        }
    }
}
