/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.ExchangeUnique;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.envers.AuditMappedBy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

/**
 * <b>Class Description : </b>TestDescription<br>
 * <br>
 * This class describes the TestDescription object, used by the Gazelle application. This class belongs to the Test Management module.
 * <p/>
 * TestDescription possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestDescription</li>
 * <li><b>test</b> : test corresponding to the TestDescription</li>
 * <li><b>description</b> : description corresponding to the TestDescription</li>
 * <li><b>gazelleLanguage</b> : gazelleLanguage corresponding to the TestDescription</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *         http://ihe.irisa.fr
 *         </pre>
 *         <p/>
 *         <pre>
 *         amiladi@irisa.fr
 *         </pre>
 * @version 1.0 , 14 mai 2009
 * @class TestDescription.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */

@XmlRootElement(name = "testDescription")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tm_test_description", schema = "public")
@SequenceGenerator(name = "tm_test_description_sequence", sequenceName = "tm_test_description_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class TestDescription extends AuditedObject implements Serializable {
    private static final String DEFAULT_DESCRIPTION = "<h2>Special Instructions</h2><p></p><h2>Description</h2><p></p><h2>Evaluation</h2><p></p>";
    private static final long serialVersionUID = -5778842442874991879L;
    private static Logger log = LoggerFactory.getLogger(TestDescription.class);
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_description_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "description", nullable = false)
    @Lob
    @Type(type = "text")
    @Pattern(regexp="^[^\\f]+$")
    private String description;

    @ManyToOne
    @AuditMappedBy(mappedBy = "testDescriptions")
    @JoinColumn(name = "gazelle_language_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    @ExchangeUnique
    private GazelleLanguage gazelleLanguage;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotAudited
    @JoinTable(name = "tm_test_test_description", joinColumns = @JoinColumn(name = "test_description_id"), inverseJoinColumns = @JoinColumn(name = "test_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_id", "test_description_id"}))
    @Fetch(value = FetchMode.SELECT)
    @ExchangeUnique
    @XmlTransient
    private Test testParent;

    public TestDescription() {
    }

    public TestDescription(String description, GazelleLanguage gazelleLanguage) {
        this.description = description;
        this.gazelleLanguage = gazelleLanguage;
    }

    public TestDescription(TestDescription inTestDescription) {
        if (inTestDescription.getDescription() != null) {
            this.setDescription(inTestDescription.getDescription());
        }
        if (inTestDescription.getGazelleLanguage() != null) {
            this.setGazelleLanguage(inTestDescription.getGazelleLanguage());
        }
    }

    public static void deleteTestDescriptionWithFind(TestDescription testDescription) throws Exception {
        EntityManager em = EntityManagerService.provideEntityManager();

        try {
            TestDescription selectedTestDescription = em.find(TestDescription.class, testDescription.getId());
            em.remove(selectedTestDescription);
        } catch (Exception e) {
            log.error("deleteTestDescriptionWithFind", e);
            throw new Exception("A testDescription cannot be deleted -  id = " + testDescription.getId(), e);
        }
    }

    public void initDefaultValues() {
        this.description = DEFAULT_DESCRIPTION;
        this.gazelleLanguage = GazelleLanguage.getDefaultLanguage();
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GazelleLanguage getGazelleLanguage() {
        return gazelleLanguage;
    }

    public void setGazelleLanguage(GazelleLanguage gazelleLanguage) {
        this.gazelleLanguage = gazelleLanguage;
    }

    public GazelleLanguage getGazelleLanguageLazy() {
        return HibernateHelper.getLazyValue(this, "gazelleLanguage", this.gazelleLanguage);
    }

    public Test getTestParent() {
        return HibernateHelper.getLazyValue(this, "testParent", this.testParent);
    }

    public void setTestParent(Test testParent) {
        this.testParent = testParent;
    }

    @Override
    public String toString() {
        return "TestDescription [id=" + id + ", description=" + description + ", gazelleLanguage=" + gazelleLanguage
                + "]";
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;
        gazelleLanguage.setLastModifierId(this.lastModifierId);
        gazelleLanguage.saveOrMerge(entityManager);

        entityManager.persist(this);

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved ");
        }
    }

}
