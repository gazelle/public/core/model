/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.gazelletest.model.definition.ContextualInformation;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestSteps;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.tee.model.TestStepInstanceExecutionStatus;
import net.ihe.gazelle.users.model.Institution;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "TestStepInstance")
@Entity
@Table(name = "tm_test_steps_instance", schema = "public")
@SequenceGenerator(name = "tm_test_steps_instance_sequence", sequenceName = "tm_test_steps_instance_id_seq", allocationSize = 1)
//Don't cache.  Noticed stale instances in query results once in a blue moon.
//@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class TestStepsInstance extends AuditedObject implements Serializable, Comparable<TestStepsInstance> {

    public static final int MAX_COMMENTS_LENGTH = 1024;
    private static final long serialVersionUID = 1705269503392318738L;
    private static Logger log = LoggerFactory.getLogger(TestStepsInstance.class);
    @XmlElement(name = "id")
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_steps_instance_sequence")
    private Integer id;

    @XmlElement(name = "TestStep")
    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "test_steps_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private TestSteps testSteps;

    @ManyToOne
    @JoinColumn(name = "system_in_session_initiator_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    @XmlElement(name = "systemInSessionInitiator")
    private SystemInSession systemInSessionInitiator;

    @ManyToOne
    @JoinColumn(name = "system_in_session_reponder_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    @XmlElement(name = "systemInSessionResponder")
    private SystemInSession systemInSessionResponder;

    @ManyToOne
    @JoinColumn(name = "test_steps_instance_status_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
    private TestStepsInstanceStatus testStepsInstanceStatus;

    /**
     * The status given by the monitor to the teststepsInstance
     */
    @XmlElement(name = "MonitorStatus")
    @Column(name = "status_id")
    private Status status;

    @Column(name = "test_steps_version", nullable = true)
    private Integer testStepsVersion;

    @Column(name = "comments", nullable = true)
    @Lob
    @Type(type = "text")
    private String comments;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "tm_test_steps_instance_input_ci_instance", joinColumns = @JoinColumn(name = "test_steps_instance_id"), inverseJoinColumns = @JoinColumn(name = "contextual_information_instance_id"))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<ContextualInformationInstance> inputContextualInformationInstanceList;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "tm_test_steps_instance_output_ci_instance", joinColumns = @JoinColumn(name = "test_steps_instance_id"), inverseJoinColumns = @JoinColumn(name = "contextual_information_instance_id"))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<ContextualInformationInstance> outputContextualInformationInstanceList;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @AuditJoinTable
    @JoinTable(name = "tm_test_steps_test_steps_data", joinColumns = @JoinColumn(name = "test_steps_id"), inverseJoinColumns = @JoinColumn(name = "test_steps_data_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "test_steps_id", "test_steps_data_id"}))
    private List<TestStepsData> testStepsDataList;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "tm_test_instance_test_steps_instance", joinColumns = @JoinColumn(name = "test_steps_instance_id"), inverseJoinColumns = @JoinColumn(name = "test_instance_id"))
    @Fetch(value = FetchMode.SELECT)
    private TestInstance testInstance;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "execution_id")
    private AutomatedTestStepExecutionEntity executionEntity;

    @ManyToOne
    @JoinColumn(name = "execution_status_id", referencedColumnName = "id", nullable = true)
    private TestStepInstanceExecutionStatus executionStatus;
    private transient static UserService userClient;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date", nullable = true)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date", nullable = true)
    private Date endDate;

    public TestStepsInstance() {

    }

    public TestStepsInstance(TestSteps testSteps, SystemInSession systemInSessionInitiator,
                             SystemInSession systemInSessionResponder) {
        this.testSteps = testSteps;
        this.systemInSessionInitiator = systemInSessionInitiator;
        this.systemInSessionResponder = systemInSessionResponder;
    }

    //TODO Réarrange join for User collection!!!
    public static List<TestStepsInstance> getTestStepsInstanceByUserByStatusByTestSession(
            TestingSession inTestingSession, TestStepsInstanceStatus inTestStepsInstanceStatus, String inUser) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Query query = entityManager.createQuery(
                "SELECT DISTINCT tsi " + "FROM TestStepsInstance tsi,TestInstanceParticipants tip "
                        + "WHERE (tsi.systemInSessionInitiator=tip.systemInSessionUser.systemInSession OR tsi.systemInSessionResponder=tip.systemInSessionUser.systemInSession) "
                        + "AND tip.systemInSessionUser.userID=:inUser "
                        + "AND tsi.testStepsInstanceStatus=:inTestStepsInstanceStatus "
                        + "AND tip.systemInSessionUser.systemInSession.testingSession=:inTestingSession");
        query.setParameter("inTestingSession", inTestingSession);
        query.setParameter("inTestStepsInstanceStatus", inTestStepsInstanceStatus);
        query.setParameter("inUser", inUser);
        List<TestStepsInstance> list = query.getResultList();
        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    public static int getCountTestStepsInstanceBySystemInSession(SystemInSession inSystemInSession) {
        if (inSystemInSession == null) {
            return 0;
        }
        if (inSystemInSession.getId() == null) {
            return 0;
        }
        TestStepsInstanceQuery q = new TestStepsInstanceQuery();
        q.addRestriction(HQLRestrictions.or(q.systemInSessionInitiator().eqRestriction(inSystemInSession),
                q.systemInSessionResponder().eqRestriction(inSystemInSession)));

        return q.getCountDistinctOnPath();
    }

    //TODO
    public static List<TestStepsInstance> getTestStepsInstanceForVendorAdminByStatusByTestSession(
            TestingSession inTestingSession, TestStepsInstanceStatus inTestStepsInstanceStatus, String vendorAdminId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        userClient = (UserService) Component.getInstance("gumUserService");
        User user = userClient.getUserById(vendorAdminId);
        Institution institution = Institution.findInstitutionWithKeyword(user.getOrganizationId());
        Query query = entityManager.createQuery("SELECT DISTINCT tsi "
                + "FROM TestStepsInstance tsi,TestInstanceParticipants tip,InstitutionSystem instSys "
                + "WHERE tip.systemInSessionUser.systemInSession.testingSession=:inTestingSession "
                + "AND tip.systemInSessionUser.systemInSession.system=instSys.system "
                + "AND instSys.institution=:inVendorAdminInst "
                + "AND tsi.testStepsInstanceStatus=:inTestStepsInstanceStatus "
                + "AND (tsi.systemInSessionInitiator=tip.systemInSessionUser.systemInSession OR tsi.systemInSessionResponder=tip.systemInSessionUser.systemInSession)");
        query.setParameter("inTestingSession", inTestingSession);
        query.setParameter("inTestStepsInstanceStatus", inTestStepsInstanceStatus);
        query.setParameter("inVendorAdminInst", institution);
        List<TestStepsInstance> list = query.getResultList();
        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    public static List<TestStepsInstance> getTestStepsInstanceByTestSteps(TestSteps inTestSteps) {
        TestStepsInstanceQuery q = new TestStepsInstanceQuery();
        q.testSteps().eq(inTestSteps);
        List<TestStepsInstance> list = q.getListDistinct();
        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    public static void deleteTestStepsInstanceWithFind(TestStepsInstance inTestStepsInstance) throws Exception {
        EntityManager em = EntityManagerService.provideEntityManager();
        try {

            TestStepsInstance testStepsInstance = em.find(TestStepsInstance.class, inTestStepsInstance.getId());
            if (testStepsInstance != null) {
                List<ContextualInformationInstance> lcii_input = testStepsInstance
                        .getInputContextualInformationInstanceList();
                if (lcii_input != null) {
                    for (ContextualInformationInstance contextualInformationInstance : lcii_input) {
                        ContextualInformationInstance
                                .removeContextualInformationinstance(contextualInformationInstance, em);
                    }
                }
                List<ContextualInformationInstance> lcii_output = testStepsInstance
                        .getOutputContextualInformationInstanceList();
                if (lcii_output != null) {
                    for (ContextualInformationInstance contextualInformationInstance : lcii_output) {
                        ContextualInformationInstance
                                .removeContextualInformationinstance(contextualInformationInstance, em);
                    }
                }
                List<TestStepsData> ltsd = testStepsInstance.getTestStepsDataList();
                testStepsInstance.setTestStepsDataList(null);
                em.merge(testStepsInstance);
                if (ltsd != null) {
                    for (TestStepsData testStepsData : ltsd) {
                        TestStepsData.removeTestStepsData(testStepsData, em);
                    }
                }
                em.remove(testStepsInstance);
                em.flush();
            }
        } catch (Exception e) {
            log.error("deleteTestStepsInstanceWithFind", e);
            throw e;
        }
    }

    public void addTestStepsData(TestStepsData testStepsData) {
        if (getTestStepsDataList() == null) {
            setTestStepsDataList(new ArrayList<TestStepsData>());
        }
        getTestStepsDataList().add(testStepsData);
    }

    public boolean removeTestStepsDataList(TestStepsData testStepsData) {
        if (getTestStepsDataList() != null) {
            return getTestStepsDataList().remove(testStepsData);
        }
        return false;
    }

    public List<TestStepsData> getTestStepsDataList() {
        return testStepsDataList;
    }

    public void setTestStepsDataList(List<TestStepsData> testStepsDataList) {
        this.testStepsDataList = testStepsDataList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TestSteps getTestSteps() {
        return testSteps;
    }

    public void setTestSteps(TestSteps testSteps) {
        this.testSteps = testSteps;
    }

    public SystemInSession getSystemInSessionInitiator() {
        return systemInSessionInitiator;
    }

    public void setSystemInSessionInitiator(SystemInSession systemInSessionInitiator) {
        this.systemInSessionInitiator = systemInSessionInitiator;
    }

    public SystemInSession getSystemInSessionResponder() {
        return systemInSessionResponder;
    }

    public void setSystemInSessionResponder(SystemInSession systemInSessionResponder) {
        this.systemInSessionResponder = systemInSessionResponder;
    }

    public TestStepsInstanceStatus getTestStepsInstanceStatus() {
        return testStepsInstanceStatus;
    }

    public void setTestStepsInstanceStatus(TestStepsInstanceStatus testStepsInstanceStatus) {
        this.testStepsInstanceStatus = testStepsInstanceStatus;
    }

    public Integer getTestStepsVersion() {
        return testStepsVersion;
    }

    public void setTestStepsVersion(Integer testStepsVersion) {
        this.testStepsVersion = testStepsVersion;
    }

    public List<ContextualInformationInstance> getInputContextualInformationInstanceList() {
        return inputContextualInformationInstanceList;
    }

    public void setInputContextualInformationInstanceList(
            List<ContextualInformationInstance> inputContextualInformationInstanceList) {
        this.inputContextualInformationInstanceList = inputContextualInformationInstanceList;
    }

    public List<ContextualInformationInstance> getOutputContextualInformationInstanceList() {
        return outputContextualInformationInstanceList;
    }

    public void setOutputContextualInformationInstanceList(
            List<ContextualInformationInstance> outputContextualInformationInstanceList) {
        this.outputContextualInformationInstanceList = outputContextualInformationInstanceList;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public TestInstance getTestInstance() {
        return testInstance;
    }

    public AutomatedTestStepExecutionEntity getExecutionEntity() {
        AutomatedTestStepExecutionEntity execution = HibernateHelper.getLazyValue(this, "executionEntity",
                this.executionEntity);
        if (execution == null) {
            setExecutionEntity(new AutomatedTestStepExecutionEntity());
            return this.executionEntity;
        } else {
            return execution;
        }
    }

    public void setExecutionEntity(AutomatedTestStepExecutionEntity executionEntity) {
        this.executionEntity = executionEntity;
    }

    public TestSteps getVersionedTestSteps() {
        if (testStepsVersion != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            AuditReader auditReader = AuditReaderFactory.get(entityManager);
            TestSteps result = auditReader.find(TestSteps.class, testSteps.getId(), testStepsVersion);
            return result;
        }
        return null;
    }

    public boolean isSystemInSessionParticipanting(SystemInSession inSystemInSession) {
        if (inSystemInSession != null) {
            if (inSystemInSession.equals(systemInSessionInitiator) || inSystemInSession
                    .equals(systemInSessionResponder)) {
                return true;
            }
        }
        return false;
    }

    public boolean isUserParticipating(User user) {
        // TODO code should be corrected if the system manager differs from the system owner
        // In the present implementation "systemInSession manager"="system owner"
        if (systemInSessionInitiator != null) {
            if (systemInSessionInitiator.getSystem().getOwnerUserId().equals(user.getId())) {
                return true;
            }
            if (Role.isUserVendorAdmin(user)) {
                Institution institution = Institution.findInstitutionWithKeyword(user.getOrganizationId());
                List<Institution> institutionList = net.ihe.gazelle.tm.systems.model.System
                        .getInstitutionsForASystem(systemInSessionInitiator.getSystem());
                for (Institution institution2 : institutionList) {
                    if (institution2.equals(institution)) {
                        return true;
                    }
                }
            }
        }
        if (systemInSessionResponder != null) {
            if (systemInSessionResponder.getSystem().getOwnerUserId().equals(user.getId())) {
                return true;
            }
            if (Role.isUserVendorAdmin(user)) {
                Institution institution = Institution.findInstitutionWithKeyword(user.getOrganizationId());
                List<Institution> institutionList = net.ihe.gazelle.tm.systems.model.System
                        .getInstitutionsForASystem(systemInSessionResponder.getSystem());
                for (Institution institution2 : institutionList) {
                    if (institution2.equals(institution)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public int compareTo(TestStepsInstance o) {
        if (this == o) {
            return 0;
        }

        int comp = this.getTestSteps().compareTo(o.getTestSteps());
        if (comp == 0) {
            comp = this.getId().compareTo(o.getId());
        }

        return comp;
    }

    public void addInputContextualInformationInstance(ContextualInformationInstance inContextualInformationInstance) {
        if (inputContextualInformationInstanceList == null) {
            inputContextualInformationInstanceList = new ArrayList<ContextualInformationInstance>();
        }
        inputContextualInformationInstanceList.add(inContextualInformationInstance);
    }

    public void addOutputContextualInformationInstance(ContextualInformationInstance inContextualInformationInstance) {
        if (outputContextualInformationInstanceList == null) {
            outputContextualInformationInstanceList = new ArrayList<ContextualInformationInstance>();
        }
        outputContextualInformationInstanceList.add(inContextualInformationInstance);
    }

    public ContextualInformationInstance getContextualInformationOutputByContextualInformation(
            ContextualInformation contextualInformation) {
        for (ContextualInformationInstance cii : this.getOutputContextualInformationInstanceList()) {
            if (cii.getContextualInformation().equals(contextualInformation)) {
                return cii;
            }
        }
        return null;
    }

    public void initTestStepsInstance(EntityManager em) {
        this.status = null;
        TestStepsInstanceStatus statusTobeAppliedToTestSteps = TestStepsInstanceStatus.getSTATUS_DISACTIVATED();
        if (!this.getTestInstance().isOrchestrated()) {
            statusTobeAppliedToTestSteps = TestStepsInstanceStatus.getSTATUS_ACTIVATED();
        }
        this.testStepsInstanceStatus = statusTobeAppliedToTestSteps;
        for (ContextualInformationInstance cii : this.getOutputContextualInformationInstanceList()) {
            cii.initContextualInformationInstance(em);
        }
        for (ContextualInformationInstance cii : this.inputContextualInformationInstanceList) {
            cii.initContextualInformationInstance(em);
        }
        em.merge(this);
        em.flush();
    }

    public TestStepInstanceExecutionStatus getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(TestStepInstanceExecutionStatus testStepExecutionStatus) {
        this.executionStatus = testStepExecutionStatus;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
