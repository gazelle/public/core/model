/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, August
 * @class GenericOIDSConfigurationForSession.java
 * @package net.ihe.gazelle.tm.systems.model
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "tm_generic_oids_ip_config_param_for_session", schema = "public", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"profile_for_id", "testing_session_id"}),
        @UniqueConstraint(columnNames = {"oid_root_value", "testing_session_id"})})
@SequenceGenerator(name = "tm_generic_oids_ip_config_param_for_session_sequence", sequenceName = "tm_generic_oids_ip_config_param_for_session_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class GenericOIDSConfigurationForSession extends AuditedObject implements Serializable,
        Comparable<GenericOIDSConfigurationForSession> {


    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(GenericOIDSConfigurationForSession.class);

    /**
     * Id attribute!
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_generic_oids_ip_config_param_for_session_sequence")
    private Integer id;

    /**
     * Name used for this type of configuration
     */
    @Column(name = "oid_for_session")
    private String oidForSession;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "profile_for_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private IntegrationProfile profileFor;

    @Column(name = "oid_root_value", nullable = false)
    private String oidRootValue;

    @Column(name = "current_oid_value", nullable = false)
    private Integer currentOIDValue;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "testing_session_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private TestingSession testingSession;

    public static List<GenericOIDSConfigurationForSession> getConfigurationParametersForSession(TestingSession ts) {
        if (ts == null) {
            return null;
        }

        GenericOIDSConfigurationForSessionQuery q = new GenericOIDSConfigurationForSessionQuery();
        q.testingSession().eq(ts);
        List<GenericOIDSConfigurationForSession> result = q.getList();
        Collections.sort(result);
        return result;
    }

    public static GenericOIDSConfigurationForSession getConfigurationParameterForSessionForProfile(TestingSession ts,
                                                                                                   IntegrationProfile profile) {
        if ((ts == null) || (profile == null)) {
            return null;
        }
        GenericOIDSConfigurationForSessionQuery q = new GenericOIDSConfigurationForSessionQuery();
        q.testingSession().eq(ts);
        q.profileFor().eq(profile);
        GenericOIDSConfigurationForSession result = null;
        try {
            result = q.getUniqueResult();
        } catch (Exception e) {
            // ExceptionLogging.logException(e, log);

        }
        return result;
    }

    /**
     * Increment the OID passed in parameters and merge the current object
     *
     * @param inConf
     */
    public static void incrementOID(GenericOIDSConfigurationForSession inConf) {
        if (inConf == null) {
            return;
        }
        inConf.incrementOID();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOidForSession() {
        return oidForSession;
    }

    public void setOidForSession(String oidForSession) {
        this.oidForSession = oidForSession;
    }

    public IntegrationProfile getProfileFor() {
        return profileFor;
    }

    public void setProfileFor(IntegrationProfile profileFor) {
        this.profileFor = profileFor;
    }

    public String getOidRootValue() {
        return oidRootValue;
    }

    public void setOidRootValue(String oidRootValue) {
        this.oidRootValue = oidRootValue;
    }

    public Integer getCurrentOIDValue() {
        return currentOIDValue;
    }

    public void setCurrentOIDValue(Integer currentOIDValue) {
        this.currentOIDValue = currentOIDValue;
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    /**
     * Increment the OID
     *
     */
    public GenericOIDSConfigurationForSession incrementOID() {
        currentOIDValue++;
        EntityManager em = EntityManagerService.provideEntityManager();

        return em.merge(this);
    }

    @Override
    public int compareTo(GenericOIDSConfigurationForSession o) {
        return getProfileFor().getKeyword().compareTo(o.getProfileFor().getKeyword());
    }

    @Override
    public String toString() {
        return oidForSession + "." + oidRootValue + "." + currentOIDValue;
    }
}
