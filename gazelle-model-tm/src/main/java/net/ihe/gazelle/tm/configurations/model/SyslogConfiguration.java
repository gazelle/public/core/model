/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.tm.configurations.model.interfaces.ServerConfiguration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tm_syslog_configuration", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class SyslogConfiguration extends AbstractConfiguration implements ServerConfiguration {

    private static final long serialVersionUID = -4693167526388352292L;

    @Column(name = "port")
    @Range(min = 0, max = 65635)
    private Integer port;

    @Column(name = "port_proxy")
    @Range(min = 0, max = 65635)
    private Integer portProxy;

    @Column(name = "port_secured")
    @Range(min = 0, max = 65635)
    private Integer portSecured;

    @ManyToOne
    @JoinColumn(name = "transport_layer_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private TransportLayer transportLayer;

    @Column(name = "protocol_version")
    private String protocolVersion;

    // ~ Constructors
    // //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new SyslogConfiguration object.
     */
    public SyslogConfiguration() {
        super();
    }

    public SyslogConfiguration(Configuration inConfiguration, String inComments) {
        super(inConfiguration, inComments);
    }

    public SyslogConfiguration(Configuration inConfiguration) {
        super(inConfiguration);
    }

    public SyslogConfiguration(SyslogConfiguration sc) {
        super(sc);
        if (sc != null) {
            this.port = sc.port;
            this.portSecured = sc.portSecured;
            this.transportLayer = sc.transportLayer;
            this.protocolVersion = sc.protocolVersion;
            getProxyPortIfNeeded(null);
        }
    }

    public SyslogConfiguration(Configuration inConfiguration, Integer inPort, Integer inPortProxy,
                               Integer inPortSecured, TransportLayer inTransportLayer) {
        super(inConfiguration);
        port = inPort;
        portProxy = inPortProxy;
        portSecured = inPortSecured;
        transportLayer = inTransportLayer;
    }

    // ~ Methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public Integer getPort() {
        return port;
    }

    @Override
    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public Integer getPortProxy() {
        return portProxy;
    }

    @Override
    public void setPortProxy(Integer portProxy) {
        this.portProxy = portProxy;
    }

    @Override
    public Integer getPortSecured() {
        return portSecured;
    }

    @Override
    public void setPortSecured(Integer portSecured) {
        this.portSecured = portSecured;
    }

    public Integer getPortIfConfNotSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == false)) {
            return port;
        }
        return null;
    }

    public Integer getPortSecuredIfConfSecure() {
        if ((this.getConfiguration() != null) && (this.getConfiguration().getIsSecured() != null)
                && (this.getConfiguration().getIsSecured().booleanValue() == true)) {
            return portSecured;
        }
        return null;
    }

    public TransportLayer getTransportLayer() {
        return transportLayer;
    }

    public void setTransportLayer(TransportLayer transportLayer) {
        this.transportLayer = transportLayer;
    }

    public String getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(String protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    @Override
    public List<String> getCSVHeaders() {
        List<String> result = super.getCSVHeaders();
        addCSVServerHeaders(this, result);
        result.add("transport layer");
        result.add("syslog version");
        return result;
    }

    @Override
    public List<String> getCSVValues() {
        List<String> result = super.getCSVValues();
        addCSVServerValues(this, result);

        if (this.transportLayer != null) {
            result.add(this.transportLayer.getName());
        } else {
            result.add("");
        }
        if (this.protocolVersion != null) {
            result.add(this.protocolVersion);
        } else {
            result.add("");
        }

        return result;
    }

}
