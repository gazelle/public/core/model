package net.ihe.gazelle.tm.messages.model;

import net.ihe.gazelle.hql.HibernateHelper;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A message parameter, part of a message, used to be displayed in lists
 */
@Entity
@Table(name = "usr_message_parameters")
@SequenceGenerator(name = "usr_message_parameters_sequence", sequenceName = "usr_message_parameters_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class MessageParameter implements Serializable, Comparable<MessageParameter> {

    private static final long serialVersionUID = 8645714949823673012L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usr_message_parameters_sequence")
    private Integer id;

    private String messageParameter;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessageParameter() {
        return messageParameter;
    }

    public void setMessageParameter(String messageParameter) {
        this.messageParameter = messageParameter;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        builder.append(messageParameter);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int compareTo(MessageParameter messageParameter) {
        return messageParameter.getId().compareTo(this.getId());
    }
}
