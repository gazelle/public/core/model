/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.filter.list.NoHQLDataModel;
import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestQuery;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.gazelletest.model.reversed.MonitorTest;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>MonitorInSession<br>
 * <br>
 * This class describes the Monitor object, used by the Gazelle application. This class belongs to the Test Management module.
 * <p/>
 * Monitor possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Monitor</li>
 * <li><b>testList</b> : the list of tests that can be verified by the monitor</li>
 * <li><b>user</b> : the user object that contains monitor contact's informations</li>
 * </ul>
 * </br>
 *
 * @author Abdallah MILADI / INRIA Rennes IHE development Project
 * <p/>
 * <pre>
 *                         http://ihe.irisa.fr
 *                         </pre>
 * <p/>
 * <pre>
 *                         amiladi@irisa.fr
 *                         </pre>
 * @version 1.0 , 8 juil. 2009
 * @class Monitor.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */
@Entity
@Table(name = "tm_monitor_in_session", schema = "public")
@SequenceGenerator(name = "tm_monitor_in_session_sequence", sequenceName = "tm_monitor_in_session_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class MonitorInSession extends AuditedObject implements Serializable, Comparable<MonitorInSession> {


    private static final long serialVersionUID = 4446846531996928618L;
    @ManyToOne
    @JoinColumn(name = "testing_session_id")
    @Fetch(value = FetchMode.SELECT)
    TestingSession testingSession;
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_monitor_in_session_sequence")
    private Integer id;
    @Column(name = "user_id")
    @Fetch(value = FetchMode.SELECT)
    private String userId;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "tm_monitor_test", joinColumns = @JoinColumn(name = "monitor_id"), inverseJoinColumns = @JoinColumn(name = "test_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "monitor_id", "test_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<Test> testList;
    // Computed set, do not update that one
    @OneToMany(mappedBy = "monitorInSession", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<MonitorTest> monitorTests;
    @OneToMany(mappedBy = "monitorInSession", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<TestInstance> testInstances;
    @Column(name = "is_activated")
    private boolean isActivated = true;

    public MonitorInSession() {
    }

    public MonitorInSession(String userId, boolean isActivated, TestingSession testingSession) {
        this.userId = userId;
        this.testingSession = testingSession;
        this.isActivated = isActivated;
    }

    public MonitorInSession(String userId, List<Test> testList, boolean isActivated, TestingSession testingSession) {
        this.userId = userId;
        this.testList = testList;
        this.testingSession = testingSession;
        this.isActivated = isActivated;
    }

    public MonitorInSession(MonitorInSession inMonitor) {
        if (inMonitor.getUserId() != null) {
            this.userId = inMonitor.getUserId();
        }
        if (inMonitor.getTestList() != null) {
            this.testList = inMonitor.getTestList();
        }
        if (inMonitor.getTestingSession() != null) {
            this.testingSession = inMonitor.getTestingSession();
        }
        this.isActivated = inMonitor.isActivated();
    }

    public static List<Test> getTestsByMonitorByDomainByIPByActor(MonitorInSession inMonitorInSession,
                                                                  List<TestType> inTestTypes, Domain inDomain, IntegrationProfile inIntegrationProfile, Actor inActor,
                                                                  IntegrationProfileOption inIntegrationProfileOption, Boolean isTested) {
        if (inMonitorInSession == null) {
            return null;
        }

        TestQuery query = new TestQuery();
        query.monitorsInSession().id().eq(inMonitorInSession.getId());

        if ((inTestTypes != null) && !inTestTypes.isEmpty()) {
            query.testType().in(inTestTypes);
        }

        if (inDomain != null) {
            query.testRoles().roleInTest().testParticipantsList().actorIntegrationProfileOption()
                    .actorIntegrationProfile().integrationProfile().domainsForDP().id().eq(inDomain.getId());
        }

        query.testRoles().roleInTest().testParticipantsList().actorIntegrationProfileOption().actorIntegrationProfile()
                .integrationProfile().eqIfValueNotNull(inIntegrationProfile);
        query.testRoles().roleInTest().testParticipantsList().actorIntegrationProfileOption().actorIntegrationProfile()
                .actor().eqIfValueNotNull(inActor);
        query.testRoles().roleInTest().testParticipantsList().actorIntegrationProfileOption()
                .integrationProfileOption().eq(inIntegrationProfileOption);
        query.testRoles().roleInTest().testParticipantsList().tested().eqIfValueNotNull(isTested);
        return query.getList();

    }

    public static List<MonitorInSession> getAllActivatedMonitorsForATestingSession(TestingSession inTestingSession) {
        return getMonitorsList(null, inTestingSession, true);
    }

    public static List<MonitorInSession> getAllActivatedMonitorsByUser(String username) {
        return getMonitorsList(username, null, true);
    }

    public static List<MonitorInSession> getAllDisactivatedMonitorsByUser(String username) {
        return getMonitorsList(username, null, false);
    }

    public static List<MonitorInSession> getDisactivatedMonitorInSessionForATestingSessionByUser(
            TestingSession inTestingSession, String username) {
        return getMonitorsList(username, inTestingSession, false);
    }

    public static MonitorInSession getActivatedMonitorInSessionForATestingSessionByUser(
            TestingSession inTestingSession, String username) {
        List<MonitorInSession> list = getMonitorsList(username, inTestingSession, true);
        if (list != null) {
            return list.get(0);
        }
        return null;
    }

    private static List<MonitorInSession> getMonitorsList(String username, TestingSession inTestingSession,
                                                          boolean isActivated) {
        MonitorInSessionQuery query = new MonitorInSessionQuery();
        query.isActivated().eq(isActivated);
        if (username != null) {
            query.userId().eq(username);
        }
        query.testingSession().eqIfValueNotNull(inTestingSession);
        List<MonitorInSession> result = query.getListNullIfEmpty();
        if (result != null) {
            Collections.sort(result);
        }
        return result;
    }

    public static List<MonitorInSession> getMonitorInSessionListForATest(Test inTest) {
        if (inTest != null) {
            MonitorInSessionQuery query = new MonitorInSessionQuery();
            query.testList().id().eq(inTest.getId());
            return query.getListNullIfEmpty();
        }
        return null;
    }

    public static List<TestInstance> getTestInstanceForAMonitorByTest(MonitorInSession inMonitorInSession, Test inTest,
                                                                      Status inTestInstanceStatus, TestingSession inTestingSession) {
        if (inTest != null) {
            TestInstanceQuery query = new TestInstanceQuery();
            query.monitorInSession().eq(inMonitorInSession);
            query.test().eq(inTest);
            query.lastStatus().eq(inTestInstanceStatus);
            query.testingSession().eq(inTestingSession);
            return query.getListNullIfEmpty();
        }
        return null;
    }

    protected static TestInstanceQuery getQuery(MonitorInSession inMonitorInSessionClaimed,
                                                MonitorInSession inMonitorInSessionPossible, Status inTestInstanceStatus, Institution inInstitution,
                                                SystemInSession inSystemInSession, Domain inDomain, IntegrationProfile inIntegrationProfile, Actor inActor,
                                                IntegrationProfileOption inIntegrationProfileOption, List<TestType> inTestTypes,
                                                TestingSession testingSession) {
        TestInstanceQuery query = new TestInstanceQuery();
        query.monitorInSession().eqIfValueNotNull(inMonitorInSessionClaimed);
        if (inMonitorInSessionPossible != null) {
            query.test().monitorsInSession().id().eq(inMonitorInSessionPossible.getId());
        }
        query.lastStatus().eqIfValueNotNull(inTestInstanceStatus);
        query.testInstanceParticipants().systemInSessionUser().systemInSession().system().institutionSystems()
                .institution().eqIfValueNotNull(inInstitution);
        query.testInstanceParticipants().systemInSessionUser().systemInSession().eqIfValueNotNull(inSystemInSession);
        if (inDomain != null) {
            query.test().testRoles().roleInTest().testParticipantsList().actorIntegrationProfileOption()
                    .actorIntegrationProfile().integrationProfile().domainsForDP().id().eq(inDomain.getId());
        }
        query.test().testRoles().roleInTest().testParticipantsList().actorIntegrationProfileOption()
                .actorIntegrationProfile().integrationProfile().eqIfValueNotNull(inIntegrationProfile);
        query.test().testRoles().roleInTest().testParticipantsList().actorIntegrationProfileOption()
                .actorIntegrationProfile().actor().eqIfValueNotNull(inActor);
        query.test().testRoles().roleInTest().testParticipantsList().actorIntegrationProfileOption()
                .integrationProfileOption().eqIfValueNotNull(inIntegrationProfileOption);
        if ((inTestTypes != null) && !inTestTypes.isEmpty()) {
            query.test().testType().in(inTestTypes);
        }
        query.testingSession().eqIfValueNotNull(testingSession);
        return query;
    }

    public static List<TestInstance> getTestInstancesFilteredForAMonitor(MonitorInSession inMonitorInSessionClaimed,
                                                                         MonitorInSession inMonitorInSessionPossible, Status inTestInstanceStatus, Institution inInstitution,
                                                                         SystemInSession inSystemInSession, Domain inDomain, IntegrationProfile inIntegrationProfile, Actor inActor,
                                                                         IntegrationProfileOption inIntegrationProfileOption, List<TestType> inTestTypes,
                                                                         TestingSession testingSession) {

        TestInstanceQuery query = getQuery(inMonitorInSessionClaimed, inMonitorInSessionPossible, inTestInstanceStatus,
                inInstitution, inSystemInSession, inDomain, inIntegrationProfile, inActor, inIntegrationProfileOption,
                inTestTypes, testingSession);

        return query.getList();
    }

    public static int getNumberTestInstancesFilteredForAMonitor(MonitorInSession inMonitorInSessionClaimed,
                                                                MonitorInSession inMonitorInSessionPossible, Status inTestInstanceStatus, Institution inInstitution,
                                                                SystemInSession inSystemInSession, Domain inDomain, IntegrationProfile inIntegrationProfile, Actor inActor,
                                                                IntegrationProfileOption inIntegrationProfileOption, List<TestType> inTestTypes,
                                                                TestingSession testingSession) {

        TestInstanceQuery query = getQuery(inMonitorInSessionClaimed, inMonitorInSessionPossible, inTestInstanceStatus,
                inInstitution, inSystemInSession, inDomain, inIntegrationProfile, inActor, inIntegrationProfileOption,
                inTestTypes, testingSession);
        return query.getCount();
    }

    public static boolean isUserAllowedToValidateTestInstance(GazelleIdentity identity, TestInstance inTestInstance, TestingSession selectedTestingSession) {
        if (identity != null) {
            if (inTestInstance != null) {
                if (identity.hasRole(Role.MONITOR)) {
                    MonitorInSession monitorInSession = MonitorInSession
                            .getActivatedMonitorInSessionForATestingSessionByUser(
                                    selectedTestingSession, identity.getUsername());
                    if (monitorInSession != null) {
                        List<Test> testList = monitorInSession.getTestList();
                        if (testList != null) {
                            return testList.contains(inTestInstance.getTest());
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean isMonitorForSelectedSession(String email, TestingSession selectedTestingSession) {
        if (email != null) {
            if (selectedTestingSession != null) {
                List<MonitorInSession> monitors = selectedTestingSession.getMonitors();
                for (MonitorInSession monitorInSession : monitors) {
                    if (monitorInSession.isActivated() && (monitorInSession.getUserId() != null)
                            && monitorInSession.getUserId().equals(email)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public Integer getId() {
        return id;
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user) {
        this.userId = user;
    }

    public List<Test> getTestList() {
        return HibernateHelper.getLazyValue(this, "testList", testList);
    }

    public void setTestList(List<Test> testList) {
        this.testList = testList;
    }

    public NoHQLDataModel<Test> getAllTestList() {
        final List<Test> res = getTestList();
        return new GazelleListDataModel<Test>(res);
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setActivated(boolean isActivated) {
        this.isActivated = isActivated;
    }

    @Override
    public int compareTo(MonitorInSession o) {
        String a = this.userId;
        String b = o.getUserId();

        return a.compareTo(b);
    }

    public Set<MonitorTest> getMonitorTests() {
        return HibernateHelper.getLazyValue(this, "monitorTests", this.monitorTests);
    }

    public Set<TestInstance> getTestInstances() {
        return HibernateHelper.getLazyValue(this, "testInstances", this.testInstances);
    }

}
