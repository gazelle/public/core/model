/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Audited
@Table(name = "tm_test_steps_option", schema = "public")
@SequenceGenerator(name = "tm_test_steps_option_sequence", sequenceName = "tm_test_steps_option_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class TestStepsOption extends AuditedObject implements Serializable {


    private static final long serialVersionUID = -461198794161845131L;

    private static String TEST_STEPS_OPTION_REQUIRED_KEYWORD = "R";
    private static String TEST_STEPS_OPTION_OPTIONAL_KEYWORD = "O";

    private static Logger log = LoggerFactory.getLogger(TestStepsOption.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_steps_option_sequence")
    private Integer id;

    @Column(name = "keyword", unique = true)
    private String keyword;

    @Column(name = "label_to_display")
    private String labelToDisplay;

    @Column(name = "description")
    private String description;

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public TestStepsOption() {
        super();
    }

    public TestStepsOption(TestStepsOption inTestStepsOption) {
        if (inTestStepsOption.getDescription() != null) {
            this.description = inTestStepsOption.getDescription();
        }
        if (inTestStepsOption.getKeyword() != null) {
            this.keyword = inTestStepsOption.getKeyword();
        }
        if (inTestStepsOption.getLabelToDisplay() != null) {
            this.labelToDisplay = inTestStepsOption.getLabelToDisplay();
        }
    }

    public TestStepsOption(String keyword, String labelToDisplay, String description) {
        this.description = description;
        this.labelToDisplay = labelToDisplay;
        this.keyword = keyword;
    }

    public static List<TestStepsOption> listTestOptions() {
        TestStepsOptionQuery q = new TestStepsOptionQuery();
        q.setCachable(true);
        return q.getList();
    }

    public static TestStepsOption getTestStepsOptionByKeyword(String keyword) {
        TestStepsOptionQuery q = new TestStepsOptionQuery();
        q.setCachable(true);
        q.keyword().eq(keyword);
        return q.getUniqueResult();
    }

    public static TestStepsOption getTEST_STEPS_OPTION_REQUIRED() {
        return getTestStepsOptionByKeyword(TEST_STEPS_OPTION_REQUIRED_KEYWORD);
    }

    public static TestStepsOption getTEST_STEPS_OPTION_OPTIONAL() {
        return getTestStepsOptionByKeyword(TEST_STEPS_OPTION_OPTIONAL_KEYWORD);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public void setLabelToDisplay(String labelToDisplay) {
        this.labelToDisplay = labelToDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        TestStepsOptionQuery query = new TestStepsOptionQuery();
        query.keyword().eq(this.keyword);
        List<TestStepsOption> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

}