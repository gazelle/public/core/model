package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Audited
@Table(name = "tm_automated_test_step_input", schema = "public")
@SequenceGenerator(name = "tm_automated_test_step_input_sequence", sequenceName = "tm_automated_test_step_input_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class AutomatedTestStepInput implements Serializable {

    private static final long serialVersionUID = -7407875622129416361L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_automated_test_step_input_sequence")
    private int id;

    @Column(name = "key")
    private String key;

    @Column(name = "label")
    private String label;

    @Column(name = "description")
    private String description;

    @Column(name = "type")
    private String type;

    @Column(name = "required")
    private boolean required;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "test_steps_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private AutomatedTestStepEntity automatedTestStep;

    public AutomatedTestStepInput() {
        // for injection
    }

    public AutomatedTestStepInput(String key, String label, String description, String type, boolean required, AutomatedTestStepEntity automatedTestStep) {
        this.key = key;
        this.label = label;
        this.description = description;
        this.type = type;
        this.required = required;
        this.automatedTestStep = automatedTestStep;
    }

    public AutomatedTestStepInput(AutomatedTestStepInputDomain domain, AutomatedTestStepEntity automatedTestStep) {
        this.key = domain.getKey();
        this.label = domain.getLabel();
        this.description = domain.getDescription();
        this.type = domain.getType();
        this.required = domain.getRequired();
        this.automatedTestStep = automatedTestStep;
    }

    public AutomatedTestStepInputDomain asDomain() {
        AutomatedTestStepInputDomain domain = new AutomatedTestStepInputDomain();
        domain.setId(this.id);
        domain.setKey(this.key);
        domain.setLabel(this.label);
        domain.setDescription(this.description);
        domain.setType(this.type);
        domain.setRequired(this.required);
        return domain;
    }

    public int getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
