package net.ihe.gazelle.tm.tee.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetGazelle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * New look-up table for all possible validation-statuses from message validation performed by validation services invoked by TEE.
 *
 * @author rchitnis
 */
@Entity
@Table(name = "validation_status", schema = "public")
@SequenceGenerator(name = "validation_status_sequence", sequenceName = "validation_status_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class ValidationStatus implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @Column(unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "validation_status_sequence")
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "key", unique = true, nullable = false, length = 45)
    private ValidationStatusEnum key;

    @Column(name = "description", nullable = false, length = 1024)
    private String description;

    @Column(name = "label_key_for_display")
    private String labelKeyForDisplay;

    public ValidationStatus() {
    }

    public ValidationStatus(Integer id, ValidationStatusEnum key, String description, String labelKeyForDisplay) {
        this.id = id;
        this.key = key;
        this.description = description;
        this.labelKeyForDisplay = labelKeyForDisplay;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ValidationStatusEnum getKey() {
        return key;
    }

    public void setKey(ValidationStatusEnum key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabelKeyForDisplay() {
        return labelKeyForDisplay;
    }

    public void setLabelKeyForDisplay(String labelKeyForDisplay) {
        this.labelKeyForDisplay = labelKeyForDisplay;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

    @Override
    public String toString() {
        return "ValidationStatus [id=" + id + ", keyword=" + key + ", description=" + description
                + ", labelKeyForDisplay=" + labelKeyForDisplay + "]";
    }


}
