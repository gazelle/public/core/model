package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "tm_profile_in_testing_session",schema = "public")
@SequenceGenerator(name = "tm_profile_in_testing_session_sequence", sequenceName = "tm_profile_in_testing_session_id_seq", allocationSize = 1)
public class ProfileInTestingSession extends AuditedObject implements Serializable, Comparable<ProfileInTestingSession> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_profile_in_testing_session_sequence")
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "testing_session_id")
    @Fetch(value = FetchMode.SELECT)
    private TestingSession testingSession;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "integration_profile_id")
    @Fetch(value = FetchMode.SELECT)
    private IntegrationProfile integrationProfile;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "domain_id")
    @Fetch(value = FetchMode.SELECT)
    private Domain domain;
    @Column(name = "comment")
    private String comment;
    @Enumerated(EnumType.STRING)
    private Testability testability;

    public ProfileInTestingSession() {
        this.comment="";
        this.testability=Testability.DECISION_PENDING;
    }
    public ProfileInTestingSession(TestingSession testingSession, IntegrationProfile integrationProfile, Domain domain) {
        this.testingSession = testingSession;
        this.integrationProfile = integrationProfile;
        this.domain = domain;
        this.testability=Testability.DECISION_PENDING;

    }
    public ProfileInTestingSession(Integer id, TestingSession testingSession, IntegrationProfile integrationProfile, Domain domain) {
        this.id = id;
        this.testingSession = testingSession;
        this.integrationProfile = integrationProfile;
        this.domain = domain;
        this.testability=Testability.DECISION_PENDING;
    }

    public ProfileInTestingSession(Integer id, TestingSession testingSession, IntegrationProfile integrationProfile, Domain domain, String comment) {
        this(id, testingSession, integrationProfile, domain);
        this.comment = comment;
        this.testability=Testability.DECISION_PENDING;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    public IntegrationProfile getIntegrationProfile() {
        return integrationProfile;
    }

    public void setIntegrationProfile(IntegrationProfile integrationProfile) {
        this.integrationProfile = integrationProfile;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Testability getTestability() {
        return testability;
    }

    public void setTestability(Testability testability) {
        this.testability = testability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ProfileInTestingSession that = (ProfileInTestingSession) o;
        return id.equals(that.id) && testingSession.equals(that.testingSession) && integrationProfile.equals(that.integrationProfile) && domain.equals(that.domain) && Objects.equals(comment, that.comment) && testability == that.testability;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, testingSession, integrationProfile, domain, comment, testability);
    }

    @Override
    public int compareTo(ProfileInTestingSession o) {
        //TODO
        return getId().compareTo(o.getId());
    }
}
