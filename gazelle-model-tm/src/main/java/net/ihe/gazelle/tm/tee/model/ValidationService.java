package net.ihe.gazelle.tm.tee.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetGazelle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * New look-up table for all possible validation-service tools that intercepted messages can be validated with.
 * <p/>
 * Messages could be validated using different tools depending on capability of the tool.
 *
 * @author tnabeel
 */
@XmlRootElement(name = "validationService")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "validation_service", schema = "public")
@SequenceGenerator(name = "validation_service_sequence", sequenceName = "validation_service_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class ValidationService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "validation_service_sequence")
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "key", unique = true, nullable = false, length = 45)
    private ValidationServiceEnum key;

    @Column(name = "description", nullable = false, length = 1024)
    private String description;

    @Column(name = "label_key_for_display")
    private String labelKeyForDisplay;

    @Column(name = "base_url")
    private String baseUrl;

    @Column(name = "url_path")
    private String urlPath;

    @OneToMany(mappedBy = "validationService")
    @XmlTransient
    private Set<MessageValidationService> messageValidationServices;
    @Column(name = "xslt_url", nullable = true, length = 1024)
    private String xsltUrl;

    public ValidationService() {
    }

    public ValidationService(Integer id, ValidationServiceEnum key, String description, String labelKeyForDisplay,
                             String baseUrl) {
        this.id = id;
        this.key = key;
        this.description = description;
        this.labelKeyForDisplay = labelKeyForDisplay;
        this.baseUrl = baseUrl;
    }

    public ValidationService(Integer id, ValidationServiceEnum key, String description, String labelKeyForDisplay,
                             String baseUrl, String xsltUrl) {
        this.id = id;
        this.key = key;
        this.description = description;
        this.labelKeyForDisplay = labelKeyForDisplay;
        this.baseUrl = baseUrl;
        this.xsltUrl = xsltUrl;
    }

    public static List<ValidationService> getAllValidationServices() {
        ValidationServiceQuery q = new ValidationServiceQuery();
        return q.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ValidationServiceEnum getKey() {
        return key;
    }

    public void setKey(ValidationServiceEnum key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabelKeyForDisplay() {
        return labelKeyForDisplay;
    }

    public void setLabelKeyForDisplay(String labelKeyForDisplay) {
        this.labelKeyForDisplay = labelKeyForDisplay;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

    @Override
    public String toString() {
        return "ValidationService [id=" + id + ", key=" + key + ", description=" + description
                + ", labelKeyForDisplay=" + labelKeyForDisplay + ", baseUrl=" + baseUrl + ", urlPath=" + urlPath + "]";
    }

    public Set<MessageValidationService> getMessageValidationServices() {
        return this.messageValidationServices;
    }

    public void setMessageValidationServices(Set<MessageValidationService> messageValidationServices) {
        this.messageValidationServices = messageValidationServices;
    }

    public MessageValidationService addMessageValidationService(MessageValidationService messageValidationService) {
        getMessageValidationServices().add(messageValidationService);
        messageValidationService.setValidationService(this);

        return messageValidationService;
    }

    public MessageValidationService removeMessageValidationService(MessageValidationService messageValidationService) {
        getMessageValidationServices().remove(messageValidationService);
        messageValidationService.setValidationService(null);

        return messageValidationService;
    }

    public String getXsltUrl() {
        return xsltUrl;
    }

    public void setXsltUrl(String xsltUrl) {
        this.xsltUrl = xsltUrl;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;
        ValidationServiceQuery query = new ValidationServiceQuery(entityManager);
        query.key().eq(this.key);
        List<ValidationService> listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }

    }
}
