/**
 *
 */
package net.ihe.gazelle.tm.tee.model;

/**
 * @author rchitnis
 */
public enum ValidationStatusEnum {
    ERROR, FAIL, WARNING, PASS;
}
