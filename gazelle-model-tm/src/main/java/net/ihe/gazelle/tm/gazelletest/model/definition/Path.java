package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author aboufahj
 */
@Entity
@Audited
@Table(name = "tm_path", schema = "public")
@SequenceGenerator(name = "tm_path_sequence", sequenceName = "tm_path_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class Path extends AuditedObject implements Serializable, Comparable<Path> {

    private static final long serialVersionUID = 1L;
    private static Logger log = LoggerFactory.getLogger(Path.class);
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_path_sequence")
    private Integer id;

    @Column(name = "keyword", nullable = false, unique = true)
    private String keyword;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "type", nullable = true)
    private String type;

    // constructors /////////

    public Path() {
    }

    public Path(String keyword, String description, String type) {
        super();
        this.keyword = keyword;
        this.description = description;
        this.type = type;
    }

    public static List<Path> getAllOPath() {
        PathQuery q = new PathQuery();
        return q.getList();
    }

    public static void deletePath(Path path) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Path pat = entityManager.find(Path.class, path.getId());
        entityManager.remove(pat);
        entityManager.flush();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Path [id=" + id + ", keyword=" + keyword + ", description=" + description + ", type=" + type + "]";
    }

    @Override
    public int compareTo(Path o) {
        return getKeyword().compareTo(o.getKeyword());
    }
}
