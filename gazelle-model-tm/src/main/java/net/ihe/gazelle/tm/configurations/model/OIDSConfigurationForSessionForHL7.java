/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, jun
 * @class OIDSConfigurationForSessionForHL7.java
 * @package net.ihe.gazelle.tm.systems.model
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "tm_oids_ip_config_param_for_session_for_hl7", schema = "public")
@SequenceGenerator(name = "tm_oids_ip_config_param_for_session_for_hl7_sequence", sequenceName = "tm_oids_ip_config_param_for_session_for_hl7_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class OIDSConfigurationForSessionForHL7 extends AuditedObject implements Serializable {


    private static final long serialVersionUID = 1L;
    /**
     * Id attribute!
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_oids_ip_config_param_for_session_for_hl7_sequence")
    private Integer id;
    /**
     * Name used for this type of configuration
     */
    @Column(name = "oid_for_session")
    private String oidForSession;
    @Column(name = "oid_for_hl7_v2")
    private String oidForHL7V2;
    @Column(name = "current_id_for_oid_hl7_v2", nullable = false)
    private Integer currentOIDForHL7V2;
    @Column(name = "oid_for_hl7_v3")
    private String oidForHL7V3;
    @Column(name = "current_id_for_oid_hl7_v3", nullable = false)
    private Integer currentOIDForHL7V3;
    @ManyToOne
    @JoinColumn(name = "testing_session_id", nullable = false, unique = true)
    @Fetch(value = FetchMode.SELECT)
    private TestingSession testingSession;

    public OIDSConfigurationForSessionForHL7() {
    }

    public OIDSConfigurationForSessionForHL7(TestingSession inTestingSession) {
        currentOIDForHL7V2 = 0;
        currentOIDForHL7V3 = 0;
        testingSession = inTestingSession;
    }

    public static OIDSConfigurationForSessionForHL7 getConfigurationParametersForSession(TestingSession ts) {
        OIDSConfigurationForSessionForHL7Query q = new OIDSConfigurationForSessionForHL7Query();
        q.testingSession().eq(ts);
        List<OIDSConfigurationForSessionForHL7> result = q.getList();

        if (result.size() > 0) {

            return result.get(0);

        } else {
            return null;
        }

    }

    /**
     * Increment the OID passed in parameters and merge the current object
     *
     * @param inOidType
     */
    public static void incrementOID(OIDSConfigurationForSessionForHL7 inConf, OidsTypeForHL7 inOidType) {
        if (inOidType.equals(OidsTypeForHL7.oidHL7V2)) {
            inConf.currentOIDForHL7V2++;
        } else if (inOidType.equals(OidsTypeForHL7.oidHL7V3)) {
            inConf.currentOIDForHL7V3++;
        }

        EntityManager em = EntityManagerService.provideEntityManager();

        em.merge(inConf);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOidForSession() {
        return oidForSession;
    }

    public void setOidForSession(String oidForSession) {
        this.oidForSession = oidForSession;
    }

    public String getOidForHL7V2() {
        return oidForHL7V2;
    }

    public void setOidForHL7V2(String oidForHL7V2) {
        this.oidForHL7V2 = oidForHL7V2;
    }

    public Integer getCurrentOIDForHL7V2() {
        return currentOIDForHL7V2;
    }

    public void setCurrentOIDForHL7V2(Integer currentOIDForHL7V2) {
        this.currentOIDForHL7V2 = currentOIDForHL7V2;
    }

    public String getOidForHL7V3() {
        return oidForHL7V3;
    }

    public void setOidForHL7V3(String oidForHL7V3) {
        this.oidForHL7V3 = oidForHL7V3;
    }

    public Integer getCurrentOIDForHL7V3() {
        return currentOIDForHL7V3;
    }

    public void setCurrentOIDForHL7V3(Integer currentOIDForHL7V3) {
        this.currentOIDForHL7V3 = currentOIDForHL7V3;
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    /**
     * Increment the OID passed in parameters and merge the current object
     *
     * @param inOidType
     */
    public OIDSConfigurationForSessionForHL7 incrementOID(OidsTypeForHL7 inOidType) {
        if (inOidType.equals(OidsTypeForHL7.oidHL7V2)) {
            currentOIDForHL7V2++;
        } else if (inOidType.equals(OidsTypeForHL7.oidHL7V3)) {
            currentOIDForHL7V3++;
        }

        EntityManager em = EntityManagerService.provideEntityManager();

        return em.merge(this);
    }

    public String toString(OidsTypeForHL7 inOidsTypeForHL7) {
        if (inOidsTypeForHL7.equals(OidsTypeForHL7.oidHL7V2)) {
            return oidForSession + "." + oidForHL7V2 + "." + currentOIDForHL7V2;
        } else {
            return oidForSession + "." + oidForHL7V3 + "." + currentOIDForHL7V3;
        }
    }

    public enum OidsTypeForHL7 {
        oidHL7V2,
        oidHL7V3
    }
}
