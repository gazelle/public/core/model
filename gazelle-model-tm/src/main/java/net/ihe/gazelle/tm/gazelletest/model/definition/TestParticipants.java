/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTestDefinition;
import net.ihe.gazelle.tf.model.*;
import net.ihe.gazelle.tm.gazelletest.model.reversed.AIPO;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>TestParticipants<br>
 * <br>
 * This class describes the TestParticipants object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * TestParticipants possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the TestParticipants</li>
 * <li><b>actor</b> : actor corresponding to the TestParticipants</li>
 * <li><b>integrationProfile</b> : integrationProfile corresponding to the TestParticipants</li>
 * <li><b>integrationProfileOption</b> : integrationProfileOption corresponding to the TestParticipants</li>
 * </ul>
 * </br>
 *
 * @author JB Meyer/ INRIA Rennes IHE development Project
 *         <p/>
 *         <pre>
 *                 http://ihe.irisa.fr
 *                 </pre>
 *         <p/>
 *         <pre>
 *                 jmeyer@irisa.fr
 *                 </pre>
 * @version 1.0 , 8 janv. 2010
 * @class TestParticipants.java
 * @package net.ihe.gazelle.tm.gazelletest.model
 */
@XmlRootElement(name = "TestParticipants")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tm_test_participants", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "actor_integration_profile_option_id", "is_tested"}))
@SequenceGenerator(name = "tm_test_participants_sequence", sequenceName = "tm_test_participants_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class TestParticipants extends AuditedObject implements Serializable, Comparable<TestParticipants> {


    private static final long serialVersionUID = 2122788882267724072L;

    private static Logger log = LoggerFactory.getLogger(TestParticipants.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_participants_sequence")
    @XmlTransient
    private Integer id;

    @ManyToOne
    // JRC : Remove nullable constraint in : @JoinColumn(name = "actor_integration_profile_option_id" , nullable = false) to be able to sync TestParticipants object with GMM
    // JRC : Remove nullable constraint @NotNull to be able to sync TestParticipants object with GMM
    @JoinColumn(name = "actor_integration_profile_option_id", nullable = false)
    @NotNull
    @Fetch(value = FetchMode.SELECT)
    private ActorIntegrationProfileOption actorIntegrationProfileOption;

    @Column(name = "is_tested", nullable = false)
    private Boolean tested;

    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "testParticipantsList")
    @NotAudited
    //@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    //@JoinTable(name = "tm_role_in_test_test_participants", joinColumns = @JoinColumn(name = "test_participants_id"), inverseJoinColumns = @JoinColumn(name = "role_in_test_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
    //       "role_in_test_id", "test_participants_id"}))
    @XmlTransient
    private List<RoleInTest> roleInTest;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotAudited
    @JoinColumn(name = "actor_integration_profile_option_id", nullable = false, insertable = false, updatable = false)
    @Fetch(value = FetchMode.SELECT)
    @XmlTransient
    private AIPO aipo;

    public TestParticipants() {

        actorIntegrationProfileOption = new ActorIntegrationProfileOption();
    }

    public TestParticipants(TestParticipants inTestParticipants) {
        if (inTestParticipants != null) {
            if (inTestParticipants.actorIntegrationProfileOption != null) {
                actorIntegrationProfileOption = new ActorIntegrationProfileOption(
                        inTestParticipants.actorIntegrationProfileOption);
            }
            if (inTestParticipants.tested != null) {
                tested = Boolean.valueOf(inTestParticipants.tested);
            }
        }
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    /**
     * Get the testParticipants for the specified actors and integrationProfile. If the result list is null we create one and return it
     *
     * @param inActor
     * @param inIntegrationProfile
     *
     * @return
     */
    public static List<TestParticipants> getTestParticipants(Actor inActor, IntegrationProfile inIntegrationProfile) {
        if ((inActor == null) || (inIntegrationProfile == null)) {
            return null;
        }
        TestParticipantsQuery q = new TestParticipantsQuery();
        q.actorIntegrationProfileOption().actorIntegrationProfile().actor().eq(inActor);
        q.actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile().eq(inIntegrationProfile);

        List<TestParticipants> listToReturn = q.getList();

        return listToReturn;

    }

    public static TestParticipants getTestParticipants(Actor inActor, IntegrationProfile inIntegrationProfile,
                                                       IntegrationProfileOption inOption) {
        TestParticipantsQuery q = new TestParticipantsQuery();
        if (inOption == null) {
            q.actorIntegrationProfileOption().actorIntegrationProfile().actor().eq(inActor);
            q.actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile().eq(inIntegrationProfile);
        } else {
            q.actorIntegrationProfileOption().integrationProfileOption().eq(inOption);
        }

        List<TestParticipants> listToReturn = q.getList();

        return listToReturn.get(0);

    }

    public static List<TestParticipants> getTestParticipantsFiltered(
            ActorIntegrationProfileOption inActorIntegrationProfileOption,
            ActorIntegrationProfile inActorIntegrationProfile, IntegrationProfile inIntegrationProfile,
            IntegrationProfileOption inIntegrationProfileOption, Actor inActor, Boolean inTested) {
        TestParticipantsQuery query = new TestParticipantsQuery();
        query.actorIntegrationProfileOption().eqIfValueNotNull(inActorIntegrationProfileOption);
        query.actorIntegrationProfileOption().actorIntegrationProfile().eqIfValueNotNull(inActorIntegrationProfile);
        query.actorIntegrationProfileOption().actorIntegrationProfile().integrationProfile()
                .eqIfValueNotNull(inIntegrationProfile);
        query.actorIntegrationProfileOption().actorIntegrationProfile().actor().eqIfValueNotNull(inActor);
        query.actorIntegrationProfileOption().integrationProfileOption().eqIfValueNotNull(inIntegrationProfileOption);
        query.tested().eqIfValueNotNull(inTested);
        return query.getList();

    }

    public static void deleteTestParticipants(TestParticipants tp, EntityManager em) {
        if (tp != null) {
            tp = em.find(TestParticipants.class, tp.getId());
            List<RoleInTest> lrit = tp.getRoleInTest();
            if ((lrit == null) || (lrit.size() == 0)) {
                em.remove(tp);
                em.flush();
            }
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<RoleInTest> getRoleInTest() {
        return HibernateHelper.getLazyValue(this, "roleInTest", this.roleInTest);
    }

    public void setRoleInTest(List<RoleInTest> roleInTest) {
        this.roleInTest = roleInTest;
    }

    public ActorIntegrationProfileOption getActorIntegrationProfileOption() {
        return actorIntegrationProfileOption;
    }

    public void setActorIntegrationProfileOption(ActorIntegrationProfileOption actorIntegrationProfileOption) {
        this.actorIntegrationProfileOption = actorIntegrationProfileOption;
    }

    public Boolean getTested() {
        return tested;
    }

    public void setTested(Boolean tested) {
        this.tested = tested;
    }

    public AIPO getAipo() {
        return HibernateHelper.getLazyValue(this, "aipo", this.aipo);
    }

    @Override
    public int compareTo(TestParticipants o) {
        return getActorIntegrationProfileOption().compareTo(o.getActorIntegrationProfileOption());
    }

    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof TestParticipants)) {
            return false;
        }
        TestParticipants otherCastedobject = (TestParticipants) other;
        if (otherCastedobject.tested.equals(this.tested)
                && otherCastedobject.actorIntegrationProfileOption.equals(this.actorIntegrationProfileOption)) {
            return true;
        }
        return false;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        actorIntegrationProfileOption.setLastModifierId(this.lastModifierId);
        actorIntegrationProfileOption.saveOrMerge(entityManager);
        TestParticipantsQuery query = new TestParticipantsQuery();
        query.tested().eq(this.tested);
        query.actorIntegrationProfileOption().eq(this.actorIntegrationProfileOption);
        List<TestParticipants> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            this.roleInTest = listDistinct.get(0).getRoleInTest();
            entityManager.merge(this);
        } else {
            if (id != null) {
                entityManager.merge(this);
            } else {
                entityManager.persist(this);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }
}