/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model.HL7;

import net.ihe.gazelle.tm.configurations.model.AbstractConfiguration;
import net.ihe.gazelle.tm.configurations.model.Configuration;
import net.ihe.gazelle.tm.configurations.model.OIDSConfigurationForSessionForHL7;
import net.ihe.gazelle.tm.configurations.model.OIDSConfigurationForSessionForHL7.OidsTypeForHL7;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.tm.systems.model.TestingSession;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.List;

@MappedSuperclass
public abstract class AbstractHL7Configuration extends AbstractConfiguration {

    private static final long serialVersionUID = 6132047520509974011L;

    /**
     * This field is entitled "Organization.id" in the GUI for HL7v3
     */
    @Column(name = "sending_receiving_application", nullable = false)
    private String sendingReceivingApplication;

    /**
     * This field is entitled "Device.id" in the GUI for HL7v3
     */
    @Column(name = "sending_receiving_facility", nullable = false)
    private String sendingReceivingFacility;

    /**
     * assigningAuthority attribute
     */
    @Column(name = "assigning_authority")
    private String assigningAuthority;

    /**
     * assigningAuthorityOID attribute
     */
    @Deprecated
    @Column(name = "ass_auth_oid")
    private String assigningAuthorityOID;

    public AbstractHL7Configuration() {
        super(new Configuration());
    }

    public AbstractHL7Configuration(Configuration inConfiguration, String inComments) {
        super(inConfiguration, inComments);
    }

    public AbstractHL7Configuration(Configuration inConfiguration) {
        super(inConfiguration);
    }

    protected AbstractHL7Configuration(AbstractHL7Configuration hh) {
        super(hh);
        if (hh != null) {
            this.assigningAuthority = hh.assigningAuthority;
            this.sendingReceivingApplication = hh.sendingReceivingApplication;
            this.sendingReceivingFacility = hh.sendingReceivingFacility;
        }
    }

    public AbstractHL7Configuration(Configuration inConfiguration, String inSendingReceivingApplication,
                                    String inSendingReceivingFacility, String inAssigningAuthority, String inComments) {
        super(inConfiguration, inComments);

        sendingReceivingApplication = inSendingReceivingApplication;
        sendingReceivingFacility = inSendingReceivingFacility;
        assigningAuthority = inAssigningAuthority;

    }

    public static String getNextAssigningAuth(System inSystem) {

        return "";
    }

    @Deprecated
    public String getAssigningAuthorityOID() {
        return assigningAuthorityOID;
    }

    @Deprecated
    public void setAssigningAuthorityOID(String assigningAuthorityOID) {
        this.assigningAuthorityOID = assigningAuthorityOID;
    }

    public String getSendingReceivingApplication() {
        return sendingReceivingApplication;
    }

    public void setSendingReceivingApplication(String sendingReceivingApplication) {
        this.sendingReceivingApplication = sendingReceivingApplication;
    }

    public String getSendingReceivingFacility() {
        return sendingReceivingFacility;
    }

    public void setSendingReceivingFacility(String sendingReceivingFacility) {
        this.sendingReceivingFacility = sendingReceivingFacility;
    }

    public String getAssigningAuthority() {
        return assigningAuthority;
    }

    public void setAssigningAuthority(String assigningAuthority) {
        this.assigningAuthority = assigningAuthority;
    }

    public String generateAssigningAuthorityOID(OidsTypeForHL7 inOIDSType,
                                                OIDSConfigurationForSessionForHL7 oidsConfiguration, TestingSession selectedTestingSession) {

        TestingSession session = selectedTestingSession;

        if (session == null) {
            return null;
        }

        if (oidsConfiguration == null) {
            return null;
        }

        if (inOIDSType == OidsTypeForHL7.oidHL7V2) {
            assigningAuthorityOID = oidsConfiguration.getOidForSession() + "." + oidsConfiguration.getOidForHL7V2()
                    + "." + oidsConfiguration.getCurrentOIDForHL7V2();
            oidsConfiguration.incrementOID(OidsTypeForHL7.oidHL7V2);
        } else if (inOIDSType == OidsTypeForHL7.oidHL7V3) {
            assigningAuthorityOID = oidsConfiguration.getOidForSession() + "." + oidsConfiguration.getOidForHL7V3()
                    + "." + oidsConfiguration.getCurrentOIDForHL7V3();
            oidsConfiguration.incrementOID(OidsTypeForHL7.oidHL7V3);
        }

        return assigningAuthorityOID;
    }

    @Override
    public List<String> getCSVHeaders() {
        List<String> result = super.getCSVHeaders();
        result.add("assigningAuthority");
        return result;
    }

    @Override
    public List<String> getCSVValues() {
        List<String> values = super.getCSVValues();

        if (assigningAuthority != null) {
            values.add(assigningAuthority);
        } else {
            values.add("");
        }

        return values;

    }

    @Override
    public String toString() {
        return "AbstractHL7Configuration [sendingReceivingApplication=" + sendingReceivingApplication
                + ", sendingReceivingFacility=" + sendingReceivingFacility + ", assigningAuthority="
                + assigningAuthority + ", assigningAuthorityOID=" + assigningAuthorityOID + ", id=" + id
                + ", configuration=" + configuration + ", comments=" + comments + ", lastChanged=" + lastChanged
                + ", lastModifierId=" + lastModifierId + "]";
    }

}
