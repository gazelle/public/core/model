package net.ihe.gazelle.tm.systems.model;

import java.io.Serializable;
import java.util.Objects;

public class TestingSessionAdminKey implements Serializable {
    private static final long serialVersionUID = 959710305090098277L;

    private String userId;
    private Integer testingSessionId;

    public TestingSessionAdminKey() {
    }

    public TestingSessionAdminKey(String userId, Integer testingSessionId) {
        this.userId = userId;
        this.testingSessionId = testingSessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getTestingSessionId() {
        return testingSessionId;
    }

    public void setTestingSessionId(Integer testingSessionId) {
        this.testingSessionId = testingSessionId;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestingSessionAdminKey that = (TestingSessionAdminKey) o;
        return this.getUserId().equals(that.getUserId())
                && this.getTestingSessionId().equals(that.getTestingSessionId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, testingSessionId);
    }
}
