package net.ihe.gazelle.tm.gazelletest.model.reversed;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.objects.model.ObjectCreator;
import net.ihe.gazelle.objects.model.ObjectReader;
import net.ihe.gazelle.tm.configurations.model.ConfigurationTypeMappedWithAIPO;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestParticipants;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceParticipants;
import net.ihe.gazelle.tm.systems.model.SystemActorProfiles;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tf_actor_integration_profile_option", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class AIPO extends AuditedObject implements java.io.Serializable {

    private static final long serialVersionUID = -7682654871498361305L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @OneToMany(mappedBy = "aipo", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<TestParticipants> testParticipants;

    @OneToMany(mappedBy = "aipo", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<SystemActorProfiles> systemActorProfiles;

    @OneToMany(mappedBy = "aipo", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<TestInstanceParticipants> testInstanceParticipants;

    @OneToMany(mappedBy = "actorIntegrationProfileOption", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<ConfigurationTypeMappedWithAIPO> configurationTypeMappedWithAIPO;

    @OneToMany(mappedBy = "AIPO", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<ObjectCreator> objectCreators;

    @OneToMany(mappedBy = "AIPO", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<ObjectReader> objectReaders;

    public AIPO() {
    }

    public Integer getId() {
        return id;
    }

    public List<TestParticipants> getTestParticipants() {
        return HibernateHelper.getLazyValue(this, "testParticipants", this.testParticipants);
    }

    public List<SystemActorProfiles> getSystemActorProfiles() {
        return HibernateHelper.getLazyValue(this, "systemActorProfiles", this.systemActorProfiles);
    }

    public List<TestInstanceParticipants> getTestInstanceParticipants() {
        return HibernateHelper.getLazyValue(this, "testInstanceParticipants", this.testInstanceParticipants);
    }

    public List<ConfigurationTypeMappedWithAIPO> getConfigurationTypeMappedWithAIPO() {
        return HibernateHelper.getLazyValue(this, "configurationTypeMappedWithAIPO",
                this.configurationTypeMappedWithAIPO);
    }

    public List<ObjectCreator> getObjectCreators() {
        return HibernateHelper.getLazyValue(this, "objectCreators", this.objectCreators);
    }

    public List<ObjectReader> getObjectReaders() {
        return HibernateHelper.getLazyValue(this, "objectReaders", this.objectReaders);
    }

}
