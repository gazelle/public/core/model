package net.ihe.gazelle.tm.application.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Class description: intro
 * <p/>
 * This class is used to fill in the intro page. Each field of the intro page is representing as an attribute. An instance of this object need to be created for each language.
 *
 * @author Anne-Gaëlle Bergé /INRIA Rennes - Bretagne Atlantique
 * @version 1.0 - April 27th 2011
 *          <p/>
 *          intro possesses the following attributes:
 *          <ul>
 *          <li><b>id</b> : ID in the database</li>
 *          <li><b>language</b> : defines for which selected language this object has to be used</li>
 *          <li><b>mainPanelHeader</b> : title of the main rich:panel (with id "MainPanel")</li>
 *          <li><b>mainPanelContent</b> : text contained in the panel entitled "MainPanel"</li>
 *          <li><b>secondaryPanelHeader</b> : title of the main rich:panel (with id "SecondaryPanel")</li>
 *          <li><b>secondaryPanelContent</b> : text contained in the panel entitled "SecondaryPanel". This panel is displayed only if this attribute is not empty</li>
 *          <li><b>secondaryPanelPosition</b> : indicates the relative position of the secondary panel from the main panel</li>
 *          </ul>
 * @class intro.java
 * @package net.ihe.gazelle.tm.application.model
 * @see > anne-gaelle.berge@inria.fr
 */

@Entity
@Table(name = "pr_intro", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "language"))
@SequenceGenerator(name = "pr_intro_sequence", sequenceName = "pr_intro_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Intro implements Serializable {

    private static Logger log = LoggerFactory.getLogger(Intro.class);
    @Id
    @GeneratedValue(generator = "pr_intro_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Column(name = "language")
    private String language;
    @Column(name = "main_panel_header")
    @Lob
    @Type(type = "text")
    private String mainPanelHeader;
    @Column(name = "main_panel_content")
    @Lob
    @Type(type = "text")
    private String mainPanelContent;

    public Intro() {
    }

    public Intro(String language) {
        this.language = language;
    }

    /**
     * @param inIntro
     * @return
     */
    public static Intro saveIntro(Intro inIntro) {
        if (inIntro != null) {
            EntityManager em = EntityManagerService.provideEntityManager();
            if ((inIntro.getMainPanelContent() != null) && inIntro.getMainPanelContent().isEmpty()) {
                inIntro.setMainPanelContent(null);
            }
            try {
                inIntro = em.merge(inIntro);
                em.flush();
                return inIntro;
            } catch (Exception e) {
                log.error("Cannot save this Intro", e);
                return null;
            }
        } else {
            return null;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMainPanelContent() {
        return mainPanelContent;
    }

    public void setMainPanelContent(String mainPanelContent) {
        this.mainPanelContent = mainPanelContent;
    }

    public String getMainPanelHeader() {
        return mainPanelHeader;
    }

    public void setMainPanelHeader(String mainPanelHeader) {
        this.mainPanelHeader = mainPanelHeader;
    }


}
