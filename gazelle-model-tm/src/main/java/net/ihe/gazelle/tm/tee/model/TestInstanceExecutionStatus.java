package net.ihe.gazelle.tm.tee.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetGazelle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>Entity class that manages statuses for a TestInstance object in ITB testing sessions.</p>
 *
 * @author rizwan (rizwan.tanoli@aegis.net)
 */
@Entity
@Table(name = "tm_test_instance_exec_status", schema = "public")
@SequenceGenerator(name = "tm_test_instance_exec_status_sequence", sequenceName = "tm_test_instance_exec_status_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class TestInstanceExecutionStatus implements Serializable {


    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_test_instance_exec_status_sequence")
    private Integer id;

    @Column(name = "key", unique = true, nullable = false)
    @Enumerated(EnumType.STRING)
    private TestInstanceExecutionStatusEnum key;

    @Column(name = "description", nullable = false, length = 1024)
    private String description;

    @Column(name = "label_key_for_display", nullable = false, length = 45)
    private String labelKeyForDisplay;

    public TestInstanceExecutionStatus() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TestInstanceExecutionStatusEnum getKey() {
        return key;
    }

    public void setKey(TestInstanceExecutionStatusEnum key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabelKeyForDisplay() {
        return labelKeyForDisplay;
    }

    public void setLabelKeyForDisplay(String labelKeyForDisplay) {
        this.labelKeyForDisplay = labelKeyForDisplay;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

    @Override
    public String toString() {
        return "TestInstanceExecutionStatus [id=" + id + ", key=" + key + ", description=" + description
                + ", labelKeyForDisplay=" + labelKeyForDisplay + "]";
    }

}
