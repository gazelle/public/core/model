package net.ihe.gazelle.tm.tee.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Enums capturing different statuses that an individual message can be in TEE.
 *
 * @author tnabeel
 */
public enum TestStepInstMessageProcessStatusEnum {

    PROCESSING(1, "processing", "Message is being processed.",
            "net.ihe.gazelle.tm.message.processing"),
    PROCESSED(2, "processed", "Message has been processed.",
            "net.ihe.gazelle.tm.message.processed"),
    ERROR(3, "error",
            "Message processing encountered an unexpected error.",
            "net.ihe.gazelle.tm.message.error");

    private Integer id;
    private boolean displayable;
    private String keyword;
    private String description;
    private String displayLabelKey;

    TestStepInstMessageProcessStatusEnum(Integer pId, String pKey, String pDescription, String pDisplayLabelKey) {
        this.id = pId;
        this.keyword = pKey;
        this.description = pDescription;
        this.displayLabelKey = pDisplayLabelKey;
    }

    public static TestStepInstMessageProcessStatusEnum getExecutionStatusByKey(
            String key) {

        for (TestStepInstMessageProcessStatusEnum status : values()) {
            if (status.keyword.equalsIgnoreCase(key)) {
                return status;
            }
        }
        return null;
    }

    public static TestStepInstMessageProcessStatusEnum getExecutionStatusFromLabelKey(
            String pLabelKey) {
        for (TestStepInstMessageProcessStatusEnum status : values()) {
            if (status.displayLabelKey.equalsIgnoreCase(pLabelKey)) {
                return status;
            }
        }
        return null;
    }

    public static List<TestStepInstMessageProcessStatusEnum> getDisplayableStatuses() {
        List<TestStepInstMessageProcessStatusEnum> statuses = new ArrayList<TestStepInstMessageProcessStatusEnum>();

        for (TestStepInstMessageProcessStatusEnum status : values()) {
            if (status.isDisplayable()) {
                statuses.add(status);
            }
        }
        return statuses;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isDisplayable() {
        return displayable;
    }

    public void setDisplayable(boolean visible) {
        this.displayable = visible;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String key) {
        this.keyword = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayLabelKey() {
        return displayLabelKey;
    }

    public void setDisplayLabelKey(String displayLabelKey) {
        this.displayLabelKey = displayLabelKey;
    }

    public boolean isError() {
        switch (this) {
            case ERROR:
                return true;
            default:
                return false;
        }
    }

    public boolean isProcessed() {
        switch (this) {
            case PROCESSED:
                return true;
            default:
                return false;
        }
    }


}
