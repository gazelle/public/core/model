/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author Abderrazek Boufahja
 * @version 1.0
 * @class GenericOIDSInstitutionForSession.java
 * @package net.ihe.gazelle.tm.systems.model
 */

@Entity
@Table(name = "tm_generic_oids_institution_for_session", schema = "public")
@SequenceGenerator(name = "tm_generic_oids_institution_for_session_sequence", sequenceName = "tm_generic_oids_institution_for_session_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class GenericOIDSInstitutionForSession extends AuditedObject implements Serializable {


    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(GenericOIDSInstitutionForSession.class);

    /**
     * Id attribute!
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_generic_oids_institution_for_session_sequence")
    private Integer id;

    /**
     * Name used for this type of configuration
     */
    @Column(name = "oid_for_session", nullable = false)
    private String oidForSession;

    @Column(name = "oid_root_value", nullable = false)
    private String oidRootValue;

    @Column(name = "current_oid_value", nullable = false)
    private Integer currentOIDValue;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "testing_session_id", nullable = false, unique = true)
    @Fetch(value = FetchMode.SELECT)
    private TestingSession testingSession;

    public static GenericOIDSInstitutionForSession getOIDInstitutionParametersForSession(TestingSession ts) {
        if (ts == null) {
            return null;
        }
        GenericOIDSInstitutionForSessionQuery q = new GenericOIDSInstitutionForSessionQuery();
        q.testingSession().eq(ts);
        List<GenericOIDSInstitutionForSession> result = q.getList();
        if (result != null) {
            if (result.size() > 0) {
                return result.get(0);
            }
        }
        return null;
    }

    /**
     * Increment the OID passed in parameters and merge the current object
     *
     * @param inOidType
     */
    public static void incrementOID(GenericOIDSInstitutionForSession inGenericOIDSInstitutionForSession) {
        if (inGenericOIDSInstitutionForSession == null) {
            return;
        }
        inGenericOIDSInstitutionForSession.incrementOID();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOidForSession() {
        return oidForSession;
    }

    public void setOidForSession(String oidForSession) {
        this.oidForSession = oidForSession;
    }

    public String getOidRootValue() {
        return oidRootValue;
    }

    public void setOidRootValue(String oidRootValue) {
        this.oidRootValue = oidRootValue;
    }

    public Integer getCurrentOIDValue() {
        return currentOIDValue;
    }

    public void setCurrentOIDValue(Integer currentOIDValue) {
        this.currentOIDValue = currentOIDValue;
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    /**
     * Increment the OID
     *
     * @param inOidType
     */
    public GenericOIDSInstitutionForSession incrementOID() {
        currentOIDValue++;
        EntityManager em = EntityManagerService.provideEntityManager();
        return em.merge(this);
    }

    @Override
    public String toString() {
        return "GenericOIDSInstitutionForSession [id=" + id + ", oidForSession=" + oidForSession + ", oidRootValue="
                + oidRootValue + ", currentOIDValue=" + currentOIDValue + ", testingSession="
                + testingSession.getDescription() + "]";
    }

    public String generateOIDForInstitution() {
        return oidForSession + "." + oidRootValue + "." + currentOIDValue;
    }

}
