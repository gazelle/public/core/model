/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.csv.CSVExportable;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tm.systems.model.*;
import net.ihe.gazelle.tm.systems.model.System;
import net.ihe.gazelle.users.model.Institution;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, sep
 * @class Configuration.java
 * @package net.ihe.gazelle.tm.systems.model
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "configuration")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "tm_configuration", schema = "public")
@SequenceGenerator(name = "tm_configuration_sequence", sequenceName = "tm_configuration_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Configuration extends AuditedObject implements Serializable, CSVExportable {

    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(Configuration.class);

    /**
     * Id attribute!
     */
    @XmlElement(name = "id")
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_configuration_sequence")
    private Integer id;

    @XmlElement(name = "configurationType")
    @ManyToOne
    @JoinColumn(name = "configurationType_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private ConfigurationType configurationType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "system_in_session_id")
    @Fetch(value = FetchMode.JOIN)
    private SystemInSession systemInSession;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "host_id")
    @Fetch(value = FetchMode.SELECT)
    private Host host;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actor_id")
    @Fetch(value = FetchMode.SELECT)
    private Actor actor;

    @XmlElement(name = "comment")
    @Column(name = "comment")
    private String comment;

    @XmlElement(name = "isSecured")
    @Column(name = "is_secured")
    private Boolean isSecured;

    /**
     * If the configuration has been approved
     */
    @XmlElement(name = "isApproved")
    @Column(name = "approved", nullable = false)
    private Boolean isApproved = false;

    public Configuration() {

    }

    public Configuration(Host inHost, Actor inActor, SystemInSession inSystemInSession, String inComment,
            Boolean isSecured) {

        host = inHost;
        actor = inActor;
        systemInSession = inSystemInSession;
        comment = inComment;
        this.isSecured = isSecured;

    }

    public Configuration(Actor inActor, String inComment, Boolean isSecured) {
        actor = inActor;
        comment = inComment;
        this.isSecured = isSecured;

    }

    public Configuration(String inComment, Boolean isSecured) {

        this.isSecured = isSecured;
        comment = inComment;
    }

    public Configuration(Configuration inConfiguration) {
        if (inConfiguration != null) {
            this.actor = inConfiguration.actor;
            this.comment = inConfiguration.comment;
            this.configurationType = inConfiguration.configurationType;
            this.host = inConfiguration.host;
            this.isApproved = inConfiguration.isApproved;
            this.isSecured = inConfiguration.isSecured;
            this.systemInSession = inConfiguration.systemInSession;
        }
    }

    /**
     * Allow to retrieve all configurations for a System
     *
     * @param inSystem
     * @param em
     * @return
     */
    @Deprecated
    public static List<Configuration> getListOfConfigurationForASystem(System inSystem, EntityManager em) {
        return getListOfConfigurationForASystem(inSystem);
    }

    public static List<Configuration> getListOfConfigurationForASystem(System inSystem) {

        ConfigurationQuery q = new ConfigurationQuery();
        q.systemInSession().system().id().eq(inSystem.getId());
        return q.getList();
    }

    @Deprecated
    public static List<Configuration> getListOfConfigurationForACompany(Institution inCompany, EntityManager em) {
        return getListOfConfigurationForACompany(inCompany);
    }

    public static List<Configuration> getListOfConfigurationForACompany(Institution inCompany) {
        ConfigurationQuery q = new ConfigurationQuery();
        InstitutionSystemQuery q1 = new InstitutionSystemQuery();
        q1.institution().id().eq(inCompany.getId());
        q.systemInSession().system().id().in(q1.system().id().getListDistinct());
        return q.getListDistinct();
    }

    @Deprecated
    public static List<Configuration> getListOfConfigurationForCompanyForSession(Institution inCompany,
            TestingSession testingSession, EntityManager em) {
        return getListOfConfigurationForCompanyForSession(inCompany, testingSession);
    }

    public static List<Configuration> getListOfConfigurationForCompanyForSession(Institution inCompany,
            TestingSession testingSession) {
        InstitutionSystemQuery q1 = new InstitutionSystemQuery();
        ConfigurationQuery q = new ConfigurationQuery();
        q1.institution().id().eq(inCompany.getId());
        q.systemInSession().testingSession().id().eq(testingSession.getId());
        q.systemInSession().system().id().in(q1.system().id().getListDistinct());

        return q.getListDistinct();
    }

    public static List<Configuration> getListOfConfigurationForASystemInSession(SystemInSession inSystemInSession) {
        ConfigurationQuery q = new ConfigurationQuery();
        q.systemInSession().id().eq(inSystemInSession.getId());
        return q.getList();

    }

    public static List<Configuration> getListOfConfigurationForATypeAndASystemInSession(
            SystemInSession inSystemInSession, String typeName) {
        ConfigurationQuery q = new ConfigurationQuery();
        q.systemInSession().id().eq(inSystemInSession.getId());
        q.configurationType().typeName().eq(typeName);
        return q.getList();
    }

    public static List<Configuration> getListOfConfigurationForACategoryAndASystemInSession(
            SystemInSession inSystemInSession, String categoryName) {
        ConfigurationQuery q = new ConfigurationQuery();
        q.systemInSession().id().eq(inSystemInSession.getId());
        q.configurationType().category().eq(categoryName);
        return q.getList();
    }

    public static ConfigurationType getConfigurationType(String clazz) throws Exception {
        ConfigurationTypeQuery q = new ConfigurationTypeQuery();
        q.className().eq(clazz);
        ConfigurationType confType = q.getUniqueResult();

        if (confType == null) {
            throw new Exception("The type " + clazz + " is unknown!");
        }
        return confType;
    }

    public static void deleteConfigurationsForSystem(System inSystem) {

        ConfigurationQuery q = new ConfigurationQuery();
        q.systemInSession().system().eq(inSystem);
        List<Configuration> listConf = q.getList();

        deleteAbstractsConfig(listConf);
    }

    public static void deleteConfigurationsForSystemInSession(SystemInSession inSystemInSession) {

        ConfigurationQuery q = new ConfigurationQuery();
        q.systemInSession().eq(inSystemInSession);
        List<Configuration> listConf = q.getList();

        deleteAbstractsConfig(listConf);
    }

    private static void deleteAbstractsConfig(List<Configuration> inList) {
        if (inList == null) {
            return;
        }
        EntityManager em = EntityManagerService.provideEntityManager();
        for (Configuration c : inList) {
            AbstractConfiguration ac;
            try {
                ac = (AbstractConfiguration) em.find(Class.forName(c.getConfigurationType().getClassName()), c.id);
                if (ac != null) {
                    em.remove(ac);
                }

                c = em.find(Configuration.class, c.getId());
                if (c != null) {
                    em.remove(c);
                }

                em.flush();

            } catch (ClassNotFoundException e) {
                log.error("Cannot delete configuration : " + e.getMessage());
            }

        }

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SystemInSession getSystemInSession() {
        return systemInSession;
    }

    public void setSystemInSession(SystemInSession systemInSession) {
        this.systemInSession = systemInSession;
    }

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    public Boolean getIsSecured() {
        return isSecured;
    }

    public void setIsSecured(Boolean isSecured) {
        this.isSecured = isSecured;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public ConfigurationType getConfigurationType() {
        return configurationType;
    }

    public void setConfigurationType(ConfigurationType configurationType) {
        this.configurationType = configurationType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(boolean isApproved) {
        this.isApproved = isApproved;
    }

    @Override
    public List<String> getCSVHeaders() {
        List<String> result = new ArrayList<String>();
        result.add("Configuration Type");
        result.add("Company");
        result.add("System");
        result.add("Host");
        result.add("Actor");
        result.add("is secured");
        result.add("is approved");
        result.add("comment");
        return result;
    }

    @Override
    public List<String> getCSVValues() {
        List<String> result = new ArrayList<String>();
        if (configurationType != null) {
            result.add(configurationType.getTypeName());
        } else {
            result.add("");
        }

        if (systemInSession != null) {
            Set<InstitutionSystem> institutionSystems = systemInSession.getSystem().getInstitutionSystems();

            String company = "";
            String esp = "";
            for (InstitutionSystem i : institutionSystems) {
                company = company + esp + i.getInstitution().getKeyword();
                if (esp == "") {
                    esp = " ";
                }
            }
            result.add(company);
        } else {
            result.add("");
        }

        if (systemInSession != null) {
            result.add(systemInSession.getSystem().getKeyword());
        } else {
            result.add("");
        }

        if (host != null) {
            result.add(host.getHostname());
        } else {
            result.add("");
        }

        if (actor != null) {
            result.add(actor.getKeyword());
        } else {
            result.add("");
        }

        if (isSecured != null) {
            result.add(Boolean.toString(isSecured));
        } else {
            result.add("");
        }

        if (isApproved != null) {
            result.add(Boolean.toString(isApproved));
        } else {
            result.add("");
        }

        if (comment != null) {
            result.add(comment);
        } else {
            result.add("");
        }

        return result;

    }

}
