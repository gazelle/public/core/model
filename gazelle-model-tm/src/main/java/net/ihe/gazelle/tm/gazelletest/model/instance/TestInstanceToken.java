package net.ihe.gazelle.tm.gazelletest.model.instance;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Entity that store info on ExternalToolToken generated by the application.
 */
@Entity
@Table(name = "tm_test_instance_token", schema = "public",
        uniqueConstraints = @UniqueConstraint(columnNames = {"value", "test_step_instance_id"}))
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class TestInstanceToken {

    @Id
    @Column(name = "value", unique = true, nullable = false)
    private String value;

    @Column(name = "userId", nullable = false)
    private String userId;

    @Column(name = "test_step_instance_id", unique = true, nullable = false)
    private Integer testStepsInstanceId;

    @Column(name = "expiration_date_time", nullable = false)
    private Timestamp expirationDateTime;

    public TestInstanceToken() {
        // Empty for serialization
    }

    /**
     * Create an ExternalToolToken object to persist.
     *
     * @param value                   literal value of the token
     * @param userId                  username linked to the token
     * @param expirationDateTime      expiration date
     * @param testStepsInstanceId     test step instance id
     */
    public TestInstanceToken(String value, String userId, Integer testStepsInstanceId, Timestamp expirationDateTime) {
        setValue(value);
        setUserId(userId);
        setTestStepsInstanceId(testStepsInstanceId);
        setExpirationDateTime(expirationDateTime);
    }

    /**
     * Getter for the value property.
     *
     * @return the value of the property.
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter for the value property.
     *
     * @param value value to set to the property.
     */
    public void setValue(String value) {
        if (StringUtils.isBlank(value)) {
            throw new IllegalArgumentException("TokenValue can not be null");
        }
        this.value = value;
    }

    /**
     * Getter for the username property.
     *
     * @return the value of the property.
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Setter for the username property.
     *
     * @param userId value to set to the property.
     */
    public void setUserId(String userId) {
        if (StringUtils.isBlank(userId)) {
            throw new IllegalArgumentException("username can not be null");
        }
        this.userId = userId;
    }

    public Integer getTestStepsInstanceId() {
        return testStepsInstanceId;
    }

    public void setTestStepsInstanceId(Integer testStepsInstanceId) {
        this.testStepsInstanceId = testStepsInstanceId;
    }

    /**
     * Getter for the creationDateTime property.
     *
     * @return the value of the property.
     */
    public Timestamp getExpirationDateTime() {
        return expirationDateTime;
    }

    /**
     * Setter for the creationDateTime property.
     *
     * @param expirationDateTime value to set to the property.
     */

    public void setExpirationDateTime(Timestamp expirationDateTime) {
        if (expirationDateTime == null) {
            throw new IllegalArgumentException("creationDateTime can not be null");
        }
        if (expirationDateTime.before(new Timestamp(System.currentTimeMillis()))) {
            throw new IllegalArgumentException("expirationDateTime is before current date");
        }
        this.expirationDateTime = expirationDateTime;
    }
}
