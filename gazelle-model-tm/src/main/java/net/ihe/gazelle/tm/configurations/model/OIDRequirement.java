/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetGazelle;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * @author abderrazek boufahja / INRIA Rennes IHE development Project
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "oidRequirement")
@Entity
@Table(name = "tm_oid_requirement")
@SequenceGenerator(name = "tm_oid_requirement_sequence", sequenceName = "tm_oid_requirement_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class OIDRequirement extends AuditedObject implements Serializable {

    // ~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    private static final long serialVersionUID = 1881422545451441951L;

    // ~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ObjectInstance id
     */
    @XmlTransient
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tm_oid_requirement_sequence")
    private Integer id;

    @Column(name = "label", unique = false, nullable = true)
    private String label;

    @OneToMany
    @JoinTable(name = "tm_oid_requirement_aipo", joinColumns = @JoinColumn(name = "oid_requirement_id"), inverseJoinColumns = @JoinColumn(name = "aipo_id"))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<ActorIntegrationProfileOption> actorIntegrationProfileOptionList;

    @XmlTransient
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "oid_root_definition_id")
    @Fetch(value = FetchMode.SELECT)
    private OIDRootDefinition oidRootDefinition;

    @Column(name = "prefix")
    private String prefix;

    // constructors ///////////////////////////////////////////////////////////////////////////////////////////////////

    public OIDRequirement() {
    }

    public OIDRequirement(String label, List<ActorIntegrationProfileOption> actorIntegrationProfileOptionList,
                          OIDRootDefinition oidRootDefinition, String prefix) {
        super();
        this.label = label;
        this.actorIntegrationProfileOptionList = actorIntegrationProfileOptionList;
        this.oidRootDefinition = oidRootDefinition;
        this.prefix = prefix;
    }

    // ~ Getters and Setters ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static List<OIDRequirement> getAllOIDRequirement() {
        return new OIDRequirementQuery().getList();
    }

    public static List<OIDRequirement> getOIDRequirementFiltered(OIDRootDefinition inOIDRootDefinition, String label) {
        OIDRequirementQuery query = new OIDRequirementQuery();
        query.oidRootDefinition().eqIfValueNotNull(inOIDRootDefinition);
        query.label().eqIfValueNotNull(label);
        return query.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public OIDRootDefinition getOidRootDefinition() {
        return oidRootDefinition;
    }

    public void setOidRootDefinition(OIDRootDefinition oidRootDefinition) {
        this.oidRootDefinition = oidRootDefinition;
    }

    public List<ActorIntegrationProfileOption> getActorIntegrationProfileOptionList() {
        return actorIntegrationProfileOptionList;
    }

    public void setActorIntegrationProfileOptionList(
            List<ActorIntegrationProfileOption> actorIntegrationProfileOptionList) {
        this.actorIntegrationProfileOptionList = actorIntegrationProfileOptionList;
    }

    public String getPrefix() {
        return prefix;
    }

    // methods ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public String toString() {
        return "OIDRequirement [id=" + id + ", label=" + label + ", oidRootDefinition=" + oidRootDefinition
                + ", prefix=" + prefix + "]";
    }

}
