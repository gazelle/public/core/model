package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.tm.tee.model.TmTestStepMessageProfile;

import java.io.Serializable;
import java.nio.charset.Charset;

/**
 * <b>Test Step Message Profile Configuration File</b>, TSMPConfigFile, is
 * used to supply additional contextual information needed to successfully
 * define the test step.<br/>
 * The TestStepConfigurationFileType object is utilized to specify the
 * type of the file
 *
 * @author rizwan.tanoli
 * @see TestStepConfigurationFileType
 * @see TmTestStepMessageProfile
 */
public class TSMPConfigFile implements Serializable {


    private static final long serialVersionUID = -5044241152479749335L;

    private String name;

    private byte[] data;

    public TSMPConfigFile() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data.clone();
    }

    public String getDataAsString() {
        return new String(data, Charset.forName("UTF-8"));
    }

    public int getSize() {
        return data.length;
    }

    public String getMimeType() {
        String mimeType = "";

        int locationOfDot = name.indexOf('.');
        if (locationOfDot > 0) {

            mimeType = name.substring(locationOfDot + 1);

        }

        return mimeType;
    }


}
