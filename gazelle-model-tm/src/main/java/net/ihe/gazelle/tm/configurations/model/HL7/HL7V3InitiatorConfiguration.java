/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.tm.configurations.model.HL7;

/**
 * @class HL7V3InitiatorConfiguration.java
 * @package net.ihe.gazelle.tm.systems.model
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, sep
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */

import net.ihe.gazelle.tm.configurations.model.Configuration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tm_hl7_v3_initiator_configuration", schema = "public")
@SequenceGenerator(name = "tm_hl7_v3_initiator_configuration_sequence", sequenceName = "tm_hl7_v3_initiator_configuration_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class HL7V3InitiatorConfiguration extends AbstractHL7Configuration implements Serializable {

    private static final long serialVersionUID = -2795220587002262409L;

    public HL7V3InitiatorConfiguration() {
        super();
    }

    public HL7V3InitiatorConfiguration(Configuration inConfiguration, String inSendingReceivingApplication,
                                       String inSendingReceivingFacility, String inAssigningAuthority, String inComments) {
        super(inConfiguration, inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority,
                inComments);
    }

    public HL7V3InitiatorConfiguration(Configuration inConfiguration, String inComments) {
        super(inConfiguration, inComments);
    }

    public HL7V3InitiatorConfiguration(Configuration inConfiguration) {
        super(inConfiguration);
    }

    public HL7V3InitiatorConfiguration(HL7V3InitiatorConfiguration hl7v3InitiatorConfiguration) {
        super(hl7v3InitiatorConfiguration);
    }

    public HL7V3InitiatorConfiguration(Configuration inConfiguration, String inSendingReceivingApplication,
                                       String inSendingReceivingFacility, String inAssigningAuthority, String inAssigningAuthorityOID,
                                       String inComments) {
        super(inConfiguration, inSendingReceivingApplication, inSendingReceivingFacility, inAssigningAuthority,
                inComments);
    }

    @Override
    @PrePersist
    @PreUpdate
    public void init() {
        this.setSendingReceivingApplication("-");
        this.setSendingReceivingFacility("-");
    }

}