package net.ihe.gazelle.tm.tee.model;

/**
 * Represents the possible enum values for message direction.
 *
 * @author tnabeel
 */
public enum MessageDirection {
    REQUEST, RESPONSE;
}
