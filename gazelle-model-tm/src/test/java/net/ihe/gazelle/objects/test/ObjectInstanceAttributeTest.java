/**
 *
 */
package net.ihe.gazelle.objects.test;

import junit.framework.TestCase;
import net.ihe.gazelle.objects.model.ObjectAttribute;
import net.ihe.gazelle.objects.model.ObjectInstance;
import net.ihe.gazelle.objects.model.ObjectInstanceAttribute;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author abderrazek boufahja
 */
public class ObjectInstanceAttributeTest extends TestCase {

    ObjectInstanceAttribute oia;

    /**
     * @param name
     */
    public ObjectInstanceAttributeTest(String name) {
        super(name);
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeMethod
    protected void setUp() throws Exception {
        super.setUp();
        oia = new ObjectInstanceAttribute();
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAttribute#ObjectInstanceAttribute(net.ihe.gazelle.objects.model.ObjectInstance, net.ihe.gazelle.objects.model.ObjectAttribute, java.lang.String)}.
     */
    @Test()
    public void testObjectInstanceAttributeObjectInstanceObjectAttributeString() {
        ObjectInstance oi = new ObjectInstance(null, null, "desc", "name", true, null);
        ObjectAttribute oa = new ObjectAttribute(null, "key", "desc");
        String description = new String("descr");
        oia = new ObjectInstanceAttribute(oi, oa, description);
        assertEquals(oia.getDescription(), description);
        assertEquals(oia.getAttribute(), oa);
        assertEquals(oia.getInstance(), oi);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAttribute#ObjectInstanceAttribute(net.ihe.gazelle.objects.model.ObjectInstanceAttribute)}.
     */
    @Test()
    public void testObjectInstanceAttributeObjectInstanceAttribute() {
        ObjectInstance oi = new ObjectInstance(null, null, "desc", "name", true, null);
        ObjectAttribute oa = new ObjectAttribute(null, "key", "desc");
        String description = new String("descr");
        oia = new ObjectInstanceAttribute(oi, oa, description);
        ObjectInstanceAttribute oai2 = new ObjectInstanceAttribute(oia);
        assertEquals(oia, oai2);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAttribute#equals(java.lang.Object)}.
     */
    @Test()
    public void testEqualsObject() {
        ObjectInstance oi = new ObjectInstance(null, null, "desc", "name", true, null);
        ObjectAttribute oa = new ObjectAttribute(null, "key", "desc");
        String description = new String("descr");
        oia = new ObjectInstanceAttribute(oi, oa, description);
        ObjectInstanceAttribute oai2 = new ObjectInstanceAttribute(oi, oa, description);
        assertTrue(oia.equals(oai2));
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAttribute#setId(java.lang.Integer)}.
     */
    @Test()
    public void testSetId() {
        Integer id = 123;
        oia.setId(id);
        assertEquals(oia.getId(), id);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAttribute#setInstance(net.ihe.gazelle.objects.model.ObjectInstance)}.
     */
    @Test()
    public void testSetInstance() {
        ObjectInstance oi = new ObjectInstance(null, null, "desc", "name", true, null);
        oia.setInstance(oi);
        assertEquals(oia.getInstance(), oi);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAttribute#setAttribute(net.ihe.gazelle.objects.model.ObjectAttribute)}.
     */
    @Test()
    public void testSetAttribute() {
        ObjectAttribute oa = new ObjectAttribute(null, "key", "desc");
        oia.setAttribute(oa);
        assertEquals(oia.getAttribute(), oa);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAttribute#setDescription(java.lang.String)}.
     */
    @Test()
    public void testSetDescription() {
        String desc = new String("desc");
        oia.setDescription(desc);
        assertEquals(oia.getDescription(), desc);
    }

}
