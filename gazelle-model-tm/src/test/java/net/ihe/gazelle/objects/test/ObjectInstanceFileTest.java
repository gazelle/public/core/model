/**
 *
 */
package net.ihe.gazelle.objects.test;

import junit.framework.TestCase;
import net.ihe.gazelle.objects.model.ObjectFile;
import net.ihe.gazelle.objects.model.ObjectInstance;
import net.ihe.gazelle.objects.model.ObjectInstanceFile;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author abderrazek boufahja
 */
public class ObjectInstanceFileTest extends TestCase {

    ObjectInstanceFile oif;

    /**
     * @param name
     */
    public ObjectInstanceFileTest(String name) {
        super(name);
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeMethod
    protected void setUp() throws Exception {
        super.setUp();
        oif = new ObjectInstanceFile();
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceFile#ObjectInstanceFile(net.ihe.gazelle.objects.model.ObjectInstance, net.ihe.gazelle.objects.model.ObjectFile, net.ihe.gazelle.tm.systems.model.SystemInSession, java.lang.String)}.
     */
    @Test()
    public void testObjectInstanceFileObjectInstanceObjectFileSystemInSessionString() {
        ObjectInstance oi = new ObjectInstance(null, null, "desc", "name", true, null);
        SystemInSession sis = new SystemInSession();
        String str = new String("str");
        ObjectFile file = new ObjectFile(null, "desc", null, 1, 2, "upl");
        oif = new ObjectInstanceFile(oi, file, sis, str);
        assertEquals(oif.getUrl(), str);
        assertEquals(oif.getFile(), file);
        assertEquals(oif.getInstance(), oi);
        assertEquals(oif.getSystem(), sis);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceFile#ObjectInstanceFile(net.ihe.gazelle.objects.model.ObjectInstanceFile)}.
     */
    @Test()
    public void testObjectInstanceFileObjectInstanceFile() {
        ObjectInstance oi = new ObjectInstance(null, null, "desc", "name", true, null);
        SystemInSession sis = new SystemInSession();
        String str = new String("str");
        ObjectFile file = new ObjectFile(null, "desc", null, 1, 2, "upl");
        oif = new ObjectInstanceFile(oi, file, sis, str);
        ObjectInstanceFile oif2 = new ObjectInstanceFile(oif);
        assertEquals(oif, oif2);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceFile#equals(java.lang.Object)}.
     */
    @Test()
    public void testEqualsObject() {
        ObjectInstance oi = new ObjectInstance(null, null, "desc", "name", true, null);
        SystemInSession sis = new SystemInSession();
        String str = new String("str");
        ObjectFile file = new ObjectFile(null, "desc", null, 1, 2, "upl");
        oif = new ObjectInstanceFile(oi, file, sis, str);
        ObjectInstanceFile oif2 = new ObjectInstanceFile(oi, file, sis, str);
        assertTrue(oif.equals(oif2));
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceFile#setId(java.lang.Integer)}.
     */
    @Test()
    public void testSetId() {
        Integer id = 34;
        oif.setId(id);
        assertEquals(oif.getId(), id);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceFile#setInstance(net.ihe.gazelle.objects.model.ObjectInstance)}.
     */
    @Test()
    public void testSetInstance() {
        ObjectInstance oi = new ObjectInstance(null, null, "desc", "name", true, null);
        oif.setInstance(oi);
        assertEquals(oif.getInstance(), oi);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceFile#setFile(net.ihe.gazelle.objects.model.ObjectFile)}.
     */
    @Test()
    public void testSetFile() {
        ObjectFile file = new ObjectFile(null, "desc", null, 1, 2, "upl");
        oif.setFile(file);
        assertEquals(oif.getFile(), file);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceFile#setSystem(net.ihe.gazelle.tm.systems.model.SystemInSession)}.
     */
    @Test()
    public void testSetSystem() {
        SystemInSession sis = new SystemInSession();
        oif.setSystem(sis);
        assertEquals(oif.getSystem(), sis);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceFile#setUrl(java.lang.String)}.
     */
    @Test()
    public void testSetUrl() {
        String url = new String("url");
        oif.setUrl(url);
        assertEquals(oif.getUrl(), url);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceFile#compareTo(net.ihe.gazelle.objects.model.ObjectInstanceFile)}.
     */
    @Test()
    public void testCompareTo() {
        oif.setId(2);
        ObjectInstanceFile oif2 = new ObjectInstanceFile();
        oif2.setId(3);
        assertTrue(oif.compareTo(oif2) < 0);
    }

}
