/**
 *
 */
package net.ihe.gazelle.objects.test;

import junit.framework.TestCase;
import net.ihe.gazelle.objects.model.ObjectAttribute;
import net.ihe.gazelle.objects.model.ObjectType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author abderrazek boufahja
 */
public class ObjectAttributeTest extends TestCase {

    ObjectAttribute objAttr;
    ObjectType ot;

    /**
     * @param name
     */
    public ObjectAttributeTest(String name) {
        super(name);
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeMethod
    protected void setUp() throws Exception {
        ot = new ObjectType();
        objAttr = new ObjectAttribute();
        super.setUp();
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectAttribute#ObjectAttribute(net.ihe.gazelle.objects.model.ObjectType, java.lang.String, java.lang.String)}.
     */
    @Test()
    public void testObjectAttributeObjectTypeStringString() {
        ot = new ObjectType("keyword", "desc", "def", "ins", null);
        String val = new String("str");
        String key = new String("key");
        ObjectAttribute oat = new ObjectAttribute(ot, key, val);
        assertNotNull(oat);
        assertEquals(oat.getDescription(), val);
        assertEquals(oat.getKeyword(), key);
        assertEquals(oat.getObject(), ot);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectAttribute#ObjectAttribute(net.ihe.gazelle.objects.model.ObjectAttribute)}.
     */
    @Test()
    public void testObjectAttributeObjectAttribute() {
        ot = new ObjectType("keyword", "desc", "def", "ins", null);
        String val = new String("str");
        String key = new String("key");
        objAttr.setDescription(val);
        objAttr.setKeyword(key);
        ObjectAttribute oatt = new ObjectAttribute(objAttr);
        assertNotNull(oatt);
        assertEquals(oatt, objAttr);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectAttribute#setId(java.lang.Integer)}.
     */
    @Test()
    public void testSetId() {
        Integer id = 22;
        objAttr.setId(id);
        assertNotNull(objAttr.getId());
        assertEquals(objAttr.getId(), id);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectAttribute#setObject(net.ihe.gazelle.objects.model.ObjectType)}.
     */
    @Test()
    public void testSetObject() {
        ot = new ObjectType("keyword", "desc", "def", "ins", null);
        objAttr.setObject(ot);
        assertEquals(objAttr.getObject(), ot);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectAttribute#setKeyword(java.lang.String)}.
     */
    @Test()
    public void testSetKeyword() {
        String key = new String("key");
        objAttr.setKeyword(key);
        assertEquals(objAttr.getKeyword(), key);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectAttribute#setDescription(java.lang.String)}.
     */
    @Test()
    public void testSetDescription() {
        String desc = new String("desc");
        objAttr.setDescription(desc);
        assertEquals(objAttr.getDescription(), desc);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectAttribute#equals(java.lang.Object)}.
     */
    @Test()
    public void testEqualsObject() {
        ot = new ObjectType("keyword", "desc", "def", "ins", null);
        ObjectAttribute oat1 = new ObjectAttribute(ot, "key", "val");
        ObjectAttribute oat2 = new ObjectAttribute(ot, "key", "val");
        assertEquals(oat1, oat2);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectAttribute#getObjectAttributeFiltered(net.ihe.gazelle.objects.model.ObjectType)}.
     */
    public void testGetObjectAttributeFiltered() {
        //fail("Not yet implemented");
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectAttribute#deleteObjectAttribute(net.ihe.gazelle.objects.model.ObjectType)}.
     */
    public void testDeleteObjectAttributeObjectType() {
        //fail("Not yet implemented");
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectAttribute#deleteObjectAttribute(net.ihe.gazelle.objects.model.ObjectAttribute)}.
     */
    public void testDeleteObjectAttributeObjectAttribute() {
        //fail("Not yet implemented");
    }

}
