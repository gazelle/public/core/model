/**
 *
 */
package net.ihe.gazelle.objects.test;

import junit.framework.TestCase;
import net.ihe.gazelle.objects.model.ObjectInstance;
import net.ihe.gazelle.objects.model.ObjectInstanceValidation;
import net.ihe.gazelle.objects.model.ObjectType;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author abderrazek boufahja
 */
public class ObjectInstanceTest extends TestCase {

    ObjectInstance oi;

    /**
     * @param name
     */
    public ObjectInstanceTest(String name) {
        super(name);
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeMethod
    protected void setUp() throws Exception {
        super.setUp();
        oi = new ObjectInstance();
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstance#ObjectInstance(net.ihe.gazelle.objects.model.ObjectType, net.ihe.gazelle.tm.systems.model.SystemInSession, java.lang.String, java.lang.String, java.lang.String, boolean)}.
     */
    @Test()
    public void testObjectInstanceObjectTypeSystemInSessionStringStringStringBoolean() {
        ObjectType ot = new ObjectType("key", "desc", "default_desc", "instructions", null);
        SystemInSession sis = new SystemInSession();
        String desc = new String("desc");
        String name = new String("name");
        boolean completed = true;
        ObjectInstanceValidation oiv = new ObjectInstanceValidation("val", "desc");
        oi = new ObjectInstance(ot, sis, desc, name, completed, oiv);
        assertEquals(oi.getDescription(), desc);
        assertEquals(oi.getName(), name);
        assertEquals(oi.getObject(), ot);
        assertEquals(oi.getSystem(), sis);
        assertEquals(oi.isCompleted(), completed);
        assertEquals(oi.getValidation(), oiv);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstance#ObjectInstance(net.ihe.gazelle.objects.model.ObjectInstance)}.
     */
    @Test()
    public void testObjectInstanceObjectInstance() {
        ObjectType ot = new ObjectType("key", "desc", "default_desc", "instructions", null);
        SystemInSession sis = new SystemInSession();
        String desc = new String("desc");
        String name = new String("name");
        boolean completed = true;
        ObjectInstanceValidation oiv = new ObjectInstanceValidation("val", "desc");
        oi = new ObjectInstance(ot, sis, desc, name, completed, oiv);
        ObjectInstance oi2 = new ObjectInstance(oi);
        assertEquals(oi, oi2);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstance#setId(java.lang.Integer)}.
     */
    @Test()
    public void testSetId() {
        Integer id = 124;
        oi.setId(id);
        assertEquals(oi.getId(), id);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstance#setObject(net.ihe.gazelle.objects.model.ObjectType)}.
     */
    @Test()
    public void testSetObject() {
        ObjectType ot = new ObjectType("key", "desc", "default_desc", "instructions", null);
        oi.setObject(ot);
        assertEquals(oi.getObject(), ot);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstance#setSystem(net.ihe.gazelle.tm.systems.model.SystemInSession)}.
     */
    @Test()
    public void testSetSystem() {
        SystemInSession sis = new SystemInSession();
        oi.setSystem(sis);
        assertEquals(sis, oi.getSystem());
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstance#setDescription(java.lang.String)}.
     */
    @Test()
    public void testSetDescription() {
        String desc = new String("desc");
        oi.setDescription(desc);
        assertEquals(oi.getDescription(), desc);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstance#setName(java.lang.String)}.
     */
    @Test()
    public void testSetName() {
        String name = new String("b");
        oi.setName(name);
        assertEquals(oi.getName(), name);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstance#setCompleted(boolean)}.
     */
    @Test()
    public void testSetCompleted() {
        boolean cc = true;
        oi.setCompleted(cc);
        assertEquals(oi.isCompleted(), cc);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstance#setValidation(net.ihe.gazelle.objects.model.ObjectInstanceValidation)}.
     */
    @Test()
    public void testSetValidation() {
        ObjectInstanceValidation oiv = new ObjectInstanceValidation("cc", "des");
        oi.setValidation(oiv);
        assertEquals(oi.getValidation(), oiv);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstance#equals(java.lang.Object)}.
     */
    @Test()
    public void testEqualsObject() {
        ObjectType ot = new ObjectType("key", "desc", "default_desc", "instructions", null);
        SystemInSession sis = new SystemInSession();
        String desc = new String("desc");
        String name = new String("name");
        boolean completed = true;
        ObjectInstanceValidation oiv = new ObjectInstanceValidation("val", "desc");
        oi = new ObjectInstance(ot, sis, desc, name, completed, oiv);
        ObjectInstance oi2 = new ObjectInstance(ot, sis, desc, name, completed, oiv);
        assertEquals(oi, oi2);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstance#compareTo(net.ihe.gazelle.objects.model.ObjectInstance)}.
     */
    @Test()
    public void testCompareTo() {
        ObjectType ot = new ObjectType("key", "desc", "default_desc", "instructions", null);
        SystemInSession sis = new SystemInSession();
        String desc = new String("desc");
        String name = new String("name");
        String name2 = new String("mame");
        boolean completed = true;
        ObjectInstanceValidation oiv = new ObjectInstanceValidation("val", "desc");
        oi = new ObjectInstance(ot, sis, desc, name, completed, oiv);
        ObjectInstance oi2 = new ObjectInstance(ot, sis, desc, name2, completed, oiv);
        int v = oi.compareTo(oi2);
        assertTrue(v > 0);
    }

}
