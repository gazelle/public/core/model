/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.objects.test;

import junit.framework.TestCase;
import net.ihe.gazelle.objects.model.Annotation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author aderrazek boufahja
 */
public class AnnotationTest extends TestCase {

    Annotation annotation;

    /**
     * @param name
     */
    public AnnotationTest(String name) {
        super(name);
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeMethod
    protected void setUp() throws Exception {
        annotation = new Annotation();
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.Annotation#Annotation(java.lang.String)}.
     */
    @Test()
    public void testAnnotationString() {
        String value = "new annotation";
        Annotation ann = new Annotation(value);
        assertNotNull(ann.getValue(), "value should not be null");
        assertEquals(ann.getValue(), value);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.Annotation#Annotation(net.ihe.gazelle.objects.model.Annotation)}.
     */
    @Test()
    public void testAnnotationAnnotation() {
        Annotation ann = new Annotation(annotation);
        assertNotNull(ann);
        assertEquals(ann, annotation);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.Annotation#setId(java.lang.Integer)}.
     */
    @Test()
    public void testSetId() {
        Integer id = 125;
        annotation.setId(id);
        assertNotNull(annotation.getId());
        assertEquals(annotation.getId(), id);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.Annotation#setValue(java.lang.String)}.
     */
    @Test()
    public void testSetValue() {
        String value = "a some value";
        annotation.setValue(value);
        assertNotNull(annotation.getValue());
        assertEquals(annotation.getValue(), value);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.Annotation#equals(java.lang.Object)}.
     */
    @Test()
    public void testEqualsObject() {
        Annotation ann1 = new Annotation("essai");
        Annotation ann2 = new Annotation("essai");
        assertEquals(ann1, ann2);
    }

}
