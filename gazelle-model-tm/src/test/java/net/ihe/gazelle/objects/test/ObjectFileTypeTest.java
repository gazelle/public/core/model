/**
 *
 */
package net.ihe.gazelle.objects.test;

import junit.framework.TestCase;
import net.ihe.gazelle.objects.model.ObjectFileType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author abderrazek boufahja
 */
public class ObjectFileTypeTest extends TestCase {

    ObjectFileType oft;

    /**
     * @param name
     */
    public ObjectFileTypeTest(String name) {
        super(name);
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeMethod
    protected void setUp() throws Exception {
        super.setUp();
        oft = new ObjectFileType();
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFileType#ObjectFileType(java.lang.String, java.lang.String, java.lang.String)}.
     */
    @Test()
    public void testObjectFileTypeStringStringString() {
        String s1 = new String("s1");
        String s2 = new String("s2");
        String s3 = new String("s3");
        oft = new ObjectFileType(s1, s2, s3, null, false);
        assertEquals(oft.getDescription(), s2);
        assertEquals(oft.getKeyword(), s1);
        assertEquals(oft.getExtensions(), s3);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFileType#ObjectFileType(net.ihe.gazelle.objects.model.ObjectFileType)}.
     */
    @Test()
    public void testObjectFileTypeObjectFileType() {
        oft.setDescription("desc");
        oft.setExtensions("extt");
        oft.setKeyword("key");
        ObjectFileType offf = new ObjectFileType(oft);
        assertEquals(offf, oft);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFileType#setId(java.lang.Integer)}.
     */
    @Test()
    public void testSetId() {
        Integer id = 123;
        oft.setId(id);
        assertEquals(oft.getId(), id);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFileType#setKeyword(java.lang.String)}.
     */
    @Test()
    public void testSetKeyword() {
        String key = new String("key");
        oft.setKeyword(key);
        assertEquals(oft.getKeyword(), key);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFileType#setDescription(java.lang.String)}.
     */
    @Test()
    public void testSetDescription() {
        String desc = new String("desc");
        oft.setDescription(desc);
        assertEquals(oft.getDescription(), desc);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFileType#setExtensions(java.lang.String)}.
     */
    @Test()
    public void testSetExtensions() {
        String ext = new String("ext");
        oft.setExtensions(ext);
        assertEquals(oft.getExtensions(), ext);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFileType#equals(java.lang.Object)}.
     */
    @Test()
    public void testEqualsObject() {
        ObjectFileType oft1 = new ObjectFileType("key", "desc", "ext", null, false);
        ObjectFileType oft2 = new ObjectFileType("key", "desc", "ext", null, false);
        assertEquals(oft1, oft2);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFileType#getListOfAllObjectFileType()}.
     */
    public void testGetListOfAllObjectFileType() {
        //fail("Not yet implemented");
    }

}
