/**
 *
 */
package net.ihe.gazelle.objects.test;

import junit.framework.TestCase;
import net.ihe.gazelle.objects.model.ObjectFile;
import net.ihe.gazelle.objects.model.ObjectFileType;
import net.ihe.gazelle.objects.model.ObjectType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author abderrazek boufahja
 */
public class ObjectFileTest extends TestCase {

    ObjectFile of;
    ObjectType ot;
    ObjectFileType oft;

    /**
     * @param name
     */
    public ObjectFileTest(String name) {
        super(name);
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeMethod
    protected void setUp() throws Exception {
        of = new ObjectFile();
        ot = new ObjectType("key", "desc", "def", "ins", null);
        oft = new ObjectFileType("key", "desc", "ext", null, false);
        super.setUp();
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#ObjectFile(net.ihe.gazelle.objects.model.ObjectType, java.lang.String, net.ihe.gazelle.objects.model.ObjectFileType, int, int, java.lang.String)}.
     */
    @Test()
    public void testObjectFileObjectTypeStringObjectFileTypeIntIntString() {
        String s1 = new String("s1");
        int i1 = 12;
        int i2 = 13;
        String s2 = new String("s2");
        of = new ObjectFile(ot, s1, oft, i1, i2, s2);
        assertNotNull(of);
        assertEquals(of.getDescription(), s1);
        assertEquals(of.getObject(), ot);
        assertEquals(of.getType(), oft);
        assertEquals(of.getMin(), i1);
        assertEquals(of.getMax(), i2);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#ObjectFile(net.ihe.gazelle.objects.model.ObjectFile)}.
     */
    @Test()
    public void testObjectFileObjectFile() {
        of.setDescription("desc");
        of.setMax(22);
        of.setMin(11);
        of.setObject(ot);
        of.setType(oft);
        of.setUploader("uploader");
        ObjectFile off = new ObjectFile(of);
        assertEquals(off, of);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#setId(java.lang.Integer)}.
     */
    @Test()
    public void testSetId() {
        Integer id = 12;
        of.setId(id);
        assertEquals(of.getId(), id);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#setObject(net.ihe.gazelle.objects.model.ObjectType)}.
     */
    @Test()
    public void testSetObject() {
        ot.setDescription("desc");
        ot.setInstructions("insc");
        ot.setKeyword("keyt");
        of.setObject(ot);
        assertEquals(of.getObject(), ot);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#setDescription(java.lang.String)}.
     */
    @Test()
    public void testSetDescription() {
        String desc = new String("desc");
        of.setDescription(desc);
        assertEquals(of.getDescription(), desc);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#setType(net.ihe.gazelle.objects.model.ObjectFileType)}.
     */
    @Test()
    public void testSetType() {
        oft.setDescription("desc");
        oft.setExtensions("ext");
        oft.setKeyword("key");
        of.setType(oft);
        assertEquals(of.getType(), oft);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#setMin(int)}.
     */
    @Test()
    public void testSetMin() {
        of.setMin(111);
        assertEquals(of.getMin(), 111);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#setMax(int)}.
     */
    @Test()
    public void testSetMax() {
        of.setMax(222);
        assertEquals(of.getMax(), 222);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#setUploader(java.lang.String)}.
     */
    @Test()
    public void testSetUploader() {
        String up = new String("up");
        of.setUploader(up);
        assertEquals(of.getUploader(), up);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#isDicom()}.
     */
    @Test()
    public void testIsDicom() {
        of = new ObjectFile();
        assertFalse(of.isDicom());
        oft.setKeyword("DICOM");
        of.setType(oft);
        assertTrue(of.isDicom());
        oft.setKeyword("CDA");
        of.setType(oft);
        assertFalse(of.isDicom());
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#isCDA()}.
     */
    @Test()
    public void testIsCDA() {
        of = new ObjectFile();
        assertFalse(of.isCDA());
        oft.setKeyword("DICOM");
        of.setType(oft);
        assertFalse(of.isCDA());
        oft.setKeyword("CDA");
        of.setType(oft);
        assertTrue(of.isCDA());
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectFile#compareTo(net.ihe.gazelle.objects.model.ObjectFile)}.
     */
    public void testCompareTo() {

    }

}
