/**
 *
 */
package net.ihe.gazelle.objects.test;


import junit.framework.TestCase;
import net.ihe.gazelle.objects.model.Annotation;
import net.ihe.gazelle.objects.model.ObjectInstance;
import net.ihe.gazelle.objects.model.ObjectInstanceAnnotation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author abderrazek boufahja
 */
public class ObjectInstanceAnnotationTest extends TestCase {

    ObjectInstanceAnnotation oia;

    /**
     * @param name
     */
    public ObjectInstanceAnnotationTest(String name) {
        super(name);
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeMethod
    protected void setUp() throws Exception {
        super.setUp();
        oia = new ObjectInstanceAnnotation();
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAnnotation#ObjectInstanceAnnotation(net.ihe.gazelle.objects.model.ObjectInstance, net.ihe.gazelle.objects.model.Annotation)}.
     */
    @Test()
    public void testObjectInstanceAnnotationObjectInstanceAnnotation() {
        ObjectInstance oi = new ObjectInstance(null, null, "desc", "name", true, null);
        Annotation ann = new Annotation("value");
        oia = new ObjectInstanceAnnotation(oi, ann);
        assertEquals(oia.getObjectInstance(), oi);
        assertEquals(oia.getAnnotation(), ann);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAnnotation#ObjectInstanceAnnotation(net.ihe.gazelle.objects.model.ObjectInstanceAnnotation)}.
     */
    @Test()
    public void testObjectInstanceAnnotationObjectInstanceAnnotation1() {
        ObjectInstance oi = new ObjectInstance(null, null, "desc", "name", true, null);
        Annotation ann = new Annotation("value");
        oia = new ObjectInstanceAnnotation(oi, ann);
        ObjectInstanceAnnotation oia2 = new ObjectInstanceAnnotation(oia);
        assertEquals(oia, oia2);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAnnotation#setObjectInstance(net.ihe.gazelle.objects.model.ObjectInstance)}.
     */
    @Test()
    public void testSetObjectInstance() {
        ObjectInstance oi = new ObjectInstance(null, null, "desc", "name", true, null);
        oia.setObjectInstance(oi);
        assertEquals(oia.getObjectInstance(), oi);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAnnotation#setAnnotation(net.ihe.gazelle.objects.model.Annotation)}.
     */
    @Test()
    public void testSetAnnotation() {
        Annotation ann = new Annotation("value");
        oia.setAnnotation(ann);
        assertEquals(oia.getAnnotation(), ann);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectInstanceAnnotation#setId(java.lang.Integer)}.
     */
    @Test()
    public void testSetId() {
        Integer id = 123;
        oia.setId(id);
        assertEquals(oia.getId(), id);
    }
}
