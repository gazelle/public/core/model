/**
 *
 */
package net.ihe.gazelle.objects.test;

import junit.framework.TestCase;
import net.ihe.gazelle.objects.model.ObjectCreator;
import net.ihe.gazelle.objects.model.ObjectType;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author abderrazek boufahja
 */
public class ObjectCreatorTest extends TestCase {

    ObjectCreator oc;
    ObjectType ot;
    ActorIntegrationProfileOption aipo;

    /**
     * @param name
     */
    public ObjectCreatorTest(String name) {
        super(name);
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeMethod
    protected void setUp() throws Exception {
        super.setUp();
        oc = new ObjectCreator();
        ot = new ObjectType("key", "desc", "def", "ins", null);
        aipo = new ActorIntegrationProfileOption();
    }

    /* (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectCreator#ObjectCreator(java.lang.String, net.ihe.gazelle.tf.model.ActorIntegrationProfileOption, net.ihe.gazelle.objects.model.ObjectType)}.
     */
    @Test()
    public void testObjectCreatorStringActorIntegrationProfileOptionObjectType() {
        String desc = new String("desc");
        oc = new ObjectCreator(desc, aipo, ot);
        assertEquals(oc.getDescription(), desc);
        assertEquals(oc.getAIPO(), aipo);
        assertEquals(oc.getObject(), ot);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectCreator#setId(java.lang.Integer)}.
     */
    @Test()
    public void testSetId() {
        Integer id = 33;
        oc.setId(id);
        assertNotNull(oc.getId());
        assertEquals(oc.getId(), id);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectCreator#setAIPO}.
     */
    @Test()
    public void testSetAIPO() {
        oc.setAIPO(aipo);
        assertNotNull(oc.getAIPO());
        assertEquals(oc.getAIPO(), aipo);
    }

    /**
     * Test method for {@link net.ihe.gazelle.objects.model.ObjectCreator#setAIPO}.
     */
    @Test()
    public void testSetObject() {
        oc.setObject(ot);
        assertNotNull(oc.getObject());
        assertEquals(oc.getObject(), ot);
    }


}
