package net.ihe.gazelle.tm.utils.fk;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class TestFixForeignKeys {

    private static Logger logger;
    private static ByteArrayOutputStream bos;

    // private static PipedInputStream pis;
    private EntityManager em;
    private EntityManagerFactory emFactory;

    public static void main(String[] args) throws Exception {
        // pis = new PipedInputStream();
        // PipedOutputStream os = new PipedOutputStream(pis);
        bos = new ByteArrayOutputStream();
        PrintStream out = new PrintStreamDoubleDelegate(bos, System.out);
        PrintStream oldOut = System.out;
        PrintStream oldErr = System.out;
        System.setOut(out);
        System.setErr(out);

        logger = LoggerFactory.getLogger(TestFixForeignKeys.class);
        ArrayList<String> queriesOK = new ArrayList<String>();

        TestFixForeignKeys testFixForeignKeys = new TestFixForeignKeys();
        List<Solver> solvers = new ArrayList<Solver>();
        solvers.add(new SolverWithAuditRecords(queriesOK));
        solvers.add(new SolverWithDelete(queriesOK));
        solvers.add(new SolverManual(queriesOK));
        testFixForeignKeys.cleanUsingSolvers(solvers);

        System.out.println("Applied queries : ");
        for (String queryOK : queriesOK) {
            System.out.println(queryOK);
        }

        System.setOut(oldOut);
        System.setErr(oldErr);
    }

    public static List<String> getLines() throws Exception {
        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        bos.reset();
        return IOUtils.readLines(bis);
    }

    protected Set<Constraint> setUp() throws Exception {
        try {
            Enumeration<URL> xmls = Thread.currentThread().getContextClassLoader()
                    .getResources("META-INF/persistence.xml");
            if (!xmls.hasMoreElements()) {
                throw new RuntimeException("Exception during database startup.");
            }
        } catch (IOException e) {
            throw new RuntimeException("Exception during database startup.");
        }

        try {
            logger.info("Building JPA EntityManager for unit tests");
            emFactory = Persistence.createEntityManagerFactory("gazellePU-tm");
            em = emFactory.createEntityManager();
        } catch (Exception ex) {
            throw new RuntimeException("Exception during JPA EntityManager instanciation.", ex);
        }

        List<String> startupLog = getLines();

        Set<Constraint> constraints = new TreeSet<Constraint>();
        for (String line : startupLog) {
            if (line.contains("Unsuccessful: alter table ") && line.contains("add constraint")) {
                line = line.substring(line.indexOf("alter table"));
                String[] split = StringUtils.split(line, ' ');

                String table = split[2].replace("public.", "");
                String id = split[5];
                String remote_id = split[8].replace("(", "").replace(")", "");
                String remote_table = split[10].replace("public.", "");

                if (table != null && id != null && remote_id != null && remote_table != null) {
                    constraints.add(new Constraint(table, id, remote_id, remote_table));
                }
            }
        }
        return constraints;
    }

    public void cleanUsingSolvers(final List<Solver> solvers) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        final Set<Constraint> constraints = setUp();

        Session session = null;
        Object delegate = em.getDelegate();
        if (delegate instanceof Session) {
            session = (Session) delegate;
        }

        for (final Constraint constraint : constraints) {
            boolean solved = false;
            while (!solved) {
                Solver solver = null;
                System.out.println(constraint);

                for (int i = 0; i < solvers.size(); i++) {
                    Solver solverNotDone = solvers.get(i);
                    System.out.println(i + " : " + solverNotDone.getLabel());
                }
                System.out.println(solvers.size() + " : Skip");
                System.out.print("Select solver : ");
                int i = -1;
                while (i < 0 || i >= solvers.size() + 1) {
                    String line = br.readLine();
                    try {
                        i = Integer.valueOf(line);
                    } catch (NumberFormatException e) {
                        i = -1;
                    }
                }
                if (i == solvers.size()) {
                    solver = null;
                } else {
                    solver = solvers.get(i);
                }
                if (solver != null) {
                    final Solver solverFinal = solver;
                    session.doWork(new Work() {
                        @Override
                        public void execute(Connection connection) throws SQLException {
                            solverFinal.fixConstraint(constraint, connection);
                        }
                    });
                    solved = solverFinal.isSolved();
                } else {
                    solved = true;
                }
            }

        }

        if (em != null) {
            em.close();
        }
        if (emFactory != null) {
            emFactory.close();
        }
    }
}
