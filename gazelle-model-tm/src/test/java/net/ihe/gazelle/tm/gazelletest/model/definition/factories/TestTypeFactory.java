package net.ihe.gazelle.tm.gazelletest.model.definition.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestType;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestTypeQuery;

import java.util.List;

public class TestTypeFactory {

    public static TestType createTestType_TYPE_CONNECTATHON() {
        TestType testType = TestType.getTYPE_CONNECTATHON();
        if (testType == null) {
            testType = provideTestType_TYPE_CONNECTATHON();
            testType = (TestType) DatabaseManager.writeObject(testType);
        }

        return testType;
    }

    public static TestType provideTestType_TYPE_CONNECTATHON() {
        TestType testType = new TestType();
        testType.setKeyword(TestType.TYPE_CONNECTATHON_STRING);
        return testType;
    }

    public static TestType createTestType_TYPE_MESA() {
        TestType testType = TestType.getTYPE_MESA();
        if (testType == null) {
            testType = new TestType();
            testType.setKeyword(TestType.TYPE_MESA_STRING);
            testType = (TestType) DatabaseManager.writeObject(testType);
        }
        return testType;
    }

    public static void cleanTestType() {
        TestTypeQuery testTypeQuery = new TestTypeQuery();
        List<TestType> testType = testTypeQuery.getListDistinct();
        for (TestType testType_ : testType) {
            DatabaseManager.removeObject(testType_);
        }
        // Todo Clean Related Objects
    }
}
