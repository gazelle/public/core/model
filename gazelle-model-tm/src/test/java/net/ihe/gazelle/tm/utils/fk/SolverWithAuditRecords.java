package net.ihe.gazelle.tm.utils.fk;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SolverWithAuditRecords extends Solver {

    public SolverWithAuditRecords(List<String> queriesOK) {
        super(queriesOK);
    }

    public boolean processMissing(String padding, String message, String querySource, Constraint constraint,
                                  Connection connection) throws SQLException {
        // Détail : La clé (test_id)=(217) n'est pas présente dans la table
        // tm_test .

        // Building the query to reinsert the data
        String finalQuery = "";
        try {
            int start = message.indexOf('(');
            message = message.substring(start + 1);
            start = message.indexOf('(');
            message = message.substring(start + 1);
            String id = message.substring(0, message.indexOf(')'));
            start = message.indexOf('"');
            message = message.substring(start + 1);
            String table = message.substring(0, message.indexOf('"'));

            Statement statement = null;
            statement = connection.createStatement();
            String query = "SELECT * FROM " + table + "_aud where id = " + id + " order by rev desc";
            System.out.println(padding + " - " + query);
            ResultSet result = statement.executeQuery(query);

            result.next();
            int rev = result.getInt("rev");
            result.next();
            rev = result.getInt("rev");
            statement.close();

            List<String> columns = new ArrayList<String>();

            statement = connection.createStatement();
            result = statement.executeQuery("select column_name from information_schema.columns where table_name = '"
                    + table + "'");
            while (result.next()) {
                columns.add(result.getString(1));
            }
            statement.close();

            finalQuery = "INSERT INTO " + table + " (";

            String columnsString = "";
            for (int i = 0; i < columns.size(); i++) {
                columnsString = columnsString + columns.get(i);
                if (i != columns.size() - 1) {
                    columnsString = columnsString + ", ";
                }
            }
            finalQuery = finalQuery + columnsString + ") select " + columnsString + " from " + table
                    + "_aud where id = " + id + " and rev = " + rev + ";";
        } catch (Throwable e) {
            logger.error(padding + "Failed to extract informations...", e);
            return false;
        }

        // trying to apply the update
        return patchRecursively(" " + padding, finalQuery, constraint, connection, querySource);
    }

    @Override
    public String getLabel() {
        return "Try to reinsert missing objects using audit tables";
    }

}
