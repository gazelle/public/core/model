package net.ihe.gazelle.tm.gazelletest.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.tm.systems.model.SystemInSessionQuery;

import java.util.List;

public class SystemInSessionFactory {

    public static SystemInSession createSystemInSessionWithMandatoryFields(String userId) {
        SystemInSession sysInSession = new SystemInSession();
        sysInSession.setTestingSession(TestingSessionFactory.createTestingSessionWithMandatoryFields("Connectathon France", userId));
        sysInSession.setSystem(SystemFactory.createSystemWithMandatoryFields());
        sysInSession = (SystemInSession) DatabaseManager.writeObject(sysInSession);
        return sysInSession;
    }

    public static void cleanSystemInSession() {
        SystemInSessionQuery sysInSessionQuery = new SystemInSessionQuery();
        List<SystemInSession> sysInSession = sysInSessionQuery.getListDistinct();
        for (SystemInSession system : sysInSession) {
            DatabaseManager.removeObject(system);
        }
        SystemFactory.cleanSystems();
    }
}
