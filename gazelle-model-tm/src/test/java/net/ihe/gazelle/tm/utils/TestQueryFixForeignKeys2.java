package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Ignore
public class TestQueryFixForeignKeys2 {

    private static Logger logger = LoggerFactory.getLogger(TestQueryFixForeignKeys2.class.getName());
    private ArrayList<String> queriesOK;
    private String lastSolvedQuery;

    public void testCleanFK() {
        try {
            queriesOK = new ArrayList<String>();
            EntityManager em = EntityManagerService.provideEntityManager();
            Object delegate = em.getDelegate();
            if (delegate instanceof Session) {
                Session session = (Session) delegate;
                session.doWork(new Work() {

                    @Override
                    public void execute(Connection connection) throws SQLException {
                        try {
                            cleanFK(connection);
                        } catch (IOException e) {
                            throw new SQLException(e);
                        }
                    }
                });
            }

            System.out.println("Applied queries : ");
            for (String queryOK : queriesOK) {
                System.out.println(queryOK);
            }
        } catch (Exception ex) {
            throw new RuntimeException("Exception during testPersistence", ex);
        }
    }

    private void cleanFK(Connection connection) throws IOException {
        List<Constraint> constraints = new ArrayList<TestQueryFixForeignKeys2.Constraint>();

        BufferedReader br = new BufferedReader(new FileReader("/tmp/server2.log"));
        for (String line = br.readLine(); line != null; line = br.readLine()) {
            if (line.contains("Unsuccessful: alter table ") && line.contains("add constraint")) {
                line = line.substring(line.indexOf("alter table"));
                String[] split = StringUtils.split(line, ' ');

                String table = split[2].replace("public.", "");
                String id = split[5];
                String remote_id = split[8].replace("(", "").replace(")", "");
                String remote_table = split[10].replace("public.", "");

                if (table != null && id != null && remote_id != null && remote_table != null) {
                    constraints.add(new Constraint(table, id, remote_id, remote_table));
                }
            }
        }
        // patchRecursively("",
        // "update tm_test_aud SET keyword='10924_DEPRECATED' where id = 10924",
        // connection);
        // patchRecursively("",
        // "update tm_test_aud SET keyword='ECHO_IM_SOP_Class_Support_DEPRECATED' where id = 323");
        // patchRecursively("",
        // "update tm_test_aud SET keyword='14352_DEPRECATED' where id = 10924");

        createConstraints(constraints, connection);
    }

    private void createConstraints(List<Constraint> constraints, Connection connection) {
        for (Constraint constraint : constraints) {

            String query = "alter table " + constraint.table + " add constraint " + constraint.id + " foreign key ("
                    + constraint.remote_id + ") references " + constraint.remote_table;

            queriesOK.add("-- SOLVING " + query);
            patchRecursively("", query, constraint, connection, query + " ");
        }
    }

    private boolean patchRecursively(String padding, String query, Constraint constraint, Connection connection,
                                     String solvedQuery) {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            try {
                System.out.println(" " + query);
                // Try to execute the update
                statement.executeUpdate(query);
            } catch (SQLException e) {
                String newError = e.getMessage();
                System.out.println(" " + newError);
                // If failure, trying to delete lines with the missing id
                if (processMissing(padding, newError, query, constraint, connection)) {
                    // If done, retry the first query
                    return patchRecursively(" " + padding, query, constraint, connection, query);
                } else {
                    return false;
                }
            }
            if (!query.equals(solvedQuery) && !solvedQuery.equals(lastSolvedQuery)) {
                queriesOK.add("-- solving " + solvedQuery);
                lastSolvedQuery = solvedQuery;
            }
            queriesOK.add(query);
            System.out.println(" OK");
            statement.close();
            return true;
        } catch (SQLException e) {
            logger.error("", e);
            return false;
        }
    }

    private boolean processMissing(String padding, String message, String query, Constraint constraint,
                                   Connection connection) throws SQLException {
        // Détail : La clé (test_id)=(217) n'est pas présente dans la table
        // tm_test .

        // Building the query to reinsert the data
        String finalQuery = "";
        try {
            if (query.startsWith("alter table")) {
                int start = message.indexOf('(');
                message = message.substring(start + 1);
                start = message.indexOf('(');
                message = message.substring(start + 1);
                String id = message.substring(0, message.indexOf(')'));
                start = message.indexOf('\u00AB');
                message = message.substring(start + 2);
                String table = message.substring(0, message.indexOf('\u00BB') - 1);

                finalQuery = "DELETE FROM " + constraint.table + " WHERE " + constraint.remote_id + "=" + id + ";";
            } else {
                /*
                 ERREUR: UPDATE ou DELETE sur la table « tm_contextual_information_instance » viole la contrainte de clé étrangère
				« fkd21c0f0e7b906045 » de la table « tm_test_steps_instance_input_ci_instance »
				  Détail : La clé (id)=(1) est toujours référencée à partir de la table « tm_test_steps_instance_input_ci_instance ».
				 */
                String id = message.substring(message.indexOf(')') + 1);
                id = id.substring(id.indexOf('(') + 1, id.indexOf(')'));

                message = message.substring(message.indexOf('«') + 1);
                message = message.substring(message.indexOf('«') + 1);
                String fk = message.substring(1, message.indexOf('»') - 1);
                message = message.substring(message.indexOf('«') + 1);
                message = message.substring(message.indexOf('«') + 1);
                String table = message.substring(1, message.indexOf('»') - 1);

                String query2 = "	SELECT"
                        + "    tc.constraint_name, tc.table_name, kcu.column_name, "
                        + "    ccu.table_name AS foreign_table_name,"
                        + "    ccu.column_name AS foreign_column_name "
                        + " FROM "
                        + "    information_schema.table_constraints AS tc "
                        + "    JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name"
                        + "    JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name"
                        + " WHERE constraint_type = 'FOREIGN KEY' and tc.constraint_name='" + fk + "';";

                Statement statement = null;
                statement = connection.createStatement();
                ResultSet result = statement.executeQuery(query2);
                result.next();
                String table1 = result.getString(2);
                String key1 = result.getString(3);
                String key2 = result.getString(4);

                String deleteKey;
                if (table.equals(table1)) {
                    deleteKey = key1;
                } else {
                    deleteKey = key2;
                }

                finalQuery = "DELETE FROM " + table + " WHERE " + deleteKey + "=" + id + ";";
            }
        } catch (Throwable e) {
            logger.error(padding + "Failed to extract informations...", e);
            return false;
        }

        // trying to apply the update
        return patchRecursively(" " + padding, finalQuery, constraint, connection, query);
    }

    private class Constraint {
        String table;
        String id;
        String remote_id;
        String remote_table;

        public Constraint(String table, String id, String remote_id, String remote_table) {
            super();
            this.table = table;
            this.id = id;
            this.remote_id = remote_id;
            this.remote_table = remote_table;
        }
    }
}
