package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.common.model.ApplicationPreference;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import org.junit.Ignore;

import javax.persistence.EntityManager;
import java.util.List;

@Ignore
public class TestQueryDevMode {

    public void testCreateCertificates() {
        EntityManager em = EntityManagerService.provideEntityManager();
        em.getTransaction().begin();
        HQLQueryBuilder<ApplicationPreference> queryBuilder = new HQLQueryBuilder<ApplicationPreference>(
                ApplicationPreference.class);
        List<ApplicationPreference> list = queryBuilder.getList();
        for (ApplicationPreference applicationPreference : list) {
            if (applicationPreference.getPreferenceName().equals("application_url")) {
                applicationPreference.setPreferenceValue("http://bento:8080/TM/");
                em.merge(applicationPreference);
            }
            if (applicationPreference.getPreferenceName().equals("application_url_basename")) {
                applicationPreference.setPreferenceValue("TM");
                em.merge(applicationPreference);
            }
        }
        em.flush();
        em.getTransaction().commit();
    }
}
