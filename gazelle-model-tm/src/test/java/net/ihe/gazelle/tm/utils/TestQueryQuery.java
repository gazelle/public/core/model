package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.hql.HQLInterval;
import net.ihe.gazelle.hql.HQLIntervalCount;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.objects.model.ObjectCreator;
import net.ihe.gazelle.objects.model.ObjectCreatorQuery;
import net.ihe.gazelle.pr.search.model.SearchLogReportQuery;
import org.junit.Ignore;

import java.util.List;

@Ignore
public class TestQueryQuery {

    public void testGetTests() {

        ObjectCreatorQuery creatorQuery = new ObjectCreatorQuery();
        creatorQuery.setMaxResults(5);
        List<ObjectCreator> list = creatorQuery.getList();

        HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.get().close();
        HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.set(null);

        for (ObjectCreator objectCreator : list) {
            System.out.println(objectCreator.getReversedAipo().getTestParticipants().size());
        }
    }

    private void addInterval(List<List<HQLIntervalCount>> intervalsList, HQLInterval interval, String keyword) {
        SearchLogReportQuery searchLogReportQuery = new SearchLogReportQuery();
        if (keyword != null) {
            searchLogReportQuery.foundSystemsForSearchLog().keyword().eq(keyword);
        }
        List<HQLIntervalCount> countPerInterval = searchLogReportQuery.date().getCountPerInterval(interval);
        System.out.println(countPerInterval.size());
        intervalsList.add(countPerInterval);
    }

    private void appendInterval(List<HQLIntervalCount> intervals, StringBuilder series) {
        series.append("[");
        boolean first = true;
        for (HQLIntervalCount hqlIntervalCount : intervals) {
            // System.out.println(hqlIntervalCount);
            if (first) {
                first = false;
            } else {
                series.append(",\r\n");
            }
            series.append("{ x: ");
            series.append(hqlIntervalCount.getIntervalStartDate().getTime() / 1000);
            series.append(", y: ");
            series.append(hqlIntervalCount.getCount());
            series.append(" }");
        }
        series.append("]");
    }
}
