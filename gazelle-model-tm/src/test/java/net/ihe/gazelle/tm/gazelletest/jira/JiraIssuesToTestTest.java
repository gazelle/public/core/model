package net.ihe.gazelle.tm.gazelletest.jira;

import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.gazelletest.model.definition.factories.TestFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

@Ignore
public class JiraIssuesToTestTest {

    @Test
    public void test() {
        net.ihe.gazelle.tm.gazelletest.model.definition.Test test = TestFactory.createConnectathonTest();
        JiraIssuesToTest jiraIssue = new JiraIssuesToTest();
        jiraIssue.setKey("TES-80");
        jiraIssue.setStatus("Open");
        jiraIssue.setSummary("Test is not ok");
        jiraIssue.setTest(test);
        DatabaseManager.writeObject(jiraIssue);
        DatabaseManager.update(test);
        assertNotNull(test.getJiraIssues());
    }

}
