package net.ihe.gazelle.tm.configurations.model;

import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by jlabbe on 09/07/15.
 */
@Ignore
public class HostTest {

    @Test
    public void getHosts() {
        HostQuery q = new HostQuery();
        q.hostname().eq("iasi1");
        Host uniqueResult = q.getUniqueResult();
        assertNotNull(uniqueResult);
    }
}
