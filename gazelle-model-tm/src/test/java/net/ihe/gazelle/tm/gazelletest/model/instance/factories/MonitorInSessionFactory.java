package net.ihe.gazelle.tm.gazelletest.model.instance.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.gazelletest.model.factories.TestingSessionFactory;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSessionQuery;
import net.ihe.gazelle.users.model.factories.UserFactory;

import java.util.List;

public class MonitorInSessionFactory {

    public static MonitorInSession createMonitorInSessionWithMandatoryFields() {
        MonitorInSession monitorInSession = new MonitorInSession();
        String userId = UserFactory.createUserWithMandatoryFields().getId();
        monitorInSession.setUserId(userId);
        monitorInSession.setTestingSession(TestingSessionFactory.createTestingSessionWithMandatoryFields("Connectathon France", userId));
        monitorInSession = (MonitorInSession) DatabaseManager.writeObject(monitorInSession);
        return monitorInSession;
    }

    public static MonitorInSession provideMonitorInSessionWithMandatoryFields() {
        MonitorInSession monitorInSession = new MonitorInSession();
        monitorInSession.setUserId(UserFactory.provideUserWithMandatoryFields().getId());
        monitorInSession.setTestingSession(TestingSessionFactory.provideTestingSessionWithMandatoryFields("Connectathon France"));
        return monitorInSession;
    }

    public static void cleanMonitorInSession() {
        MonitorInSessionQuery monitorInSessionQuery = new MonitorInSessionQuery();
        List<MonitorInSession> monitorInSession = monitorInSessionQuery.getListDistinct();
        for (MonitorInSession monitorInSession_ : monitorInSession) {
            DatabaseManager.removeObject(monitorInSession_);
        }
        UserFactory.cleanUsers();
        TestingSessionFactory.cleanTestingSession();
    }
}
