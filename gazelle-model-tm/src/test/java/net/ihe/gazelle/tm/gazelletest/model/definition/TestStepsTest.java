package net.ihe.gazelle.tm.gazelletest.model.definition;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestStepsTest {
    TestSteps testStep;

    @Before
    public void init() {
        testStep = new TestSteps();
    }

    @Test
    public void findAssertionsFromTestStepDefinitionEmpty() {
        testStep.setDescription("");
        List<AssertionStatus> assertionsFromDescription = testStep.getAssertionsFromDescription();
        assertEquals(new ArrayList<String>(), assertionsFromDescription);
    }

    @Test
    public void assertionsMustBeBetweenSquareBraquets() {
        testStep.setDescription("aaa-10");
        List<AssertionStatus> assertionsFromDescription = testStep.getAssertionsFromDescription();
        assertEquals(new ArrayList<String>(), assertionsFromDescription);
    }

    @Test
    @Ignore
    public void findAssertionsFromTestStepDefinitionAssertionOnly() {
        testStep.setDescription("[aaa-10]");
        List<AssertionStatus> assertions = new ArrayList<AssertionStatus>();
        assertions.add(new AssertionStatus("aaa-10", 1));
        List<AssertionStatus> assertionsFromDescription = testStep.getAssertionsFromDescription();
        assertEquals(assertions, assertionsFromDescription);
    }

    @Test
    @Ignore
    public void findAssertionsFromTestStepDefinitionAssertionOnly_() {

        testStep.setDescription("[[aaa-10]] [[aaa-10]");
        List<AssertionStatus> assertions = new ArrayList<AssertionStatus>();
        assertions.add(new AssertionStatus("aaa-10", 1));
        assertions.add(new AssertionStatus("aaa-10", 1));
        List<AssertionStatus> assertionsFromDescription = testStep.getAssertionsFromDescription();
        assertEquals(assertions, assertionsFromDescription);
    }

    @Test
    @Ignore
    public void findAssertionsFromTestStepDefinition_ShortAssertionOnly() {
        testStep.setDescription("[a-1]");
        List<AssertionStatus> assertions = new ArrayList<AssertionStatus>();
        assertions.add(new AssertionStatus("a-1", 1));
        List<AssertionStatus> assertionsFromDescription = testStep.getAssertionsFromDescription();
        assertEquals(assertions, assertionsFromDescription);
    }

    @Test
    @Ignore
    public void findAssertionsFromTestStepDefinition_AssertionInsideText() {
        testStep.setDescription("This is my text[a-1]and an assertion");
        List<AssertionStatus> assertions = new ArrayList<AssertionStatus>();
        assertions.add(new AssertionStatus("a-1", 1));
        List<AssertionStatus> assertionsFromDescription = testStep.getAssertionsFromDescription();
        assertEquals(assertions, assertionsFromDescription);
    }

    @Test
    public void findAssertionsFromTestStepDefinition_WithoutAssertionInsideText() {
        testStep.setDescription("The Dimension Index Pointer (0020,9165) shall be populated with the attribute tags of Stack ID (0020,9056), In-Stack Position Number (0020,9057), and Diffusion b-value (0018,9087).");
        List<AssertionStatus> assertionsFromDescription = testStep.getAssertionsFromDescription();
        assertEquals(new ArrayList<AssertionStatus>(), assertionsFromDescription);
    }

    @Test
    public void findAssertionsFromTestStepDefinition_WithoutAssertionInsideText2() {
        testStep.setDescription("Document Consumer: Access the Connectathon -> List of Samples page. Find Consent documents submitted by BPPC Content Creator under test, eg: RestrictedConsent^[company-name]. The page will tell you the XDS Registry to which the documents were registered");
        List<AssertionStatus> assertionsFromDescription = testStep.getAssertionsFromDescription();
        assertEquals(new ArrayList<AssertionStatus>(), assertionsFromDescription);
    }

    @Test
    @Ignore
    public void findAssertionsFromTestStepDefinition_WithMultipleAssertionsInsideText() {
        testStep.setDescription("The Document Consumer must determine the Patient ID and entryUUID for the document to be retrieved.  Most Doc Consumers will do this by executing the Find Document Dossiers transaction [ITI-67] and Get Document Dossiers [ITI-66], but that method is not req (...)");
        List<AssertionStatus> assertionsFromDescription = testStep.getAssertionsFromDescription();
        List<AssertionStatus> assertions = new ArrayList<AssertionStatus>();
        assertions.add(new AssertionStatus("ITI-67", 1));
        assertions.add(new AssertionStatus("ITI-66", 1));
        assertEquals(assertions, assertionsFromDescription);
    }
}
