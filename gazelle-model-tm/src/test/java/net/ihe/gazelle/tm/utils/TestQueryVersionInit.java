package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.junit.After;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

@Ignore
public class TestQueryVersionInit {

    private static Logger log = LoggerFactory.getLogger(TestQueryVersionInit.class);

    protected Set<String> allTables;

    @After
    public void setUp() throws Exception {
        allTables = new HashSet<String>();
    }

    public void testInitVersionTables() {
        EntityManager em = EntityManagerService.provideEntityManager();
        Object delegate = em.getDelegate();
        if (delegate instanceof Session) {
            Session session = (Session) delegate;
            session.doWork(new Work() {

                @Override
                public void execute(Connection connection) throws SQLException {
                    String[] types = {"TABLE"};
                    ResultSet tables = connection.getMetaData().getTables("public", "public", "%", types);
                    while (tables.next()) {
                        addTable(tables.getString(3));
                    }
                }
            });

            for (final String table : allTables) {
                session.doWork(new Work() {
                    @Override
                    public void execute(Connection connection) throws SQLException {
                        executeQuery("UPDATE " + table + " SET hibernateversion = 0", connection);
                    }
                });
            }
        }
    }

    private void addTable(String table) {
        if (!table.startsWith("pg_")) {
            System.out.println("Adding " + table);
            allTables.add(table.toLowerCase());
        }
    }

    private boolean executeQuery(String query, Connection connection) {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            try {
                System.out.println(" " + query);
                // Try to execute the update
                statement.executeUpdate(query);
            } catch (SQLException e) {
            }
            System.out.println(" OK");
            statement.close();
            return true;
        } catch (SQLException e) {
            log.error("", e);
            return false;
        }
    }
}
