package net.ihe.gazelle.tm.gazelletest.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.tm.systems.model.TestingSessionQuery;
import net.ihe.gazelle.users.model.factories.UserFactory;

import java.util.Date;
import java.util.List;

public class TestingSessionFactory {

    private static final User USER = UserFactory.createUserWithMandatoryFields();


    public static TestingSession createTestingSessionWithMandatoryFields(String name, String userId) {
        TestingSession testingSession = provideTestingSessionWithMandatoryFields(name);
        testingSession = (TestingSession) DatabaseManager.writeObject(testingSession);
        TestingSessionAdminFactory.provideTestingSessionAdminWithMandatoryFields(testingSession.getId(), userId);
        return testingSession;
    }

    public static void cleanTestingSession() {
        TestingSessionQuery testingSessionQuery = new TestingSessionQuery();
        List<TestingSession> testingSession = testingSessionQuery.getListDistinct();
        for (TestingSession s : testingSession) {
            DatabaseManager.removeObject(s);
        }
    }

    public static TestingSession provideTestingSessionWithMandatoryFields(String name) {
        TestingSession testingSession = new TestingSession();
        testingSession.setName(name);
        testingSession.setRegistrationDeadlineDate(new Date());
        testingSession.setContactLastname("toto");
        testingSession.setContactFirstname("tutu");
        testingSession.setContactEmail("tututoto@ihe.com");
        testingSession.setAllowOneCompanyPlaySeveralRolesInGroupTests(false);
        testingSession.setAllowOneCompanyPlaySeveralRolesInP2PTests(false);
        testingSession.setAllowParticipantsStartGroupTests(false);
        testingSession.setAllowReportDownload(false);
        return testingSession;
    }

    public static User getUser() {
        return USER;
    }
}
