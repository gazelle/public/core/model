package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.jdbc.Work;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.persister.collection.AbstractCollectionPersister;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.junit.After;
import org.junit.Ignore;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.Map.Entry;

@Ignore
public class TestQueryUnusedTables {

    protected Map<String, TableCount> mapTables;

    @After
    public void setUp() throws Exception {
        mapTables = new HashMap<String, TestQueryUnusedTables.TableCount>();
    }

    public void testUnusedTables() {
        EntityManager em = EntityManagerService.provideEntityManager();
        Object delegate = em.getDelegate();
        SessionFactoryImplementor factory = null;

        final List<String> tableNames = new ArrayList<String>();

        if (delegate instanceof Session) {
            Session session = (Session) delegate;
            session.doWork(new Work() {

                @Override
                public void execute(Connection connection) throws SQLException {
                    String[] types = {"TABLE"};
                    ResultSet tables = connection.getMetaData().getTables("public", "public", "%", types);
                    while (tables.next()) {
                        String table = tables.getString(3);
                        if (!table.startsWith("pg_")) {
                            System.out.println("Adding " + table);
                            tableNames.add(table.toLowerCase());
                        }
                    }
                }
            });

            SessionFactory sessionFactory = session.getSessionFactory();
            if (sessionFactory instanceof SessionFactoryImplementor) {
                factory = (SessionFactoryImplementor) sessionFactory;
            }

            for (String table : tableNames) {
                TableCount tableCount = new TableCount(table, 0);

                SQLQuery sqlQuery = session.createSQLQuery("select count(*) from " + table);
                Object uniqueResult = sqlQuery.uniqueResult();
                if (uniqueResult != null && uniqueResult instanceof Number) {
                    tableCount.count = ((Number) uniqueResult).intValue();
                }

                mapTables.put(table, tableCount);
            }
        }

        removeTable("revinfo", null);

        Map<String, ClassMetadata> allClassMetadata = factory.getAllClassMetadata();
        Set<Entry<String, ClassMetadata>> entrySet = allClassMetadata.entrySet();
        for (Entry<String, ClassMetadata> entry : entrySet) {
            ClassMetadata value = entry.getValue();
            System.out.println(value.getEntityName());
            if (value instanceof AbstractEntityPersister) {
                AbstractEntityPersister aep = (AbstractEntityPersister) value;
                removeTable(aep.getTableName(), aep);
            }
        }
        Map<String, CollectionMetadata> allCollectionMetadata = factory.getAllCollectionMetadata();
        Set<Entry<String, CollectionMetadata>> entrySet2 = allCollectionMetadata.entrySet();
        for (Entry<String, CollectionMetadata> entry : entrySet2) {
            CollectionMetadata value = entry.getValue();
            if (value instanceof AbstractCollectionPersister) {
                AbstractCollectionPersister acp = (AbstractCollectionPersister) value;
                removeTable(acp.getTableName(), acp);
            }
        }

        List<TableCount> tables = new ArrayList<TableCount>(mapTables.values());
        Collections.sort(tables);
        for (TableCount table : tables) {
            if (!table.name.endsWith("_aud")) {
                System.out.println(table);
            }
        }
    }

    private void removeTable(String table, Object persister) {
        table = table.toLowerCase();
        if (table.startsWith("public.")) {
            table = table.replace("public.", "");
        }
        if (mapTables.containsKey(table)) {
            System.out.println("Removing " + table);
            mapTables.get(table).used = true;
            mapTables.get(table).persister = persister;
            if (mapTables.get(table + "_aud") != null) {
                mapTables.get(table + "_aud").used = true;
                mapTables.get(table + "_aud").persister = persister;
            }
        }
    }

    private static final class TableCount implements Comparable<TableCount> {

        String name;
        int count;
        boolean used;
        Object persister;

        public TableCount(String name, int count) {
            super();
            this.name = name;
            this.count = count;
            this.used = false;
        }

        @Override
        public int compareTo(TableCount o) {
            if (used && !o.used) {
                return 1;
            }
            if (!used && o.used) {
                return -1;
            }
            if (count < o.count) {
                return 1;
            }
            if (count > o.count) {
                return -1;
            }
            if (count == o.count) {
                return 0;
            }
            return 0;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            TableCount other = (TableCount) obj;
            if (name == null) {
                if (other.name != null) {
                    return false;
                }
            } else if (!name.equals(other.name)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "TableCount [count=" + count + ", name=" + name + ", used=" + used + ", persister=" + persister
                    + "]";
        }
    }
}
