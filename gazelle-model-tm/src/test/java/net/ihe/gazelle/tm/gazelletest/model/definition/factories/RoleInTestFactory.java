package net.ihe.gazelle.tm.gazelletest.model.definition.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.gazelletest.model.definition.RoleInTest;
import net.ihe.gazelle.tm.gazelletest.model.definition.RoleInTestQuery;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRoles;

import java.util.ArrayList;
import java.util.List;

public class RoleInTestFactory {
    public static List<RoleInTest> createRoleInTestWithMandatoryFields() {
        RoleInTest roleInTest = new RoleInTest();
        roleInTest.setTestRoles(TestRolesFactory.createTestRolesWithMandatoryFields());
        roleInTest = (RoleInTest) DatabaseManager.writeObject(roleInTest);
        List<RoleInTest> list = new ArrayList<RoleInTest>();
        list.add(roleInTest);
        return list;
    }

    public static RoleInTest createOneRoleInTestWithMandatoryFields() {
        RoleInTest roleInTest = new RoleInTest();
        roleInTest.setTestRoles(TestRolesFactory.createTestRolesWithMandatoryFields());
        roleInTest = (RoleInTest) DatabaseManager.writeObject(roleInTest);
        return roleInTest;
    }

    public static List<RoleInTest> createRoleInTestWithMandatoryFields(List<TestRoles> testRoles) {
        RoleInTest roleInTest = new RoleInTest();
        roleInTest.setTestRoles(testRoles);
        roleInTest = (RoleInTest) DatabaseManager.writeObject(roleInTest);
        List<RoleInTest> list = new ArrayList<RoleInTest>();
        list.add(roleInTest);
        return list;
    }

    public static List<RoleInTest> provideRoleInTestWithMandatoryFields() {
        RoleInTest roleInTest = new RoleInTest();
        roleInTest.setTestRoles(TestRolesFactory.provideTestRolesWithMandatoryFields());
        List<RoleInTest> list = new ArrayList<RoleInTest>();
        list.add(roleInTest);
        return list;
    }

    public static void cleanRoleInTest() {
        RoleInTestQuery roleInTestQuery = new RoleInTestQuery();
        List<RoleInTest> roleInTest = roleInTestQuery.getListDistinct();
        for (RoleInTest item : roleInTest) {
            DatabaseManager.removeObject(item);
        }
        TestRolesFactory.cleanTestRoles();
    }
}
