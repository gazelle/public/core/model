package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tm.gazelletest.model.definition.*;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Ignore
public class TestQueryRemoveProfiles extends AbstractTestQueryDeleteElements {

    private static Logger log = LoggerFactory.getLogger(TestQueryRemoveProfiles.class);

    public void testRemoveAIPOs() {
        EntityManager em = EntityManagerService.provideEntityManager();
        Object delegate = em.getDelegate();
        if (delegate instanceof Session) {
            Session session = (Session) delegate;
            session.doWork(new Work() {

                @Override
                public void execute(Connection connection) throws SQLException {
                    removeProfilesFromMetaTests(connection);
                    removeProfilesFromTests(connection);
                }
            });
        }
    }

    public void removeProfilesFromMetaTests(Connection connection) {
        Set<Integer> metaTestsToKeep = new HashSet<Integer>();
        Set<Integer> metaTestsToDelete = new HashSet<Integer>();

        Set<Integer> testRolesToDelete = new HashSet<Integer>();

        getMetaTestsForIP("SWF", metaTestsToKeep);
        getMetaTestsForIP("PIR", metaTestsToKeep);
        HQLQueryBuilder<MetaTest> builder = new HQLQueryBuilder<MetaTest>(MetaTest.class);
        List<MetaTest> tests = builder.getList();
        for (MetaTest test : tests) {
            if (!metaTestsToKeep.contains(test.getId())) {
                metaTestsToDelete.add(test.getId());
                List<TestRoles> testRolesListDirect = test.getTestRolesList();
                for (TestRoles testRoles : testRolesListDirect) {
                    testRolesToDelete.add(testRoles.getId());
                }
            }
        }

        deleteElements(connection, metaTestsToDelete, "tm_meta_test", "id");
    }

    public void removeProfilesFromTests(Connection connection) {
        Set<Integer> testsToKeep = new HashSet<Integer>();
        Set<Integer> testsToDelete = new HashSet<Integer>();

        Set<Integer> testDescriptionsToDelete = new HashSet<Integer>();
        Set<Integer> testStepsToDelete = new HashSet<Integer>();
        Set<Integer> userCommentsToDelete = new HashSet<Integer>();

        getTestsForIP("SWF", testsToKeep);
        getTestsForIP("PIR", testsToKeep);
        HQLQueryBuilder<Test> builder = new HQLQueryBuilder<Test>(Test.class);
        List<Test> tests = builder.getList();
        for (Test test : tests) {
            if (!testsToKeep.contains(test.getId())) {
                testsToDelete.add(test.getId());

                List<TestDescription> testDescription = test.getTestDescription();
                for (TestDescription testDescription2 : testDescription) {
                    testDescriptionsToDelete.add(testDescription2.getId());
                }

                List<TestSteps> testStepsList = test.getTestStepsList();
                for (TestSteps testSteps : testStepsList) {
                    testStepsToDelete.add(testSteps.getId());
                }

                List<UserComment> userCommentList = test.getUserCommentList();
                for (UserComment userComment : userCommentList) {
                    userCommentsToDelete.add(userComment.getId());
                }
            }
        }
        deleteElements(connection, testDescriptionsToDelete, "tm_test_description", "id");
        deleteElements(connection, testStepsToDelete, "tm_test_steps", "id");
        deleteElements(connection, userCommentsToDelete, "tm_user_comment", "id");

        deleteElements(connection, testsToDelete, "tm_test", "id");
    }

    private void getTestsForIP(String ipKeyword, Set<Integer> testsToKeep) {
        HQLQueryBuilder<Test> builder = new HQLQueryBuilder<Test>(Test.class);
        builder.addEq(
                "testRoles.roleInTest.testParticipantsList.actorIntegrationProfileOption.actorIntegrationProfile.integrationProfile.keyword",
                ipKeyword);
        builder.addEq("testStatus.keyword", "ready");
        builder.addIn("testType", TestType.getTestTypesWithoutMESA());
        List<Test> list = builder.getList();
        for (Test test : list) {
            testsToKeep.add(test.getId());
        }
    }

    private void getMetaTestsForIP(String ipKeyword, Set<Integer> metaTestsToKeep) {
        HQLQueryBuilder<MetaTest> builder = new HQLQueryBuilder<MetaTest>(MetaTest.class);
        builder.addEq(
                "testRolesList.roleInTest.testParticipantsList.actorIntegrationProfileOption.actorIntegrationProfile.integrationProfile.keyword",
                ipKeyword);
        builder.addEq("testRolesList.test.testStatus.keyword", "ready");
        builder.addIn("testRolesList.test.testType", TestType.getTestTypesWithoutMESA());
        List<MetaTest> list = builder.getList();
        for (MetaTest metaTest : list) {
            metaTestsToKeep.add(metaTest.getId());
        }
    }
}
