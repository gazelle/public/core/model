package net.ihe.gazelle.tm.gazelletest.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.systems.model.SystemQuery;
import net.ihe.gazelle.users.model.factories.UserFactory;

import java.util.List;

public class SystemFactory {

    public static net.ihe.gazelle.tm.systems.model.System createSystemWithMandatoryFields() {
        net.ihe.gazelle.tm.systems.model.System sys = new net.ihe.gazelle.tm.systems.model.System();
        sys.setName("test_system");
        sys.setVersion("1");
        sys.setOwnerUserId(UserFactory.createUserWithMandatoryFields().getId());
        sys = (net.ihe.gazelle.tm.systems.model.System) DatabaseManager.writeObject(sys);
        return sys;
    }

    public static void cleanSystems() {
        SystemQuery sysQuery = new SystemQuery();
        List<net.ihe.gazelle.tm.systems.model.System> systems = sysQuery.getListDistinct();
        for (net.ihe.gazelle.tm.systems.model.System system : systems) {
            DatabaseManager.removeObject(system);
        }
        UserFactory.cleanUsers();
    }
}
