package net.ihe.gazelle.tm.gazelletest.model.definition.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.factories.ActorIntegrationProfileOptionFactory;
import net.ihe.gazelle.tm.gazelletest.model.definition.RoleInTest;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestParticipants;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestParticipantsQuery;

import java.util.List;

public class TestParticipantsFactory {
    public static TestParticipants createTestParticipantWithMandatoryFields() {
        TestParticipants testParticipants = new TestParticipants();
        testParticipants.setRoleInTest(RoleInTestFactory.createRoleInTestWithMandatoryFields());
        testParticipants.setActorIntegrationProfileOption(ActorIntegrationProfileOptionFactory
                .createActorIntegrationProfileOptionWithIntegrationProfileOption());
        testParticipants.setTested(true);
        testParticipants = (TestParticipants) DatabaseManager.writeObject(testParticipants);
        return testParticipants;
    }

    public static TestParticipants createTestParticipantWithMandatoryFields(List<RoleInTest> roleInTest) {
        TestParticipants testParticipants = new TestParticipants();
        testParticipants.setRoleInTest(roleInTest);
        testParticipants.setActorIntegrationProfileOption(ActorIntegrationProfileOptionFactory
                .createActorIntegrationProfileOptionWithIntegrationProfileOption());
        testParticipants.setTested(true);
        testParticipants = (TestParticipants) DatabaseManager.writeObject(testParticipants);
        return testParticipants;
    }

    public static TestParticipants provideTestParticipantWithMandatoryFields() {
        TestParticipants testParticipants = new TestParticipants();
        testParticipants.setRoleInTest(RoleInTestFactory.createRoleInTestWithMandatoryFields());
        testParticipants.setActorIntegrationProfileOption(ActorIntegrationProfileOptionFactory
                .createActorIntegrationProfileOptionWithIntegrationProfileOption());
        testParticipants.setTested(true);
        return testParticipants;
    }

    public static void cleanTestParticipants() {
        TestParticipantsQuery TestParticipantsQuery = new TestParticipantsQuery();
        List<TestParticipants> testParticipants = TestParticipantsQuery.getListDistinct();
        for (TestParticipants TestParticipants : testParticipants) {
            DatabaseManager.removeObject(TestParticipants);
        }
        RoleInTestFactory.cleanRoleInTest();
    }
}
