package net.ihe.gazelle.tm.gazelletest.domain;

import net.ihe.gazelle.tf.model.Transaction;
import net.ihe.gazelle.tf.model.WSTransactionUsage;
import net.ihe.gazelle.tm.gazelletest.model.definition.ContextualInformation;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRoles;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestStepsOption;
import net.ihe.gazelle.tm.tee.model.TmTestStepMessageProfile;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class TestStepDomainTest {

    private TestStepDomain testStepDomain;

    @Before
    public void setUp() throws Exception {
        testStepDomain = new TestStepDomain();
        testStepDomain.setId(1);
        testStepDomain.setTestRolesInitiator(new TestRoles());
        testStepDomain.setTestRolesResponder(new TestRoles());
        testStepDomain.setTransaction(new Transaction());
        testStepDomain.setWstransactionUsage(new WSTransactionUsage());
        testStepDomain.setStepIndex(10);
        testStepDomain.setTestStepsOption(new TestStepsOption());
        testStepDomain.setDescription("description");
        testStepDomain.setMessageType("testMessageType");
        testStepDomain.setResponderMessageType("testResponderMessageType");
        testStepDomain.setHl7Version("testHl7Version");
        testStepDomain.setSecured(false);
        testStepDomain.setAutoComplete(false);
        testStepDomain.setAutoTriggered(false);
        testStepDomain.setExpectedMessageCount(1);
        testStepDomain.setInputContextualInformationList(new ArrayList<ContextualInformation>());
        testStepDomain.setOutputContextualInformationList(new ArrayList<ContextualInformation>());
        testStepDomain.setTestParent(new net.ihe.gazelle.tm.gazelletest.model.definition.Test());
        testStepDomain.setTmTestStepMessageProfiles(new ArrayList<TmTestStepMessageProfile>());
        testStepDomain.setLastChanged(new Date());
        testStepDomain.setLastModifierId("testLastModifier");
        testStepDomain.setType(TestStepType.AUTOMATED);
        testStepDomain.setTestScriptID("testScriptID");
        testStepDomain.setInputs(new ArrayList<AutomatedTestStepInputDomain>());
    }

    @Test
    public void constructorTest() {
        TestStepDomain copy = new TestStepDomain(testStepDomain);
        assertEquals(testStepDomain, copy);
    }
}
