package net.ihe.gazelle.tm.systems.model;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SystemInSessionRegistrationStatusTest {

    @Test
    public void getPossibleRegistrationStatusBeforeDeadLine() {

        SystemInSessionRegistrationStatus[] possibleRegistrationStatusBeforeDeadLine = SystemInSessionRegistrationStatus
                .getPossibleRegistrationStatusBeforeDeadLine();
        SystemInSessionRegistrationStatus[] expected = {SystemInSessionRegistrationStatus.IN_PROGRESS,
                SystemInSessionRegistrationStatus.COMPLETED};
        assertArrayEquals(expected, possibleRegistrationStatusBeforeDeadLine);
    }

    @Test
    public void getPossibleRegistrationStatusAfterDeadLine() {
        SystemInSessionRegistrationStatus[] possibleRegistrationStatusAfterDeadLine = SystemInSessionRegistrationStatus
                .getPossibleRegistrationStatusAfterDeadLine();
        SystemInSessionRegistrationStatus[] expected = {SystemInSessionRegistrationStatus.COMPLETED,
                SystemInSessionRegistrationStatus.DROPPED};
        assertArrayEquals(expected, possibleRegistrationStatusAfterDeadLine);
    }

}
