package net.ihe.gazelle.tm.gazelletest.model.definition;

import org.junit.Test;

import static org.junit.Assert.*;

public class AutomatedTestStepInputTest {

    @Test
    public void automatedTestStepInputTest() {
        AutomatedTestStepEntity entity = new AutomatedTestStepEntity();
        AutomatedTestStepInput input = new AutomatedTestStepInput("key", "label", "description", "type", true, entity);
        assertEquals("key", input.getKey());
        assertEquals("label", input.getLabel());
        assertEquals("description", input.getDescription());
        assertEquals("type", input.getType());
        assertTrue(input.getRequired());

        input.setKey("new key");
        input.setLabel("new label");
        input.setDescription("new description");
        input.setType("new type");
        input.setRequired(false);

        assertEquals("new key", input.getKey());
        assertEquals("new label", input.getLabel());
        assertEquals("new description", input.getDescription());
        assertEquals("new type", input.getType());
        assertFalse(input.getRequired());
    }
}
