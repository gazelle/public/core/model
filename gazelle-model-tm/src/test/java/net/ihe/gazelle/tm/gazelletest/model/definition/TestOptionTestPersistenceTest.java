package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.hql.providers.detached.AbstractEntityManagerProvider;
import org.jboss.seam.contexts.Contexts;

public class TestOptionTestPersistenceTest extends AbstractEntityManagerProvider {
    @Override
    public String getHibernateConfigPath() {
        return "META-INF/hibernate.test.cfg.xml";
    }

    @Override
    public Integer getWeight() {
        if (Contexts.isApplicationContextActive()) {
            return 100;
        } else {
            return -100;
        }
    }
}
