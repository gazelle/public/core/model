package net.ihe.gazelle.tm.gazelletest.model.instance;


import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

public class TestInstanceTokenTest {

    private static final String TOKEN_VALUE = "fb6a3a0c-7390-41c8-b11a-1ba796097d77";
    private static final Integer TEST_STEP_INSTANCE_ID = 1;
    private static final String USER_NAME = "toto";
    private static final Timestamp EXPIRATION_TIMESTAMP = new Timestamp(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(1));

    /**
     * Create a valid Test Instance Token
     */
    @Test
    public void createValidTestInstanceToken(){
        TestInstanceToken testInstanceToken = new TestInstanceToken(TOKEN_VALUE, USER_NAME, TEST_STEP_INSTANCE_ID, EXPIRATION_TIMESTAMP);
        Assert.assertEquals("TokenValue shall be : " +TOKEN_VALUE, TOKEN_VALUE ,testInstanceToken.getValue() );
        Assert.assertEquals("userName shall be : " +USER_NAME, USER_NAME,testInstanceToken.getUserId() );
        Assert.assertEquals("Expiration_TimeStamp shall be : " + EXPIRATION_TIMESTAMP, EXPIRATION_TIMESTAMP, testInstanceToken.getExpirationDateTime());
    }

    /**
     * Create A TestInstanceToken without mandatory parameter : UserName
     */
    @Test(expected = IllegalArgumentException.class)
    public void createInvalidTestInstanceTokenMissingUserName(){
        TestInstanceToken testInstanceToken = new TestInstanceToken(TOKEN_VALUE, null, TEST_STEP_INSTANCE_ID, EXPIRATION_TIMESTAMP);
    }

    /**
     * Create A TestInstanceToken without mandatory parameter : UserName
     */
    @Test(expected = IllegalArgumentException.class)
    public void createInvalidTestInstanceTokenEmptyUserName(){
        TestInstanceToken testInstanceToken = new TestInstanceToken(TOKEN_VALUE, "", TEST_STEP_INSTANCE_ID, EXPIRATION_TIMESTAMP);
    }


    /**
     * Create A TestInstanceToken without mandatory parameter : TokenValue
     */
    @Test(expected = IllegalArgumentException.class)
    public void createInvalidTestInstanceTokenMissingTokenValue(){
        TestInstanceToken testInstanceToken = new TestInstanceToken(null, USER_NAME, TEST_STEP_INSTANCE_ID, EXPIRATION_TIMESTAMP);
    }

    /**
     * Create A TestInstanceToken with empty mandatory parameter : TokenValue
     */
    @Test(expected = IllegalArgumentException.class)
    public void createInvalidTestInstanceTokenEmptyTokenValue(){
        TestInstanceToken testInstanceToken = new TestInstanceToken("", USER_NAME, TEST_STEP_INSTANCE_ID, EXPIRATION_TIMESTAMP);
    }

    /**
     * Create A TestInstanceToken without mandatory parameter : creationDate
     */
    @Test(expected = IllegalArgumentException.class)
    public void createInvalidTestInstanceTokenMissingCreationDate(){
        TestInstanceToken testInstanceToken = new TestInstanceToken(TOKEN_VALUE, USER_NAME, TEST_STEP_INSTANCE_ID, null);
    }


    /**
     * Create A TestInstanceToken without mandatory parameter : creationDate
     */
    @Test(expected = IllegalArgumentException.class)
    public void createInvalidTestInstanceTokenWithExpiredCreationDate(){
        TestInstanceToken testInstanceToken = new TestInstanceToken(TOKEN_VALUE, USER_NAME, TEST_STEP_INSTANCE_ID, new Timestamp(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(9)));
    }




}