package net.ihe.gazelle.tm.gazelletest.model.definition.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.gazelletest.model.definition.Test;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRoles;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestRolesQuery;

import java.util.ArrayList;
import java.util.List;

public class TestRolesFactory {

    public static List<TestRoles> createTestRolesWithMandatoryFields() {
        TestRoles testRoles = new TestRoles();
        List<TestRoles> list = new ArrayList<TestRoles>();
        testRoles.setTest(TestFactory.createConnectathonTest());
        testRoles.setNumberOfTestsToRealize(2);
        testRoles.setTestOption(TestOptionFactory.createTestOptionWithMandatoryFields());
        testRoles = (TestRoles) DatabaseManager.writeObject(testRoles);
        list.add(testRoles);
        return list;
    }

    public static List<TestRoles> createTestRolesWithMandatoryFields(Test test) {
        TestRoles testRoles = new TestRoles();
        List<TestRoles> list = new ArrayList<TestRoles>();
        testRoles.setTest(test);
        testRoles.setNumberOfTestsToRealize(2);
        testRoles.setTestOption(TestOptionFactory.createTestOptionWithMandatoryFields());
        testRoles = (TestRoles) DatabaseManager.writeObject(testRoles);
        list.add(testRoles);
        return list;
    }

    public static void cleanTestRoles() {
        TestRolesQuery testRolesQuery = new TestRolesQuery();
        List<TestRoles> testRoles = testRolesQuery.getListDistinct();
        for (TestRoles item : testRoles) {
            DatabaseManager.removeObject(item);
        }
        TestFactory.cleanTest();
        TestOptionFactory.cleanTestOption();
    }

    public static List<TestRoles> provideTestRolesWithMandatoryFields() {
        TestRoles testRoles = new TestRoles();
        List<TestRoles> list = new ArrayList<TestRoles>();
        testRoles.setTest(TestFactory.provideConnectathonTest());
        testRoles.setNumberOfTestsToRealize(2);
        testRoles.setTestOption(TestOptionFactory.provideTestOptionWithMandatoryFields());
        list.add(testRoles);
        return list;
    }
}
