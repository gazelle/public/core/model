package net.ihe.gazelle.tm.gazelletest.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.systems.model.TestingSessionAdmin;

public class TestingSessionAdminFactory {

    public static TestingSessionAdmin createTestingSessionAdminWithMandatoryFields(Integer testingSessionId, String userId) {
        TestingSessionAdmin testingSessionAdmin = provideTestingSessionAdminWithMandatoryFields(testingSessionId, userId);
        testingSessionAdmin = (TestingSessionAdmin) DatabaseManager.writeObject(testingSessionAdmin);
        return testingSessionAdmin;
    }

    public static TestingSessionAdmin createTestingSessionAdminWithAllFields(Integer testingSessionId, String userId) {
        TestingSessionAdmin testingSessionAdmin = new TestingSessionAdmin();
        testingSessionAdmin.setTestingSessionId(testingSessionId);
        testingSessionAdmin.setUserId(userId);
        return testingSessionAdmin;
    }

    public static TestingSessionAdmin provideTestingSessionAdminWithMandatoryFields(Integer testingSessionId, String userId) {
        TestingSessionAdmin testingSessionAdmin = new TestingSessionAdmin();
        testingSessionAdmin.setTestingSessionId(testingSessionId);
        testingSessionAdmin.setUserId(userId);
        return testingSessionAdmin;
    }
}