package net.ihe.gazelle.tm.utils;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@Ignore
@SuiteClasses({AbstractTestQueryDeleteElements.class, TestQueryCreateCertificates.class, TestQueryDDL.class,
        TestQueryDeleteAIPOs.class, TestQueryDevMode.class, TestQueryFixForeignKeys.class,
        TestQueryFixForeignKeys2.class, TestQueryFixTestInstanceMonitors.class, TestQueryIdsAndUnique.class,
        TestQueryQuery.class, TestQueryRemoveProfiles.class, TestQueryUnusedTables.class, TestQueryVersionInit.class})
public class AllTests {

}
