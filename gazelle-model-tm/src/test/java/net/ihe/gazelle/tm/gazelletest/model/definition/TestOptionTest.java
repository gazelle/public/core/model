package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.junit.DatabaseManager;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestOptionTest {
    @BeforeClass
    public static void init(){
        HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.set(EntityManagerService.provideEntityManager());
    }
    @Ignore
    @Test
    public void importSaveNewTestOption() {
        TestOption to = new TestOption("required", "Required", "required test option");
        to.setAvailable(true);
        EntityManagerService.provideEntityManager().getTransaction().begin();
        to = to.saveOrMerge(EntityManagerService.provideEntityManager());
        EntityManagerService.provideEntityManager().getTransaction().commit();
        assertNotNull(to.getId());

        TestOptionQuery toq = new TestOptionQuery();
        toq.keyword().eq("required");
        TestOption testOption = toq.getUniqueResult();

        assertEquals("Required", testOption.getLabelToDisplay());
        assertNotNull(testOption.isAvailable());
        assertTrue(testOption.isAvailable());
    }
    @Ignore
    @Test
    public void importMustNotOverriteExistingTestOption() {
        TestOption to = new TestOption("required", "Required", "required test option");
        to.setAvailable(true);

        to = (TestOption) DatabaseManager.writeObject(to);

        TestOption to2 = new TestOption("required", "Required", "required test option");
        to2.setAvailable(null);
        EntityManagerService.provideEntityManager().getTransaction().begin();
        to2.saveOrMerge(EntityManagerService.provideEntityManager());
        EntityManagerService.provideEntityManager().getTransaction().commit();

        TestOptionQuery toq = new TestOptionQuery();
        toq.keyword().eq("required");
        TestOption testOption = toq.getUniqueResult();

        assertEquals("Required", testOption.getLabelToDisplay());
        assertNotNull(testOption.isAvailable());
        assertTrue(testOption.isAvailable());
    }
}
