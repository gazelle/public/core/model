package net.ihe.gazelle.tm.gazelletest.model.definition.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestOption;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestOptionQuery;

import java.util.List;

public class TestOptionFactory {

    private static int number;

    public static TestOption createTestOptionWithMandatoryFields() {
        TestOption testOption = provideTestOptionWithMandatoryFields();
        testOption = (TestOption) DatabaseManager.writeObject(testOption);
        return testOption;
    }

    public static void cleanTestOption() {
        TestOptionQuery testOptionQuery = new TestOptionQuery();
        List<TestOption> testOption = testOptionQuery.getListDistinct();
        for (TestOption testOption_ : testOption) {
            DatabaseManager.removeObject(testOption_);
        }
    }

    public static TestOption provideTestOptionWithMandatoryFields() {
        TestOption testOption = new TestOption();
        testOption.setKeyword("option" + number);
        number++;
        return testOption;
    }
}
