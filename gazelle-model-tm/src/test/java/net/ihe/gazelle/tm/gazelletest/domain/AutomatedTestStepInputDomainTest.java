package net.ihe.gazelle.tm.gazelletest.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AutomatedTestStepInputDomainTest {

    @Test
    public void test_automated_test_step_input_domain() {
        AutomatedTestStepInputDomain domain1 = new AutomatedTestStepInputDomain("key", "label", "description", "type", true);
        domain1.setId(1);
        AutomatedTestStepInputDomain domain2 = new AutomatedTestStepInputDomain();
        domain2.setId(1);
        domain2.setKey("key");
        domain2.setLabel("label");
        domain2.setDescription("description");
        domain2.setType("type");
        domain2.setRequired(true);
        assertEquals(domain1, domain2);
    }
}
