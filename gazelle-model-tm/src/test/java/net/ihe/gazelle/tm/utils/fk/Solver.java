package net.ihe.gazelle.tm.utils.fk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public abstract class Solver {

    protected static Logger logger = LoggerFactory.getLogger(Solver.class);

    protected String lastSolvedQuery = "";

    protected List<String> queriesOK;

    private boolean solved;

    public Solver(List<String> queriesOK) {
        this.queriesOK = queriesOK;
    }

    public boolean patchRecursively(String padding, String query, Constraint constraint, Connection connection,
                                    String solvedQuery) {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            try {
                System.out.println(" " + query);
                // Try to execute the update
                statement.executeUpdate(query);
            } catch (SQLException e) {
                String newError = e.getMessage();
                System.out.println(" " + newError);
                // If failure, trying to delete lines with the missing id
                if (processMissing(padding, newError, query, constraint, connection)) {
                    // If done, retry the first query
                    return patchRecursively(" " + padding, query, constraint, connection, query);
                } else {
                    return false;
                }
            }
            if (!query.equals(solvedQuery) && !solvedQuery.equals(lastSolvedQuery)) {
                queriesOK.add("-- solving " + solvedQuery);
                lastSolvedQuery = solvedQuery;
            }
            queriesOK.add(query);
            System.out.println(" OK");
            statement.close();
            return true;
        } catch (SQLException e) {
            logger.error("", e);
            return false;
        }
    }

    public abstract boolean processMissing(String padding, String message, String query, Constraint constraint,
                                           Connection connection) throws SQLException;

    public abstract String getLabel();

    public void fixConstraints(List<Constraint> constraints, Connection connection) {
        for (Constraint constraint : constraints) {
            String query = "alter table " + constraint.table + " add constraint " + constraint.id + " foreign key ("
                    + constraint.remote_id + ") references " + constraint.remote_table;

            queriesOK.add("-- SOLVING " + query);
            patchRecursively("", query, constraint, connection, query + " ");
        }
    }

    public void fixConstraint(Constraint constraint, Connection connection) {
        String query = "alter table " + constraint.table + " add constraint " + constraint.id + " foreign key ("
                + constraint.remote_id + ") references " + constraint.remote_table;

        queriesOK.add("-- SOLVING " + query);
        solved = patchRecursively("", query, constraint, connection, query + " ");
    }

    public boolean isSolved() {
        return solved;
    }

}
