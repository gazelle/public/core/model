package net.ihe.gazelle.tm.gazelletest.model.definition;

import net.ihe.gazelle.tm.gazelletest.domain.AutomatedTestStepInputDomain;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepDomain;
import net.ihe.gazelle.tm.gazelletest.domain.TestStepType;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AutomatedTestStepEntityTest {

    @Test
    public void testAutomatedTestStepEntity() {
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity();
        automatedTestStepEntity.setTestScriptID("testScriptID");
        List<AutomatedTestStepInput> automatedTestStepInputs = new ArrayList<>();
        automatedTestStepInputs.add(new AutomatedTestStepInput());
        automatedTestStepEntity.setInputs(automatedTestStepInputs);

        assertEquals("testScriptID", automatedTestStepEntity.getTestScriptID());
        assertEquals(1, automatedTestStepEntity.getInputs().size());
    }

    @Test
    public void fromDomainTest() {
        TestStepDomain testStepDomain = new TestStepDomain();
        testStepDomain.setType(TestStepType.AUTOMATED);
        testStepDomain.setTestScriptID("testScriptID");
        testStepDomain.setInputs(new ArrayList<AutomatedTestStepInputDomain>());
        AutomatedTestStepEntity automatedTestStepEntity = new AutomatedTestStepEntity(testStepDomain);

        assertEquals("testScriptID", automatedTestStepEntity.getTestScriptID());
        assertEquals(0, automatedTestStepEntity.getInputs().size());

        TestStepDomain newDomain = automatedTestStepEntity.asDomain();
        assertEquals("testScriptID", newDomain.getTestScriptID());
        assertEquals(0, newDomain.getInputs().size());
    }
}
