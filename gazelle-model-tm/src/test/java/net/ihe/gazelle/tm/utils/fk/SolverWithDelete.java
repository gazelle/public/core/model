package net.ihe.gazelle.tm.utils.fk;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class SolverWithDelete extends Solver {

    public SolverWithDelete(List<String> queriesOK) {
        super(queriesOK);
    }

    public boolean processMissing(String padding, String message, String query, Constraint constraint,
                                  Connection connection) throws SQLException {
        // Détail : La clé (test_id)=(217) n'est pas présente dans la table
        // tm_test .

        // Building the query to reinsert the data
        String finalQuery = "";
        try {
            if (query.startsWith("alter table")) {
                int start = message.indexOf('(');
                message = message.substring(start + 1);
                start = message.indexOf('(');
                message = message.substring(start + 1);
                String id = message.substring(0, message.indexOf(')'));
                start = message.indexOf('\u00AB');
                message = message.substring(start + 2);
                String table = message.substring(0, message.indexOf('\u00BB') - 1);

                finalQuery = "DELETE FROM " + constraint.table + " WHERE " + constraint.remote_id + "=" + id + ";";
            } else {
                /*
				 ERREUR: UPDATE ou DELETE sur la table « tm_contextual_information_instance » viole la contrainte de clé étrangère
				« fkd21c0f0e7b906045 » de la table « tm_test_steps_instance_input_ci_instance »
				  Détail : La clé (id)=(1) est toujours référencée à partir de la table « tm_test_steps_instance_input_ci_instance ».
				 */
                String id = message.substring(message.indexOf(')') + 1);
                id = id.substring(id.indexOf('(') + 1, id.indexOf(')'));

                message = message.substring(message.indexOf('«') + 1);
                message = message.substring(message.indexOf('«') + 1);
                String fk = message.substring(1, message.indexOf('»') - 1);
                message = message.substring(message.indexOf('«') + 1);
                message = message.substring(message.indexOf('«') + 1);
                String table = message.substring(1, message.indexOf('»') - 1);

                String query2 = "	SELECT"
                        + "    tc.constraint_name, tc.table_name, kcu.column_name, "
                        + "    ccu.table_name AS foreign_table_name,"
                        + "    ccu.column_name AS foreign_column_name "
                        + " FROM "
                        + "    information_schema.table_constraints AS tc "
                        + "    JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name"
                        + "    JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name"
                        + " WHERE constraint_type = 'FOREIGN KEY' and tc.constraint_name='" + fk + "';";

                Statement statement = null;
                statement = connection.createStatement();
                ResultSet result = statement.executeQuery(query2);
                result.next();
                String table1 = result.getString(2);
                String key1 = result.getString(3);
                String key2 = result.getString(4);

                String deleteKey;
                if (table.equals(table1)) {
                    deleteKey = key1;
                } else {
                    deleteKey = key2;
                }

                finalQuery = "DELETE FROM " + table + " WHERE " + deleteKey + "=" + id + ";";
            }
        } catch (Throwable e) {
            logger.error(padding + "Failed to extract informations...", e);
            return false;
        }

        // trying to apply the update
        return patchRecursively(" " + padding, finalQuery, constraint, connection, query);
    }

    @Override
    public String getLabel() {
        return "Try to delete objects referring deleted items";
    }

}