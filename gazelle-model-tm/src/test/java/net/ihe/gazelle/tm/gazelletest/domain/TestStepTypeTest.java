package net.ihe.gazelle.tm.gazelletest.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestStepTypeTest {

    @Test
    public void automatedTypeTest() {
        assertEquals("net.ihe.gazelle.tm.automatedStep", TestStepType.AUTOMATED.getKey());
        assertEquals(TestStepType.AUTOMATED, TestStepType.fromKey("net.ihe.gazelle.tm.automatedStep"));
    }

    @Test
    public void basicTypeTest() {
        assertEquals("net.ihe.gazelle.tm.basicTestStep", TestStepType.BASIC.getKey());
        assertEquals(TestStepType.BASIC, TestStepType.fromKey("net.ihe.gazelle.tm.basicTestStep"));
    }
}
