package net.ihe.gazelle.tm.gazelletest.model.definition.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestSteps;
import net.ihe.gazelle.tm.gazelletest.model.definition.TestStepsQuery;

import java.util.List;

public class TestStepsFactory {

    private static Integer number = 0;

    public static TestSteps createTestStepWithMandatoryFields() {
        TestSteps testStep = new TestSteps();
        testStep.setTestRolesInitiator(TestRolesFactory.createTestRolesWithMandatoryFields().get(0));
        testStep.setStepIndex(number);
        number++;
        testStep.setDescription("My super step description" + number);
        testStep = (TestSteps) DatabaseManager.writeObject(testStep);
        return testStep;
    }

    public static void cleanTestStep() {
        TestStepsQuery testStepQuery = new TestStepsQuery();
        List<TestSteps> testSteps = testStepQuery.getListDistinct();
        for (TestSteps testStep : testSteps) {
            DatabaseManager.removeObject(testStep);
        }
        TestRolesFactory.cleanTestRoles();
    }
}
