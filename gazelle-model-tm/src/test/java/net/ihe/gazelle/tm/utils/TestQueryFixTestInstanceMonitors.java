package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.tm.gazelletest.model.instance.MonitorInSession;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstance;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstanceQuery;
import org.junit.Ignore;

import javax.persistence.EntityManager;
import java.util.List;

@Ignore
public class TestQueryFixTestInstanceMonitors {

    public void testFixTestInstanceMonitors() {
        TestInstanceQuery testInstanceQuery = new TestInstanceQuery();
        List<TestInstance> testInstances = testInstanceQuery.getList();
        EntityManager em = EntityManagerService.provideEntityManager();
        em.getTransaction().begin();
        int count = 0;
        for (TestInstance testInstance : testInstances) {
            MonitorInSession prevMonitor = testInstance.getMonitorInSession();
            if (prevMonitor != null) {
                testInstance.setMonitorInSession(prevMonitor);
                if (!prevMonitor.getId().equals(testInstance.getMonitorInSession().getId())) {
                    em.merge(testInstance);
                    count++;
                }
            }
        }
        em.flush();
        em.getTransaction().commit();
        System.out.println(count);
    }

}
