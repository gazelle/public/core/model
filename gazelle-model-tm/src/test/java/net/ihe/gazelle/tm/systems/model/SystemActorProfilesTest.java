package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.List;

@Ignore
public class SystemActorProfilesTest {

    private static Logger log = LoggerFactory.getLogger(SystemActorProfilesTest.class);

    @Test
    public void getSystemActorProfilesFiltered() {
        EntityManager em = EntityManagerService.provideEntityManager();
        System s = System.getSystemForGivenKeywordSuffix("15");

        SystemActorProfilesQuery query = new SystemActorProfilesQuery();
        query.system().eqIfValueNotNull(s);
        List<SystemActorProfiles> res2 =query.getList();
        for (SystemActorProfiles sap : res2){

        }
    }
}
