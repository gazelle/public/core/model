package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.tm.systems.model.InstitutionSystem;
import net.ihe.gazelle.tm.systems.model.SystemInSession;
import net.ihe.gazelle.users.model.Address;
import net.ihe.gazelle.users.model.Institution;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
@Ignore
public class TestQueryCreateCertificates extends AbstractTestQueryJunit4 {

    private static final String PKI_HOST = "gazelle.ihe.net";
    private static final int PKI_PORT = 80;
    private static Logger logger = LoggerFactory.getLogger(TestQueryCreateCertificates.class);

    // private static final String PKI_HOST = "127.0.0.1";
    // private static final int PKI_PORT = 8080;

    @Override
    public String getDb() {
        return "gazelle-junit";
    }

    public void testCreateCertificates() {

        HQLQueryBuilder<SystemInSession> builder = new HQLQueryBuilder<SystemInSession>(SystemInSession.class);
        builder.addEq("testingSession.id", 25);
        List<SystemInSession> list = builder.getList();
        for (SystemInSession systemInSession : list) {
            String system = systemInSession.getSystem().getKeyword();
            String institution = "IHE";
            String owner = systemInSession.getSystem().getOwnerUserId();
            String country = "CH";

            Set<InstitutionSystem> institutionSystems = systemInSession.getSystem().getInstitutionSystems();
            for (InstitutionSystem institutionSystem : institutionSystems) {
                Institution organisation = institutionSystem.getInstitution();
                institution = organisation.getKeyword();
                List<Address> addresses = organisation.getAddresses();
                for (Address address : addresses) {
                    country = address.getIso3166CountryCode().getIso();
                }
            }

            System.out.println(system + "," + institution + "," + owner + "," + country);
        }
    }
}
