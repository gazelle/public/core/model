package net.ihe.gazelle.tm.gazelletest.model.definition;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestTest {
    net.ihe.gazelle.tm.gazelletest.model.definition.Test test;

    @Before
    public void setup() {
        test = new net.ihe.gazelle.tm.gazelletest.model.definition.Test();
    }

    @Test
    public void testWithoutTestStepReturnNoAssertions() {
        assertEquals(new ArrayList<String>(), test.getAssertionsFromTestSteps());
    }

    @Test
    @Ignore
    public void testReturnAnAssertionFromTestStep() {
        TestSteps testStep = new TestSteps();
        testStep.setDescription("[aaa-10]");
        List<TestSteps> testSteps = new ArrayList<TestSteps>();
        testSteps.add(testStep);
        test.setTestStepsList(testSteps);

        List<AssertionStatus> assertions = new ArrayList<AssertionStatus>();
        assertions.add(new AssertionStatus("aaa-10", 1));
        assertEquals(assertions, test.getAssertionsFromTestSteps());
    }
}
