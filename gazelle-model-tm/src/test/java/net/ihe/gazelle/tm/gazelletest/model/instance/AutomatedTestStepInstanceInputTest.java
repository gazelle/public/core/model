package net.ihe.gazelle.tm.gazelletest.model.instance;

import net.ihe.gazelle.tm.gazelletest.model.definition.AutomatedTestStepInput;
import org.junit.Test;

import static org.junit.Assert.*;

public class AutomatedTestStepInstanceInputTest {

    @Test
    public void automatedTestStepInstanceInputTest() {
        AutomatedTestStepInput inputDefinition = new AutomatedTestStepInput();
        inputDefinition.setLabel("label");
        inputDefinition.setDescription("description");
        inputDefinition.setType("file");
        inputDefinition.setRequired(true);

        AutomatedTestStepInstanceInput inputInstance = new AutomatedTestStepInstanceInput(10, 50, inputDefinition);
        inputInstance.setTestStepsInstanceId(10);
        inputInstance.setTestStepDataId(50);

        assertEquals(Integer.valueOf(10), inputInstance.getTestStepsInstanceId());
        assertEquals(Integer.valueOf(50), inputInstance.getTestStepDataId());
        assertEquals("label", inputInstance.getLabel());
        assertEquals("description", inputInstance.getDescription());
        assertEquals("file", inputInstance.getType());
        assertTrue(inputInstance.getRequired());

        inputInstance.setTestStepsInstanceId(20);
        inputInstance.setTestStepDataId(60);
        inputInstance.setLabel("new label");
        inputInstance.setDescription("new description");
        inputInstance.setType("new file");
        inputInstance.setRequired(false);

        assertEquals(Integer.valueOf(20), inputInstance.getTestStepsInstanceId());
        assertEquals(Integer.valueOf(60), inputInstance.getTestStepDataId());
        assertEquals("new label", inputInstance.getLabel());
        assertEquals("new description", inputInstance.getDescription());
        assertEquals("new file", inputInstance.getType());
        assertFalse(inputInstance.getRequired());
    }
}
