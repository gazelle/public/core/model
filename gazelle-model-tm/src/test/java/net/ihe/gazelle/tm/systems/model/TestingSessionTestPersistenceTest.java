package net.ihe.gazelle.tm.systems.model;

import net.ihe.gazelle.hql.providers.detached.AbstractEntityManagerProvider;
import org.jboss.seam.contexts.Contexts;

public class TestingSessionTestPersistenceTest extends AbstractEntityManagerProvider {
    @Override
    public String getHibernateConfigPath() {
        return "META-INF/hibernate.test.cfg.xml";
    }

    @Override
    public Integer getWeight() {
        if (Contexts.isApplicationContextActive()) {
            return 100;
        } else {
            return -100;
        }
    }
}
