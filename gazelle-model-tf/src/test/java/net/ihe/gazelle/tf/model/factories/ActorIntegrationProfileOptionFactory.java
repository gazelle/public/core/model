package net.ihe.gazelle.tf.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileOptionQuery;

import java.util.List;

public class ActorIntegrationProfileOptionFactory {

    public static ActorIntegrationProfileOption createActorIntegrationProfileOptionWithMandatoryFields() {
        ActorIntegrationProfileOption ActorIntegrationProfileOption = new ActorIntegrationProfileOption();
        ActorIntegrationProfileOption.setActorIntegrationProfile(ActorIntegrationProfileFactory
                .createActorIntegrationProfileWithMandatoryFields());
        ActorIntegrationProfileOption = (ActorIntegrationProfileOption) DatabaseManager
                .writeObject(ActorIntegrationProfileOption);
        return ActorIntegrationProfileOption;
    }

    public static ActorIntegrationProfileOption createActorIntegrationProfileOptionWithIntegrationProfileOption() {
        ActorIntegrationProfileOption actorIntegrationProfileOption = new ActorIntegrationProfileOption();
        actorIntegrationProfileOption.setActorIntegrationProfile(ActorIntegrationProfileFactory
                .createActorIntegrationProfileWithMandatoryFields());
        actorIntegrationProfileOption.setIntegrationProfileOption(IntegrationProfileOptionFactory
                .createIntegrationProfileOptionWithMandatoryFields());
        actorIntegrationProfileOption = (ActorIntegrationProfileOption) DatabaseManager
                .writeObject(actorIntegrationProfileOption);
        return actorIntegrationProfileOption;
    }

    public static void cleanActorIntegrationProfileOptions() {
        ActorIntegrationProfileOptionQuery ActorIntegrationProfileOptionQuery = new ActorIntegrationProfileOptionQuery();
        List<ActorIntegrationProfileOption> ActorIntegrationProfileOptions = ActorIntegrationProfileOptionQuery
                .getListDistinct();
        for (ActorIntegrationProfileOption ActorIntegrationProfileOption : ActorIntegrationProfileOptions) {
            DatabaseManager.removeObject(ActorIntegrationProfileOption);
        }
        ActorIntegrationProfileFactory.cleanActorIntegrationProfiles();
    }
}
