package net.ihe.gazelle.tf.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.ActorQuery;

import java.util.List;

public class ActorFactory {

    private static int number = 0;

    public static Actor createActorWithMandatoryFields() {
        Actor Actor = new Actor();
        Actor.setKeyword("ACTOR" + number);
        number++;
        Actor.setName("test actor");
        Actor = (Actor) DatabaseManager.writeObject(Actor);
        return Actor;
    }

    public static void cleanActors() {
        ActorQuery ActorQuery = new ActorQuery();
        List<Actor> Actors = ActorQuery.getListDistinct();
        for (Actor Actor : Actors) {
            DatabaseManager.removeObject(Actor);
        }
    }
}
