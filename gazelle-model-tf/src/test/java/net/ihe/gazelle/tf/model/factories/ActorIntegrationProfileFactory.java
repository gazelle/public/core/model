package net.ihe.gazelle.tf.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.ActorIntegrationProfile;
import net.ihe.gazelle.tf.model.ActorIntegrationProfileQuery;

import java.util.List;

public class ActorIntegrationProfileFactory {
    public static ActorIntegrationProfile createActorIntegrationProfileWithMandatoryFields() {
        ActorIntegrationProfile ActorIntegrationProfile = new ActorIntegrationProfile();
        ActorIntegrationProfile.setActor(ActorFactory.createActorWithMandatoryFields());
        ActorIntegrationProfile.setIntegrationProfile(IntegrationProfileFactory
                .createIntegrationProfileWithMandatoryFields());
        ActorIntegrationProfile = (ActorIntegrationProfile) DatabaseManager.writeObject(ActorIntegrationProfile);
        return ActorIntegrationProfile;
    }

    public static void cleanActorIntegrationProfiles() {
        ActorIntegrationProfileQuery ActorIntegrationProfileQuery = new ActorIntegrationProfileQuery();
        List<ActorIntegrationProfile> ActorIntegrationProfiles = ActorIntegrationProfileQuery.getListDistinct();
        for (ActorIntegrationProfile ActorIntegrationProfile : ActorIntegrationProfiles) {
            DatabaseManager.removeObject(ActorIntegrationProfile);
        }
        ActorFactory.cleanActors();
    }
}
