package net.ihe.gazelle.tf.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.DomainQuery;

import java.util.List;

public class DomainFactory {
    public static Domain createDomainWithMandatoryFields() {
        Domain domain = new Domain();
        domain.setKeyword("DOM");
        domain.setName("NAME");
        domain = (Domain) DatabaseManager.writeObject(domain);
        return domain;
    }

    public static void cleanDomains() {
        DomainQuery DomainQuery = new DomainQuery();
        List<Domain> Domains = DomainQuery.getListDistinct();
        for (Domain domain : Domains) {
            DatabaseManager.removeObject(domain);
        }
    }
}
