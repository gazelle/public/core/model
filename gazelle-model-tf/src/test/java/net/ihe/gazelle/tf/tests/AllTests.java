package net.ihe.gazelle.tf.tests;

import net.ihe.gazelle.tf.documents.DocumentLifeCycleStatusTest;
import net.ihe.gazelle.tf.documents.DocumentSectionsTest;
import net.ihe.gazelle.tf.documents.DocumentTest;
import net.ihe.gazelle.tf.documents.DocumentTypeTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({DocumentLifeCycleStatusTest.class, DocumentSectionsTest.class, DocumentTest.class,
        DocumentTypeTest.class})
public class AllTests {

}
