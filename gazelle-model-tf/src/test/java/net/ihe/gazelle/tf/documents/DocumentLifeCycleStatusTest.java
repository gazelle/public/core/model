package net.ihe.gazelle.tf.documents;

import junit.framework.TestCase;
import net.ihe.gazelle.tf.model.DocumentLifeCycleStatus;

public class DocumentLifeCycleStatusTest extends TestCase {

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testEnumeration() {
        assertEquals("Draft", DocumentLifeCycleStatus.DRAFT.getFriendlyName());
        assertEquals("Public comment", DocumentLifeCycleStatus.PUBLIC_COMMENT.getFriendlyName());
        assertEquals("Trial implementation", DocumentLifeCycleStatus.TRIAL_IMPLEMENTATION.getFriendlyName());
        assertEquals("Final text", DocumentLifeCycleStatus.FINAL_TEXT.getFriendlyName());
        assertEquals("Final text", DocumentLifeCycleStatus.valueOf("FINAL_TEXT").getFriendlyName());
    }

    public void testEnumeration_keywords() {
        assertEquals(DocumentLifeCycleStatus.DRAFT, DocumentLifeCycleStatus.valueOfByKeyword("Draft"));
        assertEquals(DocumentLifeCycleStatus.PUBLIC_COMMENT, DocumentLifeCycleStatus.valueOfByKeyword("PC"));
        assertEquals(DocumentLifeCycleStatus.TRIAL_IMPLEMENTATION, DocumentLifeCycleStatus.valueOfByKeyword("TI"));
        assertEquals(DocumentLifeCycleStatus.FINAL_TEXT, DocumentLifeCycleStatus.valueOfByKeyword("FT"));
    }
}
