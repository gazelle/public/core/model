package net.ihe.gazelle.tf.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.factories.Hl7MessageProfileFactory;
import net.jcip.annotations.NotThreadSafe;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.xml.bind.JAXBException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@NotThreadSafe
public class Hl7MessageProfileTest {
    @Before
    public void init(){
        HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.set(EntityManagerService.provideEntityManager());
    }

    @After
    public void tearDown() throws Exception {
        Hl7MessageProfileFactory.cleanHl7MessageProfile();
    }

    @Test
    public void importSaveNewHl7MessageProfile() throws JAXBException {
        Hl7MessageProfile hmp = new Hl7MessageProfile();
        hmp.setMessageType("messageType");
        hmp.setProfileOid("1.1.1.2.2.333");
        hmp.setHl7Version("superVersion");
        EntityManagerService.provideEntityManager().getTransaction().begin();
        hmp = hmp.saveOrMerge(EntityManagerService.provideEntityManager());
        EntityManagerService.provideEntityManager().getTransaction().commit();

        assertNotNull(hmp.getId());

        Hl7MessageProfileQuery hmpq = new Hl7MessageProfileQuery();
        hmpq.profileOid().eq("1.1.1.2.2.333");
        Hl7MessageProfile hmpFound = hmpq.getUniqueResult();

        assertEquals("messageType", hmpFound.getMessageType());

    }

    @Test
    public void importMustNotOverriteExistingHl7MessageProfile() throws JAXBException {
        Hl7MessageProfile hmp = new Hl7MessageProfile();
        hmp.setMessageType("messageType");
        hmp.setProfileOid("1.1.1.2.2.333");
        hmp.setHl7Version("superVersion");
        hmp = (Hl7MessageProfile) DatabaseManager.writeObject(hmp);

        Hl7MessageProfile hmp2 = new Hl7MessageProfile();
        hmp2.setMessageType("messageType5445");
        hmp2.setProfileOid("1.1.1.2.2.333");
        hmp2.setHl7Version("superVersion2");
        EntityManagerService.provideEntityManager().getTransaction().begin();
        hmp2 = hmp2.saveOrMerge(EntityManagerService.provideEntityManager());
        EntityManagerService.provideEntityManager().getTransaction().commit();

        Hl7MessageProfileQuery hmpq = new Hl7MessageProfileQuery();
        hmpq.profileOid().eq("1.1.1.2.2.333");
        Hl7MessageProfile hmpFound = hmpq.getUniqueResult();

        assertEquals(hmp.getId(), hmp2.getId());
        assertEquals("messageType", hmpFound.getMessageType());

    }
}
