package net.ihe.gazelle.tf.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.IntegrationProfileStatusType;
import net.ihe.gazelle.tf.model.IntegrationProfileStatusTypeQuery;

import java.util.List;

public class IntegrationProfileStatusTypeFactory {

    public static IntegrationProfileStatusType provideIntegrationProfileStatusTypes() {
        IntegrationProfileStatusType statusType = new IntegrationProfileStatusType();
        return statusType;
    }

    public static void cleanIntegrationProfileStatusType() {
        IntegrationProfileStatusTypeQuery statusTypeQuery = new IntegrationProfileStatusTypeQuery();
        List<IntegrationProfileStatusType> statusType = statusTypeQuery.getListDistinct();
        for (IntegrationProfileStatusType status : statusType) {
            DatabaseManager.removeObject(status);
        }
    }
}
