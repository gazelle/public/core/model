package net.ihe.gazelle.tf.database;


import net.ihe.gazelle.junit.AbstractTestQueryJunit4;

public abstract class DatabaseConnectionSetup extends AbstractTestQueryJunit4 {

    public void setUp() throws Exception {
        super.setUp();
    }
}