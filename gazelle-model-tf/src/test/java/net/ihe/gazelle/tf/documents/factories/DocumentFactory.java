package net.ihe.gazelle.tf.documents.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.Document;
import net.ihe.gazelle.tf.model.DocumentLifeCycleStatus;
import net.ihe.gazelle.tf.model.DocumentQuery;
import net.ihe.gazelle.tf.model.DocumentType;
import net.ihe.gazelle.tf.model.factories.DomainFactory;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class DocumentFactory {

    public static Document create_document() {
        Document doc = new Document();
        populateDocumentValue(doc);
        doc = (Document) DatabaseManager.writeObject(doc);
        return doc;
    }

    public static Document createDocumentWithMandatoryFields() {
        Document doc = new Document();
        Random randomGenerator = new Random();
        doc.setName("testing" + randomGenerator.nextInt());
        Calendar date = Calendar.getInstance();
        date.set(1985, Calendar.JANUARY, Calendar.THURSDAY);
        doc.setUrl("http://google.fr");
        doc.setType(DocumentType.SUPPLEMENT);
        String hash = Double.toString((Math.random()));
        hash = hash.substring(2, hash.length());
        doc.setDocument_md5_hash_code(hash);
        doc.setDomain(DomainFactory.createDomainWithMandatoryFields());
        doc = (Document) DatabaseManager.writeObject(doc);
        return doc;
    }

    private static Document populateDocumentValue(Document doc) {
        doc.setName("testing");
        Calendar date = Calendar.getInstance();
        date.set(1985, Calendar.JANUARY, Calendar.THURSDAY);
        doc.setDateOfpublication(date.getTime());
        doc.setDomain(DomainFactory.createDomainWithMandatoryFields());
        doc.setUrl("http://google.fr");
        doc.setRevision("2");
        doc.setVolume("3");
        doc.setLifecyclestatus(DocumentLifeCycleStatus.DRAFT);
        doc.setType(DocumentType.SUPPLEMENT);
        String hash = Double.toString((Math.random()));
        hash = hash.substring(2, hash.length());
        doc.setDocument_md5_hash_code(hash);
        return doc;
    }

    public static void cleanDocuments() {
        DocumentQuery documentQuery = new DocumentQuery();
        List<Document> documents = documentQuery.getListDistinct();
        for (Document doc : documents) {
            DatabaseManager.removeObject(doc);
        }
    }
}
