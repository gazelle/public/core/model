package net.ihe.gazelle.tf.documents.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.Document;
import net.ihe.gazelle.tf.model.DocumentSection;
import net.ihe.gazelle.tf.model.DocumentSectionQuery;
import net.ihe.gazelle.tf.model.DocumentSectionType;

import java.util.List;

public class DocumentSectionFactory {

    public static DocumentSection createSection(Document doc, String sectionName) {
        DocumentSection section = new DocumentSection();
        section.setDocument(doc);
        section.setSection(sectionName);
        section.setType(DocumentSectionType.TITLE);
        section = (DocumentSection) DatabaseManager.writeObject(section);
        return section;
    }

    public static void cleanDocumentSections() {
        DocumentSectionQuery sectionQuery = new DocumentSectionQuery();
        List<DocumentSection> sections = sectionQuery.getListDistinct();
        for (DocumentSection section : sections) {
            DatabaseManager.removeObject(section);
        }
    }

    public static List<DocumentSection> findDocumentSectionByName(String title) {
        DocumentSectionQuery documentSectionQuery = new DocumentSectionQuery();
        documentSectionQuery.section().eq(title);
        List<DocumentSection> docSection = documentSectionQuery.getListDistinct();
        return docSection;
    }
}
