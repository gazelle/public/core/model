package net.ihe.gazelle.tf.model.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.tf.model.Hl7MessageProfile;
import net.ihe.gazelle.tf.model.Hl7MessageProfileQuery;

import java.util.List;

public class Hl7MessageProfileFactory {

    public static void cleanHl7MessageProfile() {
        Hl7MessageProfileQuery query = new Hl7MessageProfileQuery();
        List<Hl7MessageProfile> hl7MessageProfiles = query.getListDistinct();
        for (Hl7MessageProfile hl7MessageProfile : hl7MessageProfiles) {
            DatabaseManager.removeObject(hl7MessageProfile);
        }
    }
}
