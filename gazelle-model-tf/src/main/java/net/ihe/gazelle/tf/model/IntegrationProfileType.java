/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * <b>Class Description : </b>IntegrationProfileType<br>
 * <br>
 * This class describes the Integration profile type object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * IntegrationProfileType possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Integration profile type</li>
 * <li><b>keyword</b> : keyword corresponding to the Integration profile type</li>
 * <li><b>name</b> : name corresponding to the Integration profile type</li>
 * <li><b>description</b> : description of the Integration profile type</li>
 * </ul>
 * </br> <b>Example of Integration profile type</b> : "Workflow" is an Integration profile type<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 21
 * @class IntegrationProfileType.java
 * @package net.ihe.gazelle.tf.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@XmlRootElement(name = "IntegrationProfileType")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_integration_profile_type", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "tf_integration_profile_type_sequence", sequenceName = "tf_integration_profile_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class IntegrationProfileType extends AuditedObject implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450911138909883760L;

    private static Logger log = LoggerFactory.getLogger(IntegrationProfileType.class);
    @Column(name = "keyword", unique = true, nullable = false, length = 128)
    @NotNull
    protected String keyword;
    @Column(name = "name", length = 128)
    protected String name;
    @Column(name = "description")
    @Size(max = 2048)
    protected String description;
    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_integration_profile_type_sequence")
    @XmlTransient
    private Integer id;
    @ManyToMany
    @NotAudited
    @JoinTable(name = "tf_integration_profile_type_link", joinColumns = @JoinColumn(name = "integration_profile_type_id"), inverseJoinColumns = @JoinColumn(name = "integration_profile_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "integration_profile_id", "integration_profile_type_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private List<IntegrationProfile> integrationProfiles;

    public IntegrationProfileType() {
    }

    public IntegrationProfileType(Integer id, String keyword) {
        this.id = id;
        this.keyword = keyword;
    }

    public IntegrationProfileType(Integer id, String keyword, String name, String description) {
        this.id = id;
        this.keyword = keyword;
        this.name = name;
        this.description = description;
    }

    public IntegrationProfileType(IntegrationProfileType oldType) {
        if (oldType.getId() != null) {
            this.id =  Integer.valueOf(oldType.getId());
        }
        if (oldType.getKeyword() != null) {
            this.keyword =  String.valueOf(oldType.getKeyword());
        }
        if (oldType.getName() != null) {
            this.name = String.valueOf(oldType.getName());
        }
        if (oldType.getDescription() != null) {
            this.description = String.valueOf(oldType.getDescription());
        }
    }

    public static List<IntegrationProfileType> ListAllIntegrationProfileType() {
        return AuditedObject.listAllObjectsOrderByKeyword(IntegrationProfileType.class);
    }

    public static IntegrationProfileType GetIntegrationProfileTypeByKeyword(String inKeyword) {
        return AuditedObject.getObjectByKeyword(IntegrationProfileType.class, inKeyword);
    }

    public List<IntegrationProfile> getIntegrationProfiles() {
        return integrationProfiles;
    }

    public void setIntegrationProfiles(List<IntegrationProfile> ips) {
        integrationProfiles = ips;
    }

    // Constructors

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        IntegrationProfileTypeQuery query = new IntegrationProfileTypeQuery();
        query.keyword().eq(this.keyword);
        query.name().eq(this.name);
        query.description().eq(this.description);

        List<IntegrationProfileType> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            this.integrationProfiles = listDistinct.get(0).integrationProfiles;
            entityManager.merge(this);
        } else {
            if (id != null) {
                entityManager.merge(this);
            } else {
                if (id != null) {
                    entityManager.merge(this);
                } else {
                    entityManager.persist(this);
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

}
