/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * <b>Class Description : </b>ActorIntegrationProfile<br>
 * <br>
 * This class describes the ActorIntegrationProfile object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * Actor possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the ActorIntegrationProfile</li>
 * <li><b>actor</b> : object corresponding to an Actor</li>
 * <li><b>integrationProfile</b> : object corresponding to an Integration Profile</li>
 * <li><b>description</b> : Comments/description corresponding to the ActorIntegrationProfile</li>
 * </ul>
 * </br> <b>Example of ActorIntegrationProfile</b> : ADT actor is mapped with SWF integration profile<br>
 *
 * @author J. Krych / MIR St Louis USA - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, January 23rd
 * @class ActorIntegrationProfile.java
 * @package net.ihe.gazelle.tf.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@XmlRootElement(name = "ActorIntegrationProfileOption")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_actor_integration_profile_option", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "actor_integration_profile_id", "integration_profile_option_id"}))
@SequenceGenerator(name = "tf_actor_integration_profile_option_sequence", sequenceName = "tf_actor_integration_profile_option_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class ActorIntegrationProfileOption extends AuditedObject implements java.io.Serializable,
        Comparable<ActorIntegrationProfileOption> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(ActorIntegrationProfileOption.class);

    // **************************** Attributes (existing in database as a
    // column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_actor_integration_profile_option_sequence")
    @XmlTransient
    private Integer id;

    // ************************************************************* Foreign
    // keys

    @ManyToOne(targetEntity = ActorIntegrationProfile.class)
    // JRC : Remove nullable constraint in :
    // @JoinColumn(name="actor_integration_profile_id" , nullable=false) to be
    // able to sync aip object with GMM
    @JoinColumn(name = "actor_integration_profile_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private ActorIntegrationProfile actorIntegrationProfile;

    @ManyToOne(targetEntity = IntegrationProfileOption.class)
    @JoinColumn(name = "integration_profile_option_id")
    @Fetch(value = FetchMode.SELECT)
    private IntegrationProfileOption integrationProfileOption;

    @Column(name = "maybe_supportive")
    private Boolean maybeSupportive;

    @ManyToOne
    @JoinColumn(name = "document_section")
    @Fetch(value = FetchMode.SELECT)
    @XmlElement(name = "documentSection")
    private DocumentSection documentSection;

    // *************************************************************
    // Constructors
    public ActorIntegrationProfileOption() {
    }

    public ActorIntegrationProfileOption(IntegrationProfileOption integrationProfileOption,
                                         IntegrationProfile integrationProfile, Actor actor) {
        super();

        this.integrationProfileOption = integrationProfileOption;
        this.actorIntegrationProfile = new ActorIntegrationProfile(actor, integrationProfile);

    }

    public ActorIntegrationProfileOption(ActorIntegrationProfileOption inActorIntegrationProfileOption) {

        if (inActorIntegrationProfileOption != null) {
            if (inActorIntegrationProfileOption.getActorIntegrationProfile() != null) {
                actorIntegrationProfile = new ActorIntegrationProfile(
                        inActorIntegrationProfileOption.getActorIntegrationProfile());
            }
            if (inActorIntegrationProfileOption.getIntegrationProfileOption() != null) {
                integrationProfileOption = new IntegrationProfileOption(
                        inActorIntegrationProfileOption.getIntegrationProfileOption());
            }
        }

    }

    public ActorIntegrationProfileOption(String inActor, String inIntegrationProfile, String inOption) {
        if ((inIntegrationProfile != null) && (inIntegrationProfile.trim().length() > 0) && (inActor != null)
                && (inActor.trim().length() > 0)) {
            if ((inOption == null) || (inOption.trim().length() == 0)) {
                integrationProfileOption = IntegrationProfileOption.getNoneOption();
            } else {
                integrationProfileOption = IntegrationProfileOption.findIntegrationProfileOptionWithKeyword(inOption);
            }

            actorIntegrationProfile = new ActorIntegrationProfile();

            IntegrationProfile newIP = IntegrationProfile.findIntegrationProfileWithKeyword(inIntegrationProfile);
            if (newIP == null) {

                log.error("Error !!! IntegrationProfile cannot be found constructing ActorIntegrationProfileOption");

            } else {
                actorIntegrationProfile.setIntegrationProfile(newIP);
            }

            Actor newActor = Actor.findActorWithKeyword(inActor);
            if (newActor == null) {

                log.error("Error !!! Actor cannot be found constructing ActorIntegrationProfileOption");


            } else {
                actorIntegrationProfile.setActor(newActor);
            }

        }

    }

    public static ActorIntegrationProfileOption getActorIntegrationProfileOption(Actor inActor,
                                                                                 IntegrationProfile inIntegrationProfile, IntegrationProfileOption inOption) {
        if ((inActor == null) || (inIntegrationProfile == null)) {
            return null;
        }

        if (inOption == null) {
            inOption = IntegrationProfileOption.getNoneOption();
        }

        ActorIntegrationProfileOptionQuery query = new ActorIntegrationProfileOptionQuery();
        query.actorIntegrationProfile().actor().id().eq(inActor.getId());
        query.actorIntegrationProfile().integrationProfile().id().eq(inIntegrationProfile.getId());
        query.integrationProfileOption().id().eq(inOption.getId());
        return query.getUniqueResult();

    }

    public static List<ActorIntegrationProfileOption> listAllActorIntegrationProfileOption() {
        ActorIntegrationProfileOptionQuery query = new ActorIntegrationProfileOptionQuery();
        query.actorIntegrationProfile().integrationProfile().keyword().order(true);
        return query.getList();
    }

    protected static ActorIntegrationProfileOptionQuery getQuery(ActorIntegrationProfile inActorIntegrationProfile,
                                                                 IntegrationProfile inIntegrationProfile, IntegrationProfileOption inIntegrationProfileOption, Actor inActor) {
        ActorIntegrationProfileOptionQuery query = new ActorIntegrationProfileOptionQuery();
        query.actorIntegrationProfile().eqIfValueNotNull(inActorIntegrationProfile);
        query.actorIntegrationProfile().integrationProfile().eqIfValueNotNull(inIntegrationProfile);
        query.integrationProfileOption().eqIfValueNotNull(inIntegrationProfileOption);
        query.actorIntegrationProfile().actor().eqIfValueNotNull(inActor);
        return query;
    }

    public static List<ActorIntegrationProfileOption> getActorsOptionsForIntegrationProfile(
            IntegrationProfile inIntegrationProfile) {
        ActorIntegrationProfileOptionQuery query = getQuery(null, inIntegrationProfile, null, null);
        return query.getListNullIfEmpty();
    }

    public static List<ActorIntegrationProfileOption> getIntegrationProfilesOptionsForActor(Actor inActor) {
        ActorIntegrationProfileOptionQuery query = getQuery(null, null, null, inActor);
        return query.getListNullIfEmpty();
    }

    public static List<ActorIntegrationProfileOption> getAllActorIntegrationProfileOptions() {
        ActorIntegrationProfileOptionQuery query = new ActorIntegrationProfileOptionQuery();
        return query.getListNullIfEmpty();
    }

    public static List<String> getPossibleActorsKeywordFromAIPO(IntegrationProfile iProfile) {
        ActorIntegrationProfileOptionQuery query = getQuery(null, iProfile, null, null);
        return query.actorIntegrationProfile().actor().keyword().getListDistinct();
    }

    public static List<ActorIntegrationProfileOption> getActorIntegrationProfileOptionFiltered(
            ActorIntegrationProfile inActorIntegrationProfile, IntegrationProfile inIntegrationProfile,
            IntegrationProfileOption inIntegrationProfileOption, Actor inActor) {
        ActorIntegrationProfileOptionQuery query = getQuery(inActorIntegrationProfile, inIntegrationProfile,
                inIntegrationProfileOption, inActor);
        return query.getList();
    }

    public static List<Actor> getActorsFromAIPOFiltered(ActorIntegrationProfile inActorIntegrationProfile,
                                                        IntegrationProfile inIntegrationProfile, IntegrationProfileOption inIntegrationProfileOption, Actor inActor) {
        ActorIntegrationProfileOptionQuery query = getQuery(inActorIntegrationProfile, inIntegrationProfile,
                inIntegrationProfileOption, inActor);
        query.actorIntegrationProfile().actor().keyword().order(true);
        return query.actorIntegrationProfile().actor().getListDistinct();
    }

    public static List<Actor> getActorsForIntegrationProfile(IntegrationProfile inIntegrationProfile) {
        ActorIntegrationProfileOptionQuery query = getQuery(null, inIntegrationProfile, null, null);
        return query.actorIntegrationProfile().actor().getListDistinct();
    }

    public static List<IntegrationProfile> getIntegrationProfileFromAIPOFiltered(
            ActorIntegrationProfile inActorIntegrationProfile, IntegrationProfile inIntegrationProfile,
            IntegrationProfileOption inIntegrationProfileOption, Actor inActor) {
        ActorIntegrationProfileOptionQuery query = getQuery(inActorIntegrationProfile, inIntegrationProfile,
                inIntegrationProfileOption, inActor);
        query.actorIntegrationProfile().integrationProfile().keyword().order(true);
        return query.actorIntegrationProfile().integrationProfile().getListDistinct();
    }

    public static List<IntegrationProfileOption> getIntegrationProfileOptionFromAIPOFiltered(
            ActorIntegrationProfile inActorIntegrationProfile, IntegrationProfile inIntegrationProfile,
            IntegrationProfileOption inIntegrationProfileOption, Actor inActor) {
        ActorIntegrationProfileOptionQuery query = getQuery(inActorIntegrationProfile, inIntegrationProfile,
                inIntegrationProfileOption, inActor);
        query.integrationProfileOption().keyword().order(true);
        return query.integrationProfileOption().getListDistinct();
    }

    // ***************************************************** Getters and Setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IntegrationProfileOption getIntegrationProfileOption() {
        return integrationProfileOption;
    }

    public void setIntegrationProfileOption(IntegrationProfileOption integrationProfileOption) {
        this.integrationProfileOption = integrationProfileOption;
    }

    public ActorIntegrationProfile getActorIntegrationProfile() {
        return actorIntegrationProfile;
    }

    public void setActorIntegrationProfile(ActorIntegrationProfile actorIntegrationProfile) {
        this.actorIntegrationProfile = actorIntegrationProfile;
    }

    public String toStringShort() {
        if (getActorIntegrationProfile() != null) {
            // Only Actor
            if ((getActorIntegrationProfile().getActor() != null)
                    && (getActorIntegrationProfile().getIntegrationProfile() == null)) {
                return getActorIntegrationProfile().getActor().getKeyword()
                        + "-"
                        + (getIntegrationProfileOption() != null ? "-" + getIntegrationProfileOption().getKeyword()
                        : "");
            }
            // Only Integration Profile
            else if ((getActorIntegrationProfile().getActor() == null)
                    && (getActorIntegrationProfile().getIntegrationProfile() != null)) {
                return "-"
                        + getActorIntegrationProfile().getIntegrationProfile().getKeyword()
                        + (getIntegrationProfileOption() != null ? "-" + getIntegrationProfileOption().getKeyword()
                        : "");
            }
            // Actor and Integration Profile and eventually IPO
            else {
                return getActorIntegrationProfile().getActor().getKeyword()
                        + "-"
                        + getActorIntegrationProfile().getIntegrationProfile().getKeyword()
                        + (getIntegrationProfileOption() != null ? "-" + getIntegrationProfileOption().getKeyword()
                        : "");
            }
        } else {
            return "?";
        }
    }

    @Override
    public String toString() {
        if (getActorIntegrationProfile() != null) {
            // Only Actor
            if ((getActorIntegrationProfile().getActor() != null)
                    && (getActorIntegrationProfile().getIntegrationProfile() == null)) {
                return getActorIntegrationProfile().getActor().getName()
                        + " with No Integration Profile "
                        + (getIntegrationProfileOption() != null ? " and option "
                        + getIntegrationProfileOption().getKeyword() : "");
            }
            // Only Integration Profile
            else if ((getActorIntegrationProfile().getActor() == null)
                    && (getActorIntegrationProfile().getIntegrationProfile() != null)) {
                return getActorIntegrationProfile().getIntegrationProfile().getKeyword()
                        + " with No Actor "
                        + (getIntegrationProfileOption() != null ? " and option "
                        + getIntegrationProfileOption().getKeyword() : "");
            }
            // Actor and Integration Profile and eventually IPO
            else {
                return getActorIntegrationProfile().getActor().getName()
                        + " with "
                        + getActorIntegrationProfile().getIntegrationProfile().getKeyword()
                        + (getIntegrationProfileOption() != null ? " and option "
                        + getIntegrationProfileOption().getKeyword() : "");
            }
        } else {
            return "Aip from Aipo is null";
        }

    }

    @Override
    public int compareTo(ActorIntegrationProfileOption o) {
        if (o == null) {
            return 1;
        }
        if (o.getActorIntegrationProfile() == null) {
            return 1;
        }
        if ((getActorIntegrationProfile() == null) && (o.getActorIntegrationProfile() == null)) {
            return 0;
        }

        if (getActorIntegrationProfile().getActor() == null) {
            return 1;
        }
        if (o.getActorIntegrationProfile().getActor() == null) {
            return 1;
        }
        if (getActorIntegrationProfile().getActor() == null) {
            return 1;
        }
        if (o.getActorIntegrationProfile().getActor() == null) {
            return 1;
        }

        if (this.getActorIntegrationProfile().getIntegrationProfile().getKeyword()
                .compareToIgnoreCase(o.getActorIntegrationProfile().getIntegrationProfile().getKeyword()) == 0) {
            if (this.getActorIntegrationProfile().getActor().getKeyword()
                    .compareToIgnoreCase(o.getActorIntegrationProfile().getActor().getKeyword()) == 0) {
                if (o.getActorIntegrationProfile() == null) {
                    return -1;
                }
                if (getActorIntegrationProfile() == null) {
                    return 1;
                }

                return this.getIntegrationProfileOption().getKeyword()
                        .compareToIgnoreCase(o.getIntegrationProfileOption().getKeyword());
            } else {
                return this.getActorIntegrationProfile().getActor().getKeyword()
                        .compareToIgnoreCase(o.getActorIntegrationProfile().getActor().getKeyword());
            }
        } else {
            return this.getActorIntegrationProfile().getIntegrationProfile().getKeyword()
                    .compareToIgnoreCase(o.getActorIntegrationProfile().getIntegrationProfile().getKeyword());
        }
    }

    public DocumentSection getDocumentSection() {
        return documentSection;
    }

    public void setDocumentSection(DocumentSection documentSection) {
        this.documentSection = documentSection;
    }

    public Boolean getMaybeSupportive() {
        if (maybeSupportive == null) {
            return false;
        }
        return maybeSupportive;
    }

    public void setMaybeSupportive(Boolean maybeSupportive) {
        this.maybeSupportive = maybeSupportive;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        actorIntegrationProfile.setLastModifierId(this.lastModifierId);
        actorIntegrationProfile.saveOrMerge(entityManager);
        integrationProfileOption.setLastModifierId(this.lastModifierId);
        integrationProfileOption.saveOrMerge(entityManager);
        if (documentSection != null) {
            if (documentSection.emptyDocumentSection()) {
                documentSection = null;
            } else {
                documentSection.setLastModifierId(this.lastModifierId);
                documentSection.saveOrMerge(entityManager);
            }
        }
        ActorIntegrationProfileOptionQuery query = new ActorIntegrationProfileOptionQuery();
        query.actorIntegrationProfile().eq(this.actorIntegrationProfile);
        query.integrationProfileOption().eq(this.integrationProfileOption);

        List<ActorIntegrationProfileOption> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

}
