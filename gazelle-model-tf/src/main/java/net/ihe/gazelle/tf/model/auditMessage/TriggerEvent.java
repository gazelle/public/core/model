package net.ihe.gazelle.tf.model.auditMessage;

import net.ihe.gazelle.hql.FilterLabelProvider;

public enum TriggerEvent implements FilterLabelProvider {
    APPLICATION_ACTIVITY("Application-activity"),
    AUDIT_LOG_USED("Audit-Log-Used"),
    BEGIN_STORING_INSTANCES("Begin-storing-instances"),
    HEALTH_SERVICE_EVENT("Health-service-event"),
    INSTANCES_DELETED("Instances-deleted"),
    INSTANCES_STORED("Instances-Stored"),
    MEDICATION("Medication"),
    MOBILE_MACHINE_EVENT("Mobile-machine-event"),
    NODE_AUTHENTICATION_FAILURE("Node-Authentication-failure"),
    ORDER_RECORD_EVENT("Order-record-event"),
    PATIENT_CARE_ASSIGNMENT("Patient-care-assignment"),
    PATIENT_CARE_EPISODE("Patient-care-episode"),
    PATIENT_CARE_PROTOCOL("Patient-care-protocol"),
    PATIENT_RECORD_EVENT("Patient-record-event"),
    PHI_EXPORT("PHI-export"),
    PHI_IMPORT("PHI-import"),
    PROCEDURE_RECORD_EVENT("Procedure-record-event"),
    QUERY_INFORMATION("Query Information"),
    SECURITY_ALERT("Security Alert"),
    USER_AUTHENTICATION("User Authentication"),
    STUDY_OBJECT_EVENT("Study-Object-Event"),
    STUDY_USED("Study-used");

    private String keyword;

    TriggerEvent(String keyword) {
        this.keyword = keyword;
    }

    public static TriggerEvent getTriggerEventForKeyword(String event) {
        for (TriggerEvent triggerEvent : values()) {
            if (triggerEvent.getKeyword().equals(event)) {
                return triggerEvent;
            }
        }
        return null;
    }

    public String getKeyword() {
        return keyword;
    }

    @Override
    public String getSelectableLabel() {
        return getKeyword();
    }
}
