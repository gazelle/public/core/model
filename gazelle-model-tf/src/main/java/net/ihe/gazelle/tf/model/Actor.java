/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import net.ihe.gazelle.tf.model.auditMessage.AuditMessage;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Comparator;
import java.util.List;

/**
 * <b>Class Description : </b>Actor<br>
 * <br>
 * This class describes the Actor object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * Actor possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Actor</li>
 * <li><b>name</b> : name corresponding to the Actor</li>
 * <li><b>description</b> : description corresponding to the Actor</li>
 * <li><b>combined</b> : Is the Actor is combined ?</li>
 * </ul>
 * </br> <b>Example of actor</b> : Acquisition Modality is an IHE actor<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 21
 * @class Actor.java
 * @package net.ihe.gazelle.tf.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "actor")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Audited
@Table(name = "tf_actor", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "tf_actor_sequence", sequenceName = "tf_actor_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class Actor extends AuditedObject implements java.io.Serializable, Comparable<Actor>, Comparator<Actor> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450111131541283360L;

    private static Logger log = LoggerFactory.getLogger(Actor.class);

    // Attributes (existing in database as a column)
    @Column(name = "combined")
    protected Boolean combined;
    @Column(name = "keyword", unique = true, nullable = false, length = 128)
    // @Length(max = 128 )
    @NotNull
    @XmlElement(name = "keyword")
    protected String keyword;
    @Column(name = "name", length = 128)
    // @Length(max = 128 )
    @XmlElement(name = "name")
    protected String name;
    @Column(name = "description")
    @Size(max = 2048)
    @XmlElement(name = "description")
    protected String description;
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_actor_sequence")
    @XmlElement(name = "id")
    private Integer id;
    @Transient
    private boolean toDisplay = true;
    // Variables used for Foreign Keys : tf_transaction_link table
    @OneToMany(mappedBy = "fromActor", targetEntity = TransactionLink.class, fetch = FetchType.LAZY)
    @AuditJoinTable
    private List<TransactionLink> transactionLinksWhereActingAsSource;
    @OneToMany(mappedBy = "toActor", targetEntity = TransactionLink.class, fetch = FetchType.LAZY)
    @AuditJoinTable
    private List<TransactionLink> transactionLinksWhereActingAsReceiver;
    // --------------------------------------- tf_actor_integration_profile table
    @OneToMany(mappedBy = "actor", targetEntity = ActorIntegrationProfile.class)
    @AuditJoinTable
    private List<ActorIntegrationProfile> actorIntegrationProfiles;
    // Variables used for Foreign Keys : tf_actor_integration_profile_option table
    @Transient
    private List<ActorIntegrationProfileOption> listOfActorIntegrationProfileOption;
    @OneToMany(mappedBy = "issuingActor", targetEntity = AuditMessage.class)
    @NotAudited
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<AuditMessage> auditMessages;

    // Constructors
    public Actor() {
        super();
    }

    public Actor(String keyword, String name) {
        this.keyword = keyword;
        this.name = name;
        // super(keyword, name ) ;
    }

    public Actor(String keyword, String name, String description, Boolean combined) {

        // super( keyword , name , description ) ;
        this.keyword = keyword;
        this.name = name;
        this.description = description;

        this.combined = combined;
    }

    Actor(String name2) {
        this.name = name2;
    }

    public Actor(Actor oldActor) {

        // super(oldActor) ;
        if (oldActor.keyword != null) {
            this.keyword =  String.valueOf(oldActor.keyword);
        }

        if (oldActor.name != null) {
            this.name =  String.valueOf(oldActor.name);
        }

        if (oldActor.description != null) {
            this.description =  String.valueOf(oldActor.description);
        }

        if (oldActor.getCombined() != null) {
            this.combined =  Boolean.valueOf(oldActor.getCombined());
        }

    }

    public static Actor duplicateActor(Actor ac) {
        Actor actor = new Actor(ac);
        if (actor.keyword != null) {
            actor.keyword = new String(actor.keyword + "_copy");
        }

        if (actor.name != null) {
            actor.name = new String(actor.name + "_copy");
        }
        return actor;
    }

    /**
     * Find and retrieve the Actor object associated to a keyword
     *
     * @param keywordName : String - Keyword to retrieve
     * @return Actor object
     */
    public static Actor findActorWithKeyword(String keywordName) {
        if ((keywordName == null) || (keywordName.length() == 0)) {
            log.error("findActorWithKeyword - parameter given as required parameter is null");
            return null;
        }

        return getObjectByKeyword(Actor.class, keywordName);
    }

    public static List<Actor> listAllActors() {
        ActorQuery actorQuery = new ActorQuery();
        actorQuery.keyword().order(true);
        return actorQuery.getList();
    }

    public static List<Actor> getActorFiltered(String inKeyword, IntegrationProfile inIntegrationProfile,
                                               Domain inDomain) {
        ActorQuery actorQuery = new ActorQuery();
        if (inKeyword != null) {
            actorQuery.keyword().eq(inKeyword);
        }
        if (inIntegrationProfile != null) {
            actorQuery.actorIntegrationProfiles().integrationProfile().eq(inIntegrationProfile);
        }
        if (inDomain != null) {
            actorQuery.actorIntegrationProfiles().integrationProfile().domainsForDP().id().eq(inDomain.getId());
        }
        return actorQuery.getList();
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ActorIntegrationProfile> getActorIntegrationProfiles() {
        return actorIntegrationProfiles;
    }

    public List<ActorIntegrationProfileOption> getActorIntegrationProfileOption() {
        return listOfActorIntegrationProfileOption;
    }

    public void setActorIntegrationProfileOption(List<ActorIntegrationProfileOption> l) {
        listOfActorIntegrationProfileOption = l;
    }

    public Integer getId() {
        return this.id;
    }

    // *********************************************************
    // Foreign Key for table : tf_transaction_link
    // *********************************************************

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getCombined() {
        return this.combined;
    }

    public void setCombined(Boolean combined) {
        this.combined = combined;
    }

    public List<TransactionLink> getTransactionLinksWhereActingAsSource() {
        return HibernateHelper.getLazyValue(this, "transactionLinksWhereActingAsSource",
                this.transactionLinksWhereActingAsSource);
    }

    public void setTransactionLinksWhereActingAsSource(List<TransactionLink> transactionLinks) {
        this.transactionLinksWhereActingAsSource = transactionLinks;
    }

    public void addTransactionLinksWhereActingAsSource(TransactionLink tl) {
        transactionLinksWhereActingAsSource.add(tl);
    }

    public void delTransactionLinksWhereActingAsSource(TransactionLink tl) {
        transactionLinksWhereActingAsSource.remove(tl);
    }

    public List<TransactionLink> getTransactionLinksWhereActingAsReceiver() {
        return HibernateHelper.getLazyValue(this, "transactionLinksWhereActingAsReceiver",
                this.transactionLinksWhereActingAsReceiver);
    }

    public void setTransactionLinksWhereActingAsReceiver(List<TransactionLink> transactionLinks) {
        this.transactionLinksWhereActingAsReceiver = transactionLinks;
    }

    public void addTransactionLinksWhereActingAsReceiver(TransactionLink tl) {
        transactionLinksWhereActingAsReceiver.add(tl);
    }

    public void delTransactionLinksWhereActingAsReceiver(TransactionLink tl) {
        transactionLinksWhereActingAsReceiver.remove(tl);
    }

    @FilterLabel
    public String getSelectableLabel() {
        return getKeyword() + " - " + getName();
    }

    public boolean isToDisplay() {
        return toDisplay;
    }

    public void setToDisplay(boolean toDisplay) {
        this.toDisplay = toDisplay;
    }

    @Override
    public String toString() {
        if (toDisplay) {
            return "Actor " + keyword + "-" + name;
        } else {
            return "\"";
        }
    }

    @Override
    public int compare(Actor o1, Actor o2) {
        return o1.getKeyword().compareToIgnoreCase(o2.getKeyword());
    }

    @Override
    public int compareTo(Actor o) {
        if (o == null) {
            return -1;
        }
        if (o.getKeyword() == null) {
            return -1;
        }
        if (getKeyword() == null) {
            return 1;
        }

        return getKeyword().compareToIgnoreCase(o.getKeyword());
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        this.setId(null);
        ActorQuery query = new ActorQuery();
        query.keyword().eq(this.keyword);
        List<Actor> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        entityManager.flush();
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName() + " saved " + this);
        }
    }

    public List<AuditMessage> getAuditMessages() {
        return auditMessages;
    }

    public void setAuditMessages(List<AuditMessage> auditMessages) {
        this.auditMessages = auditMessages;
    }
}
