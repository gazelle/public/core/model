/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Collections;
import java.util.List;

/**
 * <b>Class Description : </b>ProfileLink<br>
 * <br>
 * This class describes the ProfileLink object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * ProfileLink possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Profile Link</li>
 * <li><b>actor</b> The Actor object for this Profile Link</li>
 * <li><b>integrationProfile</b> The Integration Profile object for this Profile Link</li>
 * <li><b>transaction</b> The Transaction object for this Profile Link</li>
 * <li><b>transactionOption</b> The Transaction Option Type for this Profile Link (R)equired, (O)ptional, or (C)onditional</li>
 * </ul>
 * </br>
 *
 * @author Ralph Moulton / MIR IHE development Project
 * @version 1.0 - 2009, April 9
 * @class ProfileLink.java
 * @package net.ihe.gazelle.tf.model
 * @see > rmoulton@mir.wustl.edu -
 */

@XmlRootElement(name = "ProfileLink")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_profile_link", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "actor_integration_profile_id", "transaction_id", "transaction_option_type_id"}))
@SequenceGenerator(name = "tf_profile_link_sequence", sequenceName = "tf_profile_link_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class ProfileLink extends AuditedObject implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = 654986516546541L;

    private static Logger log = LoggerFactory.getLogger(ProfileLink.class);

    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_profile_link_sequence")
    @XmlTransient
    private Integer id;

    // Variables used for Foreign Keys : ActorIntegrationProfile
    @ManyToOne(targetEntity = net.ihe.gazelle.tf.model.ActorIntegrationProfile.class)
    @JoinColumn(name = "actor_integration_profile_id")
    @AuditJoinTable
    @Fetch(value = FetchMode.SELECT)
    @XmlTransient
    private ActorIntegrationProfile actorIntegrationProfile;

    @ManyToOne
    @JoinColumn(name = "transaction_id")
    @AuditJoinTable
    @Fetch(value = FetchMode.SELECT)
    private Transaction transaction;

    // Variables used for Foreign Keys : ActorIntegrationProfile
    @ManyToOne(targetEntity = net.ihe.gazelle.tf.model.TransactionOptionType.class)
    @JoinColumn(name = "transaction_option_type_id", nullable = true, unique = false)
    @AuditJoinTable
    @Fetch(value = FetchMode.SELECT)
    private TransactionOptionType transactionOptionType;

    // Constructors
    public ProfileLink() {
    }

    public ProfileLink(Integer id) {
        this.id = id;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<Transaction> getTransactionsByActorAndIP(Actor inActor, IntegrationProfile inIntegrationProfile) {
        ProfileLinkQuery query = new ProfileLinkQuery();
        query.actorIntegrationProfile().actor().eq(inActor);
        query.actorIntegrationProfile().integrationProfile().eq(inIntegrationProfile);
        return query.transaction().getListDistinct();
    }

    public static List<ProfileLink> getProfileLinksForIntegrationProfile(IntegrationProfile inIntegrationProfile) {
        ProfileLinkQuery query = new ProfileLinkQuery();
        query.actorIntegrationProfile().integrationProfile().eq(inIntegrationProfile);
        return query.getListNullIfEmpty();
    }

    public static List<ProfileLink> getProfileLinksForActor(Actor inActor) {
        ProfileLinkQuery query = new ProfileLinkQuery();
        query.actorIntegrationProfile().actor().eq(inActor);
        return query.getListNullIfEmpty();
    }

    public static List<ProfileLink> getProfileLinksForTransactionOptionType(TransactionOptionType tot) {
        if (tot == null || tot.getId() == null) {
            return null;
        }
        ProfileLinkQuery query = new ProfileLinkQuery();
        query.transactionOptionType().eq(tot);
        return query.getListNullIfEmpty();
    }

    public static int getNumberProfileLinksForTransactionOptionType(TransactionOptionType tot) {
        if (tot == null || tot.getId() == null) {
            return 0;
        }
        ProfileLinkQuery query = new ProfileLinkQuery();
        query.transactionOptionType().eq(tot);
        return query.getCount();
    }

    public static List<Transaction> getTransactionForActor(Actor inActor) {
        ProfileLinkQuery query = new ProfileLinkQuery();
        query.actorIntegrationProfile().actor().eq(inActor);
        return query.transaction().getListDistinct();
    }

    public static List<ActorIntegrationProfileOption> getAIPOByTransactionForActorActingAsFromActor(
            Transaction inTransaction) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Query query = entityManager.createQuery("SELECT DISTINCT aipo "
                + "FROM ProfileLink pl,TransactionLink tl,ActorIntegrationProfileOption aipo "
                + "WHERE tl.fromActor=pl.actorIntegrationProfile.actor "
                + "AND pl.actorIntegrationProfile=aipo.actorIntegrationProfile " + "AND pl.transaction=tl.transaction "
                + "AND tl.transaction=:inTransaction");
        query.setParameter("inTransaction", inTransaction);
        List<ActorIntegrationProfileOption> list = query.getResultList();
        if (list.size() > 0) {
            Collections.sort(list);
            return list;
        }
        return null;
    }

    public static List<ActorIntegrationProfileOption> getAIPOByTransactionForActorActingAsToActor(
            Transaction inTransaction) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Query query = entityManager.createQuery("SELECT DISTINCT aipo "
                + "FROM ProfileLink pl,TransactionLink tl,ActorIntegrationProfileOption aipo "
                + "WHERE tl.toActor=pl.actorIntegrationProfile.actor "
                + "AND pl.actorIntegrationProfile=aipo.actorIntegrationProfile " + "AND pl.transaction=tl.transaction "
                + "AND tl.transaction=:inTransaction");
        query.setParameter("inTransaction", inTransaction);
        List<ActorIntegrationProfileOption> list = query.getResultList();
        if (list.size() > 0) {
            Collections.sort(list);
            return list;
        }
        return null;
    }

    /**
     * two methods doing the same thing but with two different names cf getProfileLinksForIntegrationProfile(IntegrationProfile inIntegrationProfile)
     *
     * @param integrationProfile
     * @return
     */
    @SuppressWarnings("unchecked")
    @Deprecated
    public static List<ProfileLink> FindProfileLinksForIntegrationProfile(IntegrationProfile integrationProfile) {
        if (integrationProfile == null) {
            return null;
        }
        return getProfileLinksForIntegrationProfile(integrationProfile);
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ActorIntegrationProfile getActorIntegrationProfile() {
        return actorIntegrationProfile;
    }

    public void setActorIntegrationProfile(ActorIntegrationProfile aip) {
        actorIntegrationProfile = aip;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction t) {
        transaction = t;
    }

    public TransactionOptionType getTransactionOptionType() {
        return transactionOptionType;
    }

    public void setTransactionOptionType(TransactionOptionType transactionOptionType) {
        this.transactionOptionType = transactionOptionType;
    }

    public void saveOrMerge(EntityManager entityManager, ActorIntegrationProfile actorIntegrationProfile2) {
        this.id = null;

        this.actorIntegrationProfile = actorIntegrationProfile2;

        transaction.setLastModifierId(this.lastModifierId);
        transaction = transaction.saveOrMerge(entityManager);
        transactionOptionType.setLastModifierId(this.lastModifierId);
        transactionOptionType.saveOrMerge(entityManager);
        ProfileLinkQuery query = new ProfileLinkQuery();
        query.actorIntegrationProfile().eq(this.actorIntegrationProfile);
        query.transaction().eq(this.transaction);
        query.transactionOptionType().eq(this.transactionOptionType);
        List<ProfileLink> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }
}
