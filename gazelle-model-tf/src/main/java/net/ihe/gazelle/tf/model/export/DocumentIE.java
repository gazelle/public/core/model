package net.ihe.gazelle.tf.model.export;

import net.ihe.gazelle.tf.model.Document;
import net.ihe.gazelle.tf.model.DocumentSection;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="documents")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentIE {

    @XmlElementRefs({@XmlElementRef(name = "Document")})
    private List<Document> documents;

    @XmlElementRefs({@XmlElementRef(name = "DocumentSection")})
    private List<DocumentSection> documentSections;

    @XmlTransient
    private List<String> unknownDomains;

    @XmlTransient
    private List<Document> ignoredDocuments;

    @XmlTransient
    private List<Document> duplicatedDocuments;

    @XmlTransient
    private List<DocumentSection> ignoredSections;

    @XmlTransient
    private boolean imported;


    public DocumentIE(){
        documents = new ArrayList<>();
        documentSections = new ArrayList<>();
    }

    public DocumentIE(List<Document> documents){
        this.documentSections = new ArrayList<>();
        if (documents != null){
            for (Document document : documents){
                if (document.getDocumentSection()!= null){
                    this.documentSections.addAll(document.getDocumentSection());
                    document.setDocumentSection(null);
                }
            }
            this.documents = documents;
        } else {
            this.documents = new ArrayList<>();
        }
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public Document getDocument(String hash){
        if (this.documents != null){
            for (Document document : this.documents){
                if (document.getDocument_md5_hash_code() != null && document.getDocument_md5_hash_code().equals(hash)){
                    return document;
                } else if (document.getDocument_md5_hash_code() == null){
                    return document;
                }
            }
        }
        if (this.duplicatedDocuments != null){
            for (Document document : this.duplicatedDocuments){
                if (document.getDocument_md5_hash_code() != null && document.getDocument_md5_hash_code().equals(hash)){
                    return document;
                } else if (document.getDocument_md5_hash_code() == null){
                    return document;
                }
            }
        }
        return null;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public List<DocumentSection> getDocumentSections() {
        return documentSections;
    }

    public void setDocumentSections(List<DocumentSection> documentSections) {
        this.documentSections = documentSections;
    }

    public List<String> getUnknownDomains() {
        return unknownDomains;
    }

    public void setUnknownDomains(List<String> unknownDomains) {
        this.unknownDomains = unknownDomains;
    }

    public void addUnknownDomain(String domainKeyword){
        if (this.unknownDomains == null){
            this.unknownDomains = new ArrayList<>();
        }
        this.unknownDomains.add(domainKeyword);
    }

    public List<Document> getIgnoredDocuments() {
        return ignoredDocuments;
    }

    public void setIgnoredDocuments(List<Document> ignoredDocuments) {
        this.ignoredDocuments = ignoredDocuments;
    }

    public void addIgnoredDocument(Document document){
        if (this.ignoredDocuments == null){
            this.ignoredDocuments = new ArrayList<>();
        }
        this.ignoredDocuments.add(document);
    }

    public Document getIgnoredDocument(String hash){
        if (this.ignoredDocuments != null){
            for (Document document : this.ignoredDocuments){
                if (document.getDocument_md5_hash_code() != null && document.getDocument_md5_hash_code().equals(hash)){
                    return document;
                } else if (document.getDocument_md5_hash_code() == null){
                    return document;
                }
            }
        }
        return null;
    }

    public List<Document> getDuplicatedDocuments() {
        return duplicatedDocuments;
    }

    public void setDuplicatedDocuments(List<Document> duplicatedDocuments) {
        this.duplicatedDocuments = duplicatedDocuments;
    }

    public void addDuplicatedDocument(Document document){
        if (this.duplicatedDocuments == null){
            this.duplicatedDocuments = new ArrayList<>();
        }
        this.duplicatedDocuments.add(document);
    }

    public List<DocumentSection> getIgnoredSections() {
        return ignoredSections;
    }

    public void setIgnoredSections(List<DocumentSection> ignoredSections) {
        this.ignoredSections = ignoredSections;
    }

    public void addIgnoredSection(DocumentSection section){
        if (this.ignoredSections == null){
            this.ignoredSections = new ArrayList<>();
        }
        this.ignoredSections.add(section);
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        this.imported = imported;
    }
}
