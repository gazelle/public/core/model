package net.ihe.gazelle.tf.model.reversed;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DomainProfileId implements java.io.Serializable {

    private static final long serialVersionUID = -3847051230185729149L;

    @Column(name = "domain_id", nullable = false)
    private int domainId;

    @Column(name = "integration_profile_id", nullable = false)
    private int profileId;

    public DomainProfileId() {
        super();
    }

    public int getDomainId() {
        return domainId;
    }

    public void setDomainId(int domainId) {
        this.domainId = domainId;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + domainId;
        result = (prime * result) + profileId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DomainProfileId other = (DomainProfileId) obj;
        if (domainId != other.domainId) {
            return false;
        }
        if (profileId != other.profileId) {
            return false;
        }
        return true;
    }

}
