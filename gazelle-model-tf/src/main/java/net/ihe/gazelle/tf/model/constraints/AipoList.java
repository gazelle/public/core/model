package net.ihe.gazelle.tf.model.constraints;

import net.ihe.gazelle.tf.model.ActorIntegrationProfileOption;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aipoList", propOrder = {
        "or",
        "aipoCriterions"
})
@XmlRootElement(name = "aipoList")

@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class AipoList extends AipoCriterion {


    @XmlElementWrapper(name = "aipoCriterions")
    @XmlElementRefs({
            @XmlElementRef(name = "aipoCriterion")})

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable(name = "tf_rule_list_criterion")
    @GenericGenerator(name = "tf_rule_list_criterion_id_generator", strategy = "increment")
    @CollectionId(columns = @Column(name = "tf_rule_list_criterion_id"), type = @Type(type = "int"), generator = "tf_rule_list_criterion_id_generator")
    @Fetch(value = FetchMode.SELECT)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<AipoCriterion> aipoCriterions;

    @XmlElement(required = true)
    @Column(name = "list_or")
    private boolean or;

    public AipoList() {
        super();
        aipoCriterions = new ArrayList<AipoCriterion>();
    }

    public List<AipoCriterion> getAipoCriterions() {
        return aipoCriterions;
    }

    public void setAipoCriterions(List<AipoCriterion> aipoCriterions) {
        this.aipoCriterions = aipoCriterions;
    }

    public boolean isOr() {
        return or;
    }

    public void setOr(boolean or) {
        this.or = or;
    }

    @Override
    public AipoCriterion simplify() {
        List<AipoCriterion> newCriterions = new ArrayList<AipoCriterion>();
        for (AipoCriterion aipoCriterion : aipoCriterions) {
            newCriterions.add(aipoCriterion.simplify());
        }

        if (newCriterions.size() == 0) {
            throw new RuntimeException();
        } else if (newCriterions.size() == 1) {
            return newCriterions.get(0);
        } else {
            List<AipoCriterion> newnewCriterions = new ArrayList<AipoCriterion>();
            for (AipoCriterion aipoCriterion : newCriterions) {
                if (aipoCriterion instanceof AipoList) {
                    AipoList aipoList = (AipoList) aipoCriterion;
                    if (aipoList.isOr() == isOr()) {
                        newnewCriterions.addAll(aipoList.getAipoCriterions());
                    } else {
                        newnewCriterions.add(aipoList);
                    }
                } else {
                    newnewCriterions.add(aipoCriterion);
                }
            }
            setAipoCriterions(newnewCriterions);
            return this;
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        appendSimple(result);
        return result.toString();
    }

    @Override
    public void appendSimple(StringBuilder sb) {
        boolean first = true;
        for (AipoCriterion aipoCriterion : aipoCriterions) {
            if (first) {
                first = false;
            } else {
                if (or) {
                    sb.append(" or ");
                } else {
                    sb.append(" and ");
                }
            }
            boolean brackets = false;
            if ((aipoCriterion instanceof AipoList) && (((AipoList) aipoCriterion).getAipoCriterions().size() > 1)) {
                brackets = true;
            }

            if (brackets) {
                sb.append("(");
            }
            aipoCriterion.appendSimple(sb);
            if (brackets) {
                sb.append(")");
            }
        }
    }

    @Override
    public AipoCriterion matches(List<ActorIntegrationProfileOption> aipos) {
        if (or) {
            for (AipoCriterion criterion : aipoCriterions) {
                boolean matches = (criterion.matches(aipos) != null);
                if (matches) {
                    return criterion;
                }
            }
            return null;
        } else {
            for (AipoCriterion criterion : aipoCriterions) {
                boolean matches = (criterion.matches(aipos) != null);
                if (!matches) {
                    return null;
                }
            }
            return this;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = (prime * result) + ((aipoCriterions == null) ? 0 : aipoCriterions.hashCode());
        result = (prime * result) + (or ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AipoList other = (AipoList) obj;
        if (aipoCriterions == null) {
            if (other.aipoCriterions != null) {
                return false;
            }
        } else if (!aipoCriterions.equals(other.aipoCriterions)) {
            return false;
        }
        if (or != other.or) {
            return false;
        }
        return true;
    }

}
