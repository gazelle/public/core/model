package net.ihe.gazelle.tf.model.auditMessage;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.DocumentSection;
import net.ihe.gazelle.tf.model.Transaction;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "auditMessage")
@XmlAccessorType(XmlAccessType.PROPERTY)
@Entity
@Table(name = "tf_audit_message", schema = "public")
@SequenceGenerator(name = "tf_audit_message_sequence", sequenceName = "tf_audit_message_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class AuditMessage extends AuditedObject implements Serializable {


    private static final long serialVersionUID = -416498064616245226L;
    private static Logger log = LoggerFactory.getLogger(AuditMessage.class);

    @NotNull
    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_audit_message_sequence")
    private Integer id;

    @Column(name = "audited_event")
    private TriggerEvent auditedEvent;

    @Column(name = "oid")
    private String oid;

    @ManyToOne
    @JoinColumn(name = "audited_transaction")
    private Transaction auditedTransaction;

    @Transient
    private String transactionKeyword;

    @ManyToOne
    @JoinColumn(name = "issuing_actor")
    private Actor issuingActor;

    @ManyToOne
    @JoinColumn(name = "document_section")
    @Fetch(value = FetchMode.SELECT)
    private DocumentSection documentSection;

    @Column(name = "comment")
    private String comment;

    @Column(name = "event_code_type")
    private String eventCodeType;

    @XmlTransient
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TriggerEvent getAuditedEvent() {
        return auditedEvent;
    }

    public void setAuditedEvent(TriggerEvent auditedEvent) {
        this.auditedEvent = auditedEvent;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @XmlTransient
    public Transaction getAuditedTransaction() {
        return auditedTransaction;
    }

    public void setAuditedTransaction(Transaction auditedTransaction) {
        this.auditedTransaction = auditedTransaction;
    }

    @XmlElement(name="transactionKeyword")
    public String getTransactionKeyword() {
        if (this.transactionKeyword == null){
            if (this.auditedTransaction != null){
                this.transactionKeyword = auditedTransaction.getKeyword();
            } else {
                this.transactionKeyword = null;
            }
        }
        return transactionKeyword;
    }

    public void refreshTransactionKeyword(){
        transactionKeyword = null;
        getTransactionKeyword();
    }

    public void setTransactionKeyword(String transactionKeyword) {
        this.transactionKeyword = transactionKeyword;
    }


    public Actor getIssuingActor() {
        return issuingActor;
    }

    public void setIssuingActor(Actor issuingActor) {
        this.issuingActor = issuingActor;
    }

    public DocumentSection getDocumentSection() {
        return documentSection;
    }

    public void setDocumentSection(DocumentSection documentSection) {
        this.documentSection = documentSection;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEventCodeType() {
        return eventCodeType;
    }

    public void setEventCodeType(String eventCodeType) {
        this.eventCodeType = eventCodeType;
    }

    public void saveOrMerge(EntityManager entityManager, Transaction transaction){

        this.setAuditedTransaction(transaction);
        if (issuingActor != null){
            issuingActor.setLastModifierId(this.lastModifierId);
            issuingActor.saveOrMerge(entityManager);
        }

        if (documentSection != null) {
            if (documentSection.emptyDocumentSection()) {
                documentSection = null;
            } else {
                documentSection.setLastModifierId(this.lastModifierId);
                documentSection.saveOrMerge(entityManager);
            }
        }

        AuditMessageQuery query = new AuditMessageQuery(entityManager);
        query.auditedEvent().eq(this.auditedEvent);
        query.oid().eq(this.oid);
        query.auditedTransaction().eq(this.auditedTransaction);
        query.issuingActor().eq(this.issuingActor);
        query.documentSection().eq(this.documentSection);
        query.comment().eq(this.comment);
        query.eventCodeType().eq(this.eventCodeType);
        List<AuditMessage> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        entityManager.flush();

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName() + " saved " + this);
        }
    }
}
