package net.ihe.gazelle.tf.model.constraints.export;

import net.ihe.gazelle.tf.model.constraints.AipoRule;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="aipoRules")
@XmlAccessorType(XmlAccessType.FIELD)
public class AipoRulesIE {

//    @XmlElementWrapper(name = "aipoRulesList")
    @XmlElementRefs({@XmlElementRef(name = "aipoRule")})
    private List<AipoRule> aipoRules;

    @XmlTransient
    private List<AipoRule> duplicatedRules;

    @XmlTransient
    private List<String> ignoredRules;

    @XmlTransient
    private List<String> unknownActors;

    @XmlTransient
    private List<String> unknownIntegrationProfiles;

    @XmlTransient
    private List<String> unknownOptions;

    @XmlTransient
    private boolean imported;

    public AipoRulesIE(){
        aipoRules = new ArrayList<AipoRule>();
    }

    public AipoRulesIE(List<AipoRule> aipoRules){
        if (aipoRules != null){
            this.aipoRules = aipoRules;
        }
    }

    public List<AipoRule> getDuplicatedRules() {
        return duplicatedRules;
    }

    public void setDuplicatedRules(List<AipoRule> duplicatedRules) {
        this.duplicatedRules = duplicatedRules;
    }

    public void addDuplicatedRules(AipoRule duplicatedRule){
        if (this.duplicatedRules != null){
            this.duplicatedRules.add(duplicatedRule);
        } else {
            this.duplicatedRules = new ArrayList<AipoRule>();
            this.duplicatedRules.add(duplicatedRule);
        }
    }

    public List<AipoRule> getAipoRules() {
        return aipoRules;
    }

    public void setAipoRules(List<AipoRule> aipoRules){
        this.aipoRules = aipoRules;
    }

    public List<String> getIgnoredRules(){
        return ignoredRules;
    }

    public void setIgnoredRules(List<String> ignoredRules) {
        this.ignoredRules = ignoredRules;
    }

    public void addIgnoredRule(String ignoredRule){
        if (this.ignoredRules != null){
            this.ignoredRules.add(ignoredRule);
        } else {
            this.ignoredRules = new ArrayList<String>();
            this.ignoredRules.add(ignoredRule);
        }
    }

    public List<String> getUnknownActors() {
        return unknownActors;
    }

    public void setUnknownActors(List<String> unknownActors) {
        this.unknownActors = unknownActors;
    }

    public void addUnknownActor(String actor){
        if (this.unknownActors != null){
            this.unknownActors.add(actor);
        } else {
            this.unknownActors = new ArrayList<String>();
            this.unknownActors.add(actor);
        }
    }

    public List<String> getUnknownIntegrationProfiles() {
        return unknownIntegrationProfiles;
    }

    public void setUnknownIntegrationProfiles(List<String> unknownIntegrationProfiles) {
        this.unknownIntegrationProfiles = unknownIntegrationProfiles;
    }

    public void addUnknownIntegrationProfile(String integrationProfile){
        if (this.unknownIntegrationProfiles != null){
            this.unknownIntegrationProfiles.add(integrationProfile);
        } else {
            this.unknownIntegrationProfiles = new ArrayList<String>();
            this.unknownIntegrationProfiles.add(integrationProfile);
        }
    }

    public List<String> getUnknownOptions() {
        return unknownOptions;
    }

    public void setUnknownOptions(List<String> unknownOptions) {
        this.unknownOptions = unknownOptions;
    }

    public void addUnknownOption(String option){
        if (this.unknownOptions != null){
            this.unknownOptions.add(option);
        } else {
            this.unknownOptions = new ArrayList<String>();
            this.unknownOptions.add(option);
        }
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        this.imported = imported;
    }
}
