package net.ihe.gazelle.tf.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@XmlRootElement(name = "DocumentSection")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_document_sections", uniqueConstraints = @UniqueConstraint(columnNames = {"section", "document_id"}))
@SequenceGenerator(name = "tf_document_sections_sequence", sequenceName = "tf_document_sections_id_seq")
@Exchanged(set = SetTechnicalFramework.class)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class DocumentSection extends AuditedObject implements java.io.Serializable {


    private static final long serialVersionUID = 9000950401194780978L;

    private static Logger log = LoggerFactory.getLogger(DocumentSection.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_document_sections_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "section")
    private String section;

    @ManyToOne
    @JoinColumn(name = "document_id")
    @Fetch(value = FetchMode.SELECT)
    private Document document;

    @Column(name = "type")
    private DocumentSectionType type;

    @OneToMany(targetEntity = IntegrationProfile.class, mappedBy = "documentSection", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private List<IntegrationProfile> integrationProfile;

    @OneToMany(targetEntity = Transaction.class, mappedBy = "documentSection", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private List<Transaction> transaction;

    @OneToMany(targetEntity = ActorIntegrationProfileOption.class, mappedBy = "documentSection", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private List<ActorIntegrationProfileOption> actorIntegrationProfileOption;

    public DocumentSectionType getType() {
        return type;
    }

    public void setType(DocumentSectionType type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    @FilterLabel
    public String getSectionHumanized() {
        String result = "";
        Pattern p = Pattern.compile("[a-zA-Z]*([0-9]*_[0-9]+)*");
        Matcher match = p.matcher(section);
        if (match.find()) {
            result = section.substring(match.start(), match.end()) + section.substring(match.end()).replace("_", " ");
        }
        return result;
    }

    public Document getDocument() {
        return HibernateHelper.getLazyValue(this, "document", this.document);
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public String getPdfReference() {
        return this.getDocument().getUrl() + "#" + this.section;
    }

    public String getReferenceTitle() {
        return this.getDocument().getName() + " #" + this.getSectionHumanized();
    }

    public boolean isLinkedToOtherEntities() {
        return isLinkedToATransaction() || isLinkedToAnIntegrationProfile()
                || isLinkedToAnActorIntegrationProfileOption();
    }

    public boolean isLinkedToATransaction() {
        return checkIfObjectIsLinkedTo(getTransaction());
    }

    public boolean isLinkedToAnIntegrationProfile() {
        return checkIfObjectIsLinkedTo(getIntegrationProfile());
    }

    public boolean isLinkedToAnActorIntegrationProfileOption() {
        return checkIfObjectIsLinkedTo(getActorIntegrationProfileOption());
    }

    public <T> boolean checkIfObjectIsLinkedTo(List<T> to_check) {
        boolean result;
        if (to_check == null) {
            result = false;
        } else if (to_check.size() == 0) {
            result = false;
        } else {
            result = true;
        }
        return result;
    }

    public List<IntegrationProfile> getIntegrationProfile() {
        return HibernateHelper.getLazyValue(this, "integrationProfile", this.integrationProfile);
    }

    public List<Transaction> getTransaction() {
        return HibernateHelper.getLazyValue(this, "transaction", this.transaction);
    }

    public List<ActorIntegrationProfileOption> getActorIntegrationProfileOption() {
        return HibernateHelper.getLazyValue(this, "actorIntegrationProfileOption", this.actorIntegrationProfileOption);
    }

    public void setActorIntegrationProfileOption(List<ActorIntegrationProfileOption> actorIntegrationProfileOption) {
        this.actorIntegrationProfileOption = actorIntegrationProfileOption;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        if (!emptyDocumentSection()) {
            if (document != null) {
                document.setLastModifierId(this.lastModifierId);
                document.saveOrMerge(entityManager);
            }
            DocumentSectionQuery query = new DocumentSectionQuery();
            query.section().eq(this.section);
            query.document().eq(this.document);
            List<DocumentSection> listDistinct;
            listDistinct = query.getListDistinct();
            if (listDistinct.size() == 1) {
                this.id = listDistinct.get(0).id;
                entityManager.merge(this);
            } else {
                entityManager.persist(this);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

    public boolean emptyDocumentSection() {
        if ((section == null) && (type == null) && (document == null)) {
            return true;
        }
        return false;
    }
}
