/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import net.ihe.gazelle.tf.model.auditMessage.AuditMessage;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.commons.lang.ObjectUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <b>Class Description : </b>Transaction<br>
 * <br>
 * This class describes the Transaction object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * Transaction possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Transaction</li>
 * <li><b>keyword</b> : keyword corresponding to the Transaction</li>
 * <li><b>name</b> : name corresponding to the Transaction</li>
 * <li><b>description</b> : description corresponding to the Transaction</li>
 * </ul>
 * </br> <b>Example of Transaction</b> : "RAD-2	: Placer Order Management" is an IHE Transaction<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 21
 * @class Transaction.java
 * @package net.ihe.gazelle.tf.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "transaction")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Audited
@Table(name = "tf_transaction", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"keyword"}))
@SequenceGenerator(name = "tf_transaction_sequence", sequenceName = "tf_transaction_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class Transaction extends AuditedObject implements java.io.Serializable, Comparable<Transaction> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -5994534457658398088L;

    private static Logger log = LoggerFactory.getLogger(Transaction.class);

    // ******************************************************** column variables
    @Column(name = "keyword", unique = true, nullable = false, length = 128)
    @NotNull
    @XmlElement(name = "keyword")
    protected String keyword;
    @Column(name = "name", length = 128)
    @XmlElement(name = "name")
    protected String name;

    // ************************************************** Transaction status type
    @Column(name = "description")
    @Size(max = 2048)
    @XmlElement(name = "description")
    protected String description;

    // ****************************************** Variables used for Foreign Keys

    // --------------------------------------------------- transaction_link table
    // ---------------------------------------------------------------- row id
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_transaction_sequence")
    @XmlElement(name = "id")
    private Integer id;
    // ------------------------------------------------------------- TF reference
    @Column(name = "reference", length = 2048, nullable = true)
    private String reference;
    @ManyToOne(targetEntity = net.ihe.gazelle.tf.model.TransactionStatusType.class)
    @JoinColumn(name = "transaction_status_type_id")
    @Fetch(value = FetchMode.SELECT)
    @XmlElement(name = "transactionStatusType")
    private TransactionStatusType transactionStatusType;
    @OneToMany(mappedBy = "transaction", targetEntity = TransactionLink.class)
    @AuditJoinTable
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlElement(name = "transactionLinks")
    private List<TransactionLink> transactionLinks;
    // ---------------------------------------------------- tf_profile_link table
    @OneToMany(mappedBy = "transaction", targetEntity = ProfileLink.class)
    @AuditJoinTable
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    // Crash during Sync -- Remove @NotAudited
    private List<ProfileLink> profileLinks;
    @OneToMany(mappedBy = "transaction", targetEntity = WSTransactionUsage.class)
    @AuditJoinTable
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<WSTransactionUsage> wsTransactionUsages;
    @ManyToOne
    @JoinColumn(name = "documentSection")
    @Fetch(value = FetchMode.SELECT)
    @XmlTransient
    private DocumentSection documentSection;

    @OneToMany(mappedBy = "auditedTransaction", targetEntity = AuditMessage.class)
    @NotAudited
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<AuditMessage> auditMessages;

    @ManyToMany
    @JoinTable(name = "tf_transaction_standard", joinColumns = @JoinColumn(name = "transaction_id"),
            inverseJoinColumns = @JoinColumn(name = "standard_id"),
            uniqueConstraints = @UniqueConstraint(columnNames = {"standard_id", "transaction_id"}))
    @Fetch(value = FetchMode.SELECT)
    @AuditJoinTable
    private List<Standard> standards;

    // Constructors
    public Transaction() {
        super();
    }

    public Transaction(String keyword, String name, String description) {
        // super( keyword , name , description ) ;
        this.keyword = keyword;
        this.name = name;
        this.description = description;
    }

    public Transaction(String keyword, String name, String description, String reference) {

        // super( keyword , name , description ) ;
        this.keyword = keyword;
        this.name = name;
        this.description = description;

        this.reference = reference;
    }

    public static List<Transaction> getAllTransactions() {
        TransactionQuery query = new TransactionQuery();
        List<Transaction> transactions = query.getList();
        if (transactions.size() > 0) {
            Collections.sort(transactions);
            return transactions;
        }
        return null;
    }

    public static List<Transaction> ListAllTransactions() {
        return AuditedObject.listAllObjectsOrderByKeyword(Transaction.class);
    }

    public static Transaction GetTransactionByKeyword(String inKeyword) {
        return AuditedObject.getObjectByKeyword(Transaction.class, inKeyword);
    }

    public DocumentSection getDocumentSection() {
        return documentSection;
    }

    public void setDocumentSection(DocumentSection documentSection) {
        this.documentSection = documentSection;
    }

    public List<WSTransactionUsage> getWsTransactionUsages() {
        return wsTransactionUsages;
    }

    public void setWsTransactionUsages(List<WSTransactionUsage> wsTransactionUsages) {
        this.wsTransactionUsages = wsTransactionUsages;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String r) {
        reference = r;
    }

    public List<TransactionLink> getTransactionLinks() {
        return transactionLinks;
    }

    public void setTransactionLinks(List<TransactionLink> t) {
        this.transactionLinks = t;
    }

    public void addTransactionLink(TransactionLink l) {
        transactionLinks.add(l);
    }

    public void delTransactionLink(TransactionLink l) {
        transactionLinks.remove(l);
    }

    public List<ProfileLink> getProfileLinks() {
        return profileLinks;
    }

    public void setProfileLinks(List<ProfileLink> l) {
        profileLinks = l;
    }

    public void addProfileLink(ProfileLink l) {
        profileLinks.add(l);
    }

    public void delProfileLink(ProfileLink l) {
        profileLinks.remove(l);
    }

    public TransactionStatusType getTransactionStatusType() {
        return transactionStatusType;
    }

    public void setTransactionStatusType(TransactionStatusType t) {
        transactionStatusType = t;
    }

    @FilterLabel
    public String getSelectableLabel() {
        return getKeyword() + " - " + getName();
    }

    public int compare(Transaction o1, Transaction o2) {
        return o1.getKeyword().compareToIgnoreCase(o2.getKeyword());
    }

    @Override
    public int compareTo(Transaction o) {
        return this.getKeyword().compareTo(o.getKeyword());
    }

    public Transaction saveOrMerge(EntityManager entityManager) {
        this.id = null;
        Transaction result;
        final Ehcache cache = CacheManager.getInstance().getEhcache("TestImportCache");
        final String cacheId = cacheId(this.getClass().getName(), this.keyword);
        if (cache != null) {
            Element element = cache.get(cacheId);
            if (element == null) {

                save_(entityManager);
                result = this;
                Element elm = new Element(cacheId, this.id);
                elm.setTimeToLive(60);
                cache.put(elm);
            } else {
                result = entityManager.find(Transaction.class, element.getObjectValue());
            }
        } else {

            save_(entityManager);
            result = this;
        }
        return result;
    }

    private void save_(EntityManager entityManager) {
        if (transactionStatusType != null) {
            transactionStatusType.setLastModifierId(this.lastModifierId);
            transactionStatusType.saveOrMerge(entityManager);
        }
        if (documentSection != null) {
            if (documentSection.emptyDocumentSection()) {
                documentSection = null;
            } else {
                documentSection.setLastModifierId(this.lastModifierId);
                documentSection.saveOrMerge(entityManager);
            }
        }

        List<TransactionLink> transactionLinksBackup = (List<TransactionLink>) ObjectUtils.clone(transactionLinks);
        transactionLinks = null;

        List<Standard> standardsBackup;
        if (standards != null) {
            standardsBackup = new ArrayList<>(standards.size());
            standardsBackup.addAll(standards);
        } else {
            standardsBackup = (List<Standard>) ObjectUtils.clone(standards);
        }
        standards = null;

        List<AuditMessage> auditMessagesBackup = (List<AuditMessage>) ObjectUtils.clone(auditMessages);
        auditMessages = null;
        List<ProfileLink> profileLinksBackup = (List<ProfileLink>) ObjectUtils.clone(profileLinks);
        profileLinks = null;

        TransactionQuery query = new TransactionQuery(entityManager);
        query.keyword().eq(this.keyword);
        List<Transaction> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }

        if (transactionLinksBackup != null) {
            for (TransactionLink transactionLink : transactionLinksBackup) {
                transactionLink.setLastModifierId(this.lastModifierId);
                transactionLink.saveOrMerge(entityManager, this);
            }
        }
        this.transactionLinks = transactionLinksBackup;

        if (standardsBackup != null) {
            for (Standard standard : standardsBackup) {
                standard.setLastModifierId(this.lastModifierId);
                entityManager.merge(standard);
            }
        }
        this.standards = standardsBackup;

        if (auditMessagesBackup != null) {
            for (AuditMessage auditMessage : auditMessagesBackup) {
                auditMessage.setLastModifierId(this.lastModifierId);
                auditMessage.saveOrMerge(entityManager, this);
            }
        }
        this.auditMessages = auditMessagesBackup;

        if (profileLinksBackup != null) {
            for (ProfileLink profileLink : profileLinksBackup) {
                profileLink.setLastModifierId(this.lastModifierId);
                entityManager.merge(profileLink);
            }
        }
        this.profileLinks = profileLinksBackup;


        entityManager.merge(this);
        entityManager.flush();

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName() + " saved " + this);
        }
    }

    private String cacheId(String cacheKeyPrefix, String objectId) {
        return cacheKeyPrefix + ":" + objectId;
    }

    public List<Standard> getStandards() {
        return standards;
    }

    public void setStandards(List<Standard> standards) {
        this.standards = standards;
    }

    public List<AuditMessage> getAuditMessages() {
        return auditMessages;
    }

    public void setAuditMessages(List<AuditMessage> auditMessages) {
        this.auditMessages = auditMessages;
    }
}
