package net.ihe.gazelle.tf.model;

public enum DocumentLifeCycleStatus {
    DRAFT("Draft", "Draft"),
    FINAL_TEXT("Final text", "FT"),
    PUBLIC_COMMENT("Public comment", "PC"),
    TRIAL_IMPLEMENTATION("Trial implementation", "TI"),
    DEPRECATED("Deprecated", "Deprecated");

    private String friendlyName;

    private String keyword;

    DocumentLifeCycleStatus(String friendlyName, String keyword) {
        this.friendlyName = friendlyName;
        this.keyword = keyword;
    }

    public static DocumentLifeCycleStatus valueOfByKeyword(String keyword) throws IllegalArgumentException {
        for (DocumentLifeCycleStatus value : values()) {
            String docKeyword = value.getKeyword();
            if (docKeyword.equals(keyword)) {
                return value;
            }
        }
        throw new IllegalArgumentException("keyword " + keyword + "is not defined in DocumentLifeCycleStatus");
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getKeyword() {
        return keyword;
    }
}
