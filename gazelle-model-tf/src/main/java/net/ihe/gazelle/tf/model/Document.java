package net.ihe.gazelle.tf.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "Document")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_document")
@SequenceGenerator(name = "tf_document_sequence", sequenceName = "tf_document_id_seq")
@Exchanged(set = SetTechnicalFramework.class)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Document extends AuditedObject implements java.io.Serializable {

    private static final long serialVersionUID = 2193622166008093482L;
    private static Logger log = LoggerFactory.getLogger(Document.class);
    @OneToMany(targetEntity = DocumentSection.class, mappedBy = "document", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    protected List<DocumentSection> documentSection;
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_document_sequence")
    @XmlTransient
    private Integer id;
    @NotNull
    @Column(name = "document_name")
    private String name;
    @Column(name = "document_title")
    private String title;
    @Column(name = "document_revision")
    private String revision;
    @NotNull
    @Column(name = "document_type")
    private DocumentType type;
    @NotNull
    @Column(name = "document_url")
    private String url;
    @Column(name = "document_volume")
    private String volume;
    @ManyToOne
    @NotNull
    @JoinColumn(name = "document_domain_id")
    @Fetch(value = FetchMode.SELECT)
    private Domain domain;
    @Column(name = "document_linkstatus")
    private Integer linkStatus;
    @Column(name = "document_linkstatusdescription")
    private String linkStatusDescription;
    @Column(name = "document_lifeCyclestatus")
    private DocumentLifeCycleStatus lifecyclestatus;
    @NotNull
    @Column(name = "md5_hash_code", unique = true)
    @XmlElement(required = true)
    private String document_md5_hash_code;
    @Column(name = "document_dateOfPublication")
    private Date dateOfpublication;

    public Document() {
        super();
    }

    public Document(Document doc) {
        super();
        name = doc.name;
        title = doc.title;
        revision = doc.revision;
        type = doc.type;
        url = doc.url;
        volume = doc.volume;
        lifecyclestatus = doc.lifecyclestatus;
        domain = doc.domain;
        dateOfpublication = doc.dateOfpublication;
    }

    public String getTitle() {
        if (title != null) {
            return title.replace("_", " ");
        } else {
            return title;
        }
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<DocumentSection> getDocumentSection() {
        return HibernateHelper.getLazyValue(this, "documentSection", this.documentSection);
    }

    public void setDocumentSection(List<DocumentSection> documentSection) {
        this.documentSection = documentSection;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @FilterLabel
    public String getName() {
        if (name != null) {
            return name.replace("_", " ");
        } else {
            return name;
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public Integer getLinkStatus() {
        return linkStatus;
    }

    public void setLinkStatus(Integer linkStatus) {
        this.linkStatus = linkStatus;
    }

    public DocumentLifeCycleStatus getLifecyclestatus() {
        return lifecyclestatus;
    }

    public void setLifecyclestatus(DocumentLifeCycleStatus lifecyclestatus) {
        this.lifecyclestatus = lifecyclestatus;
    }

    public String getDocument_md5_hash_code() {
        return document_md5_hash_code;
    }

    public void setDocument_md5_hash_code(String document_md5_hash_code) {
        this.document_md5_hash_code = document_md5_hash_code;
    }

    public Date getDateOfpublication() {
        return dateOfpublication;
    }

    public void setDateOfpublication(Date dateOfpublication) {
        this.dateOfpublication = dateOfpublication;
    }

    public String getLinkStatusDescription() {
        return linkStatusDescription;
    }

    public void setLinkStatusDescription(String linkStatusDescription) {
        this.linkStatusDescription = linkStatusDescription;
    }

    public boolean isNotLinkedToOtherEntities() {
        return !hasDocumentSectionsLinkedToOtherEntities();
    }

    public boolean hasDocumentSectionsLinkedToOtherEntities() {
        boolean linkedToEntities = false;
        if (getDocumentSection() != null) {
            for (DocumentSection section : getDocumentSection()) {
                if (section.isLinkedToOtherEntities()) {
                    linkedToEntities = true;
                    break;
                }
            }
        }
        return linkedToEntities;
    }

    public List<Transaction> getTransactionsLinked() {
        TransactionQuery query = new TransactionQuery();
        query.documentSection().document().id().eq(this.id);
        List<Transaction> result = query.getListDistinct();
        return result;
    }

    public List<IntegrationProfile> getIntegrationProfileLinked() {
        IntegrationProfileQuery query = new IntegrationProfileQuery();
        query.documentSection().document().id().eq(this.id);
        List<IntegrationProfile> result = query.getListDistinct();
        return result;
    }

    public List<ActorIntegrationProfileOption> getAIPOLinked() {
        ActorIntegrationProfileOptionQuery query = new ActorIntegrationProfileOptionQuery();
        query.documentSection().document().id().eq(this.id);
        List<ActorIntegrationProfileOption> result = query.getListDistinct();
        return result;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        domain.setLastModifierId(this.lastModifierId);
        domain.saveOrMerge(entityManager);

        DocumentQuery query = new DocumentQuery();
        query.document_md5_hash_code().eq(this.document_md5_hash_code);
        List<Document> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }
}
