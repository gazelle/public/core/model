/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

// JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * <b>Class Description : </b>IntegrationProfileOption<br>
 * <br>
 * This class describes the Integration profile option object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * IntegrationProfileOption possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Integration profile option</li>
 * <li><b>keyword</b> : keyword corresponding to the Integration profile option</li>
 * <li><b>description</b> : description corresponding to the Integration profile option</li>
 * <li><b>firstDefined</b> : describes the references to the Technical Framework document (example : a chapter or ITI TF-1:9.5)</li>
 * </ul>
 * </br> <b>Example of Integration profile option</b> : "ECHO : Echocardiography" is an Integration profile option<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 21
 * @class IntegrationProfileOption.java
 * @package net.ihe.gazelle.tf.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "IntegrationProfileOption")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_integration_profile_option", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "tf_integration_profile_option_sequence", sequenceName = "tf_integration_profile_option_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class IntegrationProfileOption extends AuditedObject implements java.io.Serializable,
        Comparable<IntegrationProfileOption> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -410911131541283760L;

    private static IntegrationProfileOption noneOption;
    private static String nonOptionKeyword = "NONE";

    private static Logger log = LoggerFactory.getLogger(IntegrationProfileOption.class);
    @Column(name = "keyword", unique = true, nullable = false, length = 128)
    @NotNull
    protected String keyword;
    @Column(name = "name", length = 128)
    protected String name;
    @Column(name = "description")
    @Size(max = 2048)
    protected String description;
    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_integration_profile_option_sequence")
    @XmlTransient
    private Integer id;
    @Column(name = "reference")
    private String reference;
    @OneToMany(mappedBy = "integrationProfileOption", targetEntity = ActorIntegrationProfileOption.class)
    @NotAudited
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private List<ActorIntegrationProfileOption> listOfActorIntegrationProfileOption;
    @Transient
    private boolean toDisplay = true;

    public IntegrationProfileOption() {
    }

    public IntegrationProfileOption(String keyword, String name, String description) {

        this.keyword = keyword;
        this.name = name;
        this.description = description;
    }

    public IntegrationProfileOption(String keyword, String name, String description, String reference) {

        this.keyword = keyword;
        this.name = name;
        this.description = description;
        this.reference = reference;
    }

    public IntegrationProfileOption(String keyword, String description) {
        this.keyword = keyword;
        this.description = description;
    }

    public IntegrationProfileOption(IntegrationProfileOption oldOption) {

        // super( oldOption ) ;
        if (oldOption.keyword != null) {
            this.keyword = new String(oldOption.keyword);
        }

        if (oldOption.name != null) {
            this.name = new String(oldOption.name);
        }

        if (oldOption.description != null) {
            this.description = new String(oldOption.description);
        }

        if (oldOption.reference != null) {
            this.reference = new String(oldOption.reference);
        }

    }

    public static IntegrationProfileOption getNoneOption() {
        if (noneOption == null) {
            noneOption = findIntegrationProfileOptionWithKeyword(nonOptionKeyword);
        }
        return noneOption;
    }

    public static IntegrationProfileOption findIntegrationProfileOptionWithKeyword(String keywordName) {
        return AuditedObject.getObjectByKeyword(IntegrationProfileOption.class, keywordName);
    }

    public static List<IntegrationProfileOption> getListOfIntegrationProfileOptions(Actor inActor,
                                                                                    IntegrationProfile inIP) {

        if ((inActor == null) || (inIP == null)) {
            return null;
        }

        if ((inActor.getId() == null) || (inIP.getId() == null)) {
            return null;
        }

        return ActorIntegrationProfileOption.getIntegrationProfileOptionFromAIPOFiltered(null, inIP, null, inActor);
    }

    // *********************************************************
    // Foreign Key for table : tf_actor_integration_profile_option
    // *********************************************************

    public static List<IntegrationProfileOption> GetAllIntegrationProfileOptions() {
        return AuditedObject.listAllObjectsOrderByKeyword(IntegrationProfileOption.class);
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getKeywordFiltered() {
        if (keyword.equals("NONE")) {
            return "";
        }

        return keyword;
    }

    public String getName() {
        return name;
    }

    // Constructors

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ActorIntegrationProfileOption> getListOfActorIntegrationProfileOption() {
        return listOfActorIntegrationProfileOption;
    }

    public List<ActorIntegrationProfileOption> getActorIntegrationProfileOption() {
        return listOfActorIntegrationProfileOption;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public void setActorIntegrationProfileOption(List<ActorIntegrationProfileOption> l) {
        listOfActorIntegrationProfileOption = l;
    }

    public void addActorIntegrationProfileOption(ActorIntegrationProfileOption aip) {
        listOfActorIntegrationProfileOption.add(aip);
    }

    public void delActorIntegrationProfileOption(ActorIntegrationProfileOption aip) {
        listOfActorIntegrationProfileOption.remove(aip);
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReference() {
        return this.reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public boolean getToDisplay() {
        return toDisplay;
    }

    public void setToDisplay(boolean toDisplay) {
        this.toDisplay = toDisplay;
    }

    @Override
    public String toString() {
        return description;
    }

    public String toStringFiltered() {
        if (description.equals("None")) {
            return "";
        }
        return description;
    }

    @FilterLabel
    public String getSelectableLabel() {
        if (getKeyword().toUpperCase().equals("NONE")) {
            return " None";
        }
        return getKeyword() + " - " + getName();
    }

    @Override
    public int compareTo(IntegrationProfileOption o) {
        if (o == null) {
            return -1;
        }
        if (o.getKeyword() == null) {
            return -1;
        }
        if (getKeyword() == null) {
            return 1;
        }
        return getKeyword().compareTo(o.getKeyword());
    }

    public int compare(IntegrationProfileOption o1, IntegrationProfileOption o2) {
        return o1.getKeyword().compareToIgnoreCase(o2.getKeyword());
    }

    public boolean isNotNone() {
        return !isNone();
    }

    public boolean isNone() {
        if (getKeyword().toUpperCase().equals("NONE")) {
            return true;
        } else {
            return false;
        }
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        IntegrationProfileOptionQuery query = new IntegrationProfileOptionQuery();

        query.addRestriction(HQLRestrictions.or(query.name().eqRestriction(this.name),
                query.keyword().eqRestriction(this.keyword)));

        List<IntegrationProfileOption> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }

    }
}
