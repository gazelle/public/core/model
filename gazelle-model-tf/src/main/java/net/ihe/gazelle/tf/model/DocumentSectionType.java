package net.ihe.gazelle.tf.model;

public enum DocumentSectionType {

    TITLE("Title"),
    TABLE("Table"),
    FIGURE("Figure");

    private String friendlyName;

    DocumentSectionType(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}
