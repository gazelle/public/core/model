package net.ihe.gazelle.tf.model.constraints;

import net.ihe.gazelle.tf.model.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.swing.text.html.Option;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "aipoSingle", propOrder = {
        "actorKeyword",
        "integrationProfileKeyword",
        "optionKeyword"
})
@XmlRootElement(name = "aipoSingle")

@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class AipoSingle extends AipoCriterion {

    @ManyToOne
    @JoinColumn(name = "single_actor_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
  //  @Cascade({CascadeType.MERGE})
    private Actor actor;

    @ManyToOne
    @JoinColumn(name = "single_integration_profile_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
   // @Cascade({CascadeType.MERGE})
    private IntegrationProfile integrationProfile;

    @ManyToOne
    @JoinColumn(name = "single_option_id", nullable = true)
    @Fetch(value = FetchMode.SELECT)
  //  @Cascade({CascadeType.MERGE})
    private IntegrationProfileOption option;

    @ManyToOne
    private AipoRule aipoRule;

    @Transient
    private String actorKeyword;

    @Transient
    private String integrationProfileKeyword;

    @Transient
    private String optionKeyword;

    @XmlTransient
    public Actor getActor() {
        return this.actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    @XmlTransient
    public IntegrationProfile getIntegrationProfile() {
        return integrationProfile;
    }

    public void setIntegrationProfile(IntegrationProfile integrationProfile) {
        this.integrationProfile = integrationProfile;
    }

    public IntegrationProfileOption getOption() {
        return this.option;
    }

    @XmlTransient
    public void setOption(IntegrationProfileOption option) {
        this.option = option;
    }

    @XmlTransient
    public AipoRule getAipoRule() {
        return aipoRule;
    }

    public void setAipoRule(AipoRule aipoRule) {
        this.aipoRule = aipoRule;
    }

    public List<AipoCriterion> getAipoCriterions() {
        return new ArrayList<AipoCriterion>();
    }

    @XmlAttribute(name="actorKeyword")
    public String getActorKeyword() {
        if (this.actorKeyword == null){
            if (this.actor != null){
                return actor.getKeyword();
            }
            return null;
        }
        return actorKeyword;
    }

    public void setActorKeyword(String actorKeyword) {
        this.actorKeyword = actorKeyword;
    }

    @XmlAttribute(name="integrationProfileKeyword")
    public String getIntegrationProfileKeyword() {
        if (this.integrationProfileKeyword == null){
            if (this.integrationProfile != null){
                return integrationProfile.getKeyword();
            }
            return null;
        }
        return integrationProfileKeyword;
    }

    public void setIntegrationProfileKeyword(String integrationProfileKeyword) {
        this.integrationProfileKeyword = integrationProfileKeyword;
    }

    @XmlAttribute(name="optionKeyword")
    public String getOptionKeyword() {
        if (optionKeyword == null){
            if (this.option != null){
                return option.getKeyword();
            }
            return null;
        }
        return optionKeyword;
    }

    public void setOptionKeyword(String optionKeyword) {
        this.optionKeyword = optionKeyword;
    }

    @Override
    public AipoCriterion simplify() {
        return this;
    }

    @Override
    public String toString() {
        String actorNull = StringUtils.trimToNull(getActorKeyword());
        String ipNull = StringUtils.trimToNull(getIntegrationProfileKeyword());
        String optionNull = StringUtils.trimToNull(getOptionKeyword());

        if ((actorNull != null) && (ipNull != null) && (optionNull != null)) {
            return actorNull + "/" + ipNull + "/" + optionNull;
        } else if ((actorNull != null) && (ipNull != null)) {
            return actorNull + "/" + ipNull;
        } else {
            if (actorNull == null) {
                actorNull = "*";
            }
            if (ipNull == null) {
                ipNull = "*";
            }
            if (optionNull == null) {
                optionNull = "*";
            }
            return actorNull + "/" + ipNull + "/" + optionNull;
        }
    }

    @Override
    public void appendSimple(StringBuilder sb) {
        sb.append(toString());
    }

    @Override
    public AipoCriterion matches(List<ActorIntegrationProfileOption> aipos) {
        String actorNull = StringUtils.trimToNull(getActorKeyword());
        String ipNull = StringUtils.trimToNull(getIntegrationProfileKeyword());
        String optionNull = StringUtils.trimToNull(getOptionKeyword());

        for (ActorIntegrationProfileOption actorIntegrationProfileOption : aipos) {
            boolean allMatch = true;
            if (allMatch && (actorNull != null)) {
                if (!actorIntegrationProfileOption.getActorIntegrationProfile().getActor().getKeyword()
                        .equals(actorNull)) {
                    allMatch = false;
                }
            }
            if (allMatch && (ipNull != null)) {
                if (!actorIntegrationProfileOption.getActorIntegrationProfile().getIntegrationProfile().getKeyword()
                        .equals(ipNull)) {
                    allMatch = false;
                }
            }
            if (allMatch && (optionNull != null)) {
                if (actorIntegrationProfileOption.getIntegrationProfileOption() == null) {
                    allMatch = false;
                } else if (!actorIntegrationProfileOption.getIntegrationProfileOption().getKeyword().equals(optionNull)) {
                    allMatch = false;
                }
            }
            if (allMatch) {
                return this;
            }
        }
        return null;
    }

    public List<ActorIntegrationProfileOption> getAipos() {
        ActorIntegrationProfileOptionQuery actorIntegrationProfileOptionQuery = new ActorIntegrationProfileOptionQuery();
        String actorNull = StringUtils.trimToNull(getActorKeyword());
        String ipNull = StringUtils.trimToNull(getIntegrationProfileKeyword());
        String optionNull = StringUtils.trimToNull(getOptionKeyword());
        if (actorNull != null) {
            actorIntegrationProfileOptionQuery.actorIntegrationProfile().actor().keyword().eq(actorNull);
        }
        if (ipNull != null) {
            actorIntegrationProfileOptionQuery.actorIntegrationProfile().integrationProfile().keyword().eq(ipNull);
        }
        if (optionNull != null) {
            actorIntegrationProfileOptionQuery.integrationProfileOption().keyword().eq(optionNull);
        }
        return actorIntegrationProfileOptionQuery.getList();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = (prime * result) + ((actor == null) ? 0 : actor.hashCode());
        result = (prime * result) + ((integrationProfile == null) ? 0 : integrationProfile.hashCode());
        result = (prime * result) + ((option == null) ? 0 : option.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AipoSingle other = (AipoSingle) obj;
        if (actor == null) {
            if (other.actor != null) {
                return false;
            }
        } else if (!actor.equals(other.actor)) {
            return false;
        }
        if (integrationProfile == null) {
            if (other.integrationProfile != null) {
                return false;
            }
        } else if (!integrationProfile.equals(other.integrationProfile)) {
            return false;
        }
        if (option == null) {
            if (other.option != null) {
                return false;
            }
        } else if (!option.equals(other.option)) {
            return false;
        }
        return true;
    }

}
