package net.ihe.gazelle.tf.model;

import java.util.Comparator;

/**
 * Use IntegrationProfileComparator to Sort Integration by keyword in alphabetical order
 */
public class IntegrationProfileComparator implements Comparator<IntegrationProfile> {

    /**
     * Method compare.
     * <p/>
     * Sort Integration by keyword in alphabetical order
     *
     * @param iP1 IntegrationProfile
     * @param iP2 IntegrationProfile
     * @return int
     */
    public int compare(IntegrationProfile iP1, IntegrationProfile iP2) {
        if (iP1.getKeyword().compareTo(iP2.getKeyword()) < 0) {
            return -1;
        } else if (iP1.getKeyword().compareTo(iP2.getKeyword()) > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}