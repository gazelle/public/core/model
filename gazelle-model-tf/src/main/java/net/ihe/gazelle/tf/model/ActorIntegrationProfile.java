/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.apache.commons.lang.ObjectUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>ActorIntegrationProfile<br>
 * <br>
 * This class describes the ActorIntegrationProfile object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * Actor possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the ActorIntegrationProfile</li>
 * <li><b>actor</b> : object corresponding to an Actor</li>
 * <li><b>integrationProfile</b> : object corresponding to an Integration Profile</li>
 * <li><b>description</b> : Comments/description corresponding to the ActorIntegrationProfile</li>
 * </ul>
 * </br> <b>Example of ActorIntegrationProfile</b> : ADT actor is mapped with SWF integration profile<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, January 23rd
 * @class ActorIntegrationProfile.java
 * @package net.ihe.gazelle.tf.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@XmlRootElement(name = "ActorIntegrationProfile")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_actor_integration_profile", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "actor_id", "integration_profile_id"}))
@SequenceGenerator(name = "tf_actor_integration_profile_sequence", sequenceName = "tf_actor_integration_profile_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class ActorIntegrationProfile extends AuditedObject implements java.io.Serializable, Cloneable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450911138458283760L;
    private static Logger log = LoggerFactory.getLogger(ActorIntegrationProfile.class);

    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_actor_integration_profile_sequence")
    @XmlTransient
    private Integer id;

    // Variables used for Foreign Keys : Actor
    @ManyToOne(targetEntity = net.ihe.gazelle.tf.model.Actor.class)
    @JoinColumn(name = "actor_id")
    @Fetch(value = FetchMode.SELECT)
    private Actor actor;

    // Variables used for Foreign Keys : IntegrationProfile
    @ManyToOne(targetEntity = net.ihe.gazelle.tf.model.IntegrationProfile.class)
    @JoinColumn(name = "integration_profile_id")
    @Fetch(value = FetchMode.SELECT)
    private IntegrationProfile integrationProfile;

    // Variable used for foreign keys: tf_profile_link table
    @OneToMany(mappedBy = "actorIntegrationProfile", targetEntity = ProfileLink.class)
    // @NotAudited
    @AuditJoinTable
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<ProfileLink> profileLinks;

    // Variables used for Foreign Keys : tf_actor_integration_profile_option table
    @OneToMany(mappedBy = "actorIntegrationProfile", targetEntity = ActorIntegrationProfileOption.class)
    // @NotAudited
    @AuditJoinTable
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private List<ActorIntegrationProfileOption> listOfActorIntegrationProfileOption;

    // Constructors

    public ActorIntegrationProfile() {
        actor = new Actor();
        integrationProfile = new IntegrationProfile();
    }

    public ActorIntegrationProfile(Actor actor, IntegrationProfile integrationProfile) {
        super();
        this.actor = actor;
        this.integrationProfile = integrationProfile;
    }

    public ActorIntegrationProfile(ActorIntegrationProfile inActorIntegrationProfile) {

        if (inActorIntegrationProfile.actor != null) {
            actor = new Actor(inActorIntegrationProfile.actor);
        }
        if (inActorIntegrationProfile.integrationProfile != null) {
            integrationProfile = new IntegrationProfile(inActorIntegrationProfile.integrationProfile);
        }

    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<IntegrationProfile> getIntegrationProfileByActor(Actor inActor) {
        IntegrationProfileQuery query = new IntegrationProfileQuery();
        query.actorIntegrationProfiles().actor().eq(inActor);
        return query.getListNullIfEmpty();
    }

    public static List<Actor> getActorByIntegrationProfile(IntegrationProfile inIntegrationProfile) {
        ActorQuery query = new ActorQuery();
        query.actorIntegrationProfiles().integrationProfile().eq(inIntegrationProfile);
        return query.getListNullIfEmpty();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public IntegrationProfile getIntegrationProfile() {
        return integrationProfile;
    }

    // *********************************************************
    // Foreign Key for table : tf_profile_link
    // *********************************************************

    public void setIntegrationProfile(IntegrationProfile integrationProfile) {
        this.integrationProfile = integrationProfile;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    public List<ProfileLink> getProfileLinks() {
        return profileLinks;
    }

    public void setProfileLinks(List<ProfileLink> l) {
        profileLinks = l;
    }

    public void addProfileLink(ProfileLink l) {
        if (profileLinks == null) {
            profileLinks = new ArrayList<ProfileLink>();
        }
        profileLinks.add(l);
    }

    public void delProfileLink(ProfileLink l) {
        profileLinks.remove(l);
    }

    // *********************************************************
    // Foreign Key for table : tf_actor_integration_profile_option
    // *********************************************************
    public List<ActorIntegrationProfileOption> getActorIntegrationProfileOption() {
        return listOfActorIntegrationProfileOption;
    }

    public void setActorIntegrationProfileOption(List<ActorIntegrationProfileOption> l) {
        listOfActorIntegrationProfileOption = l;
    }

    public List<ActorIntegrationProfileOption> getListOfActorIntegrationProfileOption() {
        return listOfActorIntegrationProfileOption;
    }

    public void addActorIntegrationProfileOption(ActorIntegrationProfileOption aip) {
        if (listOfActorIntegrationProfileOption == null) {
            listOfActorIntegrationProfileOption = new ArrayList<ActorIntegrationProfileOption>();
        }
        listOfActorIntegrationProfileOption.add(aip);
    }

    public void delActorIntegrationProfileOption(ActorIntegrationProfileOption aip) {
        listOfActorIntegrationProfileOption.remove(aip);
    }

    public int compareTo(ActorIntegrationProfile aip) {
        if (aip == null) {
            return -1;
        }

        if ((actor != null) && (aip.actor != null) && (actor.compareTo(aip.actor) == 0)) {
            if ((integrationProfile != null) && (aip.integrationProfile != null)
                    && (integrationProfile.compareTo(aip.integrationProfile) == 0)) {
                return 0;

            } else if (integrationProfile == null) {
                return 1;
            } else if (aip.integrationProfile == null) {
                return -1;
            } else {
                return integrationProfile.compareTo(aip.integrationProfile);
            }
        } else if (actor == null) {
            return 1;
        } else if (aip.actor == null) {
            return -1;
        } else {
            return actor.compareTo(aip.actor);
        }

    }

    @Override
    public String toString() {
        return (integrationProfile != null ? integrationProfile.toString() + " with " : "")
                + (actor != null ? actor.toString() : "");
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        actor.setLastModifierId(this.lastModifierId);
        actor.saveOrMerge(entityManager);

        integrationProfile.setLastModifierId(this.lastModifierId);
        integrationProfile.saveOrMerge(entityManager);

        @SuppressWarnings("unchecked")
        List<ProfileLink> profileLinksBackup = (List<ProfileLink>) ObjectUtils.clone(profileLinks);
        profileLinks = null;

        ActorIntegrationProfileQuery query = new ActorIntegrationProfileQuery();
        query.actor().eq(this.actor);
        query.integrationProfile().eq(this.integrationProfile);
        List<ActorIntegrationProfile> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }

        if (profileLinksBackup != null) {
            for (ProfileLink profileLink : profileLinksBackup) {
                profileLink.setLastModifierId(this.lastModifierId);
                profileLink.saveOrMerge(entityManager, this);
            }
        }
        this.profileLinks = profileLinksBackup;
        entityManager.merge(this);
        entityManager.flush();

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }
}
