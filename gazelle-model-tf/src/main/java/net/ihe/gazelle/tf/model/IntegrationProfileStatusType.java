/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>IntegrationProfileStatusType<br>
 * <br>
 * This class describes the Integration Profile Status type object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * IntegrationProfileStatusType possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Integration Profile Status type</li>
 * <li><b>keyword</b> : keyword for the Integration Profile Status type
 * <li><b>name</b> : name corresponding to the Integration Profile Status type</li>
 * <li><b>description</b> : description corresponding to the Integration Profile Status type</li>
 * </ul>
 * </br> <b>Example of Integration Profile Status type</b> : " 'Deprecated' " is an IHE Integration Profile Status type<br>
 *
 * @author Ralph Moulton / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 21
 * @class IntegrationProfileStatusType.java
 * @package net.ihe.gazelle.tf.model
 * @see > moultonr@mir.wustl.edu - http://www.ihe-europe.org
 */
@XmlRootElement(name = "IntegrationProfileStatusType")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Audited
@Table(name = "tf_integration_profile_status_type", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
@SequenceGenerator(name = "tf_integration_profile_status_type_sequence", sequenceName = "tf_integration_profile_status_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class IntegrationProfileStatusType extends AuditedObject implements java.io.Serializable {

    public static final String DEPRECATED_KEYWORD = "DEPRECATED";
    public static final String PROPOSED_KEYWORD = "PROPOSED";
    public static final String FINALTEXT_KEYWORD = "FT";
    public static final String TRIALIMPLEMENTATION_KEYWORD = "TI";
    private static final long serialVersionUID = 265837323827643461L;
    private static Logger log = LoggerFactory.getLogger(IntegrationProfileStatusType.class);
    @Column(name = "keyword", unique = true, nullable = false, length = 128)
    // @Length(max = 128 )
    @NotNull
    protected String keyword;
    @Column(name = "name", length = 128)
    // @Length(max = 128 )
    protected String name;
    @Column(name = "description")
    @Size(max = 2048)
    protected String description;
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_integration_profile_status_type_sequence")
    @XmlTransient
    private Integer id;
    @OneToMany(mappedBy = "integrationProfileStatusType", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private Set<IntegrationProfile> integrationProfiles;

    // Constructors
    public IntegrationProfileStatusType() {
    }

    public IntegrationProfileStatusType(String keyword) {

        this.keyword = keyword;
    }

    public IntegrationProfileStatusType(String keyword, String name, String description) {

        this.keyword = keyword;
        this.name = name;
        this.description = description;
    }

    public static List<IntegrationProfileStatusType> listAllIntegrationProfileStatusType() {
        return AuditedObject.listAllObjectsOrderByKeyword(IntegrationProfileStatusType.class);
    }

    public static IntegrationProfileStatusType GetIntegrationProfileStatusTypeByKeyword(String inKeyword) {
        return AuditedObject.getObjectByKeyword(IntegrationProfileStatusType.class, inKeyword);
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<IntegrationProfile> getIntegrationProfiles() {
        return HibernateHelper.getLazyValue(this, "integrationProfiles", this.integrationProfiles);
    }

    public void setIntegrationProfiles(Set<IntegrationProfile> integrationProfiles) {
        this.integrationProfiles = integrationProfiles;
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        IntegrationProfileStatusTypeQuery query = new IntegrationProfileStatusTypeQuery();
        query.keyword().eq(this.keyword);
        query.name().eq(this.name);
        query.description().eq(this.description);

        List<IntegrationProfileStatusType> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

}
