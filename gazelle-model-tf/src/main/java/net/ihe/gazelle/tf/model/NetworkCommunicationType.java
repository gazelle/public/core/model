package net.ihe.gazelle.tf.model;

import net.ihe.gazelle.hql.FilterLabel;

public enum NetworkCommunicationType {

    HL7v2("HL7v2"),
    DICOM("Dicom"),
    WEBSERVICE("Webservice"),
    HTTP("Http"),
    NONE("None"),
    OTHER("Other"),
    SYSLOG("Syslog");

    private String label;

    private NetworkCommunicationType(String label) {
        this.label = label;
    }

    public static NetworkCommunicationType getEnumWithLabel(String networkCommunicationTypeKeyword) {
        for (NetworkCommunicationType type : values()) {
            if (type.getLabel().equals(networkCommunicationTypeKeyword)) {
                return type;
            }
        }
        return null;
    }

    @FilterLabel
    public String getLabel() {
        return label;
    }
}
