package net.ihe.gazelle.tf.model;

public enum DocumentType {
    SUPPLEMENT("Supplement", "Suppl"),
    TECHNICAL_FRAMEWORK("Technical framework", "TF"),
    USER_HANDBOOK("User handbook", "UH"),
    WHITE_PAPER("White paper", "White-paper");

    private String friendlyName;
    private String keyword;

    DocumentType(String friendlyName, String keyword) {
        this.friendlyName = friendlyName;
        this.keyword = keyword;
    }

    public static DocumentType valueOfByKeyword(String keyword) throws IllegalArgumentException {
        for (DocumentType value : values()) {
            String docKeyword = value.getKeyword();
            if (docKeyword.equals(keyword)) {
                return value;
            }
        }
        throw new IllegalArgumentException("keyword " + keyword + "is not defined in DocumentType");
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getKeyword() {
        return keyword;
    }
}