/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetTestDefinition;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * <b>Class Description : </b>Transaction<br>
 * <br>
 * This class describes the Transaction object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * Transaction possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Transaction</li>
 * <li><b>keyword</b> : keyword corresponding to the Transaction</li>
 * <li><b>name</b> : name corresponding to the Transaction</li>
 * <li><b>description</b> : description corresponding to the Transaction</li>
 * </ul>
 * </br> <b>Example of Transaction</b> : "RAD-2	: Placer Order Management" is an IHE Transaction<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 21
 * @class Transaction.java
 * @package net.ihe.gazelle.tf.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "wsTransactionUsage")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Audited
@Table(name = "tf_ws_transaction_usage", schema = "public")
@SequenceGenerator(name = "tf_ws_transaction_usage_sequence", sequenceName = "tf_ws_transaction_usage_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTestDefinition.class)
public class WSTransactionUsage extends AuditedObject implements java.io.Serializable, Comparable<WSTransactionUsage> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -5994534457658398088L;

    private static Logger log = LoggerFactory.getLogger(WSTransactionUsage.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_ws_transaction_usage_sequence")
    @XmlElement(name = "id")
    private Integer id;

    @XmlElement(name = "usage")
    @Column(name = "usage", unique = true, nullable = true)
    private String usage;

    @XmlElementRef(name = "transaction")
    @ManyToOne(targetEntity = net.ihe.gazelle.tf.model.Transaction.class)
    @JoinColumn(name = "transaction_id", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private Transaction transaction;

    public WSTransactionUsage() {
        super();
    }

    public WSTransactionUsage(String usage, Transaction transaction) {
        super();
        this.usage = usage;
        this.transaction = transaction;
    }

    public static List<WSTransactionUsage> getWSTransactionUsageFiltered(Transaction inTransaction, Actor inActorFrom,
                                                                         IntegrationProfile inIntegrationProfile, ActorIntegrationProfile aip) {
        WSTransactionUsageQuery query = new WSTransactionUsageQuery();
        query.transaction().eqIfValueNotNull(inTransaction);
        query.transaction().profileLinks().actorIntegrationProfile().actor().eqIfValueNotNull(inActorFrom);
        query.transaction().profileLinks().actorIntegrationProfile().integrationProfile()
                .eqIfValueNotNull(inIntegrationProfile);
        query.transaction().profileLinks().actorIntegrationProfile().eqIfValueNotNull(aip);
        return query.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @FilterLabel
    public String getLabel() {
        if (transaction != null) {
            return transaction.getKeyword() + ":" + usage;
        } else {
            return usage;
        }
    }

    @Override
    public int compareTo(WSTransactionUsage o) {
        if (this.getTransaction() == null) {
            return 0;
        }
        return this.getTransaction().compareTo(o.getTransaction());
    }

    @Override
    public String toString() {
        return "WSTransactionUsage [id=" + id + ", usage=" + usage + ", transaction=" + transaction + "]";
    }

    public List<WSTransactionUsage> allWSTransactionUsages() {
        EntityManager em = EntityManagerService.provideEntityManager();
        Query query = em.createQuery("Select t FROM WSTransactionUsage t");
        return query.getResultList();
    }

    public void saveOrMerge(EntityManager entityManager){
        this.id = null;

        if (transaction != null) {
            transaction.setLastModifierId(this.lastModifierId);
            transaction = transaction.saveOrMerge(entityManager);
        }

        WSTransactionUsageQuery query = new WSTransactionUsageQuery();
        query.usage().eq(this.getUsage());
        query.transaction().eq(this.getTransaction());
        List<WSTransactionUsage> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }

        entityManager.merge(this);
        entityManager.flush();

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName() + " saved " + this);
        }
    }

}
