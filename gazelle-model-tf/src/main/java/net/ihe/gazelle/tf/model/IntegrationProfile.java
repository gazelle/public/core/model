/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import net.ihe.gazelle.tf.model.reversed.DomainProfile;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <b>Class Description : </b>IntegrationProfile<br>
 * <br>
 * This class describes the Integration profile object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * IntegrationProfile possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Integration profile</li>
 * <li><b>keyword</b> : keyword corresponding to the Integration profile</li>
 * <li><b>name</b> : name corresponding to the Integration profile</li>
 * <li><b>description</b> : description corresponding to the Integration profile</li>
 * <li><b>integrationProfileType</b> : type corresponding to the Integration profile</li>
 * </ul>
 * </br> <b>Example of Integration profile</b> : "ARI : Access to Radiology Information" is an Integration profile<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 21
 * @class IntegrationProfile.java
 * @package net.ihe.gazelle.tf.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "integrationProfile")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Audited
@Table(name = "tf_integration_profile", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "tf_integration_profile_sequence", sequenceName = "tf_integration_profile_id_seq", allocationSize = 1)
//@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class IntegrationProfile extends AuditedObject implements java.io.Serializable, Comparable<IntegrationProfile> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450911131561283760L;

    private static Logger log = LoggerFactory.getLogger(IntegrationProfile.class);
    @Column(name = "keyword", unique = true, nullable = false, length = 32)
    @NotNull
    @XmlElement(name = "keyword")
    protected String keyword;
    @Column(name = "name", length = 128, unique = true)
    @XmlElement(name = "name")
    protected String name;
    @Column(name = "description")
    @Size(max = 2048)
    @XmlElement(name = "description")
    protected String description;
    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_integration_profile_sequence")
    @XmlElement(name = "id")
    private Integer id;
    @ManyToMany
    @AuditJoinTable
    @JoinTable(name = "tf_integration_profile_type_link", joinColumns = @JoinColumn(name = "integration_profile_id"), inverseJoinColumns =
    @JoinColumn(name = "integration_profile_type_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "integration_profile_id", "integration_profile_type_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlElement(name = "integrationProfileTypes")
    private List<IntegrationProfileType> integrationProfileTypes;
    // ************************************************** Transaction status type
    @ManyToOne(targetEntity = net.ihe.gazelle.tf.model.IntegrationProfileStatusType.class)
    @JoinColumn(name = "integration_profile_status_type_id")
    @Fetch(value = FetchMode.SELECT)
    @XmlElement(name = "integrationProfileStatusType")
    private IntegrationProfileStatusType integrationProfileStatusType;
    // --------------------------------------- tf_actor_integration_profile table
    @OneToMany(mappedBy = "integrationProfile", targetEntity = ActorIntegrationProfile.class)
    // @NotAudited
    @AuditJoinTable
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<ActorIntegrationProfile> actorIntegrationProfiles;
    // Variables used for Foreign Keys : tf_domain_profile table
    @ManyToMany
    @NotAudited
    @JoinTable(name = "tf_domain_profile", joinColumns = @JoinColumn(name = "integration_profile_id"), inverseJoinColumns = @JoinColumn(name =
            "domain_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "domain_id", "integration_profile_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlElement(name = "domainsForDP")
    private List<Domain> domainsForDP;
    // Computed set, do not update that one
    @OneToMany(mappedBy = "profile", fetch = FetchType.LAZY)
    @NotAudited
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<DomainProfile> domainsProfile;
    @Transient
    private List<ActorIntegrationProfileOption> listOfActorIntegrationProfileOption;
    @Transient
    private boolean toDisplay = true;
    @ManyToOne
    @JoinColumn(name = "documentSection")
    @XmlElement(name = "documentSection")
    private DocumentSection documentSection;

    public IntegrationProfile() {
    }

    public IntegrationProfile(String keyword, String name) {
        // super( keyword , name ) ;
    }

    public IntegrationProfile(List<IntegrationProfileType> integrationProfileTypes, String keyword, String name,
                              String description) {
        // super( keyword , name , description ) ;
        this.keyword = keyword;
        this.name = name;
        this.integrationProfileTypes = integrationProfileTypes;
    }

    public IntegrationProfile(String name2) {
        this.name = name2;
    }

    public IntegrationProfile(IntegrationProfile oldIntegrationProfile) {

        // super ( oldIntegrationProfile ) ;
        if (oldIntegrationProfile.keyword != null) {
            this.keyword = new String(oldIntegrationProfile.keyword);
        }

        if (oldIntegrationProfile.name != null) {
            this.name = new String(oldIntegrationProfile.name);
        }

        if (oldIntegrationProfile.description != null) {
            this.description = new String(oldIntegrationProfile.description);
        }

        EntityManager em = EntityManagerService.provideEntityManager();
        if (em != null) {
            if (oldIntegrationProfile.getIntegrationProfileTypes() != null) {
                this.integrationProfileTypes = new ArrayList<IntegrationProfileType>();
                for (IntegrationProfileType type : oldIntegrationProfile.getIntegrationProfileTypes()) {
                    this.integrationProfileTypes.add(em.find(IntegrationProfileType.class, type.getId()));
                }
            }
        }
    }

    public static IntegrationProfile duplicateIntegrationProfile(IntegrationProfile ip) {
        IntegrationProfile integrationProfile = new IntegrationProfile(ip);
        if (integrationProfile.keyword != null) {
            integrationProfile.keyword = new String(integrationProfile.keyword + "_copy");
        }

        if (integrationProfile.name != null) {
            integrationProfile.name = new String(integrationProfile.name + "_copy");
        }
        return integrationProfile;
    }

    /**
     * Get List of all Integration Profile objects
     *
     * @return List <Actor> for this Integration Profile
     * @author : jchatel for TM and PR
     */
    public static List<IntegrationProfile> listAllIntegrationProfiles() {
        return AuditedObject.listAllObjectsOrderByKeyword(IntegrationProfile.class);
    }

    public static List<Actor> getActors(String inIntegrationProfileKeyword) {

        if ((inIntegrationProfileKeyword == null) || (inIntegrationProfileKeyword.length() == 0)) {
            return null;
        }

        IntegrationProfileQuery q = new IntegrationProfileQuery();
        q.keyword().eq(inIntegrationProfileKeyword.trim());

        List<Actor> actorsList = q.actorIntegrationProfiles().actor().getListDistinct();

        if ((actorsList != null) && (actorsList.size() > 0)) {
            return actorsList;
        } else {
            return null;
        }
    }

    // Constructors

    public static List<Transaction> getTransactions(String inIntegrationProfileKeyword) {

        if ((inIntegrationProfileKeyword == null) || (inIntegrationProfileKeyword.length() == 0)) {
            return null;
        }

        IntegrationProfileQuery q = new IntegrationProfileQuery();
        q.keyword().eq(inIntegrationProfileKeyword.trim());

        List<Transaction> transactionList = q.actorIntegrationProfiles().profileLinks().transaction().getListDistinct();
        if ((transactionList != null) && (transactionList.size() > 0)) {
            Collections.sort(transactionList);
            return transactionList;
        } else {
            return null;
        }
    }

    /**
     * Find and retrieve the IntegrationProfile object associated to a keyword
     *
     * @param keywordName : String - Keyword to retrieve
     * @return IntegrationProfile object
     */
    public static IntegrationProfile findIntegrationProfileWithKeyword(String keywordName) {
        if ((keywordName == null) || (keywordName.length() == 0)) {
            log.error("findIntegrationProfileWithKeyword - parameter given as required parameter is null");
            return null;
        }

        IntegrationProfile integrationProfile = AuditedObject.getObjectByKeyword(IntegrationProfile.class, keywordName);
        if (integrationProfile != null) {
            Hibernate.initialize(integrationProfile.getDomainsProfile());
        }
        return integrationProfile;
    }

    public static Domain getDomainOfIntegrationProfile(IntegrationProfile ip) throws Exception {
        if ((ip == null) || (ip.getDomainsForDP() == null)) {
            return null;
        }

        if ((ip.getDomainsForDP() != null) && (ip.getDomainsForDP().size() == 1)) {
            return ip.getDomainsForDP().get(0);
        } else {
            StringBuffer sb = new StringBuffer();

            sb.append("ip.getDomainsForDP().size() : " + ip.getDomainsForDP().size());

            for (Domain d : ip.getDomainsForDP()) {
                sb.append("domain : " + d.getKeyword() + "\n");
            }

            throw new Exception(
                    "Several domains for this integration profile. Need to review the method  IntegrationProfile" +
                            ".getDomainOfIntegrationProfileDependingActor \n"
                            + sb.toString());
        }
    }

    public static List<Domain> getDomainOfIntegrationProfileList(IntegrationProfile ip) throws Exception {
        return ip.getDomainsForDP();
    }

    @SuppressWarnings("unchecked")
    public static List<IntegrationProfile> getPossibleIntegrationProfiles(Domain inDomain) {

        List<IntegrationProfile> listOfResults;

        if (inDomain == null) {
            return listAllIntegrationProfiles();
        }
        IntegrationProfileQuery q = new IntegrationProfileQuery();
        q.domainsForDP().keyword().eq(inDomain.getKeyword());
        listOfResults = q.getListDistinct();
        Collections.sort(listOfResults);

        return listOfResults;
    }

    @SuppressWarnings("unchecked")
    public static List<String> getPossibleIntegrationProfilesKeyword(Domain inDomain) {

        List<String> listOfResults;

        if (inDomain == null) {
            return null;
        }

        IntegrationProfileQuery q = new IntegrationProfileQuery();
        q.domainsForDP().keyword().eq(inDomain.getKeyword());
        listOfResults = q.keyword().getListDistinct();
        Collections.sort(listOfResults);

        return listOfResults;
    }

    public static List<IntegrationProfile> getAllIntegrationProfile() {
        IntegrationProfileQuery q = new IntegrationProfileQuery();
        List<IntegrationProfile> ipList = q.getListDistinct();
        Collections.sort(ipList);
        return ipList;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<IntegrationProfile> getAllIntegrationProfileForTestingSession() {
        IntegrationProfileQuery query = new IntegrationProfileQuery();

        query.keyword().order(true);

        query.addRestriction(HQLRestrictions.or(query.integrationProfileStatusType().keyword()
                        .eqRestriction(IntegrationProfileStatusType.FINALTEXT_KEYWORD),
                query.integrationProfileStatusType().keyword()
                        .eqRestriction(IntegrationProfileStatusType.TRIALIMPLEMENTATION_KEYWORD)));

        List<IntegrationProfile> ipList = query.getList();
        return ipList;
    }

    public static List<DocumentSection> getDocumentSection(String Keyword) {
        IntegrationProfileQuery integrationProfileQuery = new IntegrationProfileQuery();
        integrationProfileQuery.keyword().eq(Keyword);
        return integrationProfileQuery.documentSection().getListDistinct();
    }

    public static List<String> getAllIntegrationProfilesKeyword() {
        IntegrationProfileQuery query = new IntegrationProfileQuery();
        return query.keyword().getListDistinct();
    }

    public DocumentSection getDocumentSection() {
        return documentSection;
    }

    public void setDocumentSection(DocumentSection documentSection) {
        this.documentSection = documentSection;
    }

    // *********************************************************
    // Foreign Key for table : tf_domain_profile
    // *********************************************************

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<DomainProfile> getDomainsProfile() {
        return HibernateHelper.getLazyValue(this, "domainsProfile", this.domainsProfile);
    }

    public IntegrationProfileStatusType getIntegrationProfileStatusType() {
        return integrationProfileStatusType;
    }

    public void setIntegrationProfileStatusType(IntegrationProfileStatusType t) {
        integrationProfileStatusType = t;
    }

    public List<Domain> getDomainsForDP() {
        return domainsForDP;
    }

    public void setDomainsForDP(List<Domain> domainsForDP) {
        this.domainsForDP = domainsForDP;
    }

    public List<ActorIntegrationProfileOption> getListOfActorIntegrationProfileOption() {
        return listOfActorIntegrationProfileOption;
    }

    public void setListOfActorIntegrationProfileOption(
            List<ActorIntegrationProfileOption> listOfActorIntegrationProfileOption) {
        this.listOfActorIntegrationProfileOption = listOfActorIntegrationProfileOption;
    }

    public List<IntegrationProfileType> getIntegrationProfileTypes() {
        return integrationProfileTypes;
    }

    public void setIntegrationProfileTypes(List<IntegrationProfileType> ipTypes) {
        integrationProfileTypes = ipTypes;
    }

    public int getDomainCount() {
        if (domainsForDP == null) {
            return 0;
        } else {
            return domainsForDP.size();
        }
    }

    public List<ActorIntegrationProfile> getActorIntegrationProfiles() {
        return actorIntegrationProfiles;
    }

	/*
     * Returns the list of Integration Profile objects for a given domain
	 * If the domain is null returns a null value
	 */

    public void setActorIntegrationProfiles(List<ActorIntegrationProfile> actorIntegrationProfiles) {
        this.actorIntegrationProfiles = actorIntegrationProfiles;
    }

	/*
     * Returns the list of Integration Profile Keyword for a given domain
	 * If the domain is null returns a null value
	 */

    public boolean isToDisplay() {
        return toDisplay;
    }

    public void setToDisplay(boolean toDisplay) {
        this.toDisplay = toDisplay;
    }

    @Override
    public String toString() {
        if (toDisplay) {
            return "IntegrationProfile " + keyword + "-" + name;
        } else {
            return "\"";
        }
    }

    @FilterLabel
    public String getSelectableLabel() {
        return getKeyword() + " - " + getName();
    }

    public List<Actor> getActors() {
        List<Actor> actors = new ArrayList<Actor>();
        for (ActorIntegrationProfile aip : actorIntegrationProfiles) {
            actors.add(aip.getActor());
        }
        return actors;
    }

    public int compare(IntegrationProfile o1, IntegrationProfile o2) {
        return o1.getKeyword().compareToIgnoreCase(o2.getKeyword());
    }

    @Override
    public int compareTo(IntegrationProfile o) {
        return this.getKeyword().compareTo(o.getKeyword());
    }

    public List<Document> getDocuments() {
        DocumentQuery documentQuery = new DocumentQuery();
        documentQuery.domain().integrationProfilesForDP().id().eq(getId());
        return documentQuery.getList();
    }

    public boolean isAvailableForTestingSessions() {
        String statusKeyword = integrationProfileStatusType.getKeyword();
        boolean isAvailableForTestingSessions =
                statusKeyword.equals(IntegrationProfileStatusType.FINALTEXT_KEYWORD) || statusKeyword
                        .equals(IntegrationProfileStatusType.TRIALIMPLEMENTATION_KEYWORD);
        return isAvailableForTestingSessions;
    }

    public void saveOrMerge(EntityManager entityManager) {

        this.id = null;
        integrationProfileStatusType.setLastModifierId(this.lastModifierId);
        integrationProfileStatusType.saveOrMerge(entityManager);
        if (documentSection != null) {
            if (documentSection.emptyDocumentSection()) {
                documentSection = null;
            } else {
                documentSection.setLastModifierId(this.lastModifierId);
                documentSection.saveOrMerge(entityManager);
            }
        }

        // Set to null cause it's used by the reversed model
        domainsProfile = null;

        if (domainsForDP != null) {
            for (Domain domain : domainsForDP) {
                domain.setLastModifierId(this.lastModifierId);
                domain.saveOrMerge(entityManager);
            }
        }
        if (integrationProfileTypes != null) {
            for (IntegrationProfileType integrationProfileType : integrationProfileTypes) {
                integrationProfileType.setLastModifierId(this.lastModifierId);
                integrationProfileType.saveOrMerge(entityManager);
            }
        }

        IntegrationProfileQuery query = new IntegrationProfileQuery();
        query.addRestriction(
                HQLRestrictions.or(query.name().eqRestriction(this.name), query.keyword().eqRestriction(this.keyword)));

        List<IntegrationProfile> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        IntegrationProfile that = (IntegrationProfile) o;

        if (toDisplay != that.toDisplay) {
            return false;
        }
        if (keyword != null ? !keyword.equals(that.keyword) : that.keyword != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (integrationProfileStatusType != null ? !integrationProfileStatusType.equals(that.integrationProfileStatusType) : that
                .integrationProfileStatusType != null) {
            return false;
        }
        if (lastChanged != null ? !lastChanged.equals(that.lastChanged) : that.lastChanged != null) {
            return false;
        }
        if (lastModifierId != null ? !lastModifierId.equals(that.lastModifierId) : that.lastModifierId != null) {
            return false;
        }
        return documentSection != null ? documentSection.equals(that.documentSection) : that.documentSection == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (keyword != null ? keyword.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (integrationProfileStatusType != null ? integrationProfileStatusType.hashCode() : 0);
        result = 31 * result + (lastChanged != null ? lastChanged.hashCode() : 0);
        result = 31 * result + (lastModifierId != null ? lastModifierId.hashCode() : 0);
        result = 31 * result + (toDisplay ? 1 : 0);
        result = 31 * result + (documentSection != null ? documentSection.hashCode() : 0);
        return result;
    }
}
