package net.ihe.gazelle.tf.model;

import net.ihe.gazelle.common.model.AuditedObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "", propOrder = {
        "keyword",
        "name",
        "version",
        "networkCommunicationType",
        "url",
        "transactionKeywords"
})
@XmlRootElement(name = "standard")

@Entity
@Audited
@Table(name = "tf_standard", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "tf_standard_sequence", sequenceName = "tf_standard_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Standard extends AuditedObject implements Serializable, Comparable<Standard>, Comparator<Standard> {


    private static final long serialVersionUID = -742029584667153034L;
    private static Logger log = LoggerFactory.getLogger(Standard.class);

    @Id
    @GeneratedValue(generator = "tf_standard_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "keyword")
    private String keyword;

    @Lob
    @Type(type = "text")
    @Column(name = "name")
    private String name;

    @Column(name = "version")
    private String version;

    @Column(name = "network_communication_type")
    private NetworkCommunicationType networkCommunicationType;

    @Lob
    @Type(type = "text")
    @Column(name = "url")
    private String url;

    @ManyToMany(mappedBy = "standards", targetEntity = Transaction.class)
    private List<Transaction> transactions;

    @Transient
    private List<String> transactionKeywords;

    public Standard() {
        super();
        this.networkCommunicationType = NetworkCommunicationType.NONE;
    }


    @Override
    public int compare(Standard o1, Standard o2) {
        if (o1 != null) {
            return o1.compareTo(o2);
        } else {
            return 1;
        }
    }

    @Override
    public int compareTo(Standard o) {
        if (o == null) {
            return -1;
        }
        if (o.getKeyword() == null) {
            return -1;
        }
        if (getKeyword() == null) {
            return 1;
        }
        return getKeyword().compareToIgnoreCase(o.getKeyword());
    }

    @XmlTransient
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @XmlTransient
    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
        refreshTransactionKeywords();
    }

    @XmlElementWrapper(name = "transactions")
    @XmlElement(name="transactionKeyword")
    public List<String> getTransactionKeywords() {
        if (this.transactionKeywords == null){
            transactionKeywords = new ArrayList<>();
            if (this.transactions != null && !this.transactions.isEmpty()){
                for (Transaction transaction : this.transactions){
                    transactionKeywords.add(transaction.getKeyword());
                }
            }
        }
        return transactionKeywords;
    }

    public void refreshTransactionKeywords(){
        transactionKeywords = null;
        getTransactionKeywords();
    }

    public void setTransactionKeywords(List<String> transactionKeyword) {
        this.transactionKeywords = transactionKeyword;
    }


    public NetworkCommunicationType getNetworkCommunicationType() {
        return networkCommunicationType;
    }

    public void setNetworkCommunicationType(NetworkCommunicationType networkCommunicationType) {
        this.networkCommunicationType = networkCommunicationType;
    }

    public void saveOrMerge(EntityManager entityManager){
        this.setId(null);
        StandardQuery query = new StandardQuery(entityManager);
        query.keyword().eq(this.keyword);
        query.name().eq(this.name);
        query.version().eq(this.version);
        query.networkCommunicationType().eq(this.networkCommunicationType);
        List<Standard> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            entityManager.merge(this);
        } else {
            entityManager.persist(this);
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName() + " saved " + this);
        }
    }
}
