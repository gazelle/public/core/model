/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

//JPA imports

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * <b>Class Description : </b>Domain<br>
 * <br>
 * This class describes the Domain object, used by the Gazelle application. This class belongs to the Technical Framework module.
 * <p/>
 * Domain possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Domain</li>
 * <li><b>name</b> : name corresponding to the Domain</li>
 * <li><b>description</b> : description corresponding to the Domain</li>
 * </ul>
 * </br> <b>Example of Domain</b> : Radiology is a domain<br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 21
 * @class Domain.java
 * @package net.ihe.gazelle.tf.model
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@XmlRootElement(name = "domain")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Audited
@Table(name = "tf_domain", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "tf_domain_sequence", sequenceName = "tf_domain_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class Domain extends AuditedObject implements java.io.Serializable, Comparable<Domain> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -487631131541283760L;

    // Attributes (existing in database as a column)
    private static Logger log = LoggerFactory.getLogger(Domain.class);

    // Variables used for Foreign Keys : domain_profile table
    @Column(name = "keyword", unique = true, nullable = false, length = 128)
    @NotNull
    @XmlElement(name = "keyword")
    protected String keyword;
    @Column(name = "name", length = 128)
    @XmlElement(name = "name")
    @NotNull
    protected String name;
    @Column(name = "description")
    @Size(max = 2048)
    @XmlElement(name = "description")
    protected String description;
    @OneToMany(targetEntity = Document.class, mappedBy = "domain")
    @XmlTransient
    protected List<Document> documents;
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_domain_sequence")
    @XmlElement(name = "id")
    private Integer id;
    @ManyToMany
    @AuditJoinTable
    @JoinTable(name = "tf_domain_profile", joinColumns = @JoinColumn(name = "domain_id"), inverseJoinColumns = @JoinColumn(name = "integration_profile_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "domain_id", "integration_profile_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<IntegrationProfile> integrationProfilesForDP;

    /***********************************************************
     * Constructors
     **********************************************************/

    public Domain() {
    }

    public Domain(String keyword, String name) {

        this.keyword = keyword;
        this.name = name;
    }

    public Domain(String keyword, String name, String description) {

        this.keyword = keyword;
        this.name = name;
        this.description = description;
    }

    public Domain(String name) {
        this.name = name;
    }

    public static List<Domain> getPossibleDomains() {
        return AuditedObject.listAllObjectsOrderByKeyword(Domain.class);
    }

    /**
     * Returns the list of Domain Keyword for a given domain If the domain is null returns a null value
     *
     * @return String : List of possible domains
     */
    @SuppressWarnings("unchecked")
    public static List<String> getPossibleDomainKeywords() {
        DomainQuery query = new DomainQuery();
        query.keyword().order(true);
        return query.keyword().getListDistinct();
    }

    public static Domain getDomainByKeyword(String inKeyword) {
        if ((inKeyword == null) || (inKeyword.length() == 0)) {
            log.error("Trying to get Domain by Keyword using an empty keyword");
            return null;
        }
        return AuditedObject.getObjectByKeyword(Domain.class, inKeyword);
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public String getDescription() {
        return description;
    }

    // *********************************************************
    // Foreign Key for table : domain_profile
    // *********************************************************

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<IntegrationProfile> getIntegrationProfilesForDP() {
        return integrationProfilesForDP;
    }

    public void setIntegrationProfilesForDP(List<IntegrationProfile> integrationProfilesForDP) {
        this.integrationProfilesForDP = integrationProfilesForDP;
    }

    @FilterLabel
    public String getSelectableLabel() {
        return getKeyword() + " - " + getName();
    }

    @Override
    public int compareTo(Domain o) {
        return this.getKeyword().compareToIgnoreCase(o.getKeyword());
    }

    public void saveOrMerge(EntityManager entityManager) {
        this.id = null;

        DomainQuery query = new DomainQuery();
        query.addRestriction(HQLRestrictions.or(query.name().eqRestriction(this.name),
                query.keyword().eqRestriction(this.keyword)));

        List<Domain> listDistinct;
        listDistinct = query.getListDistinct();
        if (listDistinct.size() == 1) {
            this.id = listDistinct.get(0).id;
            this.integrationProfilesForDP = listDistinct.get(0).integrationProfilesForDP;
            entityManager.merge(this);
        } else {
            if (id != null) {
                entityManager.merge(this);
            } else {
                entityManager.persist(this);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName().toString() + " saved " + this);
        }
    }

}
