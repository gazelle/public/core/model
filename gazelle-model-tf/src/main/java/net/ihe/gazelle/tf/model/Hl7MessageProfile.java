/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tf.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.common.utils.Mergeable;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.sync.SetTechnicalFramework;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * <b>Class Description : </b>HL7MessageProfile<br>
 * <br>
 * This class describes the HL7MessageProfile object.
 * <p/>
 * HL7MessageProfile possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id of HL7MessageProfile in database</li>
 * <li><b>profileOid</b> : OID of the HL7 Message Profile</li>
 * <li><b>hl7Version</b> : The version of HL7 the message profile refers to</li>
 * <li><b>profileType</b> : The type of profile</li>
 * <li><b>domain</b> : id of the domain the message profile refers to</li>
 * <li><b>actor</b> : id of the actor concerned by the profile</li>
 * <li><b>transaction</b> : id of the transaction in which the message profile can be used</li>
 * <li><b>messageType</b> : type of message defined by the message profile</li>
 * <li><b>messageOrderControlCode</b> : ?</li>
 * <li><b>affinityDomains</b> : link the message profile to the appropriate affinity domains (eg. PAM_FR, IHE...)</li>
 * </ul>
 * </br>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 2.1 - 2011, June 24th
 */

@XmlRootElement(name = "HL7MessageProfile")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Audited
@Table(name = "tf_hl7_message_profile", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "profile_oid"))
@SequenceGenerator(name = "tf_hl7_message_profile_sequence", sequenceName = "tf_hl7_message_profile_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetTechnicalFramework.class)
public class Hl7MessageProfile extends AuditedObject implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450911138351283760L;

    private static Logger log = LoggerFactory.getLogger(Hl7MessageProfile.class);

    /**
     * Attributes (existing in database as a column)
     */

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_hl7_message_profile_sequence")
    private Integer id;

    @Column(name = "profile_oid", unique = true, nullable = false, length = 64)
    @NotNull
    @Size(max = 64)
    @XmlElement(name = "Oid")
    private String profileOid;

    @Column(name = "hl7_version", nullable = false)
    @NotNull
    @XmlElement(name = "Hl7Version")
    private String hl7Version;

    @Column(name = "profile_type")
    private String profileType;

    /**
     * The content of the message profile has to be set in Gazelle HL7 Validator using the administration section of the tool
     */
    @Column(name = "profile_content")
    @Deprecated
    private String profileContent;

    @XmlElement(name = "Domain")
    @ManyToOne
    @JoinColumn(name = "domain_id")
    @Fetch(value = FetchMode.SELECT)
    private Domain domain;

    @XmlElement(name = "Actor")
    @ManyToOne
    @JoinColumn(name = "actor_id")
    @Fetch(value = FetchMode.SELECT)
    private Actor actor;

    @XmlElement(name = "Transaction")
    @ManyToOne
    @JoinColumn(name = "transaction_id")
    @Fetch(value = FetchMode.SELECT)
    private Transaction transaction;

    @Column(name = "message_type", nullable = false, length = 15)
    @NotNull
    @Size(max = 15)
    @XmlElement(name = "TriggerEvent")
    private String messageType;

    @Column(name = "message_order_control_code")
    @Size(max = 8)
    @XmlElement(name = "MessageOrderControlCode")
    private String messageOrderControlCode;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "tf_hl7_message_profile_affinity_domain", inverseJoinColumns = @JoinColumn(name = "affinity_domain_id"), joinColumns = @JoinColumn(name = "hl7_message_profile_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "hl7_message_profile_id", "affinity_domain_id"}))
    @Fetch(value = FetchMode.SELECT)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlElement(name = "AffinityDomains")
    private List<AffinityDomain> affinityDomains;

    /**
     * Constructors
     */

    public Hl7MessageProfile() {
    }

    public Hl7MessageProfile(Integer id, String profileOid, String messageType) {
        this.id = id;
        this.profileOid = profileOid;
        this.messageType = messageType;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    /**
     * Gets an HL7MessageProfile object for a given OID
     *
     * @param inOID
     * @return
     */
    public static Hl7MessageProfile getHl7MessageProfileByOID(String inOID) throws NoResultException {
        if (inOID == null) {
            return null;
        }

        Hl7MessageProfileQuery query = new Hl7MessageProfileQuery();
        query.profileOid().eq(inOID);
        return query.getUniqueResult();
    }

    /**
     * Gets all HL7MessageProfile objects recorded into database
     *
     * @return
     */
    public static List<Hl7MessageProfile> getAllHl7MessageProfiles() {
        return new Hl7MessageProfileQuery().getListNullIfEmpty();
    }

    public static List<String> getAllHL7MessageProfileOIDs() {
        return new Hl7MessageProfileQuery().profileOid().getListDistinct();
    }

    public static List<Hl7MessageProfile> getHL7MessageProfilesFiltered(String inDomainKeyword, String inActorKeyword,
                                                                        String inTransactionKeyword, String inHL7Version, String inMessageType, String inControlCode) {
        EntityManager em = EntityManagerService.provideEntityManager();
        Session s = (Session) em.getDelegate();
        Criteria c = s.createCriteria(Hl7MessageProfile.class);
        Criterion restriction = null;

        if ((inDomainKeyword != null) && (inDomainKeyword.length() > 0)) {
            Domain d = Domain.getDomainByKeyword(inDomainKeyword);
            if (d == null) {
                return null;
            }
            if (restriction == null) {
                restriction = Restrictions.eq("domain", d);
            } else {
                restriction = Restrictions.and(restriction, Restrictions.eq("domain", d));
            }
        }
        if ((inActorKeyword != null) && (inActorKeyword.length() > 0)) {
            Actor a = Actor.findActorWithKeyword(inActorKeyword);
            if (a == null) {
                return null;
            }
            if (restriction == null) {
                restriction = Restrictions.eq("actor", a);
            } else {
                restriction = Restrictions.and(restriction, Restrictions.eq("actor", a));
            }
        }
        if ((inTransactionKeyword != null) && (inTransactionKeyword.length() > 0)) {
            Transaction t = Transaction.GetTransactionByKeyword(inTransactionKeyword);
            if (t == null) {
                return null;
            }
            if (restriction == null) {
                restriction = Restrictions.eq("transaction", t);
            } else {
                restriction = Restrictions.and(restriction, Restrictions.eq("transaction", t));
            }
        }
        if ((inHL7Version != null) && (inHL7Version.length() > 0)) {
            if (restriction == null) {
                restriction = Restrictions.ilike("hl7Version", inHL7Version);
            } else {
                restriction = Restrictions.and(restriction, Restrictions.ilike("hl7Version", inHL7Version));
            }
        }
        if ((inMessageType != null) && (inMessageType.length() > 0)) {
            if (restriction == null) {
                restriction = Restrictions.ilike("messageType", inMessageType);
            } else {
                restriction = Restrictions.and(restriction, Restrictions.ilike("messageType", inMessageType));
            }
        }
        if ((inControlCode != null) && (inControlCode.length() > 0)) {
            if (restriction == null) {
                restriction = Restrictions.ilike("messageOrderControlCode", inControlCode);
            } else {
                restriction = Restrictions
                        .and(restriction, Restrictions.ilike("messageOrderControlCode", inControlCode));
            }
        }
        if (restriction == null) {
            return null;
        } else {
            c.add(restriction);
            List<Hl7MessageProfile> hl7mpList = c.list();
            if ((hl7mpList != null) && (hl7mpList.size() > 0)) {
                return hl7mpList;
            } else {
                return null;
            }
        }
    }

    //TODO not the same result as previous version
    public static List<Hl7MessageProfile> getHL7MessageProfilesFiltered1(String inDomainKeyword, String inActorKeyword,
                                                                         String inTransactionKeyword, String inHL7Version, String inMessageType, String inControlCode) {
        Hl7MessageProfileQuery q = new Hl7MessageProfileQuery();

        if ((inDomainKeyword != null) && (inDomainKeyword.length() > 0)) {
            Domain d = Domain.getDomainByKeyword(inDomainKeyword);
            if (d == null) {
                return null;
            } else {
                q.domain().eq(d);
            }
        }
        if ((inActorKeyword != null) && (inActorKeyword.length() > 0)) {
            Actor a = Actor.findActorWithKeyword(inActorKeyword);
            if (a == null) {
                return null;
            } else {
                q.actor().eq(a);
            }
        }
        if ((inTransactionKeyword != null) && (inTransactionKeyword.length() > 0)) {
            Transaction t = Transaction.GetTransactionByKeyword(inTransactionKeyword);
            if (t == null) {
                return null;
            } else {
                q.transaction().eq(t);
            }
        }
        if ((inHL7Version != null) && (inHL7Version.length() > 0)) {
            q.hl7Version().like(inHL7Version);
        }
        if ((inMessageType != null) && (inMessageType.length() > 0)) {
            q.messageType().like(inMessageType);
        }
        if ((inControlCode != null) && (inControlCode.length() > 0)) {
            q.messageOrderControlCode().like(inControlCode);
        }
        List<Hl7MessageProfile> hl7mpList = q.getListDistinct();
        if ((hl7mpList != null) && (hl7mpList.size() > 0)) {
            return hl7mpList;
        } else {
            return null;
        }
    }

    public static List<Domain> getListOfAvailableDomains() {
        Hl7MessageProfileQuery q = new Hl7MessageProfileQuery();
        List<Domain> domains = q.domain().getListDistinct();
        if ((domains != null) && (domains.size() > 0)) {
            return domains;
        } else {
            return null;
        }
    }

    public static List<Actor> getListOfAvailableActorsForGivenDomain(String inDomainKeyword) {
        if ((inDomainKeyword == null) || (inDomainKeyword.length() == 0)) {
            return null;
        }
        Hl7MessageProfileQuery q = new Hl7MessageProfileQuery();
        q.domain().keyword().like(inDomainKeyword.trim(), HQLRestrictionLikeMatchMode.EXACT);

        List<Actor> actors = q.actor().getListDistinct();
        if ((actors != null) && (actors.size() > 0)) {
            return actors;
        } else {
            return null;
        }
    }

    //FIXME
    @Deprecated
    public static List<Domain> getListOfAvailableDomainsForGivenAffinityDomain(String inAffinityDomainKeyword) {
        if ((inAffinityDomainKeyword == null) || (inAffinityDomainKeyword.length() == 0)) {
            return null;
        }
        Hl7MessageProfileQuery q = new Hl7MessageProfileQuery();
        q.affinityDomains().keyword().eq(inAffinityDomainKeyword.trim().toUpperCase(Locale.getDefault()));
        List<Domain> domains = q.domain().getListDistinct();
        if ((domains != null) && (domains.size() > 0)) {
            return domains;
        } else {
            return null;
        }
    }

    public static List<Transaction> getListOfAvailableTransactionsForGivenActorAndDomain(String inActorKeyword,
                                                                                         String inDomainKeyword) {
        if ((inActorKeyword == null) || (inDomainKeyword == null) || (inDomainKeyword.length() == 0) || (
                inActorKeyword.length() == 0)) {
            return null;
        }
        Hl7MessageProfileQuery q = new Hl7MessageProfileQuery();

        q.actor().keyword().eq(inActorKeyword.trim().toUpperCase(Locale.getDefault()));
        q.domain().keyword().eq(inDomainKeyword.trim().toUpperCase(Locale.getDefault()));
        List<Transaction> transactions = q.transaction().getListDistinct();
        if ((transactions != null) && (transactions.size() > 0)) {
            Collections.sort(transactions);
            return transactions;
        } else {
            return null;
        }
    }

    public static List<String> getListOfAvailableMessageTypesForGivenContext(String inActorKeyword,
                                                                             String inDomainKeyword, String inTransactionKeyword) {
        if ((inActorKeyword == null) || (inDomainKeyword == null) || (inTransactionKeyword == null) || (
                inDomainKeyword.length() == 0) || (inActorKeyword.length() == 0) || (inTransactionKeyword.length()
                == 0)) {
            return null;
        }
        Hl7MessageProfileQuery q = new Hl7MessageProfileQuery();

        q.actor().keyword().eq(inActorKeyword.trim().toUpperCase(Locale.getDefault()));
        q.domain().keyword().eq(inDomainKeyword.trim().toUpperCase(Locale.getDefault()));
        q.transaction().keyword().eq(inTransactionKeyword.trim().toUpperCase(Locale.getDefault()));

        List<String> messageTypes = q.messageType().getListDistinct();
        if ((messageTypes != null) && (messageTypes.size() > 0)) {
            Collections.sort(messageTypes);
            return messageTypes;
        } else {
            return null;
        }
    }

    public static List<String> getListOfAvailableMessageCodesForGivenMessageType(String inMessageType) {
        if ((inMessageType == null) || (inMessageType.length() == 0)) {
            return null;
        }

        Hl7MessageProfileQuery q = new Hl7MessageProfileQuery();

        q.messageType().eq(inMessageType.trim().toUpperCase(Locale.getDefault()));

        List<String> codes = q.messageOrderControlCode().getListDistinct();
        if ((codes != null) && (codes.size() > 0)) {
            Collections.sort(codes);
            return codes;
        } else {
            return null;
        }
    }

    public static List<String> getListOfAvailableHL7Version(String inDomainKeyword, String inActorKeyword,
                                                            String inTransactionKeyword, String inMessageType) {
        if ((inMessageType == null) || (inMessageType.length() == 0)) {
            return null;
        }
        Hl7MessageProfileQuery q = new Hl7MessageProfileQuery();

        q.actor().keyword().eq(inActorKeyword.trim().toUpperCase(Locale.getDefault()));
        q.domain().keyword().eq(inDomainKeyword.trim().toUpperCase(Locale.getDefault()));
        q.transaction().keyword().eq(inTransactionKeyword.trim().toUpperCase(Locale.getDefault()));
        q.messageType().eq(inMessageType.trim().toUpperCase(Locale.getDefault()));

        List<String> versions = q.hl7Version().getListDistinct();
        if ((versions != null) && (versions.size() > 0)) {
            Collections.sort(versions);
            return versions;
        } else {
            return null;
        }
    }

    public static Hl7MessageProfile addMessageProfileReference(Hl7MessageProfile inHl7MessageProfile) {
        if (inHl7MessageProfile == null) {
            return null;
        }
        EntityManager em = EntityManagerService.provideEntityManager();
        try {
            em.merge(inHl7MessageProfile);
            em.flush();
            return inHl7MessageProfile;
        } catch (Exception e) {
            log.error("cannot add message profile to database", e);
            return null;
        }
    }

    public static void deleteProfile(Hl7MessageProfile selectedProfile) {
        if (selectedProfile != null) {
            EntityManager em = EntityManagerService.provideEntityManager();
            em.remove(selectedProfile);
            em.flush();
        }
    }

    public Integer getId() {
        return id;
    }

    public String getProfileOid() {
        return profileOid;
    }

    public void setProfileOid(String inProfileOid) {
        this.profileOid = inProfileOid;
    }

    public String getHl7Version() {
        return hl7Version;
    }

    public void setHl7Version(String inHl7Version) {
        this.hl7Version = inHl7Version;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String inProfileType) {
        this.profileType = inProfileType;
    }

    public String getProfileContent() {
        return profileContent;
    }

    public void setProfileContent(String inProfileContent) {
        this.profileContent = inProfileContent;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain inDomain) {
        this.domain = inDomain;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor inActor) {
        this.actor = inActor;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction inTransaction) {
        this.transaction = inTransaction;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String inMessageType) {
        this.messageType = inMessageType;
    }

    public String getMessageOrderControlCode() {
        return messageOrderControlCode;
    }

    public void setMessageOrderControlCode(String inMessageOrderControlCode) {
        this.messageOrderControlCode = inMessageOrderControlCode;
    }

    public List<AffinityDomain> getAffinityDomains() {
        return affinityDomains;
    }

    public void setAffinityDomains(List<AffinityDomain> affinityDomains) {
        this.affinityDomains = affinityDomains;
    }

    public boolean compare(Hl7MessageProfile hmp1, Hl7MessageProfile hmp2) {
        return (hmp1.profileOid == hmp2.profileOid);
    }

    public boolean compareTo(Hl7MessageProfile hmp) {
        return profileOid == hmp.profileOid;
    }

    public Hl7MessageProfile saveOrMerge(EntityManager entityManager) {
        Hl7MessageProfile hl7MessageProfile;
        if (domain != null) {
            domain.saveOrMerge(entityManager);
        }
        if (actor != null) {
            actor.saveOrMerge(entityManager);
        }
        if (transaction != null) {
            transaction = transaction.saveOrMerge(entityManager);
        }
        if (affinityDomains != null) {
            for (AffinityDomain affinityDomain : affinityDomains) {
                affinityDomain.saveOrMerge(entityManager);
            }
        }

        Hl7MessageProfileQuery query = new Hl7MessageProfileQuery(entityManager);
        query.profileOid().eq(this.profileOid);
        Hl7MessageProfile foundHl7MessageProfile = query.getUniqueResult();

        hl7MessageProfile = Mergeable.useExistingVersionIfItExists(foundHl7MessageProfile, this, entityManager);

        return hl7MessageProfile;
    }
}
