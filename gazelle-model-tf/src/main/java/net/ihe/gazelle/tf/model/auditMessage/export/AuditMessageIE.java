package net.ihe.gazelle.tf.model.auditMessage.export;

import net.ihe.gazelle.tf.model.auditMessage.AuditMessage;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="auditMessages")
@XmlAccessorType(XmlAccessType.FIELD)
public class AuditMessageIE {

    @XmlElementRefs({@XmlElementRef(name = "auditMessage")})
    private List<AuditMessage> auditMessages;

    @XmlTransient
    private List<AuditMessage> ignoredAuditMessages;

    @XmlTransient
    private List<String> unknownTransactions;

    @XmlTransient
    private List<String> unknownActors;

    @XmlTransient
    private  List<String> unknownSections;

    @XmlTransient
    private List<AuditMessage> duplicatedAuditMessages;

    @XmlTransient
    private boolean imported;

    public AuditMessageIE(){
        auditMessages = new ArrayList<AuditMessage>();
    }

    public AuditMessageIE(List<AuditMessage> auditMessages){
        if (auditMessages != null){
            this.auditMessages = auditMessages;
        }
    }

    public List<AuditMessage> getAuditMessages() {
        return auditMessages;
    }

    public void setAuditMessages(List<AuditMessage> auditMessages) {
        this.auditMessages = auditMessages;
    }

    public List<AuditMessage> getIgnoredAuditMessages() {
        return ignoredAuditMessages;
    }

    public void setIgnoredAuditMessages(List<AuditMessage> ignoredAuditMessages) {
        this.ignoredAuditMessages = ignoredAuditMessages;
    }

    public void addIgnoredAuditMessage(AuditMessage ignoredAuditMessages){
        if (this.ignoredAuditMessages == null){
            this.ignoredAuditMessages = new ArrayList<>();
        }
        this.ignoredAuditMessages.add(ignoredAuditMessages);
    }

    public List<String> getUnknownTransactions() {
        return unknownTransactions;
    }

    public void setUnknownTransactions(List<String> unknownTransactions) {
        this.unknownTransactions = unknownTransactions;
    }

    public void addUnknownTransaction(String transaction){
        if (this.unknownTransactions == null){
            this.unknownTransactions = new ArrayList<>();
        }
        this.unknownTransactions.add(transaction);
    }

    public List<String> getUnknownActors() {
        return unknownActors;
    }

    public void setUnknownActors(List<String> unknownActors) {
        this.unknownActors = unknownActors;
    }

    public void addUnknownActor(String actor){
        if (this.unknownActors == null){
            this.unknownActors = new ArrayList<>();
        }
        this.unknownActors.add(actor);
    }

    public List<String> getUnknownSections() {
        return unknownSections;
    }

    public void setUnknownSections(List<String> unknownSections) {
        this.unknownSections = unknownSections;
    }

    public void addUnknownSection(String section){
        if (this.unknownSections == null) {
            this.unknownSections = new ArrayList<>();
        }
        this.unknownSections.add(section);
    }

    public List<AuditMessage> getDuplicatedAuditMessages() {
        return duplicatedAuditMessages;
    }

    public void setDuplicatedAuditMessages(List<AuditMessage> duplicatedAuditMessages) {
        this.duplicatedAuditMessages = duplicatedAuditMessages;
    }

    public void addDuplicatedAuditMessage(AuditMessage auditMessage){
        if (this.duplicatedAuditMessages == null){
            this.duplicatedAuditMessages = new ArrayList<>();
        }
        this.duplicatedAuditMessages.add(auditMessage);
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        this.imported = imported;
    }
}
