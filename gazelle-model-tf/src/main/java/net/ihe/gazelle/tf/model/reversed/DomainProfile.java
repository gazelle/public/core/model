package net.ihe.gazelle.tf.model.reversed;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "DomainProfile")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "tf_domain_profile", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "domain_id", "integration_profile_id"}))
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class DomainProfile {

    private static Logger log = LoggerFactory.getLogger(DomainProfile.class);
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "domainId", column = @Column(name = "domain_id", nullable = false)),
            @AttributeOverride(name = "profileId", column = @Column(name = "integration_profile_id", nullable = false))})
    @XmlTransient
    private DomainProfileId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "domain_id", nullable = false, insertable = false, updatable = false)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Domain domain;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "integration_profile_id", nullable = false, insertable = false, updatable = false)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @XmlTransient
    private IntegrationProfile profile;

    public DomainProfile() {
        super();
    }

    public DomainProfileId getId() {
        return id;
    }

    public void setId(DomainProfileId id) {
        this.id = id;
    }

    public Domain getDomain() {
        return HibernateHelper.getLazyValue(this, "domain", this.domain);
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public IntegrationProfile getProfile() {
        return HibernateHelper.getLazyValue(this, "profile", this.profile);
    }

    public void setProfile(IntegrationProfile profile) {
        this.profile = profile;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }
}
