package net.ihe.gazelle.tf.model.export;

import net.ihe.gazelle.tf.model.Standard;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="standards")
@XmlAccessorType(XmlAccessType.FIELD)
public class StandardIE {

    @XmlElementRefs({@XmlElementRef(name = "standard")})
    private List<Standard> standards;

    @XmlTransient
    private List<Standard> duplicatedStandards;

    @XmlTransient
    private List<String> unknownTransaction;

    @XmlTransient
    private boolean isImported;

    public StandardIE(){
        standards = new ArrayList<Standard>();
    }

    public StandardIE(List<Standard> aipoRules){
        if (aipoRules != null){
            this.standards = aipoRules;
        }
    }

    public List<Standard> getStandards() {
        return standards;
    }

    public void setStandards(List<Standard> standards) {
        this.standards = standards;
    }

    public List<Standard> getDuplicatedStandards() {
        return duplicatedStandards;
    }

    public void setDuplicatedStandards(List<Standard> duplicatedStandards) {
        this.duplicatedStandards = duplicatedStandards;
    }

    public void addDuplicatedStandard(Standard standard){
        if (this.duplicatedStandards != null){
            this.duplicatedStandards.add(standard);
        } else {
            this.duplicatedStandards = new ArrayList<Standard>();
            this.duplicatedStandards.add(standard);
        }
    }

    public List<String> getUnknownTransaction() {
        return unknownTransaction;
    }

    public void setUnknownTransaction(List<String> unknownTransaction) {
        this.unknownTransaction = unknownTransaction;
    }

    public void addUnknownTransaction(String transactionKeyword) {
        if (this.unknownTransaction != null) {
            this.unknownTransaction.add(transactionKeyword);
        } else {
            this.unknownTransaction = new ArrayList<>();
            this.unknownTransaction.add(transactionKeyword);
        }
    }

    public boolean isImported() {
        return isImported;
    }

    public void setImported(boolean imported) {
        isImported = imported;
    }
}
