/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.common.model;

import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetGazelle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * <b>Class Description : </b>ApplicationPreference<br>
 * <br>
 * This class describes the ApplicationPreference object, used by the Gazelle application. An ApplicationPreference is a property and an associated value which are used by the application as
 * configuration. ApplicationPreference objects are stored in the database.
 * <p>
 * This class belongs to the ProductRegistry-ejb module.
 * <p>
 * User possesses the following attributes :
 * <ul>
 * <li><b>id</b> : Id of the application preference</li>
 * <li><b>preferenceName</b> : Name of the preference (eg. "application_url")</li>
 * <li><b>preferenceValue</b> : Value of the preference (eg. "http://yoko.irisa.fr:8080/ProductRegistry/")</li>
 * <li><b>description</b> : Description of this preference (eg. "URL used to reach this application from the registration email"</li>
 * </ul>
 * </br> <b>Example of ApplicationPreference</b> : (1, 'application_url', 'http://yoko.irisa.fr:8080/ProductRegistry/', 'URL used to reach this application from the registration email'); <br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, April 25th
 * @class ApplicationPreference.java
 * @package net.ihe.gazelle.users
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@Entity
@Table(name = "cmn_application_preference", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "preference_name"))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "cmn_application_preference_sequence", sequenceName = "cmn_application_preference_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetGazelle.class)
public class ApplicationPreference implements Serializable {
    // ~ Attributes
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -5994534457643398088L;

    private static Logger log = LoggerFactory.getLogger(ApplicationPreference.class);

    /**
     * Id of this object
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cmn_application_preference_sequence")
    private Integer id;

    /**
     * Name of the preference (eg. "application_url")
     */
    @Size(min = 1, max = 64)
    @NotNull
    @Column(name = "preference_name")
    private String preferenceName;

    /**
     * Value of the preference (eg. "http://yoko.irisa.fr:8080")
     */
    @NotNull
    @Column(name = "preference_value", length = 512)
    private String preferenceValue;

    /**
     * Description of this preference (eg. "URL used to reach this application from the registration email"
     */
    @NotNull
    @Column(name = "description")
    private String description;

    /**
     * class_name for the content of preference value
     */
    @NotNull
    @Column(name = "class_name")
    private String className;

    // ~ Methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static String getCertificatesUrlFromPreferences() {
        return get_preference_from_db("certificates_url").getPreferenceValue();
    }

    public static String getTlsUrl() {
        return get_preference_from_db("tls_url").getPreferenceValue();
    }

    public static ApplicationPreference get_preference_from_db(String key) {
        ApplicationPreference applicationPreference = null;
        ApplicationPreferenceQuery q = new ApplicationPreferenceQuery();
        q.preferenceName().eq(key);
        q.setCachable(true);

        try {
            applicationPreference = (ApplicationPreference) q.getUniqueResult();
        } catch (Throwable e) {
            log.error("Failed to load preference with the key : " + key);
        }

        return applicationPreference;
    }

    /**
     * Get the object id
     *
     * @return Integer : Object Id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set the object id
     *
     * @param Integer : Object Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get the preference name
     *
     * @return preferenceName : String corresponding to the name of this preference
     */
    public String getPreferenceName() {
        return preferenceName;
    }

    /**
     * Get the preference name
     *
     * @param preferenceName : String corresponding to the name of this preference
     */
    public void setPreferenceName(final String preferenceName) {
        this.preferenceName = preferenceName;
    }

    /**
     * Get the preference preferenceValue
     *
     * @return preferenceValue : String corresponding to the preferenceValue of this preference
     */
    public String getPreferenceValue() {
        return preferenceValue;
    }

    /**
     * Set the preference preferenceValue
     *
     * @param preferenceValue : String corresponding to the preferenceValue of this preference
     */
    public void setPreferenceValue(final String value) {
        this.preferenceValue = value;
    }

    /**
     * Get the preference name
     *
     * @return description : String describing this preference
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the preference description
     *
     * @param description : String describing this preference
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public final int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public final boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }
}
