/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.common.model;

//JPA imports

import net.ihe.gazelle.dates.TimestampNano;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.jboss.seam.Component;
import org.jboss.seam.contexts.Contexts;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>Audit Module<br>
 * <br>
 * This abstract class is intended to be extended by others classes, for the purpose of implementing audit module. This module helps to keep informations about changes, these informations are stored
 * in the database.
 * <ul>
 * <li><b>lastChanged</b> : When the last change has been performed.</li>
 * <li><b>lastModifiedId</b> : Who did the last change (which user)</li>
 * </ul>
 * </br> These attributes are mapped with the database, in the table corresponding to the associated object. <br>
 * <b>Example </b> : If an Actor object extends AuditModule, it will get these two additional attributes, and two associated columns in its table. <br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 21
 * @class AuditModule.java
 * @package net.ihe.gazelle.common.audit
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@MappedSuperclass
public abstract class AuditedObject {

    /**
     * Attribute lastChanged
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_changed", columnDefinition = "timestamp with time zone")
    protected Date lastChanged;

    /**
     * Attribute lastModifierId
     */
    @Column(name = "last_modifier_id")
    protected String lastModifierId;

    //TODO
    public static <T> List<T> listAllObjectsOrderByKeyword(Class<T> clazz) {
        HQLQueryBuilder<T> hqlQueryBuilder = new HQLQueryBuilder<T>(clazz);
        hqlQueryBuilder.addOrder("keyword", true);
        return hqlQueryBuilder.getList();

    }

    //TODO
    public static <T> T getObjectByKeyword(Class<T> clazz, String inKeyword) {
        HQLQueryBuilder<T> hqlQueryBuilder = new HQLQueryBuilder<T>(clazz);
        hqlQueryBuilder.addEq("keyword", inKeyword);
        return hqlQueryBuilder.getUniqueResult();
    }

    /**
     * Getter used for the lastChanged attribute.
     *
     * @return lastChanged : Timestamp corresponding to the last change.
     */
    @XmlTransient
    public Date getLastChanged() {
        return lastChanged;
    }

    /**
     * Setter used for the lastChanged attribute.
     *
     * @param lastChanged : Timestamp corresponding to the last change.
     */
    public void setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
    }

    /**
     * Getter used for the lastModifierId attribute.
     *
     * @return lastModifierId : Id of the user who performed the last change.
     */
    @XmlTransient
    public String getLastModifierId() {
        return lastModifierId;
    }

    /**
     * Setter used for the lastModifierId attribute.
     *
     * @param lastModifierId : Id of the user who performed the last change.
     */
    public void setLastModifierId(String lastModifierId) {
        this.lastModifierId = lastModifierId;
    }

    /**
     * Init Audit informations. Get the current time, find and set the user id logged in.
     */
    @PrePersist
    @PreUpdate
    public void init() {
        this.setLastChanged(new TimestampNano());
        if (Contexts.isApplicationContextActive() && Contexts.isSessionContextActive()) {
            GazelleIdentity identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
            if (identity.isLoggedIn()) {
                String newId = identity.getUsername();
                // do not override the username if the new value is empty
                if (newId != null && !newId.isEmpty()) {
                    this.setLastModifierId(newId);
                }
            }
        }
    }

    @Override
    public int hashCode() {
        return HibernateHelper.getLazyHashcode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return HibernateHelper.getLazyEquals(this, obj);
    }

}
