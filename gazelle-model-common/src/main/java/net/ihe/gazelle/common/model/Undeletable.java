package net.ihe.gazelle.common.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Add @Where(clause = "soft_deleted <> 1") and @SQLDelete(sql = "update table_name set soft_deleted = 1 where id=?") on the entity inheriting this class
 * <p/>
 * Add @Where(clause = "soft_deleted <> 1") on each reference to inherited class in a @OneToMany or @ManyToMany
 * <p/>
 * What about unicity constraints?
 */
@MappedSuperclass
public abstract class Undeletable extends AuditedObject {

    @Column(name = "soft_deleted")
    protected int softDeleted = 0;

    public int getSoftDeleted() {
        return softDeleted;
    }

    public void setSoftDeleted(int softDeleted) {
        this.softDeleted = softDeleted;
    }

}
