package net.ihe.gazelle.common.utils;

import javax.persistence.EntityManager;

public class Mergeable {

    private Mergeable() {
    }

    public static <E> E useExistingVersionIfItExists(E existing, E newOne, EntityManager em) {
        E merged;
        if (existing != null) {
            merged = existing;
        } else {
            em.persist(newOne);
            merged = newOne;
        }
        return merged;
    }
}
