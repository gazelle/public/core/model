/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.common.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * <b>Class Description : </b>LabelKeywordDescriptionClass<br>
 * <br>
 * This abstract class is intended to be extended by others classes, for the purpose of implementing a trio of label keyword and description. T
 *
 * @author Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, jun 08
 * @class LabelKeywordDescriptionClass.java
 * @package net.ihe.gazelle.common.audit
 * @see > jmeyer@irisa.fr - http://www.ihe-europe.org
 */

@MappedSuperclass
public abstract class LabelKeywordDescriptionClass extends AuditedObject {

    @Column(name = "keyword", unique = true)
    private String keyword;

    @Column(name = "label_to_display")
    private String labelToDisplay;

    @Column(name = "description")
    private String description;

    @Column(name = "rank")
    private Integer rank;

    public LabelKeywordDescriptionClass() {

    }

    public LabelKeywordDescriptionClass(String keyword, String labelToDisplay, String description, Integer rank) {
        this.description = description;
        this.labelToDisplay = labelToDisplay;
        this.keyword = keyword;
        this.rank=rank;
    }

    public LabelKeywordDescriptionClass(LabelKeywordDescriptionClass inLabelKeywordDescriptionClass) {
        if (inLabelKeywordDescriptionClass.getDescription() != null) {
            this.description = inLabelKeywordDescriptionClass.getDescription();
        }
        if (inLabelKeywordDescriptionClass.getKeyword() != null) {
            this.keyword = inLabelKeywordDescriptionClass.getKeyword();
        }
        if (inLabelKeywordDescriptionClass.getLabelToDisplay() != null) {
            this.labelToDisplay = inLabelKeywordDescriptionClass.getLabelToDisplay();
        }
        if (inLabelKeywordDescriptionClass.getRank() != null) {
            this.rank = inLabelKeywordDescriptionClass.getRank();
        }
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

    public void setLabelToDisplay(String labelToDisplay) {
        this.labelToDisplay = labelToDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }
}
