package net.ihe.gazelle.tm.utils;

import junit.framework.TestCase;
import net.ihe.gazelle.common.utils.HibernateConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * You subclass this TestCase to perform queries on a local Gazelle database. In eclipse using Project Properties -> Maven -> Lifecycle Mapping page, add process-test-resources in
 * "Goals to invoke when resource change" text box.
 *
 * @author glandais
 */
@Deprecated
public abstract class AbstractTestQuery extends TestCase {

    public static final String HIBERNATE_XML = HibernateConfiguration.HIBERNATE_XML;
    private static Logger logger = LoggerFactory.getLogger(AbstractTestQuery.class);
    protected PrintWriter log;
    private StringWriter sw;
    private boolean showsql = true;
    private String server = "localhost";
    private String db = "gazelle-dev";

    public boolean showSql() {
        return showsql;
    }

    public void setShowSql(boolean showsql) {
        this.showsql = showsql;
    }

    protected void setUp() throws Exception {
        logger.info("setup StandardBasicTypes JPA layer.");
        super.setUp();
        sw = new StringWriter();
        log = new PrintWriter(sw);
        HibernateConfiguration.setConfigOnCurrentThread("jdbc:postgresql://" + server + "/" + getDb(), "update",
                showSql());
    }

    protected void tearDown() throws Exception {
        super.tearDown();
        logger.info("Shuting down StandardBasicTypes JPA layer.");
        printLog();
    }

    protected void printLog() {
        System.out.println(sw.getBuffer().toString());
        sw = new StringWriter();
        log = new PrintWriter(sw);
    }

    public String getDb() {
        return this.db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public void setServer(String server) {
        this.server = server;
    }

}
