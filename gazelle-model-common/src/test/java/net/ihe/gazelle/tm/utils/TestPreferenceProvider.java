package net.ihe.gazelle.tm.utils;

import net.ihe.gazelle.preferences.PreferenceProvider;
import org.kohsuke.MetaInfServices;

import java.util.Date;

@MetaInfServices(PreferenceProvider.class)
public class TestPreferenceProvider implements PreferenceProvider {

    @Override
    public Object getObject(Object key) {
        return getString((String) key);
    }

    @Override
    public String getString(String key) {
        return null;
    }

    @Override
    public Integer getInteger(String key) {
        return null;
    }

    @Override
    public Boolean getBoolean(String key) {
        return false;
    }

    @Override
    public Date getDate(String key) {
        return null;
    }

    @Override
    public void setObject(Object key, Object value) {

    }

    @Override
    public void setString(String key, String value) {

    }

    @Override
    public void setInteger(String key, Integer value) {

    }

    @Override
    public void setBoolean(String key, Boolean value) {

    }

    @Override
    public void setDate(String key, Date value) {

    }

    @Override
    public int compareTo(PreferenceProvider arg0) {
        return getWeight().compareTo(arg0.getWeight());
    }

    @Override
    public Integer getWeight() {
        return -1000;
    }

}
