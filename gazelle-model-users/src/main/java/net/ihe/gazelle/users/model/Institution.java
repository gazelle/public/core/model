/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.users.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * <b>Class Description : </b>Institution<br>
 * <br>
 * This class describes the Institution object, used by the Gazelle application. An institution corresponds to a company, an institute, a sponsor, etc... An institution possesses users (for the
 * application), systems, addresses, and persons. This class belongs to the Users Framework module.
 * <p/>
 * Address possesses the following attributes :
 * <ul>
 * <li><b>id</b> : Institution id</li>
 * <li><b>name</b> : Name of the Institution</li>
 * <li><b>keyword</b> : Keyword of the Institution</li>
 * <li><b>note</b> : Note corresponding to the Institution</li>
 * <li><b>url</b> : URL of the Institution's website</li>
 * <li><b>institutionType</b> : Type of the Institution (company, public institute, sponsor)</li>
 * </ul>
 * </br> <b>Example of Institution</b> : ('INRIA', 'Institut National de Recherche en Informatique et Automatisme', 'http://www.inria.fr'); <br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, April 15th
 * @class Institution.java
 * @package net.ihe.gazelle.users
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */
@Entity
@Table(name = "usr_institution", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "usr_institution_sequence", sequenceName = "usr_institution_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
@Inheritance(strategy = InheritanceType.JOINED)
public class Institution extends AuditedObject implements Serializable, Comparable<Institution> {
    // ~ Statics variables and Class initializer
    // ///////////////////////////////////////////////////////////////////////

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = 1881413500711474531L;
    private static Logger log = LoggerFactory.getLogger(Institution.class);

    // ~ Attributes
    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Institution id
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usr_institution_sequence")
    private Integer id;

    /**
     * Name of the Institution
     */
    @NotNull(message = "Please provide your name")
    @Column(name = "name")
    private String name;

    /**
     * Keyword of the Institution
     */
    @Size(min = 1, max = 32)
    @Column(unique = true)
    @NotNull(message = "Please provide your keyword")
    @Pattern(regexp = "[^\\\\/]*", message = "{gazelle.validator.nameWithoutSlash}")
    private String keyword;

    /**
     * Note corresponding to the Institution
     */
    @Size(max = 1024)
    private String note;

    /**
     * URL of the Institution's website
     */
    @Size(max = 512)
    @NotNull(message = "Please provide your url")
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$|^ftp://.+$)", message = "{gazelle.validator.http}")
    private String url;

    /**
     * This is the URL of the repository containing Integration Statements of this company
     */
    @Column(name = "integration_statements_repository_url", nullable = true, updatable = true)
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$|^ftp://.+$)", message = "{gazelle.validator.http}")
    @Size(max = 512)
    private String integrationStatementsRepositoryUrl;

    // Variables used for Foreign Keys : institution_type table
    /**
     * Type of the Institution (company, public institute, sponsor)
     */
    @ManyToOne
    @JoinColumn(name = "institution_type_id")
    @NotNull(message = "Please select your institution type")
    @Fetch(value = FetchMode.SELECT)
    private InstitutionType institutionType;

    // Variables used for Foreign Keys with Address
    /**
     * Mailing Address corresponding to this institution
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mailing_address_id")
    @Fetch(value = FetchMode.SELECT)
    private Address mailingAddress;

    /**
     * Flag, it indicates if the institution has been activated - An institution is activated when creating user (from a new company) clicks on the activation link in the activation email
     */
    @Column(name = "activated")
    private Boolean activated;

    // Variables used for Foreign Keys : usr_institution_address table
    /**
     * Addresses of this institution
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @JoinTable(name = "usr_institution_address", joinColumns = @JoinColumn(name = "institution_id"), inverseJoinColumns = @JoinColumn(name = "address_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "institution_id", "address_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<Address> addresses;


    @OneToMany(mappedBy = "institution", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<Person> contacts;

    // ~ Constructors
    // //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new Institution object.
     *
     * @param name    : Name of this institution
     * @param keyword : Keyword of this institution
     * @param url     : URL of this institution
     */
    public Institution(final String name, final String keyword, final String url) {
        this.name = name;
        this.keyword = keyword;
        this.url = url;

    }

    /**
     * Creates a new Institution object.
     *
     * @param id      : id of this institution
     * @param name    : Name of this institution
     * @param keyword : Keyword of this institution
     * @param url     : URL of this institution
     */
    public Institution(Integer id, String name, String keyword, String url) {
        this.id = id;
        this.name = name;
        this.keyword = keyword;
        this.url = url;

    }

    /**
     * Creates a new Institution object.
     */
    public Institution() {

    }

    public Institution(String name2) {
        this.name = name2;

    }

    // ~ Methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Get the institution of the current logged in user This method is static, and used by SystemManager, etc...
     *
     * @return Institution of the logged in user
     */
    public static Institution getLoggedInInstitution() {
        GazelleIdentity identity = (GazelleIdentity) Component.getInstance("org.jboss.seam.security.identity");
        if (identity.isLoggedIn()) {
            return Institution.findInstitutionWithKeyword(identity.getOrganisationKeyword());
        } else {
            return null;
        }
    }

    public static Institution findInstitutionWithName(String name) {
        InstitutionQuery query = new InstitutionQuery();
        query.name().eq(name);
        return query.getUniqueResult();
    }

    public static Institution findInstitutionWithKeyword(String inKeyword) {
        InstitutionQuery query = new InstitutionQuery();
        query.keyword().eq(inKeyword);
        return query.getUniqueResult();
    }

    public static Institution findInstitutionWithId(Integer institutionId){
        InstitutionQuery query = new InstitutionQuery();
        query.id().eq(institutionId);
        return query.getUniqueResult();
    }

    public static void deleteInstitution(String name) {
        EntityManager em = EntityManagerService.provideEntityManager();
        if (name != null) {
            Institution institutionToDelete = findInstitutionWithName(name);
            em.remove(institutionToDelete);
        }
    }

    public static void deleteInstitution(Integer id) {
        EntityManager em = EntityManagerService.provideEntityManager();
        if (id != null) {
            Institution inst = em.find(Institution.class, id);
            em.remove(inst);
            em.flush();
        }
    }

    public static void deleteInstitutionWithoutDependencies(Institution inst) throws Exception {
        EntityManager em = EntityManagerService.provideEntityManager();
        if (inst != null) {
            if (inst.getId() != null) {
                inst = em.find(Institution.class, inst.getId());
            } else if (inst.getName() != null) {
                inst = findInstitutionWithName(inst.getName());
            }
            try {
                Address mailinAddress = inst.getMailingAddress();
                em.remove(inst);
                em.flush();
                if ((mailinAddress != null) && (mailinAddress.getAddress() != null)) {
                    Address.deleteAddress(mailinAddress);
                }
            } catch (Exception e) {
                log.error("", e);
                throw new Exception("Cannot delete institution", e);
            }
        }
    }

    public static List<Institution> listAllInstitutions() {
        InstitutionQuery query = new InstitutionQuery();
        query.id().gt(0);
        query.name().order(true, true);
        return query.getList();
    }

    public static List<Institution> listAllInstitutionsOrderByKeyword() {
        InstitutionQuery query = new InstitutionQuery();
        query.id().gt(0);
        query.keyword().order(true, true);
        return query.getList();
    }

    public static List<Institution> getPossibleInstitutions() {
        InstitutionQuery q = new InstitutionQuery();
        q.name().order(true, true);
        List<Institution> possibleInstitutionsForRegistration = q.getList();

        return possibleInstitutionsForRegistration;
    }

    @FilterLabel
    public String getFilterLabel() {
        return getKeyword() + " - " + getName();
    }

    /**
     * Get the Id of this institution
     *
     * @return : Integer id of this institution
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * Set the Id of this institution
     *
     * @param id : Id of this institution
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Get the Name of this institution
     *
     * @return name : Name of this institution
     */
    public String getName() {
        return name;
    }

    /**
     * Set the Name of this institution
     *
     * @param name : Name of this institution
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Get the Keyword of this institution (eg. INRIA or MIR)
     *
     * @return Keyword of this institution
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * Set the Keyword of this institution (eg. INRIA or MIR)
     *
     * @param keyword : Keyword of this institution
     */
    public void setKeyword(final String keyword) {
        this.keyword = keyword;
    }

    /**
     * Get the Note (comment) of this institution
     *
     * @return Note (comment) for this institution
     */
    public String getNote() {
        return note;
    }

    // *********************************************************
    // Foreign Key for object : Address
    // *********************************************************

    /**
     * Get the Note (comment) of this institution
     *
     * @param note : Note (comment) for this institution
     */
    public void setNote(final String note) {
        this.note = note;
    }

    /**
     * Get the Institution type of this institution (eg. Public, Sponsor, Company)
     *
     * @return Institution type of this institution (eg. Public, Sponsor, Company)
     */
    public InstitutionType getInstitutionType() {
        return this.institutionType;
    }

    /**
     * Set the Institution type of this institution (eg. Public, Sponsor, Company)
     *
     * @param institutionType : Institution type of this institution (eg. Public, Sponsor, Company)
     */
    public void setInstitutionType(final InstitutionType institutionType) {
        this.institutionType = institutionType;
    }

    /**
     * Get the URL of this institution
     *
     * @return String : URL of this institution
     */
    public String getUrl() {
        return url;
    }

    /**
     * Set the URL of this institution
     *
     * @param url : URL of this institution
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    public Set<Person> getContacts() {
        Hibernate.initialize(contacts);
        return contacts;
    }

    public void setContacts(Set<Person> contacts) {
        this.contacts = contacts;
    }

    /**
     * Get the URL of the repository containing Integration Statements of this company
     *
     */
    public String getIntegrationStatementsRepositoryUrl() {
        return integrationStatementsRepositoryUrl;
    }

    /**
     * Set the URL of the repository containing Integration Statements of this company
     *
     * @param integrationStatementsRepositoryUrl : URL of the repository containing Integration Statements of this company
     */
    public void setIntegrationStatementsRepositoryUrl(String integrationStatementsRepositoryUrl) {
        this.integrationStatementsRepositoryUrl = integrationStatementsRepositoryUrl;
    }

    /**
     * Method to return a String, with the institution name information
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Institution(" + name + ")";
    }

    /**
     * Get the mailing address of this institution
     *
     */
    public Address getMailingAddress() {
        return HibernateHelper.getLazyValue(this, "mailingAddress", this.mailingAddress);
    }

    /**
     * Set the mailing address of this institution
     *
     * @param mailingAddress : Address used for mailing
     */
    public void setMailingAddress(Address mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    /**
     * Get all addresses of this institution
     *
     * @return List<Address> : Addresses
     */
    public List<Address> getAddresses() {
        return addresses;
    }

    /**
     * Set all addresses of this institution
     *
     * @param addresses <Address> : Addresses
     */
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    /**
     * Get the boolean flag - It indicates if the institution is activated - An institution is activated when creating user (from a new company) clicks on the activation link in the activation email
     *
     */
    public Boolean getActivated() {
        return activated;
    }

    /**
     * Set the boolean flag - It indicates if the institution is activated - An institution is activated when creating user (from a new company) clicks on the activation link in the activation email
     *
     * @param activated : True if the institution is activated
     */
    public void setActivated(Boolean activated) {
        this.activated = activated;
    }


    @Override
    public int compareTo(Institution o) {
        if (o.getName() == null) {
            return -1;
        }
        if (getName() == null) {
            return 1;
        }
        return this.getName().compareToIgnoreCase(o.getName());
    }

}
