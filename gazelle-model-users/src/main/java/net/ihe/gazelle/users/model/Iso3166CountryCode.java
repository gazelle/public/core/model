/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.users.model;

import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * <b>Class Description : </b>Iso3166CountryCode<br>
 * <br>
 * This class describes the Iso3166CountryCode object, used by the Gazelle application. A Iso3166CountryCode is a country, respecting the iso standards. ISO 3166 is a three-part geographic coding
 * standard for coding the names of countries and dependent areas, and the principal subdivisions thereof, published by the International Organization for Standardization (ISO). The official name is
 * Codes for the representation of names of countries and their subdivisions.
 * <p/>
 * The ISO 3166 standard is maintained by the ISO 3166 Maintenance Agency (ISO 3166/MA), whose address is at the ISO central office in Geneva.
 * <p/>
 * This class belongs to the Users module.
 * <p/>
 * Iso3166CountryCode possesses the following attributes :
 * <ul>
 * <li><b>iso</b> : Iso3166CountryCode id</li>
 * <li><b>name</b> : Name of the country</li>
 * <li><b>printableName</b> : printableName of the country</li>
 * <li><b>iso3</b> : iso3 id (not a primary key, but a 3-chars code)</li>
 * <li><b>numcode</b> : Integer code for that country</li>
 * <li><b>ec</b> : Flag indicating if the country is in the European community</li>
 * <li><b>flagUrl</b> :URL to get the image of that country (flag icon)</li>
 * </ul>
 * </br> <b>Example of Iso3166CountryCode</b> : ('US', 'UNITED STATES', 'United States of America', '/img/flag/usa.jpg); <br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, April 15th
 * @class Iso3166CountryCode.java
 * @package net.ihe.gazelle.users
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "usr_iso_3166_country_code", schema = "public")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class Iso3166CountryCode implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450911131541200760L;
    private static Logger log = LoggerFactory.getLogger(Iso3166CountryCode.class);
    @Id
    @Column(name = "iso", unique = true, nullable = false, length = 2)
    @NotNull
    @Size(max = 2)
    private String iso;

    @Column(name = "name", nullable = false, length = 80)
    @NotNull
    @Size(max = 80)
    private String name;

    @Column(name = "printable_name", nullable = false, length = 80)
    @NotNull
    @Size(max = 80)
    private String printableName;

    @Column(name = "iso3", length = 3)
    @Size(max = 3)
    private String iso3;

    @Column(name = "numcode")
    private Integer numcode;

    @Column(name = "ec")
    private Boolean ec;

    @Column(name = "flag_url")
    private String flagUrl;

    // Constructors
    public Iso3166CountryCode() {
    }

    public Iso3166CountryCode(String iso, String name, String printableName) {
        this.iso = iso;
        this.name = name;
        this.printableName = printableName;
    }

    public Iso3166CountryCode(String iso, String name, String printableName, String flagUrl) {
        this.iso = iso;
        this.name = name;
        this.printableName = printableName;
        this.flagUrl = flagUrl;
    }

    public Iso3166CountryCode(String iso, String name, String printableName, String iso3, Integer numcode, Boolean ec,
                              Set<Address> addresses) {
        this.iso = iso;
        this.name = name;
        this.printableName = printableName;
        this.iso3 = iso3;
        this.numcode = numcode;
        this.ec = ec;
    }

    // Constructor of copy
    public Iso3166CountryCode(Iso3166CountryCode oldIso) {

        if (oldIso != null) {
            if (oldIso.getIso() != null) {
                this.iso =  String.valueOf(oldIso.getIso());
            }
            if (oldIso.getName() != null) {
                this.name =  String.valueOf(oldIso.getName());
            }
            if (oldIso.getPrintableName() != null) {
                this.printableName =  String.valueOf(oldIso.getPrintableName());
            }
            if (oldIso.getIso3() != null) {
                this.iso3 =  String.valueOf(oldIso.getIso3());
            }
            if (oldIso.getNumcode() != null) {
                this.numcode =  Integer.valueOf(oldIso.getNumcode());
            }
            if (oldIso.getEc() != null) {
                this.ec =  Boolean.valueOf(oldIso.getEc());
            }
            if (oldIso.getFlagUrl() != null) {
                this.flagUrl =  String.valueOf(oldIso.flagUrl);
            }
        }

    }

    // Constructor of copy - Clone
    @Override
    public Object clone() {
        return new Iso3166CountryCode(this.iso, this.name, this.printableName, this.iso3, this.numcode, this.ec, null);
    }

    public String getIso() {
        return this.iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrintableName() {
        return this.printableName;
    }

    public void setPrintableName(String printableName) {
        this.printableName = printableName;
    }

    public String getIso3() {
        return this.iso3;
    }

    public void setIso3(String iso3) {
        this.iso3 = iso3;
    }

    public Integer getNumcode() {
        return this.numcode;
    }

    public void setNumcode(Integer numcode) {
        this.numcode = numcode;
    }

    public Boolean getEc() {
        return this.ec;
    }

    public void setEc(Boolean ec) {
        this.ec = ec;
    }

    public String getFlagUrl() {

        // String imgPath = "img/flags/";
        // return imgPath.toLowerCase().concat(this.iso).concat(".gif");
        return flagUrl;
    }

    public void setFlagUrl(String flagUrl) {
        this.flagUrl = flagUrl;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((iso == null) ? 0 : iso.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Iso3166CountryCode other = (Iso3166CountryCode) obj;
        if (iso == null) {
            if (other.iso != null) {
                return false;
            }
        } else if (!iso.equals(other.iso)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Iso3166CountryCode [iso=" + iso + ", name=" + name + ", printableName=" + printableName + ", iso3="
                + iso3 + ", numcode=" + numcode + ", ec=" + ec + ", flagUrl=" + flagUrl + "]";
    }

    public String getId() {
        return iso;
    }

}
