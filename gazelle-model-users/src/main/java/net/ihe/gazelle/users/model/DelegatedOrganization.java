package net.ihe.gazelle.users.model;


import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "usr_delegated_organization", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "usr_delegated_organization_sequence", sequenceName = "usr_delegated_organization_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
@PrimaryKeyJoinColumn(name = "organization_id")
public class DelegatedOrganization extends Institution {

    private static final long serialVersionUID = 3201950241313985312L;
    @Column(name="external_id")
    private String externalId;
    @Column(name="idp_id")
    private String idpId;

    public DelegatedOrganization() {
    }

    public DelegatedOrganization(String name, String externalId, String keyword, String url, String idpId) {
        super(name, keyword, url);
        this.externalId = externalId;
        this.idpId = idpId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getIdpId() {
        return idpId;
    }

    public void setIdpId(String idpId) {
        this.idpId = idpId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DelegatedOrganization that = (DelegatedOrganization) o;
        return Objects.equals(externalId, that.externalId) && Objects.equals(idpId, that.idpId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), externalId, idpId);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DelegatedInstitution{");
        sb.append("name='").append(super.getName()).append('\'');
        sb.append("keyword='").append(super.getKeyword()).append('\'');
        sb.append("externalId='").append(externalId).append('\'');
        sb.append(", idpId='").append(idpId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
