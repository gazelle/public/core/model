/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.users.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.HibernateHelper;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * <b>Class Description : </b>Institution<br>
 * <br>
 * This class describes the Institution object, used by the Gazelle application. An institution is associated with an institution type. This class belongs to the Users Framework module.
 * <p/>
 * Institution possesses the following attributes :
 * <ul>
 * <li><b>id</b> : ID of this institution type</li>
 * <li><b>type</b> : type of institution (name describing the kind of institution, eg :Sponsor, Company, Public administration...)</li>
 * <li><b>description</b> : Description of this institution type</li>
 * </ul>
 * </br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, January 4
 * @class InstitutionType.java
 * @package net.ihe.gazelle.users
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Entity
@Table(name = "usr_institution_type", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "type"))
@SequenceGenerator(name = "usr_institution_type_sequence", sequenceName = "usr_institution_type_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class InstitutionType extends AuditedObject implements java.io.Serializable {

    private static final long serialVersionUID = 6789126518374637819L;

    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usr_institution_type_sequence")
    private Integer id;

    @Column(name = "type", unique = true, nullable = false, length = 32)
    @NotNull
    @Size(max = 32)
    private String type;

    @Column(name = "description")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "institutionType")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Set<Institution> institutions = new HashSet<Institution>(0);

    // Constructor
    public InstitutionType() {
    }

    public InstitutionType(Integer id) {
        this.id = id;
    }

    public InstitutionType(Integer id, String type) {
        this.id = id;
        this.type = type;
    }

    public InstitutionType(Integer id, String type, String description, Set<Institution> institutions) {
        this.id = id;
        this.type = type;
        this.description = description;
        this.institutions = institutions;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Institution> getInstitutions() {
        return HibernateHelper.getLazyValue(this, "institutions", this.institutions);
    }

    public void setInstitutions(Set<Institution> institutions) {
        this.institutions = institutions;
    }

}
