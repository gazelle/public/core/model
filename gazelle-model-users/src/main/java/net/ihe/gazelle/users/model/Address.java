/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.users.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <b>Class Description : </b>Address<br>
 * <br>
 * This class describes the Address object, used by the Gazelle application. An address is associated with a person or an institution. This class belongs to the Users Framework module.
 * <p/>
 * Address possesses the following attributes :
 * <ul>
 * <li><b>id</b> : Address id</li>
 * <li><b>iso3166CountryCode</b> : Country (iso3166CountryCode object)</li>
 * <li><b>address</b> : Address (number, street, avenue, etc...)</li>
 * <li><b>city</b> : Name of the city</li>
 * <li><b>zipCode</b> : Zipcode of the city</li>
 * <li><b>state</b> : Name of the state containing this city</li>
 * <li><b>phone</b> : International phone number</li>
 * <li><b>fax</b> : International fax number</li>
 * <li><b>addressType</b> : Type of this address (may be professional, personal, temporary till a date, ...)</li>
 * </ul>
 * </br> <b>Example of Address</b> : (3, 'FR', 'INRIA/IRISA, Campus Universitaire de Beaulieu', 'Rennes Cedex', '35042', 'French Florida', '+33 299 847 456 ', '+33 299 847 171',); <br>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2007, October 23
 * @class Address.java
 * @package net.ihe.gazelle.users
 */
@Entity
@Table(name = "usr_address", schema = "public")
@SequenceGenerator(name = "usr_address_sequence", sequenceName = "usr_address_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class Address extends AuditedObject implements java.io.Serializable {
    // ~ Statics variables and Class initializer
    // ///////////////////////////////////////////////////////////////////////

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -6412452506341401106L;
    private static Logger log = LoggerFactory.getLogger(Address.class);

    // ~ Attributes
    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Id of this Address
     */
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usr_address_sequence")
    private Integer id;

    /**
     * Foreign key corresponding to the country
     */
    @ManyToOne
    @JoinColumn(name = "country")
    @Fetch(value = FetchMode.SELECT)
    private Iso3166CountryCode iso3166CountryCode;

    /**
     * Address attribute (eg. 21st Ocean Drive)
     */
    @Column(name = "address")
    @Size(max = 512)
    private String address;

    /**
     * Address attribute (eg. 21st Ocean Drive)
     */
    @Column(name = "address_line_2", columnDefinition = "VARCHAR(512) NOT NULL DEFAULT ''")
    private String addressLine2;

    /**
     * City of this address
     */
    @Column(name = "city")
    private String city;

    /**
     * Zip Code of this address
     */
    @Column(name = "zip_code", length = 20)
    @Size(max = 20)
    private String zipCode;

    /**
     * State of this address
     */
    @Column(name = "state")
    private String state;

    /**
     * Phone number
     */
    @Column(name = "phone")
    @Size(max = 64)
    private String phone;

    /**
     * Fax Number
     */
    @Column(name = "fax")
    @Size(max = 64)
    private String fax;

    /**
     * Type of address (could be also a comment or description)
     */
    @Column(name = "address_type")
    private String addressType;

    // Variables used for Foreign Keys : usr_institution_address table
    /**
     * Institution with this address
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @JoinTable(name = "usr_institution_address", joinColumns = @JoinColumn(name = "address_id"), inverseJoinColumns = @JoinColumn(name = "institution_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "institution_id", "address_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<Institution> institutions;

    // ~ Constructors
    // //////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates a new Address object.
     */
    public Address() {
        // iso3166CountryCode = new Iso3166CountryCode()
    }

    /**
     * Creates a new Address object.
     *
     * @param id : Id of this address
     */
    public Address(final Integer id) {
        this.id = id;
    }

    /**
     * Creates a new Address object.
     *
     * @param iso3166CountryCode : Country of this address (foreign key)
     */
    public Address(final Iso3166CountryCode iso3166CountryCode) {
        this.iso3166CountryCode = iso3166CountryCode;
    }

    /**
     * Creates a new Address object.
     *
     * @param id                 : Id of this address
     * @param iso3166CountryCode : Country of this address (foreign key)
     * @param address            : Address
     * @param city               : City of this address
     * @param zipCode            : Zip Code of this address
     * @param state              : State of this address
     * @param phone              : Phone number of this address
     * @param fax                : Fax number of this address
     */
    public Address(final Integer id, final Iso3166CountryCode iso3166CountryCode, final String address,
                   final String city, final String zipCode, final String state, final String phone, final String fax) {
        this.id = id;
        this.iso3166CountryCode = iso3166CountryCode;
        this.address = address;
        this.city = city;
        this.zipCode = zipCode;
        this.state = state;
        this.phone = phone;
        this.fax = fax;
    }

    /**
     * Creates a new Address object.
     *
     * @param id                 : Id of this address
     * @param iso3166CountryCode : Country of this address (foreign key)
     * @param address            : Address
     * @param city               : City of this address
     * @param zipCode            : Zip Code of this address
     * @param state              : State of this address
     * @param phone              : Phone number of this address
     * @param fax                : Fax number of this address
     * @param addressType        : Type of address
     */
    public Address(final Integer id, final Iso3166CountryCode iso3166CountryCode, final String address,
                   final String city, final String zipCode, final String state, final String phone, final String fax,
                   final String addressType) {
        this.id = id;
        this.iso3166CountryCode = iso3166CountryCode;
        this.address = address;
        this.city = city;
        this.zipCode = zipCode;
        this.state = state;
        this.phone = phone;
        this.fax = fax;
        this.addressType = addressType;
    }

    /**
     * Copy constructor
     * <p/>
     * This constructor copies all attributes persisted to a new object
     */
    public Address(Address a) {

        if (a.getId() != null) {
            this.id = Integer.valueOf(a.getId());
        }
        if (a.getCopyIso3166CountryCode() != null) {
            this.iso3166CountryCode = new Iso3166CountryCode(a.getIso3166CountryCode());
        }
        if (a.getAddress() != null) {
            this.address = new String(a.getAddress());
        }
        if (a.getCity() != null) {
            this.city = new String(a.getCity());
        }
        if (a.getZipCode() != null) {
            this.zipCode = new String(a.getZipCode());
        }
        if (a.getState() != null) {
            this.state = new String(a.getState());
        }
        if (a.getPhone() != null) {
            this.phone = new String(a.getPhone());
        }
        if (a.getFax() != null) {
            this.fax = new String(a.getFax());
        }
        if (a.getAddressType() != null) {
            this.addressType = new String(a.getAddressType());
        }

    }

    // ~ Methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Delete in an Address object
     *
     * @param Address       : Address to delete (Could be for an institution, a contact)
     * @param EntityManager : Object managing persistence layer
     */
    public static void deleteAddress(Address selectedAddress) throws Exception {
        if ((selectedAddress == null) || (selectedAddress.getId() == null)) {
            return;
        }

        try {
            EntityManager em = EntityManagerService.provideEntityManager();

            if (!Address.isAddressReferencedByAnotherObject(selectedAddress)) {
                em.remove(selectedAddress);
                em.flush();
            }
        } catch (Exception e) {
            log.error("", e);
            throw new Exception(
                    "This object you want to delete possesses an address which is already used for another Object. Please contact an administrator if you need to delete it. Error info :"
                            + e.getMessage(), e);

        }
    }

    /**
     * Delete all Addresses objects mapped with a Contact
     *
     * @param Person        : This method will delete addresses of this institution (Could be for an institution, a contact)
     * @param EntityManager : Object managing persistence layer
     * @throws Exception
     */
    public static void deleteAddressForContact(Person oneContact) throws Exception {

        try {
            EntityManager em = EntityManagerService.provideEntityManager();

            if (oneContact.getAddress() != null) {
                Address addressToBeDeleted = em.find(Address.class, oneContact.getAddress().getId());
                Address.deleteAddress(addressToBeDeleted);

            }
            em.flush();
        } catch (Exception e) {
            log.error("", e);
            throw new Exception("An address cannot be deleted - for contact id = " + oneContact.getId(), e);

        }
    }

    /**
     * Delete all Addresses objects mapped with an Institution
     *
     * @param Institution : This method will delete addresses of this institution (Could be for an institution, a contact)
     */
    public static void deleteAllAddressesForCompany(Institution inst) throws Exception {
        Address addressToBeDeleted = null;

        try {

            EntityManager em = EntityManagerService.provideEntityManager();

            if (inst.getMailingAddress() != null) {
                addressToBeDeleted = em.find(Address.class, inst.getMailingAddress().getId());
                Address.deleteAddress(addressToBeDeleted);
            }

            em.flush();
        } catch (Exception e) {
            log.error("", e);
            throw new Exception("Addresses cannot be deleted - for institution id = " + inst.getId(), e);
        }

    }

    /**
     * List all Company/Institution(s) mapped/matching with an Address
     *
     * @param Address : This method will list Institution(s) with that Address OBJECT
     * @return List<Person> : List of all Company/Institution(s) matching with this Address
     */
    public static List<Institution> listCompaniesWithThatAddress(Address oneAddress) {
        InstitutionQuery query = new InstitutionQuery();
        query.mailingAddress().id().eq(oneAddress.getId());
        return query.getList();
    }

    /**
     * Define if the current Address is already used by an Institution or a Contact This method has been created to know before an object deletion if there is another Address used, and escape to a
     * ConstraintViolation exception which will all the time close the JDBC connection.
     *
     * @param Address : This method will list contact(s) with that Address OBJECT
     * @return Boolean : Flag indicating if the Address object is used by another object. If true is returned, then we canNOT delete the object (Institution or Person/Contact) because we will get a
     * ConstraintViolation exception.
     * @throws Exception
     */
    public static Boolean isAddressReferencedByAnotherObject(Address oneAddress) throws Exception {
        if ((oneAddress == null) || (oneAddress.getId() == null)) {
            return false;
        }
        InstitutionQuery institutionQuery = new InstitutionQuery();
        institutionQuery.mailingAddress().id().eq(oneAddress.getId());
        if (institutionQuery.getCount() > 0) {
            throw new Exception("Address used by institution " + institutionQuery.getUniqueResult().getKeyword());
        }
        PersonQuery personQuery = new PersonQuery();
        personQuery.address().id().eq(oneAddress.getId());
        if (personQuery.getCount() > 0) {
            Person person = personQuery.getUniqueResult();
            throw new Exception("Address used by person " + person.getLastName() + " " + person.getFirstName());
        }
        return false;
    }

    /**
     * Get the Id of this Address
     *
     * @return Integer Id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * Set the Id of this Address
     *
     * @param id : Id of this address
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Get COPY object of a Country for current Address
     *
     * @return Iso3166CountryCode : copy of the country object
     */
    public Iso3166CountryCode getCopyIso3166CountryCode() {
        return new Iso3166CountryCode(this.iso3166CountryCode);
    }

    /**
     * Get the Country of this Address
     *
     * @return Iso3166CountryCode : code of the country
     */
    public Iso3166CountryCode getIso3166CountryCode() {
        return this.iso3166CountryCode;
    }

    /**
     * Set the Country of this Address
     *
     * @param Iso3166CountryCode : code of the country
     */
    public void setIso3166CountryCode(final Iso3166CountryCode iso3166CountryCode) {
        this.iso3166CountryCode = iso3166CountryCode;
    }

    /**
     * Get the Address of this Address (eg. 21st Ocean Drive)
     *
     * @return String : Address
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * Get the Address of this Address (eg. 21st Ocean Drive)
     *
     * @param address : String address
     */
    public void setAddress(final String address) {
        this.address = address;
    }

    /**
     * Get the City of this Address
     *
     * @return String : city
     */
    public String getCity() {
        return this.city;
    }

    /**
     * Set the City of this Address
     *
     * @param city
     */
    public void setCity(final String city) {
        this.city = city;
    }

    /**
     * Get the Zip Code of this Address
     *
     * @return String : Zip Code
     */
    public String getZipCode() {
        return this.zipCode;
    }

    /**
     * Set the Zip Code of this Address
     *
     * @param zipCode : String : Zip Code
     */
    public void setZipCode(final String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * Get the State of this Address
     *
     * @return String : State
     */
    public String getState() {
        return this.state;
    }

    /**
     * Set the State of this Address
     *
     * @param state
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * Get the international Phone number of this Address
     *
     * @return String : international phone number
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     * Set the international Phone number of this Address
     *
     * @param phone : international phone number
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }

    /**
     * Get the international Fax number of this Address
     *
     * @return String : international fax number
     */
    public String getFax() {
        return this.fax;
    }

	/* Static methods for this object */

    /**
     * Set the international Fax number of this Address
     *
     * @param fax : international fax number
     */
    public void setFax(final String fax) {
        this.fax = fax;
    }

    /**
     * Get type of this Address
     *
     * @return String : Type of address
     */
    public String getAddressType() {
        return this.addressType;
    }

    /**
     * Set type of this Address
     *
     * @param addressType : Type of address
     */
    public void setAddressType(final String addressType) {
        this.addressType = addressType;
    }

    /**
     * Get institutions with this Address
     *
     * @return List<Institution>
     */
    public List<Institution> getInstitutions() {
        return institutions;
    }

    /**
     * Set institutions for this Address
     *
     * @param List <Institution>
     */
    public void setInstitutions(List<Institution> institutions) {
        this.institutions = institutions;
    }

    public String getAddressLine2() {
        if(addressLine2 == null){
            setAddressLine2("");
        }
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    /**
     * Added for [GZL-4828] need a convenient way to write the address for display
     * @return
     */
    @Override
    public String toString(){
        StringBuilder addressAsText = new StringBuilder();
        if (this.getAddress() != null && !this.getAddress().isEmpty()){
            addressAsText.append(this.getAddress());
            addressAsText.append('\n');
        }
        if (this.getAddressLine2() != null && !this.getAddressLine2().isEmpty()){
            addressAsText.append(this.getAddressLine2());
            addressAsText.append('\n');
        }
        addressAsText.append(this.getZipCode());
        addressAsText.append(' ');
        addressAsText.append(this.getCity());
        addressAsText.append('\n');
        if (this.getState() != null && !this.getState().isEmpty())
        {
            addressAsText.append(this.getState());
            addressAsText.append('\n');
        }
        if (this.getIso3166CountryCode() != null) {
            addressAsText.append(this.getIso3166CountryCode().getName());
        }
        return addressAsText.toString();
    }

}
