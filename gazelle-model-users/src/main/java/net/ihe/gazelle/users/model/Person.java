/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.users.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "usr_person", schema = "public")
@SequenceGenerator(name = "usr_person_sequence", sequenceName = "usr_person_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class Person extends AuditedObject implements java.io.Serializable, Comparable<Person> {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450761131541283760L;

    private static Logger log = LoggerFactory.getLogger(Person.class);

    // Attributes (existing in database as a column)
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usr_person_sequence")
    private Integer id;

    @Column(name = "lastname", nullable = false)
    @NotNull(message = "Please provide Last Name")
    @Size(max = 255)
    private String lastName;

    @Column(name = "firstname", nullable = false)
    @NotNull(message = "Please provide First Name")
    @Size(max = 255)
    private String firstName;

    @Column(name = "email")
    @Pattern(regexp = "^.+@[^\\.].*\\.[a-z]{2,}$", message = "{gazelle.validator.email}")
    @Size(max = 255)
    private String email;

    /**
     * Institution corresponding to this person
     */
    @ManyToOne
    @JoinColumn(name = "institution_id")
    @NotNull
    @Fetch(value = FetchMode.SELECT)
    private Institution institution;

    /**
     * Address corresponding to this person
     */
    @ManyToOne
    @JoinColumn(name = "address_id")
    @Fetch(value = FetchMode.SELECT)
    private Address address;

    @Column(name = "cell_phone")
    @Size(max = 64)
    private String cellPhone;

    @Column(name = "personal_phone")
    @Size(max = 64)
    private String personalPhone;

    @Column(name = "personal_fax")
    @Size(max = 64)
    private String personalFax;

    @Column(name = "mailing_list")
    private Boolean mailingList;

    @Column(name = "photo")
    @Size(max = 512)
    private String photo;

    @Column(name = "title")
    @Size(max = 255)
    private String title;

    // Variables used for Foreign Keys : person_function table
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @JoinTable(name = "usr_persons_functions", joinColumns = @JoinColumn(name = "person_id"), inverseJoinColumns = @JoinColumn(name = "person_function_id"))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<PersonFunction> personFunction;

    // Constructors
    public Person() {
    }

    public Person(final String lastName, final String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    // *********************************************************
    // Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static void deleteContactWithFind(Person selectedContact) {
        EntityManager em = EntityManagerService.provideEntityManager();
        Person contact = em.find(Person.class, selectedContact.getId());
        em.remove(contact);
        em.flush();
    }

    public static void deleteContact(Person selectedContact) throws Exception {

        EntityManager em = EntityManagerService.provideEntityManager();
        try {
            em.remove(selectedContact);
            em.flush();

        } catch (Exception e) {
            log.error("", e);
            throw new Exception("A contact cannot be deleted -  id = " + selectedContact.getId(), e);
        }

        Address address = selectedContact.getAddress();

        try {
            Address.deleteAddress(address);
        } catch (Exception e) {
            log.error("", e);
            throw new Exception("An address cannot be deleted for contact -  id = " + selectedContact.getId(), e);
        }
    }

    public static void deleteAllContacts(Institution inst) throws Exception {

        List<Person> listContactsForCompany = Person.listAllContactsForCompany(inst);
        try {

            for (int i = 0; i < listContactsForCompany.size(); i++) {
                Person.deleteContact(listContactsForCompany.get(i));

            }
        } catch (Exception e) {
            log.error("", e);
            throw new Exception("A contact cannot be deleted for institution -  id = " + inst.getId(), e);
        }
    }

    public static List<Person> listAllContactsForCompany(Institution i) {

        if (i != null) {
            PersonQuery q = new PersonQuery();
            q.institution().id().eq(i.getId());
            List<Person> res = q.getListDistinct();
            Collections.sort(res);
            return res;
        } else {
            log.error("listAllContactsForCompany - institution is null ");
            return null;
        }

    }

    public static boolean companyAlreadyContainsBillingContact(Institution i) {
        List<Person> listAllContactsForCompanyReturned = listAllContactsForCompany(i);
        EntityManager em = EntityManagerService.provideEntityManager();
        for (Person p : listAllContactsForCompanyReturned) {
            if (p.getPersonFunction().contains(PersonFunction.getBillingFunction(em))) {
                return true;
            }
        }

        return false;
    }

    public static List<Person> listAllContactsForCompanyWithName(String institutionName) {
        PersonQuery q = new PersonQuery();
        q.institution().name().eq(institutionName);
        List<Person> res = q.getListDistinct();
        Collections.sort(res);
        return res;
    }

    public static List<Person> listAllContacts() {
        PersonQuery q = new PersonQuery();
        q.lastName().order(true);
        return q.getList();
    }

    public static Person getUniqueContactByFunctionNameByInstitution(EntityManager em, Institution inInstitution,
                                                                     String inFunctionName) {
        if (inFunctionName == null) {
            return null;
        }
        PersonQuery pq = new PersonQuery();
        pq.personFunction().keyword().eq(inFunctionName);
        if (inInstitution != null) {
            pq.institution().eq(inInstitution);
        }
        pq.lastName().order(true);
        pq.firstName().order(true);
        Person res = pq.getUniqueResult();
        return res;
    }

    private static List<Person> listAllContactsByFunction(EntityManager em, Institution inInstitution,
                                                          PersonFunction inFunction) {
        if (inFunction == null) {
            return null;
        }
        PersonQuery pq = new PersonQuery();
        pq.personFunction().id().eq(inFunction.getId());
        if (inInstitution != null) {
            pq.institution().eq(inInstitution);
        }
        pq.lastName().order(true);
        pq.firstName().order(true);
        List<Person> res = pq.getListDistinct();
        return res;
    }

    public static List<Person> listAllTechnicalContacts(EntityManager em, Institution inInstitution) {
        return listAllContactsByFunction(em, inInstitution, PersonFunction.getTechnicalFunction(em));
    }

    public static List<Person> listAllMarketingContacts(EntityManager em, Institution inInstitution) {
        return listAllContactsByFunction(em, inInstitution, PersonFunction.getMarketingFunction(em));
    }

    public static List<Person> listAllBillingContacts(EntityManager em, Institution inInstitution) {
        return listAllContactsByFunction(em, inInstitution, PersonFunction.getBillingFunction(em));
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get the Institution of this User object
     *
     * @return institution : Institution of this User
     */
    public Institution getInstitution() {
        return institution;
    }

    /**
     * Set the Institution of this User object
     *
     * @param institution : Institution of this User
     */
    public void setInstitution(final Institution institution) {
        this.institution = institution;
    }

    public String getCellPhone() {
        return this.cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    // *********************************************************
    // Foreign Key for table : persons_functions
    // *********************************************************

    public String getPersonalPhone() {
        return this.personalPhone;
    }

    public void setPersonalPhone(String personalPhone) {
        this.personalPhone = personalPhone;
    }

    public String getPersonalFax() {
        return this.personalFax;
    }

    public void setPersonalFax(String personalFax) {
        this.personalFax = personalFax;
    }

    public Boolean getMailingList() {
        return this.mailingList;
    }

    public void setMailingList(Boolean mailingList) {
        this.mailingList = mailingList;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(final Address addressesPerson) {
        this.address = addressesPerson;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<PersonFunction> getPersonFunction() {
        return personFunction;
    }

    public void setPersonFunction(List<PersonFunction> personFunction) {
        EntityManager em = EntityManagerService.provideEntityManager();
        if (checkIfFunctionIsSetToPrimaryTechnicalContact(personFunction, em)) {
            changeInstitutionPrimaryTechnicalContact(em);
        }
        if (checkIfFunctionIsSetToBillingContact(personFunction, em)) {
            changeInstitutionBillingContact(em);
        }
        this.personFunction = personFunction;
    }

    private void changeInstitutionPrimaryTechnicalContact(EntityManager em) {
        Institution ins = em.find(Institution.class, this.getInstitution().getId());
        Set<Person> contacts = ins.getContacts();
        for (Person contact : contacts) {
            if (checkIfTheContactIsAPrimaryTechnicalRole(contact)) {
                removePrimaryTechnicalRoleFromContact(em, contact);
            }
        }
    }

    private void changeInstitutionBillingContact(EntityManager em) {
        Institution ins = em.find(Institution.class, this.getInstitution().getId());
        Set<Person> contacts = ins.getContacts();
        if (contacts != null) {
            for (Person contact : contacts) {
                if (checkIfTheContactIsABillingRole(contact)) {
                    removeBillingRoleFromContact(em, contact);
                }
            }
        }
    }

    private void removePrimaryTechnicalRoleFromContact(EntityManager em, Person contact) {
        List<PersonFunction> personFunction = contact.getPersonFunction();
        if (personFunction.contains(PersonFunction.getPrimaryTechnicalFunction(null))) {
            int indexOfPrimaryTechnical = personFunction.indexOf(PersonFunction.getPrimaryTechnicalFunction(null));
            personFunction.remove(indexOfPrimaryTechnical);
        }
        contact.personFunction = personFunction;

        em.merge(contact);
        try {
            em.flush();
        } catch (PersistenceException e) {
            log.error(e.toString());
        }
    }

    private void removeBillingRoleFromContact(EntityManager em, Person contact) {
        List<PersonFunction> personFunction = contact.getPersonFunction();
        if (personFunction.contains(PersonFunction.getBillingFunction(null))) {
            int indexOfBilling = personFunction.indexOf(PersonFunction.getBillingFunction(null));
            personFunction.remove(indexOfBilling);
        }
        contact.personFunction = personFunction;

        em.merge(contact);
        try {
            em.flush();
        } catch (PersistenceException e) {
            log.error(e.toString());
        }
    }

    private boolean checkIfTheContactIsAPrimaryTechnicalRole(Person contact) {
        return (contact.id != this.id) && contact.isAPrimaryTechnicalContact();
    }

    private boolean checkIfTheContactIsABillingRole(Person contact) {
        return (contact.id != this.id) && contact.isABillingContact();
    }

    private boolean checkIfFunctionIsSetToPrimaryTechnicalContact(List<PersonFunction> personFunction, EntityManager em) {
        boolean result = false;

        if (personFunction.contains(PersonFunction.getPrimaryTechnicalFunction(em))) {
            if (!personFunction.contains(PersonFunction.getTechnicalFunction(em))) {
                personFunction.add(PersonFunction.getTechnicalFunction(em));
            }
            result = true;
        }
        return result;
    }

    private boolean checkIfFunctionIsSetToBillingContact(List<PersonFunction> personFunction, EntityManager em) {
        boolean result = false;

        if (personFunction.contains(PersonFunction.getBillingFunction(em))) {
            result = true;
        }
        return result;
    }

    public Boolean isAMarketingContact() {
        EntityManager em = EntityManagerService.provideEntityManager();
        if (this.getPersonFunction().contains(PersonFunction.getMarketingFunction(em))) {
            return true;
        }
        return false;
    }

    public Boolean isABillingContact() {
        EntityManager em = EntityManagerService.provideEntityManager();
        if (this.getPersonFunction().contains(PersonFunction.getBillingFunction(em))) {
            return true;
        }
        return false;
    }

    public Boolean isATechnicalContact() {
        EntityManager em = EntityManagerService.provideEntityManager();
        if (this.getPersonFunction().contains(PersonFunction.getTechnicalFunction(em))) {
            return true;
        }
        return false;
    }

    public Boolean isAPrimaryTechnicalContact() {
        EntityManager em = EntityManagerService.provideEntityManager();
        if (this.getPersonFunction().contains(PersonFunction.getPrimaryTechnicalFunction(em))) {
            return true;
        }
        return false;
    }

    public String technicalContact() {
        EntityManager em = EntityManagerService.provideEntityManager();
        String result = "";
        if (this.getPersonFunction().contains(PersonFunction.getTechnicalFunction(em))) {
            result = "True";
        }
        if (this.getPersonFunction().contains(PersonFunction.getPrimaryTechnicalFunction(em))) {
            result = "Primary contact";
        }
        return result;
    }

    @Override
    public int compareTo(Person person) {
        return this.getLastName().compareTo(person.getLastName());
    }
}
