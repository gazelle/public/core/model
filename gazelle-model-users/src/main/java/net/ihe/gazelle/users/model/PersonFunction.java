/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.users.model;

import net.ihe.gazelle.common.model.AuditedObject;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.sync.SetRegistration;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "usr_person_function", schema = "public")
@SequenceGenerator(name = "usr_person_function_sequence", sequenceName = "usr_person_function_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Exchanged(set = SetRegistration.class)
public class PersonFunction extends AuditedObject implements Serializable {
    public final static String primaryTechnicalFunctionName = "PRIMARY_TECHNICAL";
    public final static String TechnicalFunctionName = "TECHNICAL";
    public final static String MarketingFunctionName = "MARKETING";
    public final static String BillingFunctionName = "FINANCIAL";
    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450911183541283760L;
    private static Logger log = LoggerFactory.getLogger(PersonFunction.class);
    private static PersonFunction primaryTechnicalFunction;
    private static PersonFunction TechnicalFunction;
    private static PersonFunction MarketingFunction;
    private static PersonFunction BillingFunction;
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usr_person_function_sequence")
    private Integer id;

    @Column(name = "name", nullable = false)
    @NotNull(message = "Please provide a function for this person")
    private String name;

    @Column(name = "keyword", nullable = false, unique = true)
    @NotNull
    private String keyword;

    @Column(name = "description")
    @Size(max = 1024)
    private String description;

    @Column(name = "is_billing")
    private Boolean isBilling;

    @ManyToMany(fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SELECT)
    @JoinTable(name = "usr_persons_functions", joinColumns = @JoinColumn(name = "person_function_id"), inverseJoinColumns = @JoinColumn(name = "person_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "person_function_id", "person_id"}))
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<Person> persons;

    // Constructors
    public PersonFunction(Integer id, String keyword, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public PersonFunction(Integer id, String keyword, String name, String description, Boolean isBilling) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.isBilling = isBilling;
    }

    public PersonFunction(String name) {
        this.name = name;
    }

    public PersonFunction() {
    }

    private static PersonFunction getFunctionByKeyword(EntityManager em, String inKeyword) {
        if ((inKeyword == null) || (inKeyword.trim().length() == 0)) {
            return null;
        }
        if (em == null) {
            em = EntityManagerService.provideEntityManager();
        }
        PersonFunctionQuery pfq = new PersonFunctionQuery();
        pfq.keyword().eq(inKeyword);

        PersonFunction uniqueResult = pfq.getUniqueResult();

        if (uniqueResult == null) {
            uniqueResult = createFunction(em, inKeyword);

        }
        return uniqueResult;
    }

    private static PersonFunction createFunction(EntityManager em, String inKeyword) {

        PersonFunction personFunction = null;
        if (inKeyword.equals(PersonFunction.primaryTechnicalFunctionName)) {
            personFunction = new PersonFunction();
            personFunction.setDescription("This person is the primary technical contact within the institution");
            personFunction.setIsBilling(false);
            personFunction.setKeyword(primaryTechnicalFunctionName);
            personFunction.setName("gazelle.users.function.primaryTechnical");
        }
        if (personFunction != null) {
            personFunction = em.merge(personFunction);
            em.flush();
        }
        return personFunction;
    }

    public static PersonFunction getTechnicalFunction(EntityManager em) {
        if (TechnicalFunction == null) {
            TechnicalFunction = PersonFunction.getFunctionByKeyword(em, PersonFunction.TechnicalFunctionName);
        }

        return TechnicalFunction;
    }

    public static PersonFunction getPrimaryTechnicalFunction(EntityManager em) {
        if (primaryTechnicalFunction == null) {
            primaryTechnicalFunction = PersonFunction.getFunctionByKeyword(em,
                    PersonFunction.primaryTechnicalFunctionName);
        }

        return primaryTechnicalFunction;
    }

    public static PersonFunction getMarketingFunction(EntityManager em) {
        if (MarketingFunction == null) {
            MarketingFunction = PersonFunction.getFunctionByKeyword(em, PersonFunction.MarketingFunctionName);
        }
        return MarketingFunction;
    }

    public static PersonFunction getBillingFunction(EntityManager em) {
        if (BillingFunction == null) {
            BillingFunction = PersonFunction.getFunctionByKeyword(em, PersonFunction.BillingFunctionName);
        }
        return BillingFunction;
    }

    public static List<PersonFunction> listPersonFunctions() {
        return new PersonFunctionQuery().getList();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Boolean getIsBilling() {
        return isBilling;
    }

    public void setIsBilling(Boolean isBilling) {
        this.isBilling = isBilling;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public String toString() {
        return "PersonFunction [id=" + id + ", name=" + name + ", keyword=" + keyword + ", description=" + description
                + ", isBilling=" + isBilling + "]";
    }

}
