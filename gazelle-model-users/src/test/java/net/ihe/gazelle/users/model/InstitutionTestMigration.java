package net.ihe.gazelle.users.model;

import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.users.model.factories.InstitutionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by jlabbe on 07/07/15.
 */
public class InstitutionTestMigration {

    @Before
    public void setUp() throws Exception {
        InstitutionFactory.createInstitutionWithMandatoryFields();
        InstitutionFactory.createInstitutionWithMandatoryFields();
    }

    @Test
    public void getInstitutions() {
        List<Institution> institutions = Institution.listAllInstitutions();
        assertNotNull(institutions);
        assertEquals(2,institutions.size());
    }

}
