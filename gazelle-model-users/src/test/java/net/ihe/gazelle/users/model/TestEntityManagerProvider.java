package net.ihe.gazelle.users.model;

import net.ihe.gazelle.hql.providers.detached.AbstractEntityManagerProvider;
import org.kohsuke.MetaInfServices;

@MetaInfServices(net.ihe.gazelle.hql.providers.EntityManagerProvider.class)
public class TestEntityManagerProvider extends AbstractEntityManagerProvider {

    @Override
    public Integer getWeight() {
        return 0;
    }

    @Override
    public String getHibernateConfigPath() {
        return "test.hibernate.xml";
    }

}
