package net.ihe.gazelle.users.model.factories;


import net.ihe.gazelle.ssov7.gum.client.application.User;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class UserFactory {

    private static int number = 1;

    public static User createUserWithMandatoryFields() {
        return provideUserWithMandatoryFields();
    }

    public static User createUserWithAllFields() {
        User user = new User();
        user.setId("testing" + number);
        user.setFirstName("Tester");
        user.setLastName("senior");
        user.setEmail("jle" + number + "@kereval.com");
        user.setActivated(true);
        user.setLastLoginTimestamp(new Timestamp(1601906470000L));
        user.setOrganizationId(InstitutionFactory.createInstitutionWithMandatoryFields().getName());
        number += 1;
        return user;
    }

    public static void cleanUsers() {
        InstitutionFactory.cleanInstitution();
    }

    public static User provideUserWithMandatoryFields() {
        User user = new User();
        user.setId("testing" + number);
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setActivated(true);
        user.setLastLoginTimestamp(new Timestamp(1601906470000L));
        user.setOrganizationId(InstitutionFactory.createInstitutionWithMandatoryFields().getName());
        user.setEmail("john.doe" + number + "@kereval.com");
        number += 1;
        return user;
    }

    public static List<User> createUserUserList() {
        List<User> users = new ArrayList<>();
        users.add(createUserWithAllFields());
        users.add(provideUserWithMandatoryFields());
        return users;
    }
}
