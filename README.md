# Gazelle Model

>:warning: **Disclaimer:**
This is an internal component of the Gazelle Test Management project, it should not be used elsewhere.

## Build and tests

The project must be build using JDK-8 and run on a JDK-7 environment.

```shell
mvn clean install
```